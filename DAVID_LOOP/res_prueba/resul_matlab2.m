% Ensayos para OMEGAFINA
% MRA 02-2023

close all; clear all; clc

%% Carga de los datos recogidos en los archivos de resultados



% velocidad wm según interrupción EQEP
omega_isr = load('wm_log.txt');
% contador de inicios de la interrupción EQEP 
eqep_1 = load('eqep_1.txt');
eqep_2 = load('eqep_2.txt');
eqep = eqep_1 * 2^16  + eqep_2;
% valor QPOSLAT fuera de la interrupción EQEP 
corr1 = load('delta_pos1.txt');
corr2 = load('delta_pos2.txt');
corr = corr1 * 2^16  + corr2;
corr = corr - ((corr>(2^31))*2^32);

omega_isr = omega_isr/65.534-500;

% represent graf

figure
plot( omega_isr, 'r');
xlabel('k')
ylabel('\omega (rad/s)')
grid
figure
stairs( eqep, 'g');

grid
figure
stairs( corr, 'g');
grid
