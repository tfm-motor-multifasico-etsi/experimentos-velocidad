/******************************************************************************
	File Name	: log_ref.c                   
	Project		: MSK2812 Motion Starter Kit
===============================================================================
	Target Sys  : DSP boards based on TMS320LF28335
	Description : Logger and reference generator functions 
	Originator/s: Technosoft Ltd.
	Status      : OK
===============================================================================
	Copyright � 2008 Technosoft
==============================================================================*/
/******************************************************************************
	Include Files
******************************************************************************/
#include "log_ref.h"	

/*-------- LOGGER variables -------*/
#ifdef __cplusplus
#pragma DATA_SECTION("LOGGER")
#else
#pragma DATA_SECTION(log_table,"LOGGER");
#endif

unsigned long log_indx;
unsigned long log_count;
unsigned long log_size;
unsigned long log_time;
int *log_p[17];
int log_table[0x3FFF];

/*------ REFERENCE variables ------*/
long ref_long;
unsigned int ref_indx;
unsigned int ref_time_val[100];
long ref_HL[100];
int pos_ref_high;
unsigned int pos_ref_low;
unsigned int ref_time;
unsigned int ref_cycles;

/****************************************************************************************************
	Functions
*****************************************************************************************************/
/****************************************************************************************************
	Routine Name: init_logger
	-------------------------
	Purpose: Initialization routine, called from the main function
	Calling Convention: extern void init_logger();
	Description: Initialize the data logger parameters.
*****************************************************************************************************/
void init_logger()
{
	log_count = 0;
	log_time = 0;
}

/****************************************************************************************************
	Routine Name: logger
	--------------------
	Purpose: Routine, called from the speed loop control RTI routine
	Calling Convention: extern void logger();
	Description: Performs data logging.
*****************************************************************************************************/
void logger()
{
	if(log_time >= log_indx && log_indx != 0xffffffff)
	{
		int **crt_p = log_p;
		while(*crt_p)
		{
			log_table[log_count++] = **crt_p++;
		}
		if(log_count >= log_size) log_indx = 0xffffffff;

	}
	log_time++;
}

/****************************************************************************************************
	Routine Name: init_reference
	----------------------------
	Purpose: Initialization routine, called from the main function
	Calling Convention: extern void init_reference();
	Description: Initializes the reference generator parameters.
*****************************************************************************************************/
void init_reference()
{
	ref_long = ref_HL[0];
	ref_time = 1;
	ref_indx = 1;
}

/****************************************************************************************************
	Routine Name: reference
	-----------------------
	Purpose: Routine, called from the speed loop control RTI routine
	Calling Convention: extern int reference();
	Description: Implements the reference generator.
	Returns: the computed value of the reference at calling instance
*****************************************************************************************************/
int reference()
{
	int retval;
	long crtInc;
	unsigned int crtTime;
				
	if(ref_long > 0) retval = (int)((ref_long + 32768) >> 16);
	else retval = (int)((ref_long - 32768) >> 16) + 1;

	if(ref_cycles != 1)
	{
		crtTime = ref_time_val[ref_indx];
		crtInc = ref_HL[ref_indx];
		ref_long += crtInc;
		if(crtTime <= ref_time++)
		{ 
			crtTime = ref_time_val[++ref_indx];
			if(!crtTime)
			{
				if(ref_cycles > 1)
				{
					ref_cycles--;
				}
				if(ref_cycles != 1)
				{
					ref_indx = 1;
					ref_time = 1;
					ref_long = ref_HL[0];
				}
			}
		}
	}
	return retval;
}

