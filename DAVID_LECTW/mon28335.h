/***************************************************************************
 File Name	:	mon28335.h
 Project	:  	P046 MSK28335
 Originator	:  	D. Erhan
 Description: 	C header file asssociated to ExtCodeFlash demo
 Copyright    � 2007 Technosoft
-----------------------------------------------------------------------------
 Constant defines
=============================================================================*/
#ifndef __MON28335_H_
#define __MON28335_H_

/* define pointers to the called functions */
#define callmon28335             (int(*)(void)) 0x00330000 // monitor's command interpreter
#define callreadchar             (int(*)(void)) 0x00330002 // "read_char" calling routine
#define callsendchar             (int(*)(int))  0x00330004 // "send_char" calling routine
#define CallInitializeData       (int(*)(void)) 0x00330006 // "InitializeData" calling routine
#define CallInitializeCOM        (int(*)(int))  0x00330008 // "InitializeCOM" calling routine
#define CallInitializeInterrupts (int(*)(void)) 0x0033000A // "InitializeInterrupts" calling routine
#define CallInitializeSystem     (int(*)(void)) 0x0033000C // "InitializeSystem" calling routine

/* baud rates F28335	150MHz */
#define B9600	0x7A0	
#define B19200	0x3D0	
#define B38400	0x1E7	
#define B56000	0x14E	
#define B115200	0xA2	

/* useful defines */
#define	 EALLOW	asm(" EALLOW")
#define	 EDIS	asm(" EDIS")

/* light on led function */
void SetReady(void);

#endif	/*__MON28335_H_*/
