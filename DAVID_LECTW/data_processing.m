 close all; clear; clc

%% Carga de los datos recogidos en los archivos de resultados
frec_cpu = 150 * 1e6;
omegas = [20, 50, 75];
omegas_lim = [[18,24]; [47, 57]; [75, 82]];
interrupts = [2, 3, 6, 9, 18];
lengths = [length(omegas), length(interrupts)];
omega_mean = zeros(lengths);
omega_std = omega_mean;

max_min = zeros([lengths, 2]);

fig = figure;
fig.Units = 'normalized';
fig.Position = [0, 0, 1, 1];
tiles = tiledlayout(fig,...
    lengths(2),1, ...
    'Padding', 'none',...
    'TileSpacing', 'compact' ...
);

fig2 = figure;
fig2.Units = 'normalized';
fig2.Position = [0, 0, 1, 1];
tiles2 = tiledlayout(fig2,...
    lengths(2),lengths(1), ...g
    'Padding', 'none',...
    'TileSpacing', 'compact' ...
);

for jj = 1:length(interrupts)
    ax = nexttile(tiles);
    ax.Title.String = sprintf("Cálculo cada %d pulsos", interrupts(jj)*10000);
    ax.YLabel.String = '\omega (rad/s)';
    ax.YLim = [0, 90];
    grid;
    hold(ax, "on");
    for ii = 1:length(omegas)
        ax2 = nexttile(tiles2);
        if ii == 1
            ax2.YLabel.String = sprintf("Frecuencia, %d pulsos", interrupts(jj)*10000);
        end
        if jj == 5
            ax2.XLabel.String = sprintf("\\omega (rad/s), \\omega_{ref} = %.2f \\omega_{sync}", omegas(ii)/100);
        end
        ax2.YLim = [0, 3000];
        ax2.XLim = omegas_lim(ii, :);
        hold(ax2, "on");
        dir = sprintf("RES_%d_%d", omegas(ii), interrupts(jj));

        omega_isr = load(sprintf('%s/ialpha_k.txt', dir));
        conta_isr = load(sprintf('%s/ialpha_ref.txt', dir));
        
        omega_isr = omega_isr/65.534-500;
        omega_mean(ii, jj) = mean(omega_isr);
        omega_std(ii, jj) = std(omega_isr);
        max_min(ii, jj, :) = [max(omega_isr), min(omega_isr)];
        histogram(ax2, omega_isr, 'BinWidth', 0.1);
        
        plt = plot(ax, omega_isr, 'r');
        plt.ColorMode="auto";
        plt.LineWidth= 2;
        
    end
        
end


%% Expected values
expected_vels = omegas'/100*1000*2*pi/60;
hv_factor =  ...
    ones(1, lengths(2)) * pi * frec_cpu ...
    ./ ((2* 2500) * (interrupts * 10000) );
interr_time = ((interrupts * 10000)/frec_cpu);
expected_ticks = expected_vels * (hv_factor.^(-1));
max_min = max_min .* (hv_factor.^(-1));

ax.XLabel.String = 'k';
fprintf("omega_mean\n");
disp(omega_mean);
fprintf("omega_std\n");
disp(omega_std);
fprintf("omega_std ./ omega_mean\n");
disp(omega_std./omega_mean);
fprintf("interrupt time\n");
disp(interr_time * 1000);
fprintf("hv_factor\n");
disp(hv_factor);
fprintf("expected_vels\n");
disp(expected_vels);
fprintf("expected_ticks\n");
disp(expected_ticks);
fprintf("max_min ticks\n");
disp(max_min);

fprintf("max_min ticks rounded\n");
disp(round(max_min));
saveas(fig, 'resp','epsc');
saveas(fig2, 'hysto','epsc');



