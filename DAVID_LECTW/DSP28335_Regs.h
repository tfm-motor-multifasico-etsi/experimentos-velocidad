/****************************************************************************
 File Name    : LF28335_Regs.h revision 1.0
 Project      : P046 - MSK28335
 Originator/s : D.Erhan
 Target Sys   : MSK28335
 Description  : C Header file with bit definitions of peripheral 
                registers and other useful defines.
 Status       : OK
 Last Update  : 12 Dec 2007
 Copyright    � 2007 Technosoft
****************************************************************************/
#ifndef LF28335_REGS_H
#define LF28335_REGS_H


#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------
// Common CPU Definitions:
//

extern cregister volatile unsigned int IFR;
extern cregister volatile unsigned int IER;

#define  EINT   asm(" clrc INTM")
#define  DINT   asm(" setc INTM")
#define  ERTM   asm(" clrc DBGM")
#define  DRTM   asm(" setc DBGM")
#define  EALLOW asm(" EALLOW")
#define  EDIS   asm(" EDIS")
#define  ESTOP0 asm(" ESTOP0")

#define M_INT1  0x0001
#define M_INT2  0x0002
#define M_INT3  0x0004
#define M_INT4  0x0008
#define M_INT5  0x0010
#define M_INT6  0x0020
#define M_INT7  0x0040
#define M_INT8  0x0080
#define M_INT9  0x0100
#define M_INT10 0x0200
#define M_INT11 0x0400
#define M_INT12 0x0800
#define M_INT13 0x1000
#define M_INT14 0x2000
#define M_DLOG  0x4000
#define M_RTOS  0x8000

#define BIT0    0x0001
#define BIT1    0x0002
#define BIT2    0x0004
#define BIT3    0x0008
#define BIT4    0x0010
#define BIT5    0x0020
#define BIT6    0x0040
#define BIT7    0x0080
#define BIT8    0x0100
#define BIT9    0x0200
#define BIT10   0x0400
#define BIT11   0x0800
#define BIT12   0x1000
#define BIT13   0x2000
#define BIT14   0x4000
#define BIT15   0x8000

#define PIEACK_GROUP1   0x0001
#define PIEACK_GROUP2   0x0002
#define PIEACK_GROUP3   0x0004
#define PIEACK_GROUP4   0x0008
#define PIEACK_GROUP5   0x0010
#define PIEACK_GROUP6   0x0020
#define PIEACK_GROUP7   0x0040
#define PIEACK_GROUP8   0x0080
#define PIEACK_GROUP9   0x0100
#define PIEACK_GROUP10  0x0200
#define PIEACK_GROUP11  0x0400
#define PIEACK_GROUP12  0x0800

/* Flash Registers */
#define FLASH_SLEEP   0x0000;
#define FLASH_STANDBY 0x0001;
#define FLASH_ACTIVE  0x0003;

//---------------------------------------------------------------------------
// For Portability, User Is Recommended To Use Following Data Type Size
// Definitions For 16-bit and 32-Bit Signed/Unsigned Integers:
//

#ifndef DSP28_DATA_TYPES
#define DSP28_DATA_TYPES
typedef int             int16;
typedef long            int32;
typedef unsigned int    Uint16;
typedef unsigned long   Uint32;
typedef float           float32;
typedef long double     float64;
#endif

//---------------------------------------------------------------------------
// External Registers References
//---------------------------------------------------------------------------
extern volatile struct ADC_REGS AdcRegs;
extern volatile struct ADC_RESULT_MIRROR_REGS AdcMirror;

extern volatile struct CPUTIMER_REGS CpuTimer0Regs;
extern struct CPUTIMER_VARS CpuTimer0;

// CpuTimer 1 and CpuTimer2 are reserved for DSP BIOS & other RTOS. Comment out CpuTimer1 and CpuTimer2 if using DSP BIOS or other RTOS
extern volatile struct CPUTIMER_REGS CpuTimer1Regs;
extern volatile struct CPUTIMER_REGS CpuTimer2Regs;

extern struct CPUTIMER_VARS CpuTimer1;
extern struct CPUTIMER_VARS CpuTimer2;

extern volatile struct DMA_REGS DmaRegs;

extern volatile struct DEV_EMU_REGS DevEmuRegs;

extern volatile struct ECAN_REGS ECanaRegs;
extern volatile struct ECAN_MBOXES ECanaMboxes;
extern volatile struct LAM_REGS ECanaLAMRegs;
extern volatile struct MOTO_REGS ECanaMOTORegs;
extern volatile struct MOTS_REGS ECanaMOTSRegs;

extern volatile struct ECAN_REGS ECanbRegs;
extern volatile struct ECAN_MBOXES ECanbMboxes;
extern volatile struct LAM_REGS ECanbLAMRegs;
extern volatile struct MOTO_REGS ECanbMOTORegs;
extern volatile struct MOTS_REGS ECanbMOTSRegs;

extern volatile struct ECAP_REGS ECap1Regs;
extern volatile struct ECAP_REGS ECap2Regs;
extern volatile struct ECAP_REGS ECap3Regs;
extern volatile struct ECAP_REGS ECap4Regs;
extern volatile struct ECAP_REGS ECap5Regs;
extern volatile struct ECAP_REGS ECap6Regs;

extern volatile struct EPWM_REGS EPwm1Regs;
extern volatile struct EPWM_REGS EPwm2Regs;
extern volatile struct EPWM_REGS EPwm3Regs;
extern volatile struct EPWM_REGS EPwm4Regs;
extern volatile struct EPWM_REGS EPwm5Regs;
extern volatile struct EPWM_REGS EPwm6Regs;

extern volatile struct EQEP_REGS EQep1Regs;
extern volatile struct EQEP_REGS EQep2Regs;

extern volatile struct GPIO_CTRL_REGS GpioCtrlRegs;
extern volatile struct GPIO_DATA_REGS GpioDataRegs;
extern volatile struct GPIO_INT_REGS GpioIntRegs;

extern volatile struct I2C_REGS I2caRegs;

extern volatile struct MCBSP_REGS McbspaRegs;
extern volatile struct MCBSP_REGS McbspbRegs;

extern volatile struct PIE_CTRL_REGS PieCtrlRegs;

extern volatile struct SCI_REGS SciaRegs;
extern volatile struct SCI_REGS ScibRegs;
extern volatile struct SCI_REGS ScicRegs;

extern volatile struct SPI_REGS SpiaRegs;
extern volatile struct SPI_REGS SpibRegs;
extern volatile struct SPI_REGS SpicRegs;
extern volatile struct SPI_REGS SpidRegs;

extern volatile struct SYS_CTRL_REGS SysCtrlRegs;
extern volatile struct CSM_REGS CsmRegs;
extern volatile struct CSM_PWL CsmPwl;
extern volatile struct FLASH_REGS FlashRegs;

extern volatile struct XINTF_REGS XintfRegs;
extern volatile struct XINTRUPT_REGS XIntruptRegs;

extern struct PIE_VECT_TABLE PieVectTable;

//***************************************************************************
// TMS320LF28335 REGISTERS DEFINITION
//***************************************************************************

//---------------------------------------------------------------------------
// ADC Individual Register Bit Definitions:

struct ADCTRL1_BITS {     // bits  description
    Uint16  rsvd1:4;      // 3:0   reserved
    Uint16  SEQ_CASC:1;   // 4     Cascaded sequencer mode
    Uint16  SEQ_OVRD:1;   // 5     Sequencer override 
    Uint16  CONT_RUN:1;   // 6     Continuous run
    Uint16  CPS:1;        // 7     ADC core clock pre-scalar
    Uint16  ACQ_PS:4;     // 11:8  Acquisition window size
    Uint16  SUSMOD:2;     // 13:12 Emulation suspend mode
    Uint16  RESET:1;      // 14    ADC reset
    Uint16  rsvd2:1;      // 15    reserved
};


union ADCTRL1_REG {
   Uint16                all;
   struct ADCTRL1_BITS   bit;
};


struct ADCTRL2_BITS {         // bits  description
    Uint16  EPWM_SOCB_SEQ2:1; // 0     EPWM compare B SOC mask for SEQ2
    Uint16  rsvd1:1;          // 1     reserved
    Uint16  INT_MOD_SEQ2:1;   // 2     SEQ2 Interrupt mode
    Uint16  INT_ENA_SEQ2:1;   // 3     SEQ2 Interrupt enable
    Uint16  rsvd2:1;          // 4     reserved
    Uint16  SOC_SEQ2:1;       // 5     Start of conversion for SEQ2
    Uint16  RST_SEQ2:1;       // 6     Reset SEQ2
    Uint16  EXT_SOC_SEQ1:1;   // 7     External start of conversion for SEQ1
    Uint16  EPWM_SOCA_SEQ1:1; // 8     EPWM compare B SOC mask for SEQ1
    Uint16  rsvd3:1;          // 9     reserved
    Uint16  INT_MOD_SEQ1:1;   // 10    SEQ1 Interrupt mode
    Uint16  INT_ENA_SEQ1:1;   // 11    SEQ1 Interrupt enable
    Uint16  rsvd4:1;          // 12    reserved
    Uint16  SOC_SEQ1:1;       // 13    Start of conversion trigger for SEQ1
    Uint16  RST_SEQ1:1;       // 14    Restart sequencer 1   
    Uint16  EPWM_SOCB_SEQ:1;  // 15    EPWM compare B SOC enable
};


union ADCTRL2_REG {
   Uint16                all;
   struct ADCTRL2_BITS   bit;
};


struct ADCASEQSR_BITS {       // bits   description
    Uint16  SEQ1_STATE:4;     // 3:0    SEQ1 state
    Uint16  SEQ2_STATE:3;     // 6:4    SEQ2 state
    Uint16  rsvd1:1;          // 7      reserved
    Uint16  SEQ_CNTR:4;       // 11:8   Sequencing counter status 
    Uint16  rsvd2:4;          // 15:12  reserved  
};

union ADCASEQSR_REG {
   Uint16                 all;
   struct ADCASEQSR_BITS  bit;
};


struct ADCMAXCONV_BITS {      // bits  description
    Uint16  MAX_CONV1:4;      // 3:0   Max number of conversions
    Uint16  MAX_CONV2:3;      // 6:4   Max number of conversions    
    Uint16  rsvd1:9;          // 15:7  reserved 
};

union ADCMAXCONV_REG {
   Uint16                  all;
   struct ADCMAXCONV_BITS  bit;
};


struct ADCCHSELSEQ1_BITS {    // bits   description
    Uint16  CONV00:4;         // 3:0    Conversion selection 00
    Uint16  CONV01:4;         // 7:4    Conversion selection 01
    Uint16  CONV02:4;         // 11:8   Conversion selection 02
    Uint16  CONV03:4;         // 15:12  Conversion selection 03
};

union  ADCCHSELSEQ1_REG{
   Uint16                    all;
   struct ADCCHSELSEQ1_BITS  bit;
};

struct ADCCHSELSEQ2_BITS {    // bits   description
    Uint16  CONV04:4;         // 3:0    Conversion selection 04
    Uint16  CONV05:4;         // 7:4    Conversion selection 05
    Uint16  CONV06:4;         // 11:8   Conversion selection 06
    Uint16  CONV07:4;         // 15:12  Conversion selection 07
};

union  ADCCHSELSEQ2_REG{
   Uint16                    all;
   struct ADCCHSELSEQ2_BITS  bit;
};

struct ADCCHSELSEQ3_BITS {    // bits   description
    Uint16  CONV08:4;         // 3:0    Conversion selection 08
    Uint16  CONV09:4;         // 7:4    Conversion selection 09
    Uint16  CONV10:4;         // 11:8   Conversion selection 10
    Uint16  CONV11:4;         // 15:12  Conversion selection 11
};

union  ADCCHSELSEQ3_REG{
   Uint16                    all;
   struct ADCCHSELSEQ3_BITS  bit;
};

struct ADCCHSELSEQ4_BITS {    // bits   description
    Uint16  CONV12:4;         // 3:0    Conversion selection 12
    Uint16  CONV13:4;         // 7:4    Conversion selection 13
    Uint16  CONV14:4;         // 11:8   Conversion selection 14
    Uint16  CONV15:4;         // 15:12  Conversion selection 15
};

union  ADCCHSELSEQ4_REG {
   Uint16                    all;
   struct ADCCHSELSEQ4_BITS  bit;
};

struct ADCTRL3_BITS {         // bits   description
    Uint16   SMODE_SEL:1;     // 0      Sampling mode select
    Uint16   ADCCLKPS:4;      // 4:1    ADC core clock divider
    Uint16   ADCPWDN:1;       // 5      ADC powerdown
    Uint16   ADCBGRFDN:2;     // 7:6    ADC bandgap/ref power down
    Uint16   rsvd1:8;         // 15:8   reserved
}; 

union  ADCTRL3_REG {
   Uint16                all;
   struct ADCTRL3_BITS   bit;
};


struct ADCST_BITS {           // bits   description
    Uint16   INT_SEQ1:1;      // 0      SEQ1 Interrupt flag  
    Uint16   INT_SEQ2:1;      // 1      SEQ2 Interrupt flag
    Uint16   SEQ1_BSY:1;      // 2      SEQ1 busy status
    Uint16   SEQ2_BSY:1;      // 3      SEQ2 busy status
    Uint16   INT_SEQ1_CLR:1;  // 4      SEQ1 Interrupt clear
    Uint16   INT_SEQ2_CLR:1;  // 5      SEQ2 Interrupt clear
    Uint16   EOS_BUF1:1;      // 6      End of sequence buffer1
    Uint16   EOS_BUF2:1;      // 7      End of sequence buffer2
    Uint16   rsvd1:8;         // 15:8   reserved
};

                             
union  ADCST_REG {            
   Uint16             all;    
   struct ADCST_BITS  bit;    
};                           

struct ADCREFSEL_BITS {       // bits   description
	Uint16   rsvd1:14;        // 13:0   reserved  
	Uint16   REF_SEL:2;       // 15:14  Reference select
};
union ADCREFSEL_REG {
	Uint16		all;
	struct ADCREFSEL_BITS bit;
};

struct ADCOFFTRIM_BITS{       // bits   description
	int16	OFFSET_TRIM:9;    // 8:0    Offset Trim  
	Uint16	rsvd1:7;          // 15:9   reserved
};

union ADCOFFTRIM_REG{
	Uint16		all;
	struct ADCOFFTRIM_BITS bit;
};
struct ADC_REGS {
    union ADCTRL1_REG      ADCTRL1;       // ADC Control 1
    union ADCTRL2_REG      ADCTRL2;       // ADC Control 2
    union ADCMAXCONV_REG   ADCMAXCONV;    // Max conversions
    union ADCCHSELSEQ1_REG ADCCHSELSEQ1;  // Channel select sequencing control 1
    union ADCCHSELSEQ2_REG ADCCHSELSEQ2;  // Channel select sequencing control 2
    union ADCCHSELSEQ3_REG ADCCHSELSEQ3;  // Channel select sequencing control 3
    union ADCCHSELSEQ4_REG ADCCHSELSEQ4;  // Channel select sequencing control 4
    union ADCASEQSR_REG    ADCASEQSR;     // Autosequence status register
    Uint16                 ADCRESULT0;    // Conversion Result Buffer 0
    Uint16                 ADCRESULT1;    // Conversion Result Buffer 1
    Uint16                 ADCRESULT2;    // Conversion Result Buffer 2
    Uint16                 ADCRESULT3;    // Conversion Result Buffer 3
    Uint16                 ADCRESULT4;    // Conversion Result Buffer 4
    Uint16                 ADCRESULT5;    // Conversion Result Buffer 5
    Uint16                 ADCRESULT6;    // Conversion Result Buffer 6
    Uint16                 ADCRESULT7;    // Conversion Result Buffer 7
    Uint16                 ADCRESULT8;    // Conversion Result Buffer 8
    Uint16                 ADCRESULT9;    // Conversion Result Buffer 9
    Uint16                 ADCRESULT10;   // Conversion Result Buffer 10
    Uint16                 ADCRESULT11;   // Conversion Result Buffer 11
    Uint16                 ADCRESULT12;   // Conversion Result Buffer 12
    Uint16                 ADCRESULT13;   // Conversion Result Buffer 13
    Uint16                 ADCRESULT14;   // Conversion Result Buffer 14
    Uint16                 ADCRESULT15;   // Conversion Result Buffer 15
    union ADCTRL3_REG      ADCTRL3;       // ADC Control 3  
    union ADCST_REG        ADCST;         // ADC Status Register
    Uint16				   rsvd1;
    Uint16                 rsvd2;
    union ADCREFSEL_REG    ADCREFSEL;     // Reference Select Register
    union ADCOFFTRIM_REG   ADCOFFTRIM;    // Offset Trim Register
};


struct ADC_RESULT_MIRROR_REGS
{
    Uint16                 ADCRESULT0;    // Conversion Result Buffer 0
    Uint16                 ADCRESULT1;    // Conversion Result Buffer 1
    Uint16                 ADCRESULT2;    // Conversion Result Buffer 2
    Uint16                 ADCRESULT3;    // Conversion Result Buffer 3
    Uint16                 ADCRESULT4;    // Conversion Result Buffer 4
    Uint16                 ADCRESULT5;    // Conversion Result Buffer 5
    Uint16                 ADCRESULT6;    // Conversion Result Buffer 6
    Uint16                 ADCRESULT7;    // Conversion Result Buffer 7
    Uint16                 ADCRESULT8;    // Conversion Result Buffer 8
    Uint16                 ADCRESULT9;    // Conversion Result Buffer 9
    Uint16                 ADCRESULT10;   // Conversion Result Buffer 10
    Uint16                 ADCRESULT11;   // Conversion Result Buffer 11
    Uint16                 ADCRESULT12;   // Conversion Result Buffer 12
    Uint16                 ADCRESULT13;   // Conversion Result Buffer 13
    Uint16                 ADCRESULT14;   // Conversion Result Buffer 14
    Uint16                 ADCRESULT15;   // Conversion Result Buffer 15
};

//---------------------------------------------------------------------------
// CPU Timer Register Bit Definitions:
//
//
// TCR: Control register bit definitions:
struct  TCR_BITS {          // bits  description
   Uint16    rsvd1:4;       // 3:0   reserved
   Uint16    TSS:1;         // 4     Timer Start/Stop
   Uint16    TRB:1;         // 5     Timer reload
   Uint16    rsvd2:4;       // 9:6   reserved
   Uint16    SOFT:1;        // 10    Emulation modes
   Uint16    FREE:1;        // 11
   Uint16    rsvd3:2;       // 12:13 reserved
   Uint16    TIE:1;         // 14    Output enable
   Uint16    TIF:1;         // 15    Interrupt flag
};

union TCR_REG {
   Uint16           all;
   struct TCR_BITS  bit;
};

// TPR: Pre-scale low bit definitions:
struct  TPR_BITS {        // bits  description
   Uint16     TDDR:8;     // 7:0   Divide-down low
   Uint16     PSC:8;      // 15:8  Prescale counter low
};

union TPR_REG {
   Uint16           all;
   struct TPR_BITS  bit;
};

// TPRH: Pre-scale high bit definitions:
struct  TPRH_BITS {       // bits  description
   Uint16     TDDRH:8;      // 7:0   Divide-down high
   Uint16     PSCH:8;       // 15:8  Prescale counter high
};

union TPRH_REG {
   Uint16           all;
   struct TPRH_BITS bit;
};

// TIM, TIMH: Timer register definitions:
struct TIM_REG {
   Uint16  LSW;
   Uint16  MSW;
};

union TIM_GROUP {
   Uint32          all;
   struct TIM_REG  half;
};

// PRD, PRDH: Period register definitions:
struct PRD_REG {
   Uint16  LSW;
   Uint16  MSW;
};

union PRD_GROUP {
   Uint32          all;
   struct PRD_REG  half;
};

//---------------------------------------------------------------------------
// CPU Timer Register File:
//
struct CPUTIMER_REGS {
   union TIM_GROUP TIM;   // Timer counter register
   union PRD_GROUP PRD;   // Period register
   union TCR_REG   TCR;   // Timer control register
   Uint16          rsvd1; // reserved
   union TPR_REG   TPR;   // Timer pre-scale low
   union TPRH_REG  TPRH;  // Timer pre-scale high
};

//---------------------------------------------------------------------------
// CPU Timer Support Variables:
//
struct CPUTIMER_VARS {
   volatile struct  CPUTIMER_REGS  *RegsAddr;
   Uint32    InterruptCount;
   float   CPUFreqInMHz;
   float   PeriodInUSec;
};

//----------------------------------------------------
// Channel MODE register bit definitions:
struct MODE_BITS {           // bits   description
   Uint16 PERINTSEL:5;       // 4:0    Peripheral Interrupt and Sync Select Bits (R/W):
                             //        0     no interrupt
							 //        1     SEQ1INT & ADCSYNC
                             //        2     SEQ2INT
							 //        3     XINT1
							 //        4     XINT2
							 //        5     XINT3
							 //        6     XINT4
							 //        7     XINT5
							 //        8     XINT6
							 //        9     XINT7
							 //        10    XINT13
							 //        11    TINT0
							 //        12    TINT1
							 //        13    TINT2
							 //        14    MXEVTA & MXSYNCA
							 //        15    MREVTA & MRSYNCA
							 //        16    MXEVTB & MXSYNCB
							 //        17    MREVTB & MRSYNCB
							 //        18:23 no interrupt
   Uint16 rsvd1:2;           // 6:5    (R=0:0)
   Uint16 OVRINTE:1;         // 7      Overflow Interrupt Enable (R/W):
                             //        0     overflow interrupt disabled
                             //        1     overflow interrupt enabled
   Uint16 PERINTE:1;         // 8      Peripheral Interrupt Enable Bit (R/W):
                             //        0     peripheral interrupt disabled
                             //        1     peripheral interrupt enabled
   Uint16 CHINTMODE:1;       // 9      Channel Interrupt Mode Bit (R/W):
                             //        0     generate interrupt at beginning of new transfer
                             //        1     generate interrupt at end of transfer
   Uint16 ONESHOT:1;         // 10     One Shot Mode Bit (R/W):
                             //        0     only interrupt event triggers single burst transfer
                             //        1     first interrupt triggers burst, continue until transfer count is zero
   Uint16 CONTINUOUS:1;      // 11     Continous Mode Bit (R/W):
                             //        0     stop when transfer count is zero
                             //        1     re-initialize when transfer count is zero
   Uint16 SYNCE:1;           // 12     Sync Enable Bit (R/W):
                             //        0     ignore selected interrupt sync signal
                             //        1     enable selected interrupt sync signal
   Uint16 SYNCSEL:1;         // 13     Sync Select Bit (R/W):
                             //        0     sync signal controls source wrap counter
                             //        1     sync signal controls destination wrap counter
   Uint16 DATASIZE:1;        // 14     Data Size Mode Bit (R/W):
                             //        0     16-bit data transfer size
                             //        1     32-bit data transfer size
   Uint16 CHINTE:1;          // 15     Channel Interrupt Enable Bit (R/W):
                             //        0     channel interrupt disabled
                             //        1     channel interrupt enabled
};

union MODE_REG {
   Uint16                all;
   struct MODE_BITS      bit;
};

//----------------------------------------------------
// Channel CONTROL register bit definitions:
struct CONTROL_BITS {        // bits   description
   Uint16 RUN:1;             // 0      Run Bit (R=0/W=1)
   Uint16 HALT:1;            // 1      Halt Bit (R=0/W=1)
   Uint16 SOFTRESET:1;       // 2      Soft Reset Bit (R=0/W=1)
   Uint16 PERINTFRC:1;       // 3      Interrupt Force Bit (R=0/W=1)
   Uint16 PERINTCLR:1;       // 4      Interrupt Clear Bit (R=0/W=1)
   Uint16 SYNCFRC:1;         // 5      Sync Force Bit (R=0/W=1)
   Uint16 SYNCCLR:1;         // 6      Sync Clear Bit (R=0/W=1)
   Uint16 ERRCLR:1;          // 7      Error Clear Bit (R=0/W=1)
   Uint16 PERINTFLG:1;       // 8      Interrupt Flag Bit (R):
                             //        0     no interrupt pending
                             //        1     interrupt pending
   Uint16 SYNCFLG:1;         // 9      Sync Flag Bit (R):
                             //        0     no sync pending
                             //        1     sync pending
   Uint16 SYNCERR:1;         // 10     Sync Error Flag Bit (R):
                             //        0     no sync error
                             //        1     sync error detected
   Uint16 TRANSFERSTS:1;     // 11     Transfer Status Bit (R):
                             //        0     no transfer in progress or pending
                             //        1     transfer in progress or pending
   Uint16 BURSTSTS:1;        // 12     Burst Status Bit (R):
                             //        0     no burst in progress or pending
                             //        1     burst in progress or pending
   Uint16 RUNSTS:1;          // 13     Run Status Bit (R):
                             //        0     channel not running or halted
                             //        1     channel running
   Uint16 OVRFLG:1;          // 14     Overflow Flag Bit(R)
                             //        0     no overflow event
                             //        1     overflow event
   Uint16 rsvd1:1;           // 15     (R=0)
};

union CONTROL_REG {
   Uint16                 all;
   struct CONTROL_BITS    bit;
};

//----------------------------------------------------
// DMACTRL register bit definitions:
struct DMACTRL_BITS {        // bits   description
   Uint16 HARDRESET:1;       // 0      Hard Reset Bit (R=0/W=1)
   Uint16 PRIORITYRESET:1;   // 1      Priority Reset Bit (R=0/W=1)
   Uint16 rsvd1:14;          // 15:2   (R=0:0)
};

union DMACTRL_REG {
   Uint16                 all;
   struct DMACTRL_BITS    bit;
};

//----------------------------------------------------
// DEBUGCTRL register bit definitions:
struct DEBUGCTRL_BITS {      // bits   description
   Uint16 rsvd1:15;          // 14:0   (R=0:0)
   Uint16 FREE:1;            // 15     Debug Mode Bit (R/W):
                             //        0     halt after current read-write operation
                             //        1     continue running
};

union DEBUGCTRL_REG {
   Uint16                 all;
   struct DEBUGCTRL_BITS  bit;
};


//----------------------------------------------------
// PRIORITYCTRL1 register bit definitions:
struct PRIORITYCTRL1_BITS {  // bits   description
   Uint16 CH1PRIORITY:1;     // 0      Ch1 Priority Bit (R/W):
                             //        0     same priority as all other channels
                             //        1     highest priority channel
   Uint16 rsvd1:15;          // 15:1   (R=0:0)
};

union PRIORITYCTRL1_REG {
   Uint16                     all;
   struct PRIORITYCTRL1_BITS  bit;
};


//----------------------------------------------------
// PRIORITYSTAT register bit definitions:
struct PRIORITYSTAT_BITS {    // bits   description
   Uint16 ACTIVESTS:3;        // 2:0    Active Channel Status Bits (R):
                              //        0,0,0  no channel active
                              //        0,0,1  Ch1 channel active
                              //        0,1,0  Ch2 channel active
                              //        0,1,1  Ch3 channel active
                              //        1,0,0  Ch4 channel active
                              //        1,0,1  Ch5 channel active
                              //        1,1,0  Ch6 channel active
   Uint16 rsvd1:1;            // 3      (R=0)
   Uint16 ACTIVESTS_SHADOW:3; // 6:4    Active Channel Status Shadow Bits (R):
                              //        0,0,0  no channel active and interrupted by Ch1
                              //        0,0,1  cannot occur
                              //        0,1,0  Ch2 was active and interrupted by Ch1
                              //        0,1,1  Ch3 was active and interrupted by Ch1
                              //        1,0,0  Ch4 was active and interrupted by Ch1
                              //        1,0,1  Ch5 was active and interrupted by Ch1
                              //        1,1,0  Ch6 was active and interrupted by Ch1
   Uint16 rsvd2:9;            // 15:7   (R=0:0)
};

union PRIORITYSTAT_REG {
   Uint16                     all;
   struct PRIORITYSTAT_BITS   bit;
};

// Burst Size
struct BURST_SIZE_BITS {  // bits  description
   Uint16 BURSTSIZE:5;    // 4:0   Burst transfer size
   Uint16 rsvd1:11;       // 15:5  reserved
};

union BURST_SIZE_REG {
   Uint16                  all;
   struct BURST_SIZE_BITS  bit;
};

// Burst Count
struct BURST_COUNT_BITS { // bits  description
   Uint16 BURSTCOUNT:5;   // 4:0   Burst transfer size
   Uint16 rsvd1:11;       // 15:5  reserved
};

union BURST_COUNT_REG {
   Uint16                   all;
   struct BURST_COUNT_BITS  bit;
};



//----------------------------------------------------
// DMA Channel Registers:
struct CH_REGS {
   union  MODE_REG            MODE;                 // Mode Register
   union  CONTROL_REG         CONTROL;              // Control Register

   union  BURST_SIZE_REG      BURST_SIZE;           // Burst Size Register
   union  BURST_COUNT_REG     BURST_COUNT;          // Burst Count Register
   int16                      SRC_BURST_STEP;       // Source Burst Step Register
   int16                      DST_BURST_STEP;       // Destination Burst Step Register

   Uint16                     TRANSFER_SIZE;        // Transfer Size Register
   Uint16                     TRANSFER_COUNT;       // Transfer Count Register
   int16                      SRC_TRANSFER_STEP;    // Source Transfer Step Register
   int16                      DST_TRANSFER_STEP;    // Destination Transfer Step Register

   Uint16                     SRC_WRAP_SIZE;        // Source Wrap Size Register
   Uint16                     SRC_WRAP_COUNT;       // Source Wrap Count Register
   int16                      SRC_WRAP_STEP;        // Source Wrap Step Register

   Uint16                     DST_WRAP_SIZE;        // Destination Wrap Size Register
   Uint16                     DST_WRAP_COUNT;       // Destination Wrap Count Register
   int16                      DST_WRAP_STEP;        // Destination Wrap Step Register

   Uint32                     SRC_BEG_ADDR_SHADOW;  // Source Begin Address Shadow Register
   Uint32                     SRC_ADDR_SHADOW;      // Source Address Shadow Register
   Uint32                     SRC_BEG_ADDR_ACTIVE;  // Source Begin Address Active Register
   Uint32                     SRC_ADDR_ACTIVE;      // Source Address Active Register

   Uint32                     DST_BEG_ADDR_SHADOW;  // Destination Begin Address Shadow Register
   Uint32                     DST_ADDR_SHADOW;      // Destination Address Shadow Register
   Uint32                     DST_BEG_ADDR_ACTIVE;  // Destination Begin Address Active Register
   Uint32                     DST_ADDR_ACTIVE;      // Destination Address Active Register
};

//----------------------------------------------------
// DMA Registers:
struct DMA_REGS {
   union  DMACTRL_REG         DMACTRL;              // DMA Control Register
   union  DEBUGCTRL_REG       DEBUGCTRL;            // Debug Control Register
   Uint16                     rsvd0;                // reserved
   Uint16                     rsvd1;                //
   union  PRIORITYCTRL1_REG   PRIORITYCTRL1;        // Priority Control 1 Register
   Uint16                     rsvd2;                //
   union  PRIORITYSTAT_REG    PRIORITYSTAT;         // Priority Status Register
   Uint16                     rsvd3[25];            //
   struct CH_REGS             CH1;                  // DMA Channel 1 Registers
   struct CH_REGS             CH2;                  // DMA Channel 2 Registers
   struct CH_REGS             CH3;                  // DMA Channel 3 Registers
   struct CH_REGS             CH4;                  // DMA Channel 4 Registers
   struct CH_REGS             CH5;                  // DMA Channel 5 Registers
   struct CH_REGS             CH6;                  // DMA Channel 6 Registers
};

//---------------------------------------------------------------------------
// Device Emulation Register Bit Definitions:
//
// Device Configuration Register Bit Definitions
struct DEVICECNF_BITS  {     // bits  description
   Uint16 rsvd1:3;           // 2:0   reserved
   Uint16 VMAPS:1;           // 3     VMAP Status
   Uint16 rsvd2:1;           // 4     reserved
   Uint16 XRSn:1;            // 5     XRSn Signal Status
   Uint16 rsvd3:10;          // 15:6
   Uint16 rsvd4:3;           // 18:16
   Uint16 ENPROT:1;          // 19    Enable/Disable pipeline protection
   Uint16 MONPRIV:1;         // 20    MONPRIV enable bit
   Uint16 rsvd5:1;           // 21    reserved
   Uint16 EMU0SEL:2;         // 23,22 EMU0 Mux select
   Uint16 EMU1SEL:2;         // 25,24 EMU1 Mux select
   Uint16 MCBSPCON:1;        // 26    McBSP-B to EMU0/EMU1 pins control
   Uint16 rsvd6:5;           // 31:27 reserved
};

union DEVICECNF_REG {
   Uint32                 all;
   struct DEVICECNF_BITS  bit;
};

// PARTID 
struct PARTID_BITS   {  // bits  description
   Uint16 PARTNO:8;     // 7:0   Part Number
   Uint16 PARTTYPE:8;   // 15:8  Part Type
};

union PARTID_REG {
   Uint16              all;
   struct PARTID_BITS  bit;
};

struct DEV_EMU_REGS {
   union DEVICECNF_REG DEVICECNF;  // device configuration
   union PARTID_REG    PARTID;     // Part ID
   Uint16              REVID;      // Device ID
   Uint16              PROTSTART;  // Write-Read protection start
   Uint16              PROTRANGE;  // Write-Read protection range
   Uint16              rsvd2[202];
};

/* --------------------------------------------------- */
/* eCAN Control & Status Registers                     */
/* ----------------------------------------------------*/

/* eCAN Mailbox enable register (CANME) bit definitions */
struct  CANME_BITS {      // bit  description
   Uint16      ME0:1;     // 0   Enable Mailbox 0
   Uint16      ME1:1;     // 1   Enable Mailbox 1
   Uint16      ME2:1;     // 2   Enable Mailbox 2
   Uint16      ME3:1;     // 3   Enable Mailbox 3
   Uint16      ME4:1;     // 4   Enable Mailbox 4
   Uint16      ME5:1;     // 5   Enable Mailbox 5
   Uint16      ME6:1;     // 6   Enable Mailbox 6
   Uint16      ME7:1;     // 7   Enable Mailbox 7
   Uint16      ME8:1;     // 8   Enable Mailbox 8
   Uint16      ME9:1;     // 9   Enable Mailbox 9
   Uint16      ME10:1;    // 10  Enable Mailbox 10
   Uint16      ME11:1;    // 11  Enable Mailbox 11
   Uint16      ME12:1;    // 12  Enable Mailbox 12
   Uint16      ME13:1;    // 13  Enable Mailbox 13
   Uint16      ME14:1;    // 14  Enable Mailbox 14
   Uint16      ME15:1;    // 15  Enable Mailbox 15
   Uint16      ME16:1;    // 16  Enable Mailbox 16
   Uint16      ME17:1;    // 17  Enable Mailbox 17
   Uint16      ME18:1;    // 18  Enable Mailbox 18
   Uint16      ME19:1;    // 19  Enable Mailbox 19
   Uint16      ME20:1;    // 20  Enable Mailbox 20
   Uint16      ME21:1;    // 21  Enable Mailbox 21
   Uint16      ME22:1;    // 22  Enable Mailbox 22
   Uint16      ME23:1;    // 23  Enable Mailbox 23
   Uint16      ME24:1;    // 24  Enable Mailbox 24
   Uint16      ME25:1;    // 25  Enable Mailbox 25
   Uint16      ME26:1;    // 26  Enable Mailbox 26
   Uint16      ME27:1;    // 27  Enable Mailbox 27
   Uint16      ME28:1;    // 28  Enable Mailbox 28
   Uint16      ME29:1;    // 29  Enable Mailbox 29
   Uint16      ME30:1;    // 30  Enable Mailbox 30
   Uint16      ME31:1;    // 31  Enable Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANME_REG {
   Uint32             all;
   struct CANME_BITS  bit;
};

/* eCAN Mailbox direction register (CANMD) bit definitions */
struct  CANMD_BITS {      // bit  description
   Uint16      MD0:1;     // 0   0 -> Tx 1 -> Rx
   Uint16      MD1:1;     // 1   0 -> Tx 1 -> Rx
   Uint16      MD2:1;     // 2   0 -> Tx 1 -> Rx
   Uint16      MD3:1;     // 3   0 -> Tx 1 -> Rx
   Uint16      MD4:1;     // 4   0 -> Tx 1 -> Rx
   Uint16      MD5:1;     // 5   0 -> Tx 1 -> Rx
   Uint16      MD6:1;     // 6   0 -> Tx 1 -> Rx
   Uint16      MD7:1;     // 7   0 -> Tx 1 -> Rx
   Uint16      MD8:1;     // 8   0 -> Tx 1 -> Rx
   Uint16      MD9:1;     // 9   0 -> Tx 1 -> Rx
   Uint16      MD10:1;    // 10  0 -> Tx 1 -> Rx
   Uint16      MD11:1;    // 11  0 -> Tx 1 -> Rx
   Uint16      MD12:1;    // 12  0 -> Tx 1 -> Rx
   Uint16      MD13:1;    // 13  0 -> Tx 1 -> Rx
   Uint16      MD14:1;    // 14  0 -> Tx 1 -> Rx
   Uint16      MD15:1;    // 15  0 -> Tx 1 -> Rx
   Uint16      MD16:1;    // 16  0 -> Tx 1 -> Rx
   Uint16      MD17:1;    // 17  0 -> Tx 1 -> Rx
   Uint16      MD18:1;    // 18  0 -> Tx 1 -> Rx
   Uint16      MD19:1;    // 19  0 -> Tx 1 -> Rx
   Uint16      MD20:1;    // 20  0 -> Tx 1 -> Rx
   Uint16      MD21:1;    // 21  0 -> Tx 1 -> Rx
   Uint16      MD22:1;    // 22  0 -> Tx 1 -> Rx
   Uint16      MD23:1;    // 23  0 -> Tx 1 -> Rx
   Uint16      MD24:1;    // 24  0 -> Tx 1 -> Rx
   Uint16      MD25:1;    // 25  0 -> Tx 1 -> Rx
   Uint16      MD26:1;    // 26  0 -> Tx 1 -> Rx
   Uint16      MD27:1;    // 27  0 -> Tx 1 -> Rx
   Uint16      MD28:1;    // 28  0 -> Tx 1 -> Rx
   Uint16      MD29:1;    // 29  0 -> Tx 1 -> Rx
   Uint16      MD30:1;    // 30  0 -> Tx 1 -> Rx
   Uint16      MD31:1;    // 31  0 -> Tx 1 -> Rx

};

/* Allow access to the bit fields or entire register */
union CANMD_REG {
   Uint32             all;
   struct CANMD_BITS  bit;
};

/* eCAN Transmit Request Set register (CANTRS) bit definitions */
struct  CANTRS_BITS {      // bit  description
   Uint16      TRS0:1;     // 0   TRS for Mailbox 0
   Uint16      TRS1:1;     // 1   TRS for Mailbox 1
   Uint16      TRS2:1;     // 2   TRS for Mailbox 2
   Uint16      TRS3:1;     // 3   TRS for Mailbox 3
   Uint16      TRS4:1;     // 4   TRS for Mailbox 4
   Uint16      TRS5:1;     // 5   TRS for Mailbox 5
   Uint16      TRS6:1;     // 6   TRS for Mailbox 6
   Uint16      TRS7:1;     // 7   TRS for Mailbox 7
   Uint16      TRS8:1;     // 8   TRS for Mailbox 8
   Uint16      TRS9:1;     // 9   TRS for Mailbox 9
   Uint16      TRS10:1;    // 10  TRS for Mailbox 10
   Uint16      TRS11:1;    // 11  TRS for Mailbox 11
   Uint16      TRS12:1;    // 12  TRS for Mailbox 12
   Uint16      TRS13:1;    // 13  TRS for Mailbox 13
   Uint16      TRS14:1;    // 14  TRS for Mailbox 14
   Uint16      TRS15:1;    // 15  TRS for Mailbox 15
   Uint16      TRS16:1;    // 16  TRS for Mailbox 16
   Uint16      TRS17:1;    // 17  TRS for Mailbox 17
   Uint16      TRS18:1;    // 18  TRS for Mailbox 18
   Uint16      TRS19:1;    // 19  TRS for Mailbox 19
   Uint16      TRS20:1;    // 20  TRS for Mailbox 20
   Uint16      TRS21:1;    // 21  TRS for Mailbox 21
   Uint16      TRS22:1;    // 22  TRS for Mailbox 22
   Uint16      TRS23:1;    // 23  TRS for Mailbox 23
   Uint16      TRS24:1;    // 24  TRS for Mailbox 24
   Uint16      TRS25:1;    // 25  TRS for Mailbox 25
   Uint16      TRS26:1;    // 26  TRS for Mailbox 26
   Uint16      TRS27:1;    // 27  TRS for Mailbox 27
   Uint16      TRS28:1;    // 28  TRS for Mailbox 28
   Uint16      TRS29:1;    // 29  TRS for Mailbox 29
   Uint16      TRS30:1;    // 30  TRS for Mailbox 30
   Uint16      TRS31:1;    // 31  TRS for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANTRS_REG {
   Uint32              all;
   struct CANTRS_BITS  bit;
};

/* eCAN Transmit Request Reset register (CANTRR) bit definitions */
struct  CANTRR_BITS {      // bit  description
   Uint16      TRR0:1;     // 0   TRR for Mailbox 0
   Uint16      TRR1:1;     // 1   TRR for Mailbox 1
   Uint16      TRR2:1;     // 2   TRR for Mailbox 2
   Uint16      TRR3:1;     // 3   TRR for Mailbox 3
   Uint16      TRR4:1;     // 4   TRR for Mailbox 4
   Uint16      TRR5:1;     // 5   TRR for Mailbox 5
   Uint16      TRR6:1;     // 6   TRR for Mailbox 6
   Uint16      TRR7:1;     // 7   TRR for Mailbox 7
   Uint16      TRR8:1;     // 8   TRR for Mailbox 8
   Uint16      TRR9:1;     // 9   TRR for Mailbox 9
   Uint16      TRR10:1;    // 10  TRR for Mailbox 10
   Uint16      TRR11:1;    // 11  TRR for Mailbox 11
   Uint16      TRR12:1;    // 12  TRR for Mailbox 12
   Uint16      TRR13:1;    // 13  TRR for Mailbox 13
   Uint16      TRR14:1;    // 14  TRR for Mailbox 14
   Uint16      TRR15:1;    // 15  TRR for Mailbox 15
   Uint16      TRR16:1;    // 16  TRR for Mailbox 16
   Uint16      TRR17:1;    // 17  TRR for Mailbox 17
   Uint16      TRR18:1;    // 18  TRR for Mailbox 18
   Uint16      TRR19:1;    // 19  TRR for Mailbox 19
   Uint16      TRR20:1;    // 20  TRR for Mailbox 20
   Uint16      TRR21:1;    // 21  TRR for Mailbox 21
   Uint16      TRR22:1;    // 22  TRR for Mailbox 22
   Uint16      TRR23:1;    // 23  TRR for Mailbox 23
   Uint16      TRR24:1;    // 24  TRR for Mailbox 24
   Uint16      TRR25:1;    // 25  TRR for Mailbox 25
   Uint16      TRR26:1;    // 26  TRR for Mailbox 26
   Uint16      TRR27:1;    // 27  TRR for Mailbox 27
   Uint16      TRR28:1;    // 28  TRR for Mailbox 28
   Uint16      TRR29:1;    // 29  TRR for Mailbox 29
   Uint16      TRR30:1;    // 30  TRR for Mailbox 30
   Uint16      TRR31:1;    // 31  TRR for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANTRR_REG {
   Uint32              all;
   struct CANTRR_BITS  bit;
};

/* eCAN Transmit Acknowledge register (CANTA) bit definitions */
struct  CANTA_BITS {      // bit  description
   Uint16      TA0:1;     // 0   TA for Mailbox 0
   Uint16      TA1:1;     // 1   TA for Mailbox 1
   Uint16      TA2:1;     // 2   TA for Mailbox 2
   Uint16      TA3:1;     // 3   TA for Mailbox 3
   Uint16      TA4:1;     // 4   TA for Mailbox 4
   Uint16      TA5:1;     // 5   TA for Mailbox 5
   Uint16      TA6:1;     // 6   TA for Mailbox 6
   Uint16      TA7:1;     // 7   TA for Mailbox 7
   Uint16      TA8:1;     // 8   TA for Mailbox 8
   Uint16      TA9:1;     // 9   TA for Mailbox 9
   Uint16      TA10:1;    // 10  TA for Mailbox 10
   Uint16      TA11:1;    // 11  TA for Mailbox 11
   Uint16      TA12:1;    // 12  TA for Mailbox 12
   Uint16      TA13:1;    // 13  TA for Mailbox 13
   Uint16      TA14:1;    // 14  TA for Mailbox 14
   Uint16      TA15:1;    // 15  TA for Mailbox 15
   Uint16      TA16:1;    // 16  TA for Mailbox 16
   Uint16      TA17:1;    // 17  TA for Mailbox 17
   Uint16      TA18:1;    // 18  TA for Mailbox 18
   Uint16      TA19:1;    // 19  TA for Mailbox 19
   Uint16      TA20:1;    // 20  TA for Mailbox 20
   Uint16      TA21:1;    // 21  TA for Mailbox 21
   Uint16      TA22:1;    // 22  TA for Mailbox 22
   Uint16      TA23:1;    // 23  TA for Mailbox 23
   Uint16      TA24:1;    // 24  TA for Mailbox 24
   Uint16      TA25:1;    // 25  TA for Mailbox 25
   Uint16      TA26:1;    // 26  TA for Mailbox 26
   Uint16      TA27:1;    // 27  TA for Mailbox 27
   Uint16      TA28:1;    // 28  TA for Mailbox 28
   Uint16      TA29:1;    // 29  TA for Mailbox 29
   Uint16      TA30:1;    // 30  TA for Mailbox 30
   Uint16      TA31:1;    // 31  TA for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANTA_REG {
   Uint32             all;
   struct CANTA_BITS  bit;
};

/* eCAN Transmit Abort Acknowledge register (CANAA) bit definitions */
struct  CANAA_BITS {      // bit  description
   Uint16      AA0:1;     // 0   AA for Mailbox 0
   Uint16      AA1:1;     // 1   AA for Mailbox 1
   Uint16      AA2:1;     // 2   AA for Mailbox 2
   Uint16      AA3:1;     // 3   AA for Mailbox 3
   Uint16      AA4:1;     // 4   AA for Mailbox 4
   Uint16      AA5:1;     // 5   AA for Mailbox 5
   Uint16      AA6:1;     // 6   AA for Mailbox 6
   Uint16      AA7:1;     // 7   AA for Mailbox 7
   Uint16      AA8:1;     // 8   AA for Mailbox 8
   Uint16      AA9:1;     // 9   AA for Mailbox 9
   Uint16      AA10:1;    // 10  AA for Mailbox 10
   Uint16      AA11:1;    // 11  AA for Mailbox 11
   Uint16      AA12:1;    // 12  AA for Mailbox 12
   Uint16      AA13:1;    // 13  AA for Mailbox 13
   Uint16      AA14:1;    // 14  AA for Mailbox 14
   Uint16      AA15:1;    // 15  AA for Mailbox 15
   Uint16      AA16:1;    // 16  AA for Mailbox 16
   Uint16      AA17:1;    // 17  AA for Mailbox 17
   Uint16      AA18:1;    // 18  AA for Mailbox 18
   Uint16      AA19:1;    // 19  AA for Mailbox 19
   Uint16      AA20:1;    // 20  AA for Mailbox 20
   Uint16      AA21:1;    // 21  AA for Mailbox 21
   Uint16      AA22:1;    // 22  AA for Mailbox 22
   Uint16      AA23:1;    // 23  AA for Mailbox 23
   Uint16      AA24:1;    // 24  AA for Mailbox 24
   Uint16      AA25:1;    // 25  AA for Mailbox 25
   Uint16      AA26:1;    // 26  AA for Mailbox 26
   Uint16      AA27:1;    // 27  AA for Mailbox 27
   Uint16      AA28:1;    // 28  AA for Mailbox 28
   Uint16      AA29:1;    // 29  AA for Mailbox 29
   Uint16      AA30:1;    // 30  AA for Mailbox 30
   Uint16      AA31:1;    // 31  AA for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANAA_REG {
   Uint32             all;
   struct CANAA_BITS  bit;
};

/* eCAN Received Message Pending register (CANRMP) bit definitions */
struct  CANRMP_BITS {      // bit  description
   Uint16      RMP0:1;     // 0   RMP for Mailbox 0
   Uint16      RMP1:1;     // 1   RMP for Mailbox 1
   Uint16      RMP2:1;     // 2   RMP for Mailbox 2
   Uint16      RMP3:1;     // 3   RMP for Mailbox 3
   Uint16      RMP4:1;     // 4   RMP for Mailbox 4
   Uint16      RMP5:1;     // 5   RMP for Mailbox 5
   Uint16      RMP6:1;     // 6   RMP for Mailbox 6
   Uint16      RMP7:1;     // 7   RMP for Mailbox 7
   Uint16      RMP8:1;     // 8   RMP for Mailbox 8
   Uint16      RMP9:1;     // 9   RMP for Mailbox 9
   Uint16      RMP10:1;    // 10  RMP for Mailbox 10
   Uint16      RMP11:1;    // 11  RMP for Mailbox 11
   Uint16      RMP12:1;    // 12  RMP for Mailbox 12
   Uint16      RMP13:1;    // 13  RMP for Mailbox 13
   Uint16      RMP14:1;    // 14  RMP for Mailbox 14
   Uint16      RMP15:1;    // 15  RMP for Mailbox 15
   Uint16      RMP16:1;    // 16  RMP for Mailbox 16
   Uint16      RMP17:1;    // 17  RMP for Mailbox 17
   Uint16      RMP18:1;    // 18  RMP for Mailbox 18
   Uint16      RMP19:1;    // 19  RMP for Mailbox 19
   Uint16      RMP20:1;    // 20  RMP for Mailbox 20
   Uint16      RMP21:1;    // 21  RMP for Mailbox 21
   Uint16      RMP22:1;    // 22  RMP for Mailbox 22
   Uint16      RMP23:1;    // 23  RMP for Mailbox 23
   Uint16      RMP24:1;    // 24  RMP for Mailbox 24
   Uint16      RMP25:1;    // 25  RMP for Mailbox 25
   Uint16      RMP26:1;    // 26  RMP for Mailbox 26
   Uint16      RMP27:1;    // 27  RMP for Mailbox 27
   Uint16      RMP28:1;    // 28  RMP for Mailbox 28
   Uint16      RMP29:1;    // 29  RMP for Mailbox 29
   Uint16      RMP30:1;    // 30  RMP for Mailbox 30
   Uint16      RMP31:1;    // 31  RMP for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANRMP_REG {
   Uint32              all;
   struct CANRMP_BITS  bit;
};

/* eCAN Received Message Lost register (CANRML) bit definitions */
struct  CANRML_BITS {      // bit  description
   Uint16      RML0:1;     // 0   RML for Mailbox 0
   Uint16      RML1:1;     // 1   RML for Mailbox 1
   Uint16      RML2:1;     // 2   RML for Mailbox 2
   Uint16      RML3:1;     // 3   RML for Mailbox 3
   Uint16      RML4:1;     // 4   RML for Mailbox 4
   Uint16      RML5:1;     // 5   RML for Mailbox 5
   Uint16      RML6:1;     // 6   RML for Mailbox 6
   Uint16      RML7:1;     // 7   RML for Mailbox 7
   Uint16      RML8:1;     // 8   RML for Mailbox 8
   Uint16      RML9:1;     // 9   RML for Mailbox 9
   Uint16      RML10:1;    // 10  RML for Mailbox 10
   Uint16      RML11:1;    // 11  RML for Mailbox 11
   Uint16      RML12:1;    // 12  RML for Mailbox 12
   Uint16      RML13:1;    // 13  RML for Mailbox 13
   Uint16      RML14:1;    // 14  RML for Mailbox 14
   Uint16      RML15:1;    // 15  RML for Mailbox 15
   Uint16      RML16:1;    // 16  RML for Mailbox 16
   Uint16      RML17:1;    // 17  RML for Mailbox 17
   Uint16      RML18:1;    // 18  RML for Mailbox 18
   Uint16      RML19:1;    // 19  RML for Mailbox 19
   Uint16      RML20:1;    // 20  RML for Mailbox 20
   Uint16      RML21:1;    // 21  RML for Mailbox 21
   Uint16      RML22:1;    // 22  RML for Mailbox 22
   Uint16      RML23:1;    // 23  RML for Mailbox 23
   Uint16      RML24:1;    // 24  RML for Mailbox 24
   Uint16      RML25:1;    // 25  RML for Mailbox 25
   Uint16      RML26:1;    // 26  RML for Mailbox 26
   Uint16      RML27:1;    // 27  RML for Mailbox 27
   Uint16      RML28:1;    // 28  RML for Mailbox 28
   Uint16      RML29:1;    // 29  RML for Mailbox 29
   Uint16      RML30:1;    // 30  RML for Mailbox 30
   Uint16      RML31:1;    // 31  RML for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANRML_REG {
   Uint32              all;
   struct CANRML_BITS  bit;
};

/* eCAN Remote Frame Pending register (CANRFP) bit definitions */
struct  CANRFP_BITS {      // bit  description
   Uint16      RFP0:1;     // 0   RFP for Mailbox 0
   Uint16      RFP1:1;     // 1   RFP for Mailbox 1
   Uint16      RFP2:1;     // 2   RFP for Mailbox 2
   Uint16      RFP3:1;     // 3   RFP for Mailbox 3
   Uint16      RFP4:1;     // 4   RFP for Mailbox 4
   Uint16      RFP5:1;     // 5   RFP for Mailbox 5
   Uint16      RFP6:1;     // 6   RFP for Mailbox 6
   Uint16      RFP7:1;     // 7   RFP for Mailbox 7
   Uint16      RFP8:1;     // 8   RFP for Mailbox 8
   Uint16      RFP9:1;     // 9   RFP for Mailbox 9
   Uint16      RFP10:1;    // 10  RFP for Mailbox 10
   Uint16      RFP11:1;    // 11  RFP for Mailbox 11
   Uint16      RFP12:1;    // 12  RFP for Mailbox 12
   Uint16      RFP13:1;    // 13  RFP for Mailbox 13
   Uint16      RFP14:1;    // 14  RFP for Mailbox 14
   Uint16      RFP15:1;    // 15  RFP for Mailbox 15
   Uint16      RFP16:1;    // 16  RFP for Mailbox 16
   Uint16      RFP17:1;    // 17  RFP for Mailbox 17
   Uint16      RFP18:1;    // 18  RFP for Mailbox 18
   Uint16      RFP19:1;    // 19  RFP for Mailbox 19
   Uint16      RFP20:1;    // 20  RFP for Mailbox 20
   Uint16      RFP21:1;    // 21  RFP for Mailbox 21
   Uint16      RFP22:1;    // 22  RFP for Mailbox 22
   Uint16      RFP23:1;    // 23  RFP for Mailbox 23
   Uint16      RFP24:1;    // 24  RFP for Mailbox 24
   Uint16      RFP25:1;    // 25  RFP for Mailbox 25
   Uint16      RFP26:1;    // 26  RFP for Mailbox 26
   Uint16      RFP27:1;    // 27  RFP for Mailbox 27
   Uint16      RFP28:1;    // 28  RFP for Mailbox 28
   Uint16      RFP29:1;    // 29  RFP for Mailbox 29
   Uint16      RFP30:1;    // 30  RFP for Mailbox 30
   Uint16      RFP31:1;    // 31  RFP for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANRFP_REG {
   Uint32              all;
   struct CANRFP_BITS  bit;
};

/* eCAN Global Acceptance Mask register (CANGAM) bit definitions */
struct  CANGAM_BITS {   // bits  description
   Uint16 GAM150:16;    // 15:0  Global acceptance mask bits 0-15
   Uint16 GAM2816:13;   // 28:16 Global acceptance mask bits 16-28
   Uint16 rsvd:2;       // 30:29 reserved
   Uint16 AMI:1;        // 31    AMI bit
};

/* Allow access to the bit fields or entire register */
union CANGAM_REG {
   Uint32              all;
   struct CANGAM_BITS  bit;
};


/* eCAN Master Control register (CANMC) bit definitions */
struct  CANMC_BITS {       // bits  description
   Uint16      MBNR:5;     // 4:0   MBX # for CDR bit
   Uint16      SRES:1;     // 5     Soft reset
   Uint16      STM:1;      // 6     Self-test mode
   Uint16      ABO:1;      // 7     Auto bus-on
   Uint16      CDR:1;      // 8     Change data request
   Uint16      WUBA:1;     // 9     Wake-up on bus activity
   Uint16      DBO:1;      // 10    Data-byte order
   Uint16      PDR:1;      // 11    Power-down mode request
   Uint16      CCR:1;      // 12    Change configuration request
   Uint16      SCB:1;      // 13    SCC compatibility bit
   Uint16      TCC:1;      // 14    TSC MSB clear bit
   Uint16      MBCC:1;     // 15    TSC clear bit thru mailbox 16
   Uint16      SUSP:1;     // 16    SUSPEND free/soft bit
   Uint16      rsvd:15;    // 31:17  reserved
};

/* Allow access to the bit fields or entire register */
union CANMC_REG {
   Uint32             all;
   struct CANMC_BITS  bit;
};

/* eCAN Bit -timing configuration register (CANBTC) bit definitions */
struct  CANBTC_BITS {  // bits  description
   Uint16  TSEG2REG:3; // 2:0   TSEG2 register value
   Uint16  TSEG1REG:4; // 6:3   TSEG1 register value
   Uint16  SAM:1;      // 7     Sample-point setting
   Uint16  SJWREG:2;   // 9:8   Synchroniztion Jump Width register value
   Uint16  rsvd1:6;    // 15:10 reserved
   Uint16  BRPREG:8;   // 23:16 Baudrate prescaler register value
   Uint16  rsvd2:8;    // 31:24 reserved
};

/* Allow access to the bit fields or entire register */
union CANBTC_REG {
   Uint32              all;
   struct CANBTC_BITS  bit;
};

/* eCAN Error & Status register (CANES) bit definitions */
struct  CANES_BITS {    // bits  description
   Uint16   TM:1;       // 0     Transmit Mode
   Uint16   RM:1;       // 1     Receive Mode
   Uint16   rsvd1:1;    // 2     reserved
   Uint16   PDA:1;      // 3     Power-down acknowledge
   Uint16   CCE:1;      // 4     Change Configuration Enable
   Uint16   SMA:1;      // 5     Suspend Mode Acknowledge
   Uint16   rsvd2:10;   // 15:6  reserved
   Uint16   EW:1;       // 16    Warning status
   Uint16   EP:1;       // 17    Error Passive status
   Uint16   BO:1;       // 18    Bus-off status
   Uint16   ACKE:1;     // 19    Acknowledge error
   Uint16   SE:1;       // 20    Stuff error
   Uint16   CRCE:1;     // 21    CRC error
   Uint16   SA1:1;      // 22    Stuck at Dominant error
   Uint16   BE:1;       // 23    Bit error
   Uint16   FE:1;       // 24    Framing error
   Uint16   rsvd3:7;    // 31:25 reserved
};

/* Allow access to the bit fields or entire register */
union CANES_REG {
   Uint32             all;
   struct CANES_BITS  bit;
};


/* eCAN Transmit Error Counter register (CANTEC) bit definitions */
struct  CANTEC_BITS {  // bits  description
   Uint16 TEC:8;       // 7:0   TEC
   Uint16 rsvd1:8;     // 15:8  reserved
   Uint16 rsvd2:16;    // 31:16  reserved
};

/* Allow access to the bit fields or entire register */
union CANTEC_REG {
   Uint32              all;
   struct CANTEC_BITS  bit;
};

/* eCAN Receive Error Counter register (CANREC) bit definitions */
struct  CANREC_BITS {  // bits  description
   Uint16 REC:8;       // 7:0   REC
   Uint16 rsvd1:8;     // 15:8  reserved
   Uint16 rsvd2:16;    // 31:16 reserved
};

/* Allow access to the bit fields or entire register */
union CANREC_REG {
   Uint32              all;
   struct CANREC_BITS  bit;
};

/* eCAN Global Interrupt Flag 0 (CANGIF0) bit definitions */
struct  CANGIF0_BITS {  // bits  description
   Uint16   MIV0:5;     // 4:0   Mailbox Interrupt Vector
   Uint16   rsvd1:3;    // 7:5   reserved
   Uint16   WLIF0:1;    // 8     Warning level interrupt flag
   Uint16   EPIF0:1;    // 9     Error-passive interrupt flag
   Uint16   BOIF0:1;    // 10    Bus-off interrupt flag
   Uint16   RMLIF0:1;   // 11    Received message lost interrupt flag
   Uint16   WUIF0:1;    // 12    Wakeup interrupt flag
   Uint16   WDIF0:1;    // 13    Write denied interrupt flag
   Uint16   AAIF0:1;    // 14    Abort Ack interrupt flag
   Uint16   GMIF0:1;    // 15    Global MBX interrupt flag
   Uint16   TCOF0:1;    // 16    TSC Overflow flag
   Uint16   MTOF0:1;    // 17    Mailbox Timeout flag
   Uint16   rsvd2:14;   // 31:18 reserved
};

/* Allow access to the bit fields or entire register */
union CANGIF0_REG {
   Uint32               all;
   struct CANGIF0_BITS  bit;
};

/* eCAN Global Interrupt Mask register (CANGIM) bit definitions */
struct  CANGIM_BITS { // bits  description
   Uint16  I0EN:1;    // 0      Interrupt 0 enable
   Uint16  I1EN:1;    // 1      Interrupt 1 enable
   Uint16  GIL:1;     // 2      Global Interrupt Level
   Uint16  rsvd1:5;   // 7:3    reserved
   Uint16  WLIM:1;    // 8      Warning level interrupt mask
   Uint16  EPIM:1;    // 9      Error-passive interrupt mask
   Uint16  BOIM:1;    // 10     Bus-off interrupt mask
   Uint16  RMLIM:1;   // 11     Received message lost interrupt mask
   Uint16  WUIM:1;    // 12     Wakeup interrupt mask
   Uint16  WDIM:1;    // 13     Write denied interrupt mask
   Uint16  AAIM:1;    // 14     Abort Ack interrupt mask
   Uint16  rsvd2:1;   // 15     reserved
   Uint16  TCOM:1;    // 16     TSC overflow interrupt mask
   Uint16  MTOM:1;    // 17     MBX Timeout interrupt mask
   Uint16  rsvd3:14;  // 31:18  reserved
};

/* Allow access to the bit fields or entire register */
union CANGIM_REG {
   Uint32              all;
   struct CANGIM_BITS  bit;
};


/* eCAN Global Interrupt Flag 1 (eCANGIF1) bit definitions */
struct  CANGIF1_BITS {     // bits  description
   Uint16      MIV1:5;     // 4:0   Mailbox Interrupt Vector
   Uint16      rsvd1:3;    // 7:5   reserved
   Uint16      WLIF1:1;    // 8     Warning level interrupt flag
   Uint16      EPIF1:1;    // 9     Error-passive interrupt flag
   Uint16      BOIF1:1;    // 10    Bus-off interrupt flag
   Uint16      RMLIF1:1;   // 11    Received message lost interrupt flag
   Uint16      WUIF1:1;    // 12    Wakeup interrupt flag
   Uint16      WDIF1:1;    // 13    Write denied interrupt flag
   Uint16      AAIF1:1;    // 14    Abort Ack interrupt flag
   Uint16      GMIF1:1;    // 15    Global MBX interrupt flag
   Uint16      TCOF1:1;    // 16    TSC Overflow flag
   Uint16      MTOF1:1;    // 17    Mailbox Timeout flag
   Uint16      rsvd2:14;   // 31:18 reserved
};

/* Allow access to the bit fields or entire register */
union CANGIF1_REG {
   Uint32               all;
   struct CANGIF1_BITS  bit;
};


/* eCAN Mailbox Interrupt Mask register (CANMIM) bit definitions */
struct  CANMIM_BITS {      // bit  description
   Uint16      MIM0:1;     // 0   MIM for Mailbox 0
   Uint16      MIM1:1;     // 1   MIM for Mailbox 1
   Uint16      MIM2:1;     // 2   MIM for Mailbox 2
   Uint16      MIM3:1;     // 3   MIM for Mailbox 3
   Uint16      MIM4:1;     // 4   MIM for Mailbox 4
   Uint16      MIM5:1;     // 5   MIM for Mailbox 5
   Uint16      MIM6:1;     // 6   MIM for Mailbox 6
   Uint16      MIM7:1;     // 7   MIM for Mailbox 7
   Uint16      MIM8:1;     // 8   MIM for Mailbox 8
   Uint16      MIM9:1;     // 9   MIM for Mailbox 9
   Uint16      MIM10:1;    // 10  MIM for Mailbox 10
   Uint16      MIM11:1;    // 11  MIM for Mailbox 11
   Uint16      MIM12:1;    // 12  MIM for Mailbox 12
   Uint16      MIM13:1;    // 13  MIM for Mailbox 13
   Uint16      MIM14:1;    // 14  MIM for Mailbox 14
   Uint16      MIM15:1;    // 15  MIM for Mailbox 15
   Uint16      MIM16:1;    // 16  MIM for Mailbox 16
   Uint16      MIM17:1;    // 17  MIM for Mailbox 17
   Uint16      MIM18:1;    // 18  MIM for Mailbox 18
   Uint16      MIM19:1;    // 19  MIM for Mailbox 19
   Uint16      MIM20:1;    // 20  MIM for Mailbox 20
   Uint16      MIM21:1;    // 21  MIM for Mailbox 21
   Uint16      MIM22:1;    // 22  MIM for Mailbox 22
   Uint16      MIM23:1;    // 23  MIM for Mailbox 23
   Uint16      MIM24:1;    // 24  MIM for Mailbox 24
   Uint16      MIM25:1;    // 25  MIM for Mailbox 25
   Uint16      MIM26:1;    // 26  MIM for Mailbox 26
   Uint16      MIM27:1;    // 27  MIM for Mailbox 27
   Uint16      MIM28:1;    // 28  MIM for Mailbox 28
   Uint16      MIM29:1;    // 29  MIM for Mailbox 29
   Uint16      MIM30:1;    // 30  MIM for Mailbox 30
   Uint16      MIM31:1;    // 31  MIM for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANMIM_REG {
   Uint32              all;
   struct CANMIM_BITS  bit;
};

/* eCAN Mailbox Interrupt Level register (CANMIL) bit definitions */
struct  CANMIL_BITS {      // bit  description
   Uint16      MIL0:1;     // 0   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL1:1;     // 1   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL2:1;     // 2   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL3:1;     // 3   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL4:1;     // 4   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL5:1;     // 5   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL6:1;     // 6   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL7:1;     // 7   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL8:1;     // 8   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL9:1;     // 9   0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL10:1;    // 10  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL11:1;    // 11  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL12:1;    // 12  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL13:1;    // 13  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL14:1;    // 14  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL15:1;    // 15  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL16:1;    // 16  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL17:1;    // 17  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL18:1;    // 18  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL19:1;    // 19  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL20:1;    // 20  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL21:1;    // 21  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL22:1;    // 22  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL23:1;    // 23  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL24:1;    // 24  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL25:1;    // 25  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL26:1;    // 26  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL27:1;    // 27  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL28:1;    // 28  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL29:1;    // 29  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL30:1;    // 30  0 -> Int 9.5   1 -> Int 9.6
   Uint16      MIL31:1;    // 31  0 -> Int 9.5   1 -> Int 9.6

};

/* Allow access to the bit fields or entire register */
union CANMIL_REG {
   Uint32              all;
   struct CANMIL_BITS  bit;
};


/* eCAN Overwrite Protection Control register (CANOPC) bit definitions */
struct  CANOPC_BITS {      // bit  description
   Uint16      OPC0:1;     // 0   OPC for Mailbox 0
   Uint16      OPC1:1;     // 1   OPC for Mailbox 1
   Uint16      OPC2:1;     // 2   OPC for Mailbox 2
   Uint16      OPC3:1;     // 3   OPC for Mailbox 3
   Uint16      OPC4:1;     // 4   OPC for Mailbox 4
   Uint16      OPC5:1;     // 5   OPC for Mailbox 5
   Uint16      OPC6:1;     // 6   OPC for Mailbox 6
   Uint16      OPC7:1;     // 7   OPC for Mailbox 7
   Uint16      OPC8:1;     // 8   OPC for Mailbox 8
   Uint16      OPC9:1;     // 9   OPC for Mailbox 9
   Uint16      OPC10:1;    // 10  OPC for Mailbox 10
   Uint16      OPC11:1;    // 11  OPC for Mailbox 11
   Uint16      OPC12:1;    // 12  OPC for Mailbox 12
   Uint16      OPC13:1;    // 13  OPC for Mailbox 13
   Uint16      OPC14:1;    // 14  OPC for Mailbox 14
   Uint16      OPC15:1;    // 15  OPC for Mailbox 15
   Uint16      OPC16:1;    // 16  OPC for Mailbox 16
   Uint16      OPC17:1;    // 17  OPC for Mailbox 17
   Uint16      OPC18:1;    // 18  OPC for Mailbox 18
   Uint16      OPC19:1;    // 19  OPC for Mailbox 19
   Uint16      OPC20:1;    // 20  OPC for Mailbox 20
   Uint16      OPC21:1;    // 21  OPC for Mailbox 21
   Uint16      OPC22:1;    // 22  OPC for Mailbox 22
   Uint16      OPC23:1;    // 23  OPC for Mailbox 23
   Uint16      OPC24:1;    // 24  OPC for Mailbox 24
   Uint16      OPC25:1;    // 25  OPC for Mailbox 25
   Uint16      OPC26:1;    // 26  OPC for Mailbox 26
   Uint16      OPC27:1;    // 27  OPC for Mailbox 27
   Uint16      OPC28:1;    // 28  OPC for Mailbox 28
   Uint16      OPC29:1;    // 29  OPC for Mailbox 29
   Uint16      OPC30:1;    // 30  OPC for Mailbox 30
   Uint16      OPC31:1;    // 31  OPC for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANOPC_REG {
   Uint32              all;
   struct CANOPC_BITS  bit;
};


/* eCAN TX I/O Control Register (CANTIOC) bit definitions */
struct  CANTIOC_BITS { // bits  description
   Uint16  rsvd1:3;    // 2:0   reserved
   Uint16  TXFUNC:1;   // 3     TXFUNC
   Uint16  rsvd2:12;   // 15:4  reserved
   Uint16  rsvd3:16;   // 31:16 reserved
};

/* Allow access to the bit fields or entire register */
union CANTIOC_REG {
   Uint32               all;
   struct CANTIOC_BITS  bit;
};

/* eCAN RX I/O Control Register (CANRIOC) bit definitions */
struct  CANRIOC_BITS { // bits  description
   Uint16  rsvd1:3;    // 2:0   reserved
   Uint16  RXFUNC:1;   // 3     RXFUNC
   Uint16  rsvd2:12;   // 15:4  reserved
   Uint16  rsvd3:16;   // 31:16 reserved
};

/* Allow access to the bit fields or entire register */
union CANRIOC_REG {
   Uint32               all;
   struct CANRIOC_BITS  bit;
};


/* eCAN Time-out Control register (CANTOC) bit definitions */
struct  CANTOC_BITS {      // bit  description
   Uint16      TOC0:1;     // 0   TOC for Mailbox 0
   Uint16      TOC1:1;     // 1   TOC for Mailbox 1
   Uint16      TOC2:1;     // 2   TOC for Mailbox 2
   Uint16      TOC3:1;     // 3   TOC for Mailbox 3
   Uint16      TOC4:1;     // 4   TOC for Mailbox 4
   Uint16      TOC5:1;     // 5   TOC for Mailbox 5
   Uint16      TOC6:1;     // 6   TOC for Mailbox 6
   Uint16      TOC7:1;     // 7   TOC for Mailbox 7
   Uint16      TOC8:1;     // 8   TOC for Mailbox 8
   Uint16      TOC9:1;     // 9   TOC for Mailbox 9
   Uint16      TOC10:1;    // 10  TOC for Mailbox 10
   Uint16      TOC11:1;    // 11  TOC for Mailbox 11
   Uint16      TOC12:1;    // 12  TOC for Mailbox 12
   Uint16      TOC13:1;    // 13  TOC for Mailbox 13
   Uint16      TOC14:1;    // 14  TOC for Mailbox 14
   Uint16      TOC15:1;    // 15  TOC for Mailbox 15
   Uint16      TOC16:1;    // 16  TOC for Mailbox 16
   Uint16      TOC17:1;    // 17  TOC for Mailbox 17
   Uint16      TOC18:1;    // 18  TOC for Mailbox 18
   Uint16      TOC19:1;    // 19  TOC for Mailbox 19
   Uint16      TOC20:1;    // 20  TOC for Mailbox 20
   Uint16      TOC21:1;    // 21  TOC for Mailbox 21
   Uint16      TOC22:1;    // 22  TOC for Mailbox 22
   Uint16      TOC23:1;    // 23  TOC for Mailbox 23
   Uint16      TOC24:1;    // 24  TOC for Mailbox 24
   Uint16      TOC25:1;    // 25  TOC for Mailbox 25
   Uint16      TOC26:1;    // 26  TOC for Mailbox 26
   Uint16      TOC27:1;    // 27  TOC for Mailbox 27
   Uint16      TOC28:1;    // 28  TOC for Mailbox 28
   Uint16      TOC29:1;    // 29  TOC for Mailbox 29
   Uint16      TOC30:1;    // 30  TOC for Mailbox 30
   Uint16      TOC31:1;    // 31  TOC for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANTOC_REG {
   Uint32              all;
   struct CANTOC_BITS  bit;
};


/* eCAN Time-out Status register (CANTOS) bit definitions */
struct  CANTOS_BITS {            // bit  description
   Uint16      TOS0:1;     // 0   TOS for Mailbox 0
   Uint16      TOS1:1;     // 1   TOS for Mailbox 1
   Uint16      TOS2:1;     // 2   TOS for Mailbox 2
   Uint16      TOS3:1;     // 3   TOS for Mailbox 3
   Uint16      TOS4:1;     // 4   TOS for Mailbox 4
   Uint16      TOS5:1;     // 5   TOS for Mailbox 5
   Uint16      TOS6:1;     // 6   TOS for Mailbox 6
   Uint16      TOS7:1;     // 7   TOS for Mailbox 7
   Uint16      TOS8:1;     // 8   TOS for Mailbox 8
   Uint16      TOS9:1;     // 9   TOS for Mailbox 9
   Uint16      TOS10:1;    // 10  TOS for Mailbox 10
   Uint16      TOS11:1;    // 11  TOS for Mailbox 11
   Uint16      TOS12:1;    // 12  TOS for Mailbox 12
   Uint16      TOS13:1;    // 13  TOS for Mailbox 13
   Uint16      TOS14:1;    // 14  TOS for Mailbox 14
   Uint16      TOS15:1;    // 15  TOS for Mailbox 15
   Uint16      TOS16:1;    // 16  TOS for Mailbox 16
   Uint16      TOS17:1;    // 17  TOS for Mailbox 17
   Uint16      TOS18:1;    // 18  TOS for Mailbox 18
   Uint16      TOS19:1;    // 19  TOS for Mailbox 19
   Uint16      TOS20:1;    // 20  TOS for Mailbox 20
   Uint16      TOS21:1;    // 21  TOS for Mailbox 21
   Uint16      TOS22:1;    // 22  TOS for Mailbox 22
   Uint16      TOS23:1;    // 23  TOS for Mailbox 23
   Uint16      TOS24:1;    // 24  TOS for Mailbox 24
   Uint16      TOS25:1;    // 25  TOS for Mailbox 25
   Uint16      TOS26:1;    // 26  TOS for Mailbox 26
   Uint16      TOS27:1;    // 27  TOS for Mailbox 27
   Uint16      TOS28:1;    // 28  TOS for Mailbox 28
   Uint16      TOS29:1;    // 29  TOS for Mailbox 29
   Uint16      TOS30:1;    // 30  TOS for Mailbox 30
   Uint16      TOS31:1;    // 31  TOS for Mailbox 31

};

/* Allow access to the bit fields or entire register */
union CANTOS_REG {
   Uint32              all;
   struct CANTOS_BITS  bit;
};

/**************************************/
/* eCAN Control & Status register file */
/**************************************/

struct ECAN_REGS {
   union CANME_REG   CANME;          // Mailbox Enable
   union CANMD_REG   CANMD;          // Mailbox Direction
   union CANTRS_REG  CANTRS;         // Transmit Request Set
   union CANTRR_REG  CANTRR;         // Transmit Request Reset
   union CANTA_REG   CANTA;          // Transmit Acknowledge
   union CANAA_REG   CANAA;          // Abort Acknowledge
   union CANRMP_REG  CANRMP;         // Received Message Pending
   union CANRML_REG  CANRML;         // Received Message Lost
   union CANRFP_REG  CANRFP;         // Remote Frame Pending
   union CANGAM_REG  CANGAM;         // Global Acceptance Mask
   union CANMC_REG   CANMC;          // Master Control
   union CANBTC_REG  CANBTC;         // Bit Timing
   union CANES_REG   CANES;          // Error Status
   union CANTEC_REG  CANTEC;         // Transmit Error Counter
   union CANREC_REG  CANREC;         // Receive Error Counter
   union CANGIF0_REG CANGIF0;        // Global Interrupt Flag 0
   union CANGIM_REG  CANGIM;         // Global Interrupt Mask 0
   union CANGIF1_REG CANGIF1;        // Global Interrupt Flag 1
   union CANMIM_REG  CANMIM;         // Mailbox Interrupt Mask
   union CANMIL_REG  CANMIL;         // Mailbox Interrupt Level
   union CANOPC_REG  CANOPC;         // Overwrite Protection Control
   union CANTIOC_REG CANTIOC;        // TX I/O Control
   union CANRIOC_REG CANRIOC;        // RX I/O Control
   Uint32            CANTSC;         // Time-stamp counter
   union CANTOC_REG  CANTOC;         // Time-out Control
   union CANTOS_REG  CANTOS;         // Time-out Status

};

/* --------------------------------------------------- */
/* eCAN Mailbox Registers                               */
/* ----------------------------------------------------*/

/* eCAN Message ID (MSGID) bit definitions */
struct  CANMSGID_BITS {        // bits  description
   Uint16      EXTMSGID_L:16;  // 0:15
   Uint16      EXTMSGID_H:2;   // 16:17
   Uint16      STDMSGID:11;    // 18:28
   Uint16      AAM:1;          // 29
   Uint16      AME:1;          // 30
   Uint16      IDE:1;          // 31

};

/* Allow access to the bit fields or entire register */
union CANMSGID_REG {
   Uint32                all;
   struct CANMSGID_BITS  bit;
};

/* eCAN Message Control Register (MSGCTRL) bit definitions */
struct  CANMSGCTRL_BITS {     // bits  description
   Uint16 DLC:4;          // 0:3
   Uint16 RTR:1;          // 4
   Uint16 rsvd1:3;        // 7:5   reserved
   Uint16 TPL:5;          // 12:8
   Uint16 rsvd2:3;        // 15:13 reserved
   Uint16 rsvd3:16;       // 31:16 reserved
};

/* Allow access to the bit fields or entire register */
union CANMSGCTRL_REG {
   Uint32                  all;
   struct CANMSGCTRL_BITS  bit;
};

/* eCAN Message Data Register low (MDR_L) word definitions */
struct  CANMDL_WORDS {      // bits  description
   Uint16      LOW_WORD:16; // 0:15
   Uint16      HI_WORD:16;  // 31:16
};

/* eCAN Message Data Register low (MDR_L) byte definitions */
struct  CANMDL_BYTES {      // bits   description
   Uint16      BYTE3:8;     // 31:24
   Uint16      BYTE2:8;     // 23:16
   Uint16      BYTE1:8;     // 15:8
   Uint16      BYTE0:8;     // 7:0
};


/* Allow access to the bit fields or entire register */

union CANMDL_REG {
   Uint32                all;
   struct CANMDL_WORDS   word;
   struct CANMDL_BYTES   byte;
};



/* eCAN Message Data Register high  (MDR_H) word definitions */
struct  CANMDH_WORDS {         // bits  description
   Uint16      LOW_WORD:16;    // 0:15
   Uint16      HI_WORD:16;     // 31:16
};

/* eCAN Message Data Register low (MDR_H) byte definitions */
struct  CANMDH_BYTES {      // bits   description
   Uint16      BYTE7:8;     // 63:56
   Uint16      BYTE6:8;     // 55:48
   Uint16      BYTE5:8;     // 47:40
   Uint16      BYTE4:8;     // 39:32
};

/* Allow access to the bit fields or entire register */
union CANMDH_REG {
   Uint32                  all;
   struct CANMDH_WORDS     word;
   struct CANMDH_BYTES     byte;
};


struct MBOX {
   union CANMSGID_REG     MSGID;
   union CANMSGCTRL_REG   MSGCTRL;
   union CANMDL_REG       MDL;
   union CANMDH_REG       MDH;
};

/**************************************/
/*          eCAN Mailboxes             */
/**************************************/

struct ECAN_MBOXES {
   struct MBOX MBOX0;
   struct MBOX MBOX1;
   struct MBOX MBOX2;
   struct MBOX MBOX3;
   struct MBOX MBOX4;
   struct MBOX MBOX5;
   struct MBOX MBOX6;
   struct MBOX MBOX7;
   struct MBOX MBOX8;
   struct MBOX MBOX9;
   struct MBOX MBOX10;
   struct MBOX MBOX11;
   struct MBOX MBOX12;
   struct MBOX MBOX13;
   struct MBOX MBOX14;
   struct MBOX MBOX15;
   struct MBOX MBOX16;
   struct MBOX MBOX17;
   struct MBOX MBOX18;
   struct MBOX MBOX19;
   struct MBOX MBOX20;
   struct MBOX MBOX21;
   struct MBOX MBOX22;
   struct MBOX MBOX23;
   struct MBOX MBOX24;
   struct MBOX MBOX25;
   struct MBOX MBOX26;
   struct MBOX MBOX27;
   struct MBOX MBOX28;
   struct MBOX MBOX29;
   struct MBOX MBOX30;
   struct MBOX MBOX31;
};

/* eCAN Local Acceptance Mask (LAM) bit definitions */
struct  CANLAM_BITS {                // bits  description
   Uint16      LAM_L:16;     // 0:15
   Uint16      LAM_H:13;     // 16:28
   Uint16 rsvd1:2;           // 29:30   reserved
   Uint16      LAMI:1;       // 31
};

/* Allow access to the bit fields or entire register */
union CANLAM_REG {
   Uint32        all;
   struct CANLAM_BITS  bit;
};


/**************************************/
/*    eCAN Local Acceptance Masks      */
/**************************************/

/* eCAN LAM File */
struct LAM_REGS {
   union CANLAM_REG LAM0;
   union CANLAM_REG LAM1;
   union CANLAM_REG LAM2;
   union CANLAM_REG LAM3;
   union CANLAM_REG LAM4;
   union CANLAM_REG LAM5;
   union CANLAM_REG LAM6;
   union CANLAM_REG LAM7;
   union CANLAM_REG LAM8;
   union CANLAM_REG LAM9;
   union CANLAM_REG LAM10;
   union CANLAM_REG LAM11;
   union CANLAM_REG LAM12;
   union CANLAM_REG LAM13;
   union CANLAM_REG LAM14;
   union CANLAM_REG LAM15;
   union CANLAM_REG LAM16;
   union CANLAM_REG LAM17;
   union CANLAM_REG LAM18;
   union CANLAM_REG LAM19;
   union CANLAM_REG LAM20;
   union CANLAM_REG LAM21;
   union CANLAM_REG LAM22;
   union CANLAM_REG LAM23;
   union CANLAM_REG LAM24;
   union CANLAM_REG LAM25;
   union CANLAM_REG LAM26;
   union CANLAM_REG LAM27;
   union CANLAM_REG LAM28;
   union CANLAM_REG LAM29;
   union CANLAM_REG LAM30;
   union CANLAM_REG LAM31;
};

/* Mailbox MOTS File */

struct MOTS_REGS {
   Uint32 MOTS0;
   Uint32 MOTS1;
   Uint32 MOTS2;
   Uint32 MOTS3;
   Uint32 MOTS4;
   Uint32 MOTS5;
   Uint32 MOTS6;
   Uint32 MOTS7;
   Uint32 MOTS8;
   Uint32 MOTS9;
   Uint32 MOTS10;
   Uint32 MOTS11;
   Uint32 MOTS12;
   Uint32 MOTS13;
   Uint32 MOTS14;
   Uint32 MOTS15;
   Uint32 MOTS16;
   Uint32 MOTS17;
   Uint32 MOTS18;
   Uint32 MOTS19;
   Uint32 MOTS20;
   Uint32 MOTS21;
   Uint32 MOTS22;
   Uint32 MOTS23;
   Uint32 MOTS24;
   Uint32 MOTS25;
   Uint32 MOTS26;
   Uint32 MOTS27;
   Uint32 MOTS28;
   Uint32 MOTS29;
   Uint32 MOTS30;
   Uint32 MOTS31;
};

/* Mailbox MOTO File */

struct MOTO_REGS {
   Uint32 MOTO0;
   Uint32 MOTO1;
   Uint32 MOTO2;
   Uint32 MOTO3;
   Uint32 MOTO4;
   Uint32 MOTO5;
   Uint32 MOTO6;
   Uint32 MOTO7;
   Uint32 MOTO8;
   Uint32 MOTO9;
   Uint32 MOTO10;
   Uint32 MOTO11;
   Uint32 MOTO12;
   Uint32 MOTO13;
   Uint32 MOTO14;
   Uint32 MOTO15;
   Uint32 MOTO16;
   Uint32 MOTO17;
   Uint32 MOTO18;
   Uint32 MOTO19;
   Uint32 MOTO20;
   Uint32 MOTO21;
   Uint32 MOTO22;
   Uint32 MOTO23;
   Uint32 MOTO24;
   Uint32 MOTO25;
   Uint32 MOTO26;
   Uint32 MOTO27;
   Uint32 MOTO28;
   Uint32 MOTO29;
   Uint32 MOTO30;
   Uint32 MOTO31;
};


//----------------------------------------------------
// Capture control register 1 bit definitions */                                    
struct ECCTL1_BITS {          // bits   description
   Uint16 CAP1POL:1;          // 0      Capture Event 1 Polarity select
   Uint16 CTRRST1:1;          // 1      Counter Reset on Capture Event 1
   Uint16 CAP2POL:1;          // 2      Capture Event 2 Polarity select
   Uint16 CTRRST2:1;          // 3      Counter Reset on Capture Event 2
   Uint16 CAP3POL:1;          // 4      Capture Event 3 Polarity select
   Uint16 CTRRST3:1;          // 5      Counter Reset on Capture Event 3
   Uint16 CAP4POL:1;          // 6      Capture Event 4 Polarity select
   Uint16 CTRRST4:1;          // 7      Counter Reset on Capture Event 4
   Uint16 CAPLDEN:1;          // 8      Enable Loading CAP1-4 regs on a Cap Event
   Uint16 PRESCALE:5;         // 13:9   Event Filter prescale select
   Uint16 FREE_SOFT:2;        // 15:14  Emulation mode
};

union ECCTL1_REG {
   Uint16              all;
   struct ECCTL1_BITS  bit;
};


// In V1.1 the STOPVALUE bit field was changed to 
// STOP_WRAP.  This correlated to a silicon change from
// F2833x Rev 0 to Rev A. 
//----------------------------------------------------
// Capture control register 2 bit definitions */                                    
struct ECCTL2_BITS {          // bits   description
   Uint16 CONT_ONESHT:1;      // 0      Continuous or one-shot
   Uint16 STOP_WRAP:2;        // 2:1    Stop value for one-shot, Wrap for continuous
   Uint16 REARM:1;            // 3      One-shot re-arm
   Uint16 TSCTRSTOP:1;        // 4      TSCNT counter stop
   Uint16 SYNCI_EN:1;         // 5      Counter sync-in select
   Uint16 SYNCO_SEL:2;        // 7:6    Sync-out mode
   Uint16 SWSYNC:1;           // 8      SW forced counter sync
   Uint16 CAP_APWM:1;         // 9      CAP/APWM operating mode select
   Uint16 APWMPOL:1;          // 10     APWM output polarity select
   Uint16 rsvd1:5;            // 15:11  
};


union ECCTL2_REG {
   Uint16              all;
   struct ECCTL2_BITS  bit;
};


//----------------------------------------------------
// ECAP interrupt enable register bit definitions */                                    
struct ECEINT_BITS {          // bits   description
   Uint16 rsvd1:1;            // 0      reserved
   Uint16 CEVT1:1;            // 1      Capture Event 1 Interrupt Enable
   Uint16 CEVT2:1;            // 2      Capture Event 2 Interrupt Enable
   Uint16 CEVT3:1;            // 3      Capture Event 3 Interrupt Enable
   Uint16 CEVT4:1;            // 4      Capture Event 4 Interrupt Enable         
   Uint16 CTROVF:1;           // 5      Counter Overflow Interrupt Enable
   Uint16 CTR_EQ_PRD:1;       // 6      Period Equal Interrupt Enable
   Uint16 CTR_EQ_CMP:1;       // 7      Compare Equal Interrupt Enable
   Uint16 rsvd2:8;            // 15:8   reserved
};


union ECEINT_REG {
   Uint16              all;
   struct ECEINT_BITS  bit;
};

//----------------------------------------------------
// ECAP interrupt flag register bit definitions */                                    
struct ECFLG_BITS {           // bits   description
   Uint16 INT:1;              // 0      Global Flag
   Uint16 CEVT1:1;            // 1      Capture Event 1 Interrupt Flag
   Uint16 CEVT2:1;            // 2      Capture Event 2 Interrupt Flag
   Uint16 CEVT3:1;            // 3      Capture Event 3 Interrupt Flag
   Uint16 CEVT4:1;            // 4      Capture Event 4 Interrupt Flag         
   Uint16 CTROVF:1;           // 5      Counter Overflow Interrupt Flag
   Uint16 CTR_EQ_PRD:1;       // 6      Period Equal Interrupt Flag
   Uint16 CTR_EQ_CMP:1;       // 7      Compare Equal Interrupt Flag
   Uint16 rsvd2:8;            // 15:8   reserved
};


union ECFLG_REG {
   Uint16              all;
   struct ECFLG_BITS   bit;
};


//----------------------------------------------------

struct ECAP_REGS {
   Uint32              TSCTR;    // Time stamp counter 
   Uint32              CTRPHS;   // Counter phase
   Uint32              CAP1;     // Capture 1 
   Uint32              CAP2;     // Capture 2    
   Uint32              CAP3;     // Capture 3 
   Uint32              CAP4;     // Capture 4   
   Uint16              rsvd1[8]; // reserved
   union   ECCTL1_REG  ECCTL1;   // Capture Control Reg 1
   union   ECCTL2_REG  ECCTL2;   // Capture Control Reg 2
   union   ECEINT_REG  ECEINT;   // ECAP interrupt enable
   union   ECFLG_REG   ECFLG;    // ECAP interrupt flags
   union   ECFLG_REG   ECCLR;    // ECAP interrupt clear
   union   ECEINT_REG  ECFRC;    // ECAP interrupt force
   Uint16              rsvd2[6]; // reserved   
};

    
 
//----------------------------------------------------
// Time base control register bit definitions */                                    
struct TBCTL_BITS {          // bits   description
   Uint16 CTRMODE:2;         // 1:0    Counter Mode
   Uint16 PHSEN:1;           // 2      Phase load enable
   Uint16 PRDLD:1;           // 3      Active period load
   Uint16 SYNCOSEL:2;        // 5:4    Sync output select
   Uint16 SWFSYNC:1;         // 6      Software force sync pulse
   Uint16 HSPCLKDIV:3;       // 9:7    High speed time pre-scale
   Uint16 CLKDIV:3;          // 12:10  Timebase clock pre-scale
   Uint16 PHSDIR:1;          // 13     Phase Direction
   Uint16 FREE_SOFT:2;       // 15:14  Emulation mode 
};

union TBCTL_REG {
   Uint16              all;
   struct TBCTL_BITS   bit;
};

//----------------------------------------------------
// Time base status register bit definitions */                                    
struct TBSTS_BITS {          // bits   description
   Uint16 CTRDIR:1;          // 0      Counter direction status
   Uint16 SYNCI:1;           // 1      External input sync status
   Uint16 CTRMAX:1;          // 2      Counter max latched status
   Uint16 rsvd1:13;          // 15:3   reserved
};

union TBSTS_REG {
   Uint16              all;
   struct TBSTS_BITS   bit;
};

//----------------------------------------------------
// Compare control register bit definitions */                                    
struct CMPCTL_BITS {          // bits   description
   Uint16 LOADAMODE:2;        // 0:1    Active compare A
   Uint16 LOADBMODE:2;        // 3:2    Active compare B
   Uint16 SHDWAMODE:1;        // 4      Compare A block operating mode
   Uint16 rsvd1:1;            // 5      reserved
   Uint16 SHDWBMODE:1;        // 6      Compare B block operating mode
   Uint16 rsvd2:1;            // 7      reserved
   Uint16 SHDWAFULL:1;        // 8      Compare A Shadow registers full Status
   Uint16 SHDWBFULL:1;        // 9      Compare B Shadow registers full Status
   Uint16 rsvd3:6;            // 15:10  reserved
};


union CMPCTL_REG {
   Uint16                all;
   struct CMPCTL_BITS    bit;
};

//----------------------------------------------------
// Action qualifier register bit definitions */                                    
struct AQCTL_BITS {           // bits   description
   Uint16 ZRO:2;              // 1:0    Action Counter = Zero
   Uint16 PRD:2;              // 3:2    Action Counter = Period
   Uint16 CAU:2;              // 5:4    Action Counter = Compare A up
   Uint16 CAD:2;              // 7:6    Action Counter = Compare A down
   Uint16 CBU:2;              // 9:8    Action Counter = Compare B up
   Uint16 CBD:2;              // 11:10  Action Counter = Compare B down
   Uint16 rsvd:4;             // 15:12  reserved
};

union AQCTL_REG {
   Uint16                all;
   struct AQCTL_BITS     bit;
};

//----------------------------------------------------
// Action qualifier SW force register bit definitions */                                    
struct AQSFRC_BITS {           // bits   description
   Uint16 ACTSFA:2;            // 1:0    Action when One-time SW Force A invoked
   Uint16 OTSFA:1;             // 2      One-time SW Force A output
   Uint16 ACTSFB:2;            // 4:3    Action when One-time SW Force B invoked
   Uint16 OTSFB:1;             // 5      One-time SW Force A output
   Uint16 RLDCSF:2;            // 7:6    Reload from Shadow options
   Uint16 rsvd1:8;             // 15:8   reserved
};

union AQSFRC_REG {
   Uint16                 all;
   struct AQSFRC_BITS     bit;
};

//----------------------------------------------------
// Action qualifier continuous SW force register bit definitions */                                    
struct AQCSFRC_BITS {          // bits   description
   Uint16 CSFA:2;              // 1:0    Continuous Software Force on output A
   Uint16 CSFB:2;              // 3:2    Continuous Software Force on output B
   Uint16 rsvd1:12;            // 15:4   reserved
};

union AQCSFRC_REG {
   Uint16                  all;
   struct AQCSFRC_BITS     bit;
};


// As of version 1.1
// Changed the MODE bit-field to OUT_MODE
// Added the bit-field IN_MODE
// This corresponds to changes in silicon as of F2833x devices
// Rev A silicon.
//----------------------------------------------------
// Dead-band generator control register bit definitions                                    
struct DBCTL_BITS {          // bits   description
   Uint16 OUT_MODE:2;      	 // 1:0    Dead Band Output Mode Control 
   Uint16 POLSEL:2;          // 3:2    Polarity Select Control 
   Uint16 IN_MODE:2;         // 5:4    Dead Band Input Select Mode Control
   Uint16 rsvd1:10;          // 15:4   reserved
};

union DBCTL_REG {
   Uint16                  all;
   struct DBCTL_BITS       bit;
};


//----------------------------------------------------
// Trip zone select register bit definitions                                   
struct TZSEL_BITS {           // bits   description
   Uint16  CBC1:1;            // 0      TZ1 CBC select
   Uint16  CBC2:1;            // 1      TZ2 CBC select
   Uint16  CBC3:1;            // 2      TZ3 CBC select
   Uint16  CBC4:1;            // 3      TZ4 CBC select
   Uint16  CBC5:1;            // 4      TZ5 CBC select
   Uint16  CBC6:1;            // 5      TZ6 CBC select
   Uint16  rsvd1:2;           // 7:6    reserved
   Uint16  OSHT1:1;           // 8      One-shot TZ1 select             
   Uint16  OSHT2:1;           // 9      One-shot TZ2 select             
   Uint16  OSHT3:1;           // 10     One-shot TZ3 select             
   Uint16  OSHT4:1;           // 11     One-shot TZ4 select             
   Uint16  OSHT5:1;           // 12     One-shot TZ5 select             
   Uint16  OSHT6:1;           // 13     One-shot TZ6 select             
   Uint16  rsvd2:2;           // 15:14  reserved
};

union TZSEL_REG {
   Uint16                  all;
   struct TZSEL_BITS       bit;
};


//----------------------------------------------------
// Trip zone control register bit definitions */                                    
struct TZCTL_BITS {         // bits   description
   Uint16 TZA:2;            // 1:0    TZ1 to TZ6 Trip Action On EPWMxA
   Uint16 TZB:2;            // 3:2    TZ1 to TZ6 Trip Action On EPWMxB
   Uint16 rsvd:12;          // 15:4   reserved
};

union TZCTL_REG {
   Uint16                  all;
   struct TZCTL_BITS       bit;
};


//----------------------------------------------------
// Trip zone control register bit definitions */                                    
struct TZEINT_BITS {         // bits   description
   Uint16  rsvd1:1;          // 0      reserved
   Uint16  CBC:1;            // 1      Trip Zones Cycle By Cycle Int Enable
   Uint16  OST:1;            // 2      Trip Zones One Shot Int Enable
   Uint16  rsvd2:13;         // 15:3   reserved
};   


union TZEINT_REG {
   Uint16                  all;
   struct TZEINT_BITS      bit;
};


//----------------------------------------------------
// Trip zone flag register bit definitions */                                    
struct TZFLG_BITS {         // bits   description
   Uint16  INT:1;           // 0      Global status
   Uint16  CBC:1;           // 1      Trip Zones Cycle By Cycle Int
   Uint16  OST:1;           // 2      Trip Zones One Shot Int
   Uint16  rsvd2:13;        // 15:3   reserved
};

union TZFLG_REG {
   Uint16                  all;
   struct TZFLG_BITS       bit;
};

//----------------------------------------------------
// Trip zone flag clear register bit definitions */                                    
struct TZCLR_BITS {         // bits   description
   Uint16  INT:1;           // 0      Global status
   Uint16  CBC:1;           // 1      Trip Zones Cycle By Cycle Int
   Uint16  OST:1;           // 2      Trip Zones One Shot Int
   Uint16  rsvd2:13;        // 15:3   reserved
};

union TZCLR_REG {
   Uint16                  all;
   struct TZCLR_BITS       bit;
};

//----------------------------------------------------
// Trip zone flag force register bit definitions */                                    
struct TZFRC_BITS {         // bits   description
   Uint16  rsvd1:1;         // 0      reserved
   Uint16  CBC:1;           // 1      Trip Zones Cycle By Cycle Int
   Uint16  OST:1;           // 2      Trip Zones One Shot Int
   Uint16  rsvd2:13;        // 15:3   reserved
};

union TZFRC_REG {
   Uint16                  all;
   struct TZFRC_BITS       bit;
};

//----------------------------------------------------
// Event trigger select register bit definitions */                                    
struct ETSEL_BITS {         // bits   description
   Uint16  INTSEL:3;        // 2:0    EPWMxINTn Select
   Uint16  INTEN:1;         // 3      EPWMxINTn Enable
   Uint16  rsvd1:4;         // 7:4    reserved
   Uint16  SOCASEL:3;       // 10:8   Start of conversion A Select
   Uint16  SOCAEN:1;        // 11     Start of conversion A Enable
   Uint16  SOCBSEL:3;       // 14:12  Start of conversion B Select
   Uint16  SOCBEN:1;        // 15     Start of conversion B Enable
};

union ETSEL_REG {
   Uint16                  all;
   struct ETSEL_BITS       bit;
};


//----------------------------------------------------
// Event trigger pre-scale register bit definitions */                                    
struct ETPS_BITS {         // bits   description
   Uint16  INTPRD:2;       // 1:0    EPWMxINTn Period Select
   Uint16  INTCNT:2;       // 3:2    EPWMxINTn Counter Register
   Uint16  rsvd1:4;        // 7:4    reserved
   Uint16  SOCAPRD:2;      // 9:8    EPWMxSOCA Period Select
   Uint16  SOCACNT:2;      // 11:10  EPWMxSOCA Counter Register
   Uint16  SOCBPRD:2;      // 13:12  EPWMxSOCB Period Select
   Uint16  SOCBCNT:2;      // 15:14  EPWMxSOCB Counter Register
};

union ETPS_REG {
   Uint16                  all;
   struct ETPS_BITS        bit;
};

//----------------------------------------------------
// Event trigger Flag register bit definitions */                                    
struct ETFLG_BITS {         // bits   description
   Uint16  INT:1;           // 0	EPWMxINTn Flag
   Uint16  rsvd1:1;         // 1	reserved
   Uint16  SOCA:1;          // 2	EPWMxSOCA Flag
   Uint16  SOCB:1;          // 3	EPWMxSOCB Flag
   Uint16  rsvd2:12;        // 15:4	reserved
};

union ETFLG_REG {
   Uint16                   all;
   struct ETFLG_BITS        bit;
};


//----------------------------------------------------
// Event trigger Clear register bit definitions */                                    
struct ETCLR_BITS {         // bits   description
   Uint16  INT:1;           // 0	EPWMxINTn Clear
   Uint16  rsvd1:1;         // 1	reserved
   Uint16  SOCA:1;          // 2	EPWMxSOCA Clear
   Uint16  SOCB:1;          // 3	EPWMxSOCB Clear
   Uint16  rsvd2:12;        // 15:4	reserved
};

union ETCLR_REG {
   Uint16                   all;
   struct ETCLR_BITS        bit;
};

//----------------------------------------------------
// Event trigger Force register bit definitions */                                    
struct ETFRC_BITS {         // bits   description
   Uint16  INT:1;           // 0	EPWMxINTn Force
   Uint16  rsvd1:1;         // 1	reserved
   Uint16  SOCA:1;          // 2	EPWMxSOCA Force
   Uint16  SOCB:1;          // 3	EPWMxSOCB Force
   Uint16  rsvd2:12;        // 15:4	reserved
};

union ETFRC_REG {
   Uint16                  all;
   struct ETFRC_BITS        bit;
};
//----------------------------------------------------
// PWM chopper control register bit definitions */                                    
struct PCCTL_BITS {         // bits   description
   Uint16  CHPEN:1;         // 0      PWM chopping enable
   Uint16  OSHTWTH:4;       // 4:1    One-shot pulse width
   Uint16  CHPFREQ:3;       // 7:5    Chopping clock frequency
   Uint16  CHPDUTY:3;       // 10:8   Chopping clock Duty cycle
   Uint16  rsvd1:5;         // 15:11  reserved
};


union PCCTL_REG {
   Uint16                  all;
   struct PCCTL_BITS       bit;
};

struct HRCNFG_BITS {       	// bits   description
   Uint16  EDGMODE:2;     	// 1:0    Edge Mode select Bits
   Uint16  CTLMODE:1;     	// 2      Control mode Select Bit
   Uint16  HRLOAD:1;      	// 3      Shadow mode Select Bit
   Uint16  rsvd1:12;      	// 15:4   reserved
};

union HRCNFG_REG {
   Uint16                  	all;
   struct HRCNFG_BITS       bit;
};


struct TBPHS_HRPWM_REG {   	// bits   description
   Uint16  TBPHSHR;     	// 15:0   Extension register for HRPWM Phase (8 bits)
   Uint16  TBPHS;           // 31:16  Phase offset register
};

union TBPHS_HRPWM_GROUP {
   Uint32                  all;
   struct TBPHS_HRPWM_REG  half;
};

struct CMPA_HRPWM_REG {   	// bits   description
   Uint16  CMPAHR;     	    // 15:0   Extension register for HRPWM compare (8 bits)
   Uint16  CMPA;            // 31:16  Compare A reg
};

union CMPA_HRPWM_GROUP {
   Uint32                 all;
   struct CMPA_HRPWM_REG  half;
};


struct EPWM_REGS {
   union  TBCTL_REG           TBCTL;   // 
   union  TBSTS_REG           TBSTS;   // 
   union  TBPHS_HRPWM_GROUP   TBPHS;   // Union of TBPHS:TBPHSHR
   Uint16                     TBCTR;   // Counter
   Uint16                     TBPRD;   // Period register set 
   Uint16                     rsvd1;   // 
   union  CMPCTL_REG          CMPCTL;  // Compare control
   union  CMPA_HRPWM_GROUP    CMPA;    // Union of CMPA:CMPAHR
   Uint16                     CMPB;    // Compare B reg
   union  AQCTL_REG           AQCTLA;  // Action qual output A
   union  AQCTL_REG           AQCTLB;  // Action qual output B
   union  AQSFRC_REG          AQSFRC;  // Action qual SW force
   union  AQCSFRC_REG         AQCSFRC; // Action qualifier continuous SW force 
   union  DBCTL_REG           DBCTL;   // Dead-band control
   Uint16                     DBRED;   // Dead-band rising edge delay
   Uint16                     DBFED;   // Dead-band falling edge delay
   union  TZSEL_REG           TZSEL;   // Trip zone select
   Uint16                     rsvd2;   
   union  TZCTL_REG           TZCTL;   // Trip zone control
   union  TZEINT_REG          TZEINT;  // Trip zone interrupt enable
   union  TZFLG_REG           TZFLG;   // Trip zone interrupt flags
   union  TZCLR_REG           TZCLR;   // Trip zone clear   
   union  TZFRC_REG    	      TZFRC;   // Trip zone force interrupt
   union  ETSEL_REG           ETSEL;   // Event trigger selection
   union  ETPS_REG            ETPS;    // Event trigger pre-scaler
   union  ETFLG_REG           ETFLG;   // Event trigger flags
   union  ETCLR_REG           ETCLR;   // Event trigger clear   
   union  ETFRC_REG           ETFRC;   // Event trigger force
   union  PCCTL_REG           PCCTL;   // PWM chopper control
   Uint16                     rsvd3;   // 
   union  HRCNFG_REG          HRCNFG;  // HRPWM Config Reg
};
    
 

//----------------------------------------------------
// Capture decoder control register bit definitions */                                    
struct QDECCTL_BITS {         // bits   description
   Uint16 rsvd1:5;            // 4:0    reserved
   Uint16 QSP:1;              // 5      QEPS input polarity
   Uint16 QIP:1;              // 6      QEPI input polarity
   Uint16 QBP:1;              // 7      QEPB input polarity
   Uint16 QAP:1;              // 8      QEPA input polarity
   Uint16 IGATE:1;            // 9      Index pulse gating option
   Uint16 SWAP:1;             // 10     CLK/DIR signal source for Position Counter
   Uint16 XCR:1;              // 11     External clock rate
   Uint16 SPSEL:1;            // 12     Sync output pin select
   Uint16 SOEN:1;             // 13     Enable position compare sync
   Uint16 QSRC:2;             // 15:14  Position counter source
};

union QDECCTL_REG {
   Uint16              all;
   struct QDECCTL_BITS   bit;
};


//----------------------------------------------------
// QEP control register bit definitions */                                    
struct QEPCTL_BITS {           // bits   description
   Uint16 WDE:1;               // 0      QEP watchdog enable
   Uint16 UTE:1;               // 1      QEP unit timer enable
   Uint16 QCLM:1;              // 2      QEP capture latch mode
   Uint16 QPEN:1;              // 3      Quadrature position counter enable
   Uint16 IEL:2;               // 5:4    Index event latch
   Uint16 SEL:1;               // 6      Strobe event latch
   Uint16 SWI:1;               // 7      Software init position counter
   Uint16 IEI:2;               // 9:8    Index event init of position count
   Uint16 SEI:2;               // 11:10  Strobe event init
   Uint16 PCRM:2;              // 13:12  Position counter reset
   Uint16 FREE_SOFT:2;         // 15:14  Emulation mode
};

union QEPCTL_REG {
   Uint16               all;
   struct QEPCTL_BITS   bit;
};         


//----------------------------------------------------
// Quadrature capture control register bit definitions */                                    
struct QCAPCTL_BITS {          // bits   description
   Uint16 UPPS:4;              // 3:0    Unit position pre-scale         
   Uint16 CCPS:3;              // 6:4    QEP capture timer pre-scale
   Uint16 rsvd1:8;             // 14:7   reserved
   Uint16 CEN:1;               // 15     Enable QEP capture
};


union QCAPCTL_REG {
   Uint16               all;
   struct QCAPCTL_BITS  bit;
}; 



//----------------------------------------------------
// Position compare control register bit definitions */                                    
struct QPOSCTL_BITS {          // bits   description
   Uint16 PCSPW:12;            // 11:0   Position compare sync pulse width
   Uint16 PCE:1;               // 12     Position compare enable/disable
   Uint16 PCPOL:1;             // 13     Polarity of sync output
   Uint16 PCLOAD:1;            // 14     Position compare of shadow load
   Uint16 PCSHDW:1;            // 15     Position compare shadow enable
};

union QPOSCTL_REG {
   Uint16               all;
   struct QPOSCTL_BITS  bit;
};         

//----------------------------------------------------
// QEP interrupt control register bit definitions */                                    
struct QEINT_BITS {          // bits   description
   Uint16 rsvd1:1;           // 0      reserved
   Uint16 PCE:1;             // 1      Position counter error
   Uint16 QPE:1;             // 2      Quadrature phase error
   Uint16 QDC:1;             // 3      Quadrature dir change
   Uint16 WTO:1;             // 4      Watchdog timeout
   Uint16 PCU:1;             // 5      Position counter underflow
   Uint16 PCO:1;             // 6      Position counter overflow
   Uint16 PCR:1;             // 7      Position compare ready
   Uint16 PCM:1;             // 8      Position compare match
   Uint16 SEL:1;             // 9      Strobe event latch
   Uint16 IEL:1;             // 10     Event latch
   Uint16 UTO:1;             // 11     Unit timeout
   Uint16 rsvd2:4;           // 15:12  reserved
};


union QEINT_REG {
   Uint16               all;
   struct QEINT_BITS    bit;
};


//----------------------------------------------------
// QEP interrupt status register bit definitions */                                    
struct QFLG_BITS {          // bits   description
   Uint16 INT:1;            // 0      Global interrupt 
   Uint16 PCE:1;            // 1      Position counter error
   Uint16 PHE:1;            // 2      Quadrature phase error
   Uint16 QDC:1;            // 3      Quadrature dir change
   Uint16 WTO:1;            // 4      Watchdog timeout
   Uint16 PCU:1;            // 5      Position counter underflow
   Uint16 PCO:1;            // 6      Position counter overflow
   Uint16 PCR:1;            // 7      Position compare ready
   Uint16 PCM:1;            // 8      Position compare match
   Uint16 SEL:1;            // 9      Strobe event latch
   Uint16 IEL:1;            // 10     Event latch
   Uint16 UTO:1;            // 11     Unit timeout
   Uint16 rsvd2:4;          // 15:12  reserved
};


union QFLG_REG {
   Uint16               all;
   struct QFLG_BITS     bit;
};

//----------------------------------------------------
// QEP interrupt force register bit definitions */                                    
struct QFRC_BITS {          // bits   description
   Uint16 reserved:1;       // 0      Reserved
   Uint16 PCE:1;            // 1      Position counter error
   Uint16 PHE:1;            // 2      Quadrature phase error
   Uint16 QDC:1;            // 3      Quadrature dir change
   Uint16 WTO:1;            // 4      Watchdog timeout
   Uint16 PCU:1;            // 5      Position counter underflow
   Uint16 PCO:1;            // 6      Position counter overflow
   Uint16 PCR:1;            // 7      Position compare ready
   Uint16 PCM:1;            // 8      Position compare match
   Uint16 SEL:1;            // 9      Strobe event latch
   Uint16 IEL:1;            // 10     Event latch
   Uint16 UTO:1;            // 11     Unit timeout
   Uint16 rsvd2:4;          // 15:12  reserved
};


union QFRC_REG {
   Uint16               all;
   struct QFRC_BITS     bit;
};

// V1.1 Added UPEVNT (bit 7) This reflects changes 
// made as of F2833x Rev A devices
//----------------------------------------------------
// QEP status register bit definitions */                                    
struct QEPSTS_BITS {          // bits   description
   Uint16 PCEF:1;             // 0      Position counter error
   Uint16 FIMF:1;             // 1      First index marker
   Uint16 CDEF:1;             // 2      Capture direction error
   Uint16 COEF:1;             // 3      Capture overflow error
   Uint16 QDLF:1;             // 4      QEP direction latch
   Uint16 QDF:1;              // 5      Quadrature direction
   Uint16 FIDF:1;             // 6      Direction on first index marker
   Uint16 UPEVNT:1;           // 7      Unit position event flag
   Uint16 rsvd1:8;            // 15:8   reserved
};

union QEPSTS_REG {
   Uint16               all;
   struct QEPSTS_BITS   bit;
};

//----------------------------------------------------

struct EQEP_REGS {
   Uint32              QPOSCNT;   // Position counter 
   Uint32              QPOSINIT;  // Position counter init
   Uint32              QPOSMAX;   // Maximum position count
   Uint32              QPOSCMP;   // Position compare
   Uint32              QPOSILAT;  // Index position latch
   Uint32              QPOSSLAT;  // Strobe position latch
   Uint32              QPOSLAT;   // Position latch
   Uint32              QUTMR;     // Unit timer
   Uint32              QUPRD;     // Unit period
   Uint16              QWDTMR;    // QEP watchdog timer
   Uint16              QWDPRD;    // QEP watchdog period
   union  QDECCTL_REG  QDECCTL;   // Quadrature decoder control
   union  QEPCTL_REG   QEPCTL;    // QEP control 
   union  QCAPCTL_REG  QCAPCTL;   // Quadrature capture control     
   union  QPOSCTL_REG  QPOSCTL;   // Position compare control
   union  QEINT_REG    QEINT;     // QEP interrupt control        
   union  QFLG_REG     QFLG;      // QEP interrupt flag
   union  QFLG_REG     QCLR;      // QEP interrupt clear                   
   union  QFRC_REG     QFRC;      // QEP interrupt force                   
   union  QEPSTS_REG   QEPSTS;    // QEP status
   Uint16              QCTMR;     // QEP capture timer
   Uint16              QCPRD;     // QEP capture period
   Uint16              QCTMRLAT;  // QEP capture latch
   Uint16              QCPRDLAT;  // QEP capture period latch
   Uint16              rsvd1[30]; // reserved
};

    
 
//----------------------------------------------------
// GPIO A control register bit definitions */
struct GPACTRL_BITS {        // bits   description
   Uint16 QUALPRD0:8;        // 7:0    Qual period
   Uint16 QUALPRD1:8;        // 15:8   Qual period
   Uint16 QUALPRD2:8;        // 23:16  Qual period
   Uint16 QUALPRD3:8;        // 31:24  Qual period
};

union GPACTRL_REG {
   Uint32              all;
   struct GPACTRL_BITS bit;
};

//----------------------------------------------------
// GPIO B control register bit definitions */
struct GPBCTRL_BITS {        // bits   description
   Uint16 QUALPRD0:8;        // 7:0    Qual period
   Uint16 QUALPRD1:8;        // 15:8   Qual period
   Uint16 QUALPRD2:8;        // 23:16  Qual period
   Uint16 QUALPRD3:8;        // 31:24
};

union GPBCTRL_REG {
   Uint32              all;
   struct GPBCTRL_BITS bit;
};

//----------------------------------------------------
// GPIO A Qual/MUX select register bit definitions */
struct GPA1_BITS {            // bits   description
   Uint16 GPIO0:2;            // 1:0    GPIO0
   Uint16 GPIO1:2;            // 3:2    GPIO1
   Uint16 GPIO2:2;            // 5:4    GPIO2
   Uint16 GPIO3:2;            // 7:6    GPIO3
   Uint16 GPIO4:2;            // 9:8    GPIO4
   Uint16 GPIO5:2;            // 11:10  GPIO5
   Uint16 GPIO6:2;            // 13:12  GPIO6
   Uint16 GPIO7:2;            // 15:14  GPIO7
   Uint16 GPIO8:2;            // 17:16  GPIO8
   Uint16 GPIO9:2;            // 19:18  GPIO9
   Uint16 GPIO10:2;           // 21:20  GPIO10
   Uint16 GPIO11:2;           // 23:22  GPIO11
   Uint16 GPIO12:2;           // 25:24  GPIO12
   Uint16 GPIO13:2;           // 27:26  GPIO13
   Uint16 GPIO14:2;           // 29:28  GPIO14
   Uint16 GPIO15:2;           // 31:30  GPIO15
};


struct GPA2_BITS {            // bits   description
   Uint16 GPIO16:2;           // 1:0    GPIO16
   Uint16 GPIO17:2;           // 3:2    GPIO17
   Uint16 GPIO18:2;           // 5:4    GPIO18
   Uint16 GPIO19:2;           // 7:6    GPIO19
   Uint16 GPIO20:2;           // 9:8    GPIO20
   Uint16 GPIO21:2;           // 11:10  GPIO21
   Uint16 GPIO22:2;           // 13:12  GPIO22
   Uint16 GPIO23:2;           // 15:14  GPIO23
   Uint16 GPIO24:2;           // 17:16  GPIO24
   Uint16 GPIO25:2;           // 19:18  GPIO25
   Uint16 GPIO26:2;           // 21:20  GPIO26
   Uint16 GPIO27:2;           // 23:22  GPIO27
   Uint16 GPIO28:2;           // 25:24  GPIO28
   Uint16 GPIO29:2;           // 27:26  GPIO29
   Uint16 GPIO30:2;           // 29:28  GPIO30
   Uint16 GPIO31:2;           // 31:30  GPIO31
};

struct GPB1_BITS {            // bits   description
   Uint16 GPIO32:2;           // 1:0    GPIO32
   Uint16 GPIO33:2;           // 3:2    GPIO33
   Uint16 GPIO34:2;           // 5:4    GPIO34
   Uint16 GPIO35:2;           // 7:6    GPIO35
   Uint16 GPIO36:2;           // 9:8    GPIO36
   Uint16 GPIO37:2;           // 11:10  GPIO37
   Uint16 GPIO38:2;           // 13:12  GPIO38
   Uint16 GPIO39:2;           // 15:14  GPIO39
   Uint16 GPIO40:2;           // 17:16  GPIO40
   Uint16 GPIO41:2;           // 19:16  GPIO41
   Uint16 GPIO42:2;           // 21:20  GPIO42
   Uint16 GPIO43:2;           // 23:22  GPIO43
   Uint16 GPIO44:2;           // 25:24  GPIO44
   Uint16 GPIO45:2;           // 27:26  GPIO45
   Uint16 GPIO46:2;           // 29:28  GPIO46
   Uint16 GPIO47:2;           // 31:30  GPIO47
};

struct GPB2_BITS  {           // bits   description
   Uint16 GPIO48:2;           // 1:0    GPIO48
   Uint16 GPIO49:2;           // 3:2    GPIO49
   Uint16 GPIO50:2;           // 5:4    GPIO50
   Uint16 GPIO51:2;           // 7:6    GPIO51
   Uint16 GPIO52:2;           // 9:8    GPIO52
   Uint16 GPIO53:2;           // 11:10  GPIO53
   Uint16 GPIO54:2;           // 13:12  GPIO54
   Uint16 GPIO55:2;           // 15:14  GPIO55
   Uint16 GPIO56:2;           // 17:16  GPIO56
   Uint16 GPIO57:2;           // 19:18  GPIO57
   Uint16 GPIO58:2;           // 21:20  GPIO58
   Uint16 GPIO59:2;           // 23:22  GPIO59
   Uint16 GPIO60:2;           // 25:24  GPIO60
   Uint16 GPIO61:2;           // 27:26  GPIO61
   Uint16 GPIO62:2;           // 29:28  GPIO62
   Uint16 GPIO63:2;           // 31:30  GPIO63
};

struct GPC1_BITS {            // bits   description
   Uint16 GPIO64:2;           // 1:0    GPIO64
   Uint16 GPIO65:2;           // 3:2    GPIO65
   Uint16 GPIO66:2;           // 5:4    GPIO66
   Uint16 GPIO67:2;           // 7:6    GPIO67
   Uint16 GPIO68:2;           // 9:8    GPIO68
   Uint16 GPIO69:2;           // 11:10  GPIO69
   Uint16 GPIO70:2;           // 13:12  GPIO70
   Uint16 GPIO71:2;           // 15:14  GPIO71
   Uint16 GPIO72:2;           // 17:16  GPIO72
   Uint16 GPIO73:2;           // 19:18  GPIO73
   Uint16 GPIO74:2;           // 21:20  GPIO74
   Uint16 GPIO75:2;           // 23:22  GPIO75
   Uint16 GPIO76:2;           // 25:24  GPIO76
   Uint16 GPIO77:2;           // 27:26  GPIO77
   Uint16 GPIO78:2;           // 29:28  GPIO78
   Uint16 GPIO79:2;           // 31:30  GPIO79
};


struct GPC2_BITS {            // bits   description
   Uint16 GPIO80:2;           // 1:0    GPIO80
   Uint16 GPIO81:2;           // 3:2    GPIO81
   Uint16 GPIO82:2;           // 5:4    GPIO82
   Uint16 GPIO83:2;           // 7:6    GPIO83
   Uint16 GPIO84:2;           // 9:8    GPIO84
   Uint16 GPIO85:2;           // 11:10  GPIO85
   Uint16 GPIO86:2;           // 13:12  GPIO86
   Uint16 GPIO87:2;           // 15:14  GPIO87
   Uint16 rsvd:16;            // 31:16  reserved
};


union GPA1_REG {
   Uint32              all;
   struct GPA1_BITS    bit;
};

union GPA2_REG {
   Uint32              all;
   struct GPA2_BITS    bit;
};

union GPB1_REG {
   Uint32              all;
   struct GPB1_BITS    bit;
};

union GPB2_REG {
   Uint32              all;
   struct GPB2_BITS    bit;
};

union GPC1_REG {
   Uint32              all;
   struct GPC1_BITS    bit;
};

union GPC2_REG {
   Uint32              all;
   struct GPC2_BITS    bit;
};

//----------------------------------------------------
// GPIO A DIR/TOGGLE/SET/CLEAR register bit definitions */
struct GPADAT_BITS {          // bits   description
   Uint16 GPIO0:1;            // 0      GPIO0
   Uint16 GPIO1:1;            // 1      GPIO1
   Uint16 GPIO2:1;            // 2      GPIO2
   Uint16 GPIO3:1;            // 3      GPIO3
   Uint16 GPIO4:1;            // 4      GPIO4
   Uint16 GPIO5:1;            // 5      GPIO5
   Uint16 GPIO6:1;            // 6      GPIO6
   Uint16 GPIO7:1;            // 7      GPIO7
   Uint16 GPIO8:1;            // 8      GPIO8
   Uint16 GPIO9:1;            // 9      GPIO9
   Uint16 GPIO10:1;           // 10     GPIO10
   Uint16 GPIO11:1;           // 11     GPIO11
   Uint16 GPIO12:1;           // 12     GPIO12
   Uint16 GPIO13:1;           // 13     GPIO13
   Uint16 GPIO14:1;           // 14     GPIO14
   Uint16 GPIO15:1;           // 15     GPIO15
   Uint16 GPIO16:1;           // 16     GPIO16
   Uint16 GPIO17:1;           // 17     GPIO17
   Uint16 GPIO18:1;           // 18     GPIO18
   Uint16 GPIO19:1;           // 19     GPIO19
   Uint16 GPIO20:1;           // 20     GPIO20
   Uint16 GPIO21:1;           // 21     GPIO21
   Uint16 GPIO22:1;           // 22     GPIO22
   Uint16 GPIO23:1;           // 23     GPIO23
   Uint16 GPIO24:1;           // 24     GPIO24
   Uint16 GPIO25:1;           // 25     GPIO25
   Uint16 GPIO26:1;           // 26     GPIO26
   Uint16 GPIO27:1;           // 27     GPIO27
   Uint16 GPIO28:1;           // 28     GPIO28
   Uint16 GPIO29:1;           // 29     GPIO29
   Uint16 GPIO30:1;           // 30     GPIO30
   Uint16 GPIO31:1;           // 31     GPIO31
};

struct GPBDAT_BITS {          // bits   description
   Uint16 GPIO32:1;           // 0      GPIO32
   Uint16 GPIO33:1;           // 1      GPIO33
   Uint16 GPIO34:1;           // 2      GPIO34
   Uint16 GPIO35:1;           // 3      GPIO35
   Uint16 GPIO36:1;           // 4      GPIO36
   Uint16 GPIO37:1;           // 5      GPIO37
   Uint16 GPIO38:1;           // 6      GPIO38
   Uint16 GPIO39:1;           // 7      GPIO39
   Uint16 GPIO40:1;           // 8      GPIO40
   Uint16 GPIO41:1;           // 9      GPIO41
   Uint16 GPIO42:1;           // 10     GPIO42
   Uint16 GPIO43:1;           // 11     GPIO43
   Uint16 GPIO44:1;           // 12     GPIO44
   Uint16 GPIO45:1;           // 13     GPIO45
   Uint16 GPIO46:1;           // 14     GPIO46
   Uint16 GPIO47:1;           // 15     GPIO47
   Uint16 GPIO48:1;           // 16     GPIO48
   Uint16 GPIO49:1;           // 17     GPIO49
   Uint16 GPIO50:1;           // 18     GPIO50
   Uint16 GPIO51:1;           // 19     GPIO51
   Uint16 GPIO52:1;           // 20     GPIO52
   Uint16 GPIO53:1;           // 21     GPIO53
   Uint16 GPIO54:1;           // 22     GPIO54
   Uint16 GPIO55:1;           // 23     GPIO55
   Uint16 GPIO56:1;           // 24     GPIO56
   Uint16 GPIO57:1;           // 25     GPIO57
   Uint16 GPIO58:1;           // 26     GPIO58
   Uint16 GPIO59:1;           // 27     GPIO59
   Uint16 GPIO60:1;           // 28     GPIO60
   Uint16 GPIO61:1;           // 29     GPIO61
   Uint16 GPIO62:1;           // 30     GPIO62
   Uint16 GPIO63:1;           // 31     GPIO63
};

struct GPCDAT_BITS {          // bits   description
   Uint16 GPIO64:1;           // 0      GPIO64
   Uint16 GPIO65:1;           // 1      GPIO65
   Uint16 GPIO66:1;           // 2      GPIO66
   Uint16 GPIO67:1;           // 3      GPIO67
   Uint16 GPIO68:1;           // 4      GPIO68
   Uint16 GPIO69:1;           // 5      GPIO69
   Uint16 GPIO70:1;           // 6      GPIO70
   Uint16 GPIO71:1;           // 7      GPIO71
   Uint16 GPIO72:1;           // 8      GPIO72
   Uint16 GPIO73:1;           // 9      GPIO73
   Uint16 GPIO74:1;           // 10     GPIO74
   Uint16 GPIO75:1;           // 11     GPIO75
   Uint16 GPIO76:1;           // 12     GPIO76
   Uint16 GPIO77:1;           // 13     GPIO77
   Uint16 GPIO78:1;           // 14     GPIO78
   Uint16 GPIO79:1;           // 15     GPIO79
   Uint16 GPIO80:1;           // 16     GPIO80
   Uint16 GPIO81:1;           // 17     GPIO81
   Uint16 GPIO82:1;           // 18     GPIO82
   Uint16 GPIO83:1;           // 19     GPIO83
   Uint16 GPIO84:1;           // 20     GPIO84
   Uint16 GPIO85:1;           // 21     GPIO85
   Uint16 GPIO86:1;           // 22     GPIO86
   Uint16 GPIO87:1;           // 23     GPIO87
   Uint16 rsvd1:8;            // 31:24  reserved
};


union GPADAT_REG {
   Uint32              all;
   struct GPADAT_BITS  bit;
};

union GPBDAT_REG {
   Uint32              all;
   struct GPBDAT_BITS  bit;
};

union GPCDAT_REG {
   Uint32              all;
   struct GPCDAT_BITS  bit;
};




//----------------------------------------------------
// GPIO Xint1/XINT2/XNMI select register bit definitions */
struct GPIOXINT_BITS {        // bits   description
    Uint16 GPIOSEL:5;         // 4:0    Select GPIO interrupt input source
    Uint16 rsvd1:11;          // 15:5   reserved
};

union GPIOXINT_REG {
   Uint16                all;
   struct GPIOXINT_BITS  bit;
};


struct GPIO_CTRL_REGS {
   union  GPACTRL_REG  GPACTRL;   // GPIO A Control Register (GPIO0 to 31)
   union  GPA1_REG     GPAQSEL1;  // GPIO A Qualifier Select 1 Register (GPIO0 to 15)
   union  GPA2_REG     GPAQSEL2;  // GPIO A Qualifier Select 2 Register (GPIO16 to 31)
   union  GPA1_REG     GPAMUX1;   // GPIO A Mux 1 Register (GPIO0 to 15)
   union  GPA2_REG     GPAMUX2;   // GPIO A Mux 2 Register (GPIO16 to 31)
   union  GPADAT_REG   GPADIR;    // GPIO A Direction Register (GPIO0 to 31)
   union  GPADAT_REG   GPAPUD;    // GPIO A Pull Up Disable Register (GPIO0 to 31)
   Uint32              rsvd1;
   union  GPBCTRL_REG  GPBCTRL;   // GPIO B Control Register (GPIO32 to 63)
   union  GPB1_REG     GPBQSEL1;  // GPIO B Qualifier Select 1 Register (GPIO32 to 47)
   union  GPB2_REG     GPBQSEL2;  // GPIO B Qualifier Select 2 Register (GPIO48 to 63)
   union  GPB1_REG     GPBMUX1;   // GPIO B Mux 1 Register (GPIO32 to 47)
   union  GPB2_REG     GPBMUX2;   // GPIO B Mux 2 Register (GPIO48 to 63)
   union  GPBDAT_REG   GPBDIR;    // GPIO B Direction Register (GPIO32 to 63)
   union  GPBDAT_REG   GPBPUD;    // GPIO B Pull Up Disable Register (GPIO32 to 63)
   Uint16              rsvd2[8];
   union  GPC1_REG     GPCMUX1;   // GPIO C Mux 1 Register (GPIO64 to 79)
   union  GPC2_REG     GPCMUX2;   // GPIO C Mux 2 Register (GPIO80 to 95)
   union  GPCDAT_REG   GPCDIR;    // GPIO C Direction Register (GPIO64 to 95)
   union  GPCDAT_REG   GPCPUD;    // GPIO C Pull Up Disable Register (GPIO64 to 95)
};

struct GPIO_DATA_REGS {
   union  GPADAT_REG       GPADAT;       // GPIO Data Register (GPIO0 to 31)
   union  GPADAT_REG       GPASET;       // GPIO Data Set Register (GPIO0 to 31)
   union  GPADAT_REG       GPACLEAR;     // GPIO Data Clear Register (GPIO0 to 31)
   union  GPADAT_REG       GPATOGGLE;    // GPIO Data Toggle Register (GPIO0 to 31)
   union  GPBDAT_REG       GPBDAT;       // GPIO Data Register (GPIO32 to 63)
   union  GPBDAT_REG       GPBSET;       // GPIO Data Set Register (GPIO32 to 63)
   union  GPBDAT_REG       GPBCLEAR;     // GPIO Data Clear Register (GPIO32 to 63)
   union  GPBDAT_REG       GPBTOGGLE;    // GPIO Data Toggle Register (GPIO32 to 63)
   union  GPCDAT_REG       GPCDAT;       // GPIO Data Register (GPIO64 to 95)
   union  GPCDAT_REG       GPCSET;       // GPIO Data Set Register (GPIO64 to 95)
   union  GPCDAT_REG       GPCCLEAR;     // GPIO Data Clear Register (GPIO64 to 95)
   union  GPCDAT_REG       GPCTOGGLE;    // GPIO Data Toggle Register (GPIO64 to 95)
   Uint16                  rsvd1[8];
};

struct GPIO_INT_REGS {
   union  GPIOXINT_REG     GPIOXINT1SEL; // XINT1 GPIO Input Selection
   union  GPIOXINT_REG     GPIOXINT2SEL; // XINT2 GPIO Input Selection
   union  GPIOXINT_REG     GPIOXNMISEL;  // XNMI_Xint13 GPIO Input Selection
   union  GPIOXINT_REG     GPIOXINT3SEL; // XINT3 GPIO Input Selection
   union  GPIOXINT_REG     GPIOXINT4SEL; // XINT4 GPIO Input Selection
   union  GPIOXINT_REG     GPIOXINT5SEL; // XINT5 GPIO Input Selection
   union  GPIOXINT_REG     GPIOXINT6SEL; // XINT6 GPIO Input Selection
   union  GPIOXINT_REG     GPIOXINT7SEL; // XINT7 GPIO Input Selection
   union  GPADAT_REG       GPIOLPMSEL;   // Low power modes GP I/O input select
};

//----------------------------------------------------
// I2C interrupt vector register bit definitions */
struct I2CISRC_BITS {         // bits   description
   Uint16 INTCODE:3;          // 2:0    Interrupt code
   Uint16 rsvd1:13;           // 15:3   reserved
};

union I2CISRC_REG {
   Uint16                 all;
   struct I2CISRC_BITS    bit;
};

//----------------------------------------------------
// I2C interrupt mask register bit definitions */
struct I2CIER_BITS {          // bits   description
   Uint16 ARBL:1;             // 0      Arbitration lost interrupt
   Uint16 NACK:1;             // 1      No ack interrupt
   Uint16 ARDY:1;             // 2      Register access ready interrupt
   Uint16 RRDY:1;             // 3      Recieve data ready interrupt
   Uint16 XRDY:1;             // 4      Transmit data ready interrupt
   Uint16 SCD:1;              // 5      Stop condition detection
   Uint16 AAS:1;              // 6      Address as slave
   Uint16 rsvd:9;             // 15:7   reserved
};

union I2CIER_REG {
   Uint16                 all;
   struct I2CIER_BITS     bit;
};

//----------------------------------------------------
// I2C status register bit definitions */
struct I2CSTR_BITS {          // bits   description
   Uint16 ARBL:1;             // 0      Arbitration lost interrupt
   Uint16 NACK:1;             // 1      No ack interrupt
   Uint16 ARDY:1;             // 2      Register access ready interrupt
   Uint16 RRDY:1;             // 3      Recieve data ready interrupt
   Uint16 XRDY:1;             // 4      Transmit data ready interrupt
   Uint16 SCD:1;              // 5      Stop condition detection
   Uint16 rsvd1:2;            // 7:6    reserved
   Uint16 AD0:1;              // 8      Address Zero
   Uint16 AAS:1;              // 9      Address as slave
   Uint16 XSMT:1;             // 10     XMIT shift empty
   Uint16 RSFULL:1;           // 11     Recieve shift full
   Uint16 BB:1;               // 12     Bus busy
   Uint16 NACKSNT:1;          // 13     A no ack sent
   Uint16 SDIR:1;             // 14     Slave direction
   Uint16 rsvd2:1;            // 15     reserved
};

union I2CSTR_REG {
   Uint16                 all;
   struct I2CSTR_BITS     bit;
};


//----------------------------------------------------
// I2C mode control register bit definitions */
struct I2CMDR_BITS {          // bits   description
   Uint16 BC:3;               // 2:0    Bit count
   Uint16 FDF:1;              // 3      Free data format
   Uint16 STB:1;              // 4      Start byte
   Uint16 IRS:1;              // 5      I2C Reset not
   Uint16 DLB:1;              // 6      Digital loopback
   Uint16 RM:1;               // 7      Repeat mode
   Uint16 XA:1;               // 8      Expand address
   Uint16 TRX:1;              // 9      Transmitter/reciever
   Uint16 MST:1;              // 10     Master/slave
   Uint16 STP:1;              // 11     Stop condition
   Uint16 rsvd1:1;            // 12     reserved
   Uint16 STT:1;              // 13     Start condition
   Uint16 FREE:1;             // 14     Emulation mode
   Uint16 NACKMOD:1;          // 15     No Ack mode
};

union I2CMDR_REG {
   Uint16                 all;
   struct I2CMDR_BITS     bit;
};

//----------------------------------------------------
// I2C pre-scaler register bit definitions */
struct I2CPSC_BITS {         // bits   description
   Uint16 IPSC:8;            // 7:0    pre-scaler
   Uint16 rsvd1:8;           // 15:8   reserved
};


union I2CPSC_REG {
   Uint16                 all;
   struct I2CPSC_BITS     bit;
};


//----------------------------------------------------
// TX FIFO control register bit definitions */
struct I2CFFTX_BITS {         // bits   description
   Uint16 TXFFIL:5;           // 4:0    FIFO interrupt level
   Uint16 TXFFIENA:1;         // 5      FIFO interrupt enable/disable
   Uint16 TXFFINTCLR:1;       // 6      FIFO clear
   Uint16 TXFFINT:1;          // 7      FIFO interrupt flag
   Uint16 TXFFST:5;           // 12:8   FIFO level status
   Uint16 TXFFRST:1;          // 13     FIFO reset
   Uint16 I2CFFEN:1;          // 14     enable/disable TX & RX FIFOs
   Uint16 rsvd1:1;            // 15     reserved

};

union I2CFFTX_REG {
   Uint16                 all;
   struct I2CFFTX_BITS    bit;
};

//----------------------------------------------------
// RX FIFO control register bit definitions */
struct I2CFFRX_BITS {         // bits   description
   Uint16 RXFFIL:5;           // 4:0    FIFO interrupt level
   Uint16 RXFFIENA:1;         // 5      FIFO interrupt enable/disable
   Uint16 RXFFINTCLR:1;       // 6      FIFO clear
   Uint16 RXFFINT:1;          // 7      FIFO interrupt flag
   Uint16 RXFFST:5;           // 12:8   FIFO level
   Uint16 RXFFRST:1;          // 13     FIFO reset
   Uint16 rsvd1:2;            // 15:14  reserved
};

union I2CFFRX_REG {
   Uint16                 all;
   struct I2CFFRX_BITS    bit;
};


//----------------------------------------------------

struct I2C_REGS {
   Uint16              I2COAR;    // Own address register
   union  I2CIER_REG   I2CIER;    // Interrupt enable
   union  I2CSTR_REG   I2CSTR;    // Interrupt status
   Uint16              I2CCLKL;   // Clock divider low
   Uint16              I2CCLKH;   // Clock divider high
   Uint16              I2CCNT;    // Data count
   Uint16              I2CDRR;    // Data recieve
   Uint16              I2CSAR;    // Slave address
   Uint16              I2CDXR;    // Data transmit
   union  I2CMDR_REG   I2CMDR;    // Mode
   union  I2CISRC_REG  I2CISRC;   // Interrupt source
   Uint16              rsvd1;     // reserved
   union  I2CPSC_REG   I2CPSC;    // Pre-scaler
   Uint16              rsvd2[19]; // reserved
   union  I2CFFTX_REG  I2CFFTX;   // Transmit FIFO
   union  I2CFFRX_REG  I2CFFRX;   // Recieve FIFO
};

//---------------------------------------------------------------------------
// McBSP Individual Register Bit Definitions:
//
// McBSP DRR2 register bit definitions:
struct  DRR2_BITS {       // bit   description
   Uint16     HWLB:8;     // 16:23 High word low byte
   Uint16     HWHB:8;     // 24:31 High word high byte
};                                                                 

union DRR2_REG {
   Uint16            all;
   struct DRR2_BITS  bit;
};

// McBSP DRR1 register bit definitions:
struct  DRR1_BITS {       // bit   description
   Uint16     LWLB:8;     // 16:23 Low word low byte
   Uint16     LWHB:8;     // 24:31 low word high byte
};

union DRR1_REG {
   Uint16            all;
   struct DRR1_BITS  bit;
};

// McBSP DXR2 register bit definitions:
struct  DXR2_BITS {       // bit   description
   Uint16     HWLB:8;     // 16:23 High word low byte
   Uint16     HWHB:8;     // 24:31 High word high byte
};

union DXR2_REG {
   Uint16            all;
   struct DXR2_BITS  bit;
};

// McBSP DXR1 register bit definitions:
struct  DXR1_BITS {       // bit   description
   Uint16     LWLB:8;     // 16:23 Low word low byte
   Uint16     LWHB:8;     // 24:31 low word high byte
};               

union DXR1_REG {
   Uint16              all;
   struct DXR1_BITS  bit;
};

// SPCR2 control register bit definitions:
struct  SPCR2_BITS {       // bit   description
   Uint16     XRST:1;      // 0     transmit reset
   Uint16     XRDY:1;      // 1     transmit ready
   Uint16     XEMPTY:1;    // 2     Transmit empty    
   Uint16     XSYNCERR:1;  // 3     Transmit syn errorINT flag
   Uint16     XINTM:2;     // 5:4   Transmit interrupt types
   Uint16     GRST:1;      // 6     CLKG reset     
   Uint16     FRST:1;      // 7     Frame sync reset
   Uint16     SOFT:1;      // 8     SOFT bit
   Uint16     FREE:1;      // 9     FREE bit
   Uint16     rsvd:6;      // 15:10 reserved
}; 

union SPCR2_REG {
   Uint16               all;
   struct SPCR2_BITS  bit;
};
         
// SPCR1 control register bit definitions:
struct  SPCR1_BITS {       // bit   description
   Uint16     RRST:1;      // 0     Receive reset
   Uint16     RRDY:1;      // 1     Receive  ready
   Uint16     RFULL:1;     // 2     Receive  full    
   Uint16     RSYNCERR:1;  // 7     Receive  syn error
   Uint16     RINTM:2;     // 5:4   Receive  interrupt types
   Uint16     ABIS:1;      // 6     ABIS mode select     
   Uint16     DXENA:1;     // 7     DX hi-z enable     
   Uint16     rsvd:3;      // 10:8  reserved  
   Uint16     CLKSTP:2;    // 12:11 CLKSTOP mode bit
   Uint16     RJUST:2;     // 13:14 Right justified
   Uint16     DLB:1;       // 15    Digital loop back
}; 

union SPCR1_REG {
   Uint16              all;
   struct SPCR1_BITS bit;
};                                                               

// RCR2 control register bit definitions:
struct  RCR2_BITS {         // bit    description
   Uint16     RDATDLY:2;    // 1:0    Receive data delay
   Uint16     RFIG:1;       // 2      Receive  frame sync ignore
   Uint16     RCOMPAND:2;   // 4:3    Receive  Companding Mode selects
   Uint16     RWDLEN2:3;    // 7:5    Receive  word length   
   Uint16     RFRLEN2:7;    // 14:8   Receive  Frame sync
   Uint16     RPHASE:1;     // 15     Receive Phase
}; 

union RCR2_REG {
   Uint16           all;
   struct RCR2_BITS bit;
};
     
// RCR1 control register bit definitions:
struct  RCR1_BITS {         // bit   description
   Uint16     rsvd1:5;      // 4:0   reserved  
   Uint16     RWDLEN1:3;    // 7:5   Receive word length   
   Uint16     RFRLEN1:7;    // 14:8  Receive frame length    
   Uint16     rsvd2:1;      // 15    reserved  
}; 

union RCR1_REG {
   Uint16           all;
   struct RCR1_BITS bit;
};    

// XCR2 control register bit definitions:

struct  XCR2_BITS {         // bit    description
   Uint16     XDATDLY:2;    // 1:0    Transmit data delay
   Uint16     XFIG:1;       // 2      Transmit frame sync ignore
   Uint16     XCOMPAND:2;   // 4:3    Transmit Companding Mode selects
   Uint16     XWDLEN2:3;    // 7:5    Transmit  word length   
   Uint16     XFRLEN2:7;    // 14:8   Transmit  Frame sync
   Uint16     XPHASE:1;     // 15     Transmit Phase
}; 

union XCR2_REG {
   Uint16           all;
   struct XCR2_BITS bit;
};
     
// XCR1 control register bit definitions:
struct  XCR1_BITS {         // bit   description
   Uint16     rsvd1:5;      // 4:0   reserved      
   Uint16     XWDLEN1:3;    // 7:5   Transmit word length    
   Uint16     XFRLEN1:7;    // 14:8  Transmit frame length    
   Uint16     rsvd2:1;      // 15    reserved  
}; 

union XCR1_REG {
   Uint16             all;
   struct XCR1_BITS bit;
};         

// SRGR2 Sample rate generator control register bit definitions:
struct  SRGR2_BITS {        // bit  description
   Uint16     FPER:12;      // 11:0 Frame period
   Uint16     FSGM:1;       // 12   Frame sync generator mode 
   Uint16     CLKSM:1;      // 13   Sample rate generator mode
   Uint16     rsvd:1;       // 14   reserved 
   Uint16     GSYNC:1;      // 15   CLKG sync   
}; 

union  SRGR2_REG {
   Uint16              all;
   struct  SRGR2_BITS  bit;
};

// SRGR1 control register bit definitions:
struct  SRGR1_BITS {        // bit   description
   Uint16     CLKGDV:8;     // 7:0   CLKG divider 
   Uint16     FWID:8;       // 15:8  Frame width
}; 

union  SRGR1_REG {
   Uint16              all;
   struct  SRGR1_BITS  bit;
};

// MCR2 Multichannel control register bit definitions:
struct  MCR2_BITS {         // bit   description
   Uint16     XMCM:2;       // 1:0   Transmit multichannel mode      
   Uint16     XCBLK:3;      // 2:4   Transmit current block    
   Uint16     XPABLK:2;     // 5:6   Transmit partition A Block 
   Uint16     XPBBLK:2;     // 7:8   Transmit partition B Block 
   Uint16     XMCME:1;      // 9     Transmit multi-channel enhance mode 
   Uint16     rsvd:6;       // 15:10 reserved  
}; 

union  MCR2_REG {
   Uint16             all;
   struct  MCR2_BITS  bit;
};
      
// MCR1 Multichannel control register bit definitions:
struct  MCR1_BITS {         // bit   description
   Uint16     RMCM:1;       // 0     Receive multichannel mode  
   Uint16     rsvd:1;       // 1     reserved     
   Uint16     RCBLK:3;      // 4:2   Receive current block    
   Uint16     RPABLK:2;     // 6:5   Receive partition A Block 
   Uint16     RPBBLK:2;     // 7:8   Receive partition B Block 
   Uint16     RMCME:1;      // 9     Receive multi-channel enhance mode 
   Uint16     rsvd1:6;      // 15:10 reserved   
}; 

union  MCR1_REG {
   Uint16             all;
   struct  MCR1_BITS  bit;
};
 
// RCERA control register bit definitions:
struct  RCERA_BITS {         // bit description
   Uint16     RCEA0:1;       // 0   Receive Channel enable bit  
   Uint16     RCEA1:1;       // 1   Receive Channel enable bit  
   Uint16     RCEA2:1;       // 2   Receive Channel enable bit  
   Uint16     RCEA3:1;       // 3   Receive Channel enable bit   
   Uint16     RCEA4:1;       // 4   Receive Channel enable bit  
   Uint16     RCEA5:1;       // 5   Receive Channel enable bit  
   Uint16     RCEA6:1;       // 6   Receive Channel enable bit  
   Uint16     RCEA7:1;       // 7   Receive Channel enable bit 
   Uint16     RCEA8:1;       // 8   Receive Channel enable bit  
   Uint16     RCEA9:1;       // 9   Receive Channel enable bit  
   Uint16     RCEA10:1;      // 10  Receive Channel enable bit  
   Uint16     RCEA11:1;      // 11  Receive Channel enable bit 
   Uint16     RCEA12:1;      // 12  Receive Channel enable bit  
   Uint16     RCEA13:1;      // 13  Receive Channel enable bit  
   Uint16     RCEA14:1;      // 14  Receive Channel enable bit  
   Uint16     RCEA15:1;      // 15  Receive Channel enable bit 
}; 

union RCERA_REG {
   Uint16              all;
   struct  RCERA_BITS  bit;
};  

// RCERB control register bit definitions:
struct  RCERB_BITS {         // bit description
   Uint16     RCEB0:1;       // 0   Receive Channel enable bit  
   Uint16     RCEB1:1;       // 1   Receive Channel enable bit  
   Uint16     RCEB2:1;       // 2   Receive Channel enable bit  
   Uint16     RCEB3:1;       // 3   Receive Channel enable bit   
   Uint16     RCEB4:1;       // 4   Receive Channel enable bit  
   Uint16     RCEB5:1;       // 5   Receive Channel enable bit  
   Uint16     RCEB6:1;       // 6   Receive Channel enable bit  
   Uint16     RCEB7:1;       // 7   Receive Channel enable bit 
   Uint16     RCEB8:1;       // 8   Receive Channel enable bit  
   Uint16     RCEB9:1;       // 9   Receive Channel enable bit  
   Uint16     RCEB10:1;      // 10  Receive Channel enable bit  
   Uint16     RCEB11:1;      // 11  Receive Channel enable bit 
   Uint16     RCEB12:1;      // 12  Receive Channel enable bit  
   Uint16     RCEB13:1;      // 13  Receive Channel enable bit  
   Uint16     RCEB14:1;      // 14  Receive Channel enable bit  
   Uint16     RCEB15:1;      // 15  Receive Channel enable bit   
}; 

union RCERB_REG {
   Uint16              all;
   struct  RCERB_BITS  bit;
};

// XCERA control register bit definitions:
struct  XCERA_BITS {         // bit description
   Uint16     XCERA0:1;       // 0   Receive Channel enable bit  
   Uint16     XCERA1:1;       // 1   Receive Channel enable bit  
   Uint16     XCERA2:1;       // 2   Receive Channel enable bit  
   Uint16     XCERA3:1;       // 3   Receive Channel enable bit   
   Uint16     XCERA4:1;       // 4   Receive Channel enable bit  
   Uint16     XCERA5:1;       // 5   Receive Channel enable bit  
   Uint16     XCERA6:1;       // 6   Receive Channel enable bit  
   Uint16     XCERA7:1;       // 7   Receive Channel enable bit 
   Uint16     XCERA8:1;       // 8   Receive Channel enable bit  
   Uint16     XCERA9:1;       // 9   Receive Channel enable bit  
   Uint16     XCERA10:1;      // 10  Receive Channel enable bit  
   Uint16     XCERA11:1;      // 11  Receive Channel enable bit 
   Uint16     XCERA12:1;      // 12  Receive Channel enable bit  
   Uint16     XCERA13:1;      // 13  Receive Channel enable bit  
   Uint16     XCERA14:1;      // 14  Receive Channel enable bit  
   Uint16     XCERA15:1;      // 15  Receive Channel enable bit 
}; 

union XCERA_REG {
   Uint16              all;
   struct  XCERA_BITS  bit;
};  

// XCERB control register bit definitions:
struct  XCERB_BITS {         // bit description
   Uint16     XCERB0:1;       // 0   Receive Channel enable bit  
   Uint16     XCERB1:1;       // 1   Receive Channel enable bit  
   Uint16     XCERB2:1;       // 2   Receive Channel enable bit  
   Uint16     XCERB3:1;       // 3   Receive Channel enable bit   
   Uint16     XCERB4:1;       // 4   Receive Channel enable bit  
   Uint16     XCERB5:1;       // 5   Receive Channel enable bit  
   Uint16     XCERB6:1;       // 6   Receive Channel enable bit  
   Uint16     XCERB7:1;       // 7   Receive Channel enable bit 
   Uint16     XCERB8:1;       // 8   Receive Channel enable bit  
   Uint16     XCERB9:1;       // 9   Receive Channel enable bit  
   Uint16     XCERB10:1;      // 10  Receive Channel enable bit  
   Uint16     XCERB11:1;      // 11  Receive Channel enable bit 
   Uint16     XCERB12:1;      // 12  Receive Channel enable bit  
   Uint16     XCERB13:1;      // 13  Receive Channel enable bit  
   Uint16     XCERB14:1;      // 14  Receive Channel enable bit  
   Uint16     XCERB15:1;      // 15  Receive Channel enable bit 
}; 

union XCERB_REG {
   Uint16              all;
   struct  XCERB_BITS  bit;
};
  
// PCR control register bit definitions:
struct  PCR_BITS {          // bit description
   Uint16     CLKRP:1;       // 0   Receive Clock polarity
   Uint16     CLKXP:1;       // 1   Transmit clock polarity  
   Uint16     FSRP:1;        // 2   Receive Frame synchronization polarity  
   Uint16     FSXP:1;        // 3   Transmit Frame synchronization polarity   
   Uint16     DR_STAT:1;     // 4   DR pin status - reserved for this McBSP  
   Uint16     DX_STAT:1;     // 5   DX pin status - reserved for this McBSP  
   Uint16     CLKS_STAT:1;   // 6   CLKS pin status - reserved for 28x -McBSP  
   Uint16     SCLKME:1;      // 7   Enhanced sample clock mode selection bit.
   Uint16     CLKRM:1;       // 8   Receiver Clock Mode 
   Uint16     CLKXM:1;       // 9   Transmitter Clock Mode.  
   Uint16     FSRM:1;        // 10  Receive Frame Synchronization Mode  
   Uint16     FSXM:1;        // 11  Transmit Frame Synchronization Mode
   Uint16     RIOEN:1;       // 12  General Purpose I/O Mode - reserved in this 28x-McBSP    
   Uint16     XIOEN:1;       // 13  General Purpose I/O Mode - reserved in this 28x-McBSP
   Uint16     IDEL_EN:1;     // 14  reserved in this 28x-McBSP
   Uint16     rsvd:1  ;      // 15  reserved
}; 

union PCR_REG {
   Uint16            all;
   struct  PCR_BITS  bit;
};
  
// RCERC control register bit definitions:
struct  RCERC_BITS {         // bit description
   Uint16     RCEC0:1;       // 0   Receive Channel enable bit  
   Uint16     RCEC1:1;       // 1   Receive Channel enable bit  
   Uint16     RCEC2:1;       // 2   Receive Channel enable bit  
   Uint16     RCEC3:1;       // 3   Receive Channel enable bit   
   Uint16     RCEC4:1;       // 4   Receive Channel enable bit  
   Uint16     RCEC5:1;       // 5   Receive Channel enable bit  
   Uint16     RCEC6:1;       // 6   Receive Channel enable bit  
   Uint16     RCEC7:1;       // 7   Receive Channel enable bit 
   Uint16     RCEC8:1;       // 8   Receive Channel enable bit  
   Uint16     RCEC9:1;       // 9   Receive Channel enable bit  
   Uint16     RCEC10:1;      // 10  Receive Channel enable bit  
   Uint16     RCEC11:1;      // 11  Receive Channel enable bit 
   Uint16     RCEC12:1;      // 12  Receive Channel enable bit  
   Uint16     RCEC13:1;      // 13  Receive Channel enable bit  
   Uint16     RCEC14:1;      // 14  Receive Channel enable bit  
   Uint16     RCEC15:1;      // 15  Receive Channel enable bit 
}; 

union RCERC_REG {
   Uint16              all;
   struct  RCERC_BITS  bit;
};  

// RCERD control register bit definitions:
struct  RCERD_BITS {         // bit description
   Uint16     RCED0:1;       // 0   Receive Channel enable bit  
   Uint16     RCED1:1;       // 1   Receive Channel enable bit  
   Uint16     RCED2:1;       // 2   Receive Channel enable bit  
   Uint16     RCED3:1;       // 3   Receive Channel enable bit   
   Uint16     RCED4:1;       // 4   Receive Channel enable bit  
   Uint16     RCED5:1;       // 5   Receive Channel enable bit  
   Uint16     RCED6:1;       // 6   Receive Channel enable bit  
   Uint16     RCED7:1;       // 7   Receive Channel enable bit 
   Uint16     RCED8:1;       // 8   Receive Channel enable bit  
   Uint16     RCED9:1;       // 9   Receive Channel enable bit  
   Uint16     RCED10:1;      // 10  Receive Channel enable bit  
   Uint16     RCED11:1;      // 11  Receive Channel enable bit 
   Uint16     RCED12:1;      // 12  Receive Channel enable bit  
   Uint16     RCED13:1;      // 13  Receive Channel enable bit  
   Uint16     RCED14:1;      // 14  Receive Channel enable bit  
   Uint16     RCED15:1;      // 15  Receive Channel enable bit 
}; 

union RCERD_REG {
   Uint16              all;
   struct  RCERD_BITS  bit;
};

// XCERC control register bit definitions:
struct  XCERC_BITS {         // bit description
   Uint16     XCERC0:1;       // 0   Receive Channel enable bit  
   Uint16     XCERC1:1;       // 1   Receive Channel enable bit  
   Uint16     XCERC2:1;       // 2   Receive Channel enable bit  
   Uint16     XCERC3:1;       // 3   Receive Channel enable bit   
   Uint16     XCERC4:1;       // 4   Receive Channel enable bit  
   Uint16     XCERC5:1;       // 5   Receive Channel enable bit  
   Uint16     XCERC6:1;       // 6   Receive Channel enable bit  
   Uint16     XCERC7:1;       // 7   Receive Channel enable bit 
   Uint16     XCERC8:1;       // 8   Receive Channel enable bit  
   Uint16     XCERC9:1;       // 9   Receive Channel enable bit  
   Uint16     XCERC10:1;      // 10  Receive Channel enable bit  
   Uint16     XCERC11:1;      // 11  Receive Channel enable bit 
   Uint16     XCERC12:1;      // 12  Receive Channel enable bit  
   Uint16     XCERC13:1;      // 13  Receive Channel enable bit  
   Uint16     XCERC14:1;      // 14  Receive Channel enable bit  
   Uint16     XCERC15:1;      // 15  Receive Channel enable bit 
}; 

union XCERC_REG {
   Uint16              all;
   struct  XCERC_BITS  bit;
};  

// XCERD control register bit definitions:
struct  XCERD_BITS {         // bit description
   Uint16     XCERD0:1;       // 0   Receive Channel enable bit  
   Uint16     XCERD1:1;       // 1   Receive Channel enable bit  
   Uint16     XCERD2:1;       // 2   Receive Channel enable bit  
   Uint16     XCERD3:1;       // 3   Receive Channel enable bit   
   Uint16     XCERD4:1;       // 4   Receive Channel enable bit  
   Uint16     XCERD5:1;       // 5   Receive Channel enable bit  
   Uint16     XCERD6:1;       // 6   Receive Channel enable bit  
   Uint16     XCERD7:1;       // 7   Receive Channel enable bit 
   Uint16     XCERD8:1;       // 8   Receive Channel enable bit  
   Uint16     XCERD9:1;       // 9   Receive Channel enable bit  
   Uint16     XCERD10:1;      // 10  Receive Channel enable bit  
   Uint16     XCERD11:1;      // 11  Receive Channel enable bit 
   Uint16     XCERD12:1;      // 12  Receive Channel enable bit  
   Uint16     XCERD13:1;      // 13  Receive Channel enable bit  
   Uint16     XCERD14:1;      // 14  Receive Channel enable bit  
   Uint16     XCERD15:1;      // 15  Receive Channel enable bit 
}; 

union XCERD_REG {
   Uint16              all;
   struct  XCERD_BITS  bit;
};
  
// RCERE control register bit definitions:
struct  RCERE_BITS {         // bit description
   Uint16     RCEE0:1;       // 0   Receive Channel enable bit  
   Uint16     RCEE1:1;       // 1   Receive Channel enable bit  
   Uint16     RCEE2:1;       // 2   Receive Channel enable bit  
   Uint16     RCEE3:1;       // 3   Receive Channel enable bit   
   Uint16     RCEE4:1;       // 4   Receive Channel enable bit  
   Uint16     RCEE5:1;       // 5   Receive Channel enable bit  
   Uint16     RCEE6:1;       // 6   Receive Channel enable bit  
   Uint16     RCEE7:1;       // 7   Receive Channel enable bit 
   Uint16     RCEE8:1;       // 8   Receive Channel enable bit  
   Uint16     RCEE9:1;       // 9   Receive Channel enable bit  
   Uint16     RCEE10:1;      // 10  Receive Channel enable bit  
   Uint16     RCEE11:1;      // 11  Receive Channel enable bit 
   Uint16     RCEE12:1;      // 12  Receive Channel enable bit  
   Uint16     RCEE13:1;      // 13  Receive Channel enable bit  
   Uint16     RCEE14:1;      // 14  Receive Channel enable bit  
   Uint16     RCEE15:1;      // 15  Receive Channel enable bit 
}; 

union RCERE_REG {
   Uint16              all;
   struct  RCERE_BITS  bit;
};  

// RCERF control register bit definitions:
struct  RCERF_BITS {         // bit   description
   Uint16     RCEF0:1;       // 0   Receive Channel enable bit  
   Uint16     RCEF1:1;       // 1   Receive Channel enable bit  
   Uint16     RCEF2:1;       // 2   Receive Channel enable bit  
   Uint16     RCEF3:1;       // 3   Receive Channel enable bit   
   Uint16     RCEF4:1;       // 4   Receive Channel enable bit  
   Uint16     RCEF5:1;       // 5   Receive Channel enable bit  
   Uint16     RCEF6:1;       // 6   Receive Channel enable bit  
   Uint16     RCEF7:1;       // 7   Receive Channel enable bit 
   Uint16     RCEF8:1;       // 8   Receive Channel enable bit  
   Uint16     RCEF9:1;       // 9   Receive Channel enable bit  
   Uint16     RCEF10:1;      // 10  Receive Channel enable bit  
   Uint16     RCEF11:1;      // 11  Receive Channel enable bit 
   Uint16     RCEF12:1;      // 12  Receive Channel enable bit  
   Uint16     RCEF13:1;      // 13  Receive Channel enable bit  
   Uint16     RCEF14:1;      // 14  Receive Channel enable bit  
   Uint16     RCEF15:1;      // 15  Receive Channel enable bit 
}; 

union RCERF_REG {
   Uint16              all;
   struct  RCERF_BITS  bit;
};

// XCERE control register bit definitions:
struct  XCERE_BITS {         // bit description
   Uint16     XCERE0:1;       // 0   Receive Channel enable bit  
   Uint16     XCERE1:1;       // 1   Receive Channel enable bit  
   Uint16     XCERE2:1;       // 2   Receive Channel enable bit  
   Uint16     XCERE3:1;       // 3   Receive Channel enable bit   
   Uint16     XCERE4:1;       // 4   Receive Channel enable bit  
   Uint16     XCERE5:1;       // 5   Receive Channel enable bit  
   Uint16     XCERE6:1;       // 6   Receive Channel enable bit  
   Uint16     XCERE7:1;       // 7   Receive Channel enable bit 
   Uint16     XCERE8:1;       // 8   Receive Channel enable bit  
   Uint16     XCERE9:1;       // 9   Receive Channel enable bit  
   Uint16     XCERE10:1;      // 10  Receive Channel enable bit  
   Uint16     XCERE11:1;      // 11  Receive Channel enable bit 
   Uint16     XCERE12:1;      // 12  Receive Channel enable bit  
   Uint16     XCERE13:1;      // 13  Receive Channel enable bit  
   Uint16     XCERE14:1;      // 14  Receive Channel enable bit  
   Uint16     XCERE15:1;      // 15  Receive Channel enable bit 
}; 

union XCERE_REG {
   Uint16              all;
   struct  XCERE_BITS  bit;
};  

// XCERF control register bit definitions:
struct  XCERF_BITS {         // bit description
   Uint16     XCERF0:1;       // 0   Receive Channel enable bit  
   Uint16     XCERF1:1;       // 1   Receive Channel enable bit  
   Uint16     XCERF2:1;       // 2   Receive Channel enable bit  
   Uint16     XCERF3:1;       // 3   Receive Channel enable bit   
   Uint16     XCERF4:1;       // 4   Receive Channel enable bit  
   Uint16     XCERF5:1;       // 5   Receive Channel enable bit  
   Uint16     XCERF6:1;       // 6   Receive Channel enable bit  
   Uint16     XCERF7:1;       // 7   Receive Channel enable bit 
   Uint16     XCERF8:1;       // 8   Receive Channel enable bit  
   Uint16     XCERF9:1;       // 9   Receive Channel enable bit  
   Uint16     XCERF10:1;      // 10  Receive Channel enable bit  
   Uint16     XCERF11:1;      // 11  Receive Channel enable bit 
   Uint16     XCERF12:1;      // 12  Receive Channel enable bit  
   Uint16     XCERF13:1;      // 13  Receive Channel enable bit  
   Uint16     XCERF14:1;      // 14  Receive Channel enable bit  
   Uint16     XCERF15:1;      // 15  Receive Channel enable bit 
}; 

union XCERF_REG {
   Uint16              all;
   struct  XCERF_BITS  bit;
};                   

// RCERG control register bit definitions:
struct  RCERG_BITS {         // bit description
   Uint16     RCEG0:1;       // 0   Receive Channel enable bit  
   Uint16     RCEG1:1;       // 1   Receive Channel enable bit  
   Uint16     RCEG2:1;       // 2   Receive Channel enable bit  
   Uint16     RCEG3:1;       // 3   Receive Channel enable bit   
   Uint16     RCEG4:1;       // 4   Receive Channel enable bit  
   Uint16     RCEG5:1;       // 5   Receive Channel enable bit  
   Uint16     RCEG6:1;       // 6   Receive Channel enable bit  
   Uint16     RCEG7:1;       // 7   Receive Channel enable bit 
   Uint16     RCEG8:1;       // 8   Receive Channel enable bit  
   Uint16     RCEG9:1;       // 9   Receive Channel enable bit  
   Uint16     RCEG10:1;      // 10  Receive Channel enable bit  
   Uint16     RCEG11:1;      // 11  Receive Channel enable bit 
   Uint16     RCEG12:1;      // 12  Receive Channel enable bit  
   Uint16     RCEG13:1;      // 13  Receive Channel enable bit  
   Uint16     RCEG14:1;      // 14  Receive Channel enable bit  
   Uint16     RCEG15:1;      // 15  Receive Channel enable bit 
}; 

union RCERG_REG {
   Uint16              all;
   struct  RCERG_BITS  bit;
};  

// RCERH control register bit definitions:
struct  RCERH_BITS {         // bit description
   Uint16     RCEH0:1;       // 0   Receive Channel enable bit  
   Uint16     RCEH1:1;       // 1   Receive Channel enable bit  
   Uint16     RCEH2:1;       // 2   Receive Channel enable bit  
   Uint16     RCEH3:1;       // 3   Receive Channel enable bit   
   Uint16     RCEH4:1;       // 4   Receive Channel enable bit  
   Uint16     RCEH5:1;       // 5   Receive Channel enable bit  
   Uint16     RCEH6:1;       // 6   Receive Channel enable bit  
   Uint16     RCEH7:1;       // 7   Receive Channel enable bit 
   Uint16     RCEH8:1;       // 8   Receive Channel enable bit  
   Uint16     RCEH9:1;       // 9   Receive Channel enable bit  
   Uint16     RCEH10:1;      // 10  Receive Channel enable bit  
   Uint16     RCEH11:1;      // 11  Receive Channel enable bit 
   Uint16     RCEH12:1;      // 12  Receive Channel enable bit  
   Uint16     RCEH13:1;      // 13  Receive Channel enable bit  
   Uint16     RCEH14:1;      // 14  Receive Channel enable bit  
   Uint16     RCEH15:1;      // 15  Receive Channel enable bit 
}; 

union RCERH_REG {
   Uint16              all;
   struct  RCERH_BITS  bit;
};

// XCERG control register bit definitions:
struct  XCERG_BITS {         // bit description
   Uint16     XCERG0:1;       // 0   Receive Channel enable bit  
   Uint16     XCERG1:1;       // 1   Receive Channel enable bit  
   Uint16     XCERG2:1;       // 2   Receive Channel enable bit  
   Uint16     XCERG3:1;       // 3   Receive Channel enable bit   
   Uint16     XCERG4:1;       // 4   Receive Channel enable bit  
   Uint16     XCERG5:1;       // 5   Receive Channel enable bit  
   Uint16     XCERG6:1;       // 6   Receive Channel enable bit  
   Uint16     XCERG7:1;       // 7   Receive Channel enable bit 
   Uint16     XCERG8:1;       // 8   Receive Channel enable bit  
   Uint16     XCERG9:1;       // 9   Receive Channel enable bit  
   Uint16     XCERG10:1;      // 10  Receive Channel enable bit  
   Uint16     XCERG11:1;      // 11  Receive Channel enable bit 
   Uint16     XCERG12:1;      // 12  Receive Channel enable bit  
   Uint16     XCERG13:1;      // 13  Receive Channel enable bit  
   Uint16     XCERG14:1;      // 14  Receive Channel enable bit  
   Uint16     XCERG15:1;      // 15  Receive Channel enable bit 
}; 

union XCERG_REG {
   Uint16              all;
   struct  XCERG_BITS  bit;
};  

// XCERH control register bit definitions:
struct  XCERH_BITS {         // bit description
   Uint16     XCEH0:1;       // 0   Receive Channel enable bit  
   Uint16     XCEH1:1;       // 1   Receive Channel enable bit  
   Uint16     XCEH2:1;       // 2   Receive Channel enable bit  
   Uint16     XCEH3:1;       // 3   Receive Channel enable bit   
   Uint16     XCEH4:1;       // 4   Receive Channel enable bit  
   Uint16     XCEH5:1;       // 5   Receive Channel enable bit  
   Uint16     XCEH6:1;       // 6   Receive Channel enable bit  
   Uint16     XCEH7:1;       // 7   Receive Channel enable bit 
   Uint16     XCEH8:1;       // 8   Receive Channel enable bit  
   Uint16     XCEH9:1;       // 9   Receive Channel enable bit  
   Uint16     XCEH10:1;      // 10  Receive Channel enable bit  
   Uint16     XCEH11:1;      // 11  Receive Channel enable bit 
   Uint16     XCEH12:1;      // 12  Receive Channel enable bit  
   Uint16     XCEH13:1;      // 13  Receive Channel enable bit  
   Uint16     XCEH14:1;      // 14  Receive Channel enable bit  
   Uint16     XCEH15:1;      // 15  Receive Channel enable bit 
}; 

union XCERH_REG {
   Uint16              all;
   struct  XCERH_BITS  bit;
};


// McBSP Interrupt enable register for RINT/XINT 
struct  MFFINT_BITS {       // bits description
    Uint16     XINT:1;      // 0    XINT  interrupt enable
    Uint16     XEVTA:1;     // 1    XEVTA interrupt enable
    Uint16     RINT:1;      // 2    RINT  interrupt enable
    Uint16     REVTA:1;     // 3    REVTA interrupt enable
    Uint16     rsvd:12;     // 15:4 reserved
};

union MFFINT_REG {
   Uint16              all;
   struct MFFINT_BITS  bit;
};

// McBSP pin status register in McBSP mode
struct  MFFST_BITS {       // bits description
    Uint16     EOBX:1;     // 0    EOBX flag
    Uint16     FSX:1;      // 1    FSX flag
    Uint16     EOBR:1;     // 2    EOBR flag
    Uint16     FSR:1;      // 3    FSR flag
    Uint16     rsvd:12;    // 15:4 reserved
};

union MFFST_REG {
   Uint16            all;
   struct MFFST_BITS bit;
};


//---------------------------------------------------------------------------
// McBSP Register File:
//
struct  MCBSP_REGS {      
   union DRR2_REG    DRR2;       // 0x7800, MCBSP Data receive register bits 31-16 
   union DRR1_REG    DRR1;       // 0x7801, MCBSP Data receive register bits 15-0 
   union DXR2_REG    DXR2;       // 0x7802, MCBSP Data transmit register bits 31-16 
   union DXR1_REG    DXR1;       // 0x7803, MCBSP Data transmit register bits 15-0 
   union SPCR2_REG   SPCR2;      // 0x7804, MCBSP control register bits 31-16 
   union SPCR1_REG   SPCR1;      // 0x7805, MCBSP control register bits 15-0 
   union RCR2_REG    RCR2;       // 0x7806, MCBSP receive control register bits 31-16 
   union RCR1_REG    RCR1;       // 0x7807, MCBSP receive control register bits 15-0 
   union XCR2_REG    XCR2;       // 0x7808, MCBSP transmit control register bits 31-16 
   union XCR1_REG    XCR1;       // 0x7809, MCBSP transmit control register bits 15-0 
   union SRGR2_REG   SRGR2;      // 0x7810, MCBSP sample rate gen register bits 31-16 
   union SRGR1_REG   SRGR1;      // 0x7811, MCBSP sample rate gen register bits 15-0  
   union MCR2_REG    MCR2;       // 0x7812, MCBSP multichannel register bits 31-16 
   union MCR1_REG    MCR1;       // 0x7813, MCBSP multichannel register bits 15-0    
   union RCERA_REG   RCERA;      // 0x7814, MCBSP Receive channel enable partition A 
   union RCERB_REG   RCERB;      // 0x7815, MCBSP Receive channel enable partition B 
   union XCERA_REG   XCERA;      // 0x7816, MCBSP Transmit channel enable partition A 
   union XCERB_REG   XCERB;      // 0x7817, MCBSP Transmit channel enable partition B            
   union PCR_REG     PCR;        // 0x7818, MCBSP Pin control register bits 15-0  
   union RCERC_REG   RCERC;      // 0x7819, MCBSP Receive channel enable partition C 
   union RCERD_REG   RCERD;      // 0x7820, MCBSP Receive channel enable partition D
   union XCERC_REG   XCERC;      // 0x7821, MCBSP Transmit channel enable partition C 
   union XCERD_REG   XCERD;      // 0x7823, MCBSP Transmit channel enable partition D             
   union RCERE_REG   RCERE;      // 0x7824, MCBSP Receive channel enable partition E 
   union RCERF_REG   RCERF;      // 0x7825, MCBSP Receive channel enable partition F
   union XCERE_REG   XCERE;      // 0x7826, MCBSP Transmit channel enable partition E
   union XCERF_REG   XCERF;      // 0x7827, MCBSP Transmit channel enable partition F            
   union RCERG_REG   RCERG;      // 0x7828, MCBSP Receive channel enable partition G
   union RCERH_REG   RCERH;      // 0x7829, MCBSP Receive channel enable partition H
   union XCERG_REG   XCERG;      // 0x7830, MCBSP Transmit channel enable partition G 
   union XCERH_REG   XCERH;      // 0x7831, MCBSP Transmit channel enable partition H
   Uint16            rsvd1[4];   // 0x7832 - 0x7835, reserved              
   union MFFINT_REG  MFFINT;     // 0x7836, MCBSP Interrupt enable register for RINT/XINT  
   union MFFST_REG   MFFST;      // 0x7837, MCBSP pin status register in McBSP mode
};


//---------------------------------------------------------------------------
// PIE Control Register Bit Definitions:
//
// PIECTRL: Register bit definitions:
struct PIECTRL_BITS {      // bits description
   Uint16  ENPIE:1;        // 0    Enable PIE block
   Uint16  PIEVECT:15;     // 15:1 Fetched vector address
};

union PIECTRL_REG {
   Uint16                 all;
   struct PIECTRL_BITS  bit;
};  

// PIEIER: Register bit definitions:
struct PIEIER_BITS {       // bits description
   Uint16 INTx1:1;         // 0    INTx.1
   Uint16 INTx2:1;         // 1    INTx.2
   Uint16 INTx3:1;         // 2    INTx.3
   Uint16 INTx4:1;         // 3    INTx.4
   Uint16 INTx5:1;         // 4    INTx.5
   Uint16 INTx6:1;         // 5    INTx.6
   Uint16 INTx7:1;         // 6    INTx.7
   Uint16 INTx8:1;         // 7    INTx.8
   Uint16 rsvd:8;          // 15:8 reserved
};

union PIEIER_REG {
   Uint16              all;
   struct PIEIER_BITS  bit;
}; 

// PIEIFR: Register bit definitions:
struct PIEIFR_BITS {       // bits description
   Uint16 INTx1:1;         // 0    INTx.1
   Uint16 INTx2:1;         // 1    INTx.2
   Uint16 INTx3:1;         // 2    INTx.3
   Uint16 INTx4:1;         // 3    INTx.4
   Uint16 INTx5:1;         // 4    INTx.5
   Uint16 INTx6:1;         // 5    INTx.6
   Uint16 INTx7:1;         // 6    INTx.7
   Uint16 INTx8:1;         // 7    INTx.8
   Uint16 rsvd:8;          // 15:8 reserved
};

union PIEIFR_REG {
   Uint16              all;
   struct PIEIFR_BITS  bit;
};

// PIEACK: Register bit definitions:
struct PIEACK_BITS {       // bits description
   Uint16 ACK1:1;          // 0    Acknowledge PIE interrupt group 1
   Uint16 ACK2:1;          // 1    Acknowledge PIE interrupt group 2
   Uint16 ACK3:1;          // 2    Acknowledge PIE interrupt group 3
   Uint16 ACK4:1;          // 3    Acknowledge PIE interrupt group 4
   Uint16 ACK5:1;          // 4    Acknowledge PIE interrupt group 5
   Uint16 ACK6:1;          // 5    Acknowledge PIE interrupt group 6
   Uint16 ACK7:1;          // 6    Acknowledge PIE interrupt group 7
   Uint16 ACK8:1;          // 7    Acknowledge PIE interrupt group 8
   Uint16 ACK9:1;          // 8    Acknowledge PIE interrupt group 9
   Uint16 ACK10:1;         // 9    Acknowledge PIE interrupt group 10
   Uint16 ACK11:1;         // 10   Acknowledge PIE interrupt group 11
   Uint16 ACK12:1;         // 11   Acknowledge PIE interrupt group 12
   Uint16 rsvd:4;          // 15:12 reserved
};

union PIEACK_REG {
   Uint16              all;
   struct PIEACK_BITS  bit;
};

//---------------------------------------------------------------------------
// PIE Control Register File:
//
struct PIE_CTRL_REGS {
   union PIECTRL_REG PIECTRL;       // PIE control register
   union PIEACK_REG  PIEACK;        // PIE acknowledge
   union PIEIER_REG  PIEIER1;       // PIE int1 IER register  
   union PIEIFR_REG  PIEIFR1;       // PIE int1 IFR register
   union PIEIER_REG  PIEIER2;       // PIE INT2 IER register 
   union PIEIFR_REG  PIEIFR2;       // PIE INT2 IFR register
   union PIEIER_REG  PIEIER3;       // PIE INT3 IER register 
   union PIEIFR_REG  PIEIFR3;       // PIE INT3 IFR register
   union PIEIER_REG  PIEIER4;       // PIE INT4 IER register             
   union PIEIFR_REG  PIEIFR4;       // PIE INT4 IFR register
   union PIEIER_REG  PIEIER5;       // PIE INT5 IER register  
   union PIEIFR_REG  PIEIFR5;       // PIE INT5 IFR register
   union PIEIER_REG  PIEIER6;       // PIE INT6 IER register 
   union PIEIFR_REG  PIEIFR6;       // PIE INT6 IFR register
   union PIEIER_REG  PIEIER7;       // PIE INT7 IER register 
   union PIEIFR_REG  PIEIFR7;       // PIE INT7 IFR register
   union PIEIER_REG  PIEIER8;       // PIE INT8 IER register
   union PIEIFR_REG  PIEIFR8;       // PIE INT8 IFR register
   union PIEIER_REG  PIEIER9;       // PIE INT9 IER register  
   union PIEIFR_REG  PIEIFR9;       // PIE INT9 IFR register
   union PIEIER_REG  PIEIER10;      // PIE int10 IER register 
   union PIEIFR_REG  PIEIFR10;      // PIE int10 IFR register
   union PIEIER_REG  PIEIER11;      // PIE int11 IER register 
   union PIEIFR_REG  PIEIFR11;      // PIE int11 IFR register
   union PIEIER_REG  PIEIER12;      // PIE int12 IER register
   union PIEIFR_REG  PIEIFR12;      // PIE int12 IFR register
};     


//---------------------------------------------------------------------------
// SCI Individual Register Bit Definitions

//----------------------------------------------------------
// SCICCR communication control register bit definitions:
//

struct  SCICCR_BITS {        // bit    description
   Uint16 SCICHAR:3;         // 2:0    Character length control        
   Uint16 ADDRIDLE_MODE:1;   // 3      ADDR/IDLE Mode control
   Uint16 LOOPBKENA:1;       // 4      Loop Back enable
   Uint16 PARITYENA:1;       // 5      Parity enable   
   Uint16 PARITY:1;          // 6      Even or Odd Parity
   Uint16 STOPBITS:1;        // 7      Number of Stop Bits
   Uint16 rsvd1:8;           // 15:8   reserved
}; 


union SCICCR_REG {
   Uint16              all;
   struct SCICCR_BITS  bit;
};

//-------------------------------------------
// SCICTL1 control register 1 bit definitions:
//
                       
struct  SCICTL1_BITS {       // bit    description
   Uint16 RXENA:1;           // 0      SCI receiver enable
   Uint16 TXENA:1;           // 1      SCI transmitter enable
   Uint16 SLEEP:1;           // 2      SCI sleep  
   Uint16 TXWAKE:1;          // 3      Transmitter wakeup method
   Uint16 rsvd:1;            // 4      reserved
   Uint16 SWRESET:1;         // 5      Software reset   
   Uint16 RXERRINTENA:1;     // 6      Recieve interrupt enable
   Uint16 rsvd1:9;           // 15:7   reserved

}; 

union SCICTL1_REG {
   Uint16               all;
   struct SCICTL1_BITS  bit;
};

//---------------------------------------------
// SCICTL2 control register 2 bit definitions:
// 

struct  SCICTL2_BITS {       // bit    description
   Uint16 TXINTENA:1;        // 0      Transmit interrupt enable    
   Uint16 RXBKINTENA:1;      // 1      Receiver-buffer break enable
   Uint16 rsvd:4;            // 5:2    reserved
   Uint16 TXEMPTY:1;         // 6      Transmitter empty flag
   Uint16 TXRDY:1;           // 7      Transmitter ready flag  
   Uint16 rsvd1:8;           // 15:8   reserved

}; 

union SCICTL2_REG {
   Uint16               all;
   struct SCICTL2_BITS  bit;
};

//---------------------------------------------------
// SCIRXST Receiver status register bit definitions:
//

struct  SCIRXST_BITS {       // bit    description
   Uint16 rsvd:1;            // 0      reserved
   Uint16 RXWAKE:1;          // 1      Receiver wakeup detect flag
   Uint16 PE:1;              // 2      Parity error flag
   Uint16 OE:1;              // 3      Overrun error flag
   Uint16 FE:1;              // 4      Framing error flag
   Uint16 BRKDT:1;           // 5      Break-detect flag   
   Uint16 RXRDY:1;           // 6      Receiver ready flag
   Uint16 RXERROR:1;         // 7      Receiver error flag

}; 

union SCIRXST_REG {
   Uint16               all;
   struct SCIRXST_BITS  bit;
};

//----------------------------------------------------
// SCIRXBUF Receiver Data Buffer with FIFO bit definitions:
// 

struct  SCIRXBUF_BITS {      // bits   description
   Uint16 RXDT:8;            // 7:0    Receive word
   Uint16 rsvd:6;            // 13:8   reserved
   Uint16 SCIFFPE:1;         // 14     SCI PE error in FIFO mode
   Uint16 SCIFFFE:1;         // 15     SCI FE error in FIFO mode
};

union SCIRXBUF_REG {
   Uint16                all;
   struct SCIRXBUF_BITS  bit;
};

//--------------------------------------------------
// SCIPRI Priority control register bit definitions:
// 
//
                                                   
struct  SCIPRI_BITS {        // bit    description
   Uint16 rsvd:3;            // 2:0    reserved
   Uint16 FREE:1;            // 3      Free emulation suspend mode
   Uint16 SOFT:1;            // 4      Soft emulation suspend mode
   Uint16 rsvd1:3;           // 7:5    reserved
}; 

union SCIPRI_REG {
   Uint16              all;
   struct SCIPRI_BITS  bit;
};

//-------------------------------------------------
// SCI FIFO Transmit register bit definitions:
// 
//
                                                  
struct  SCIFFTX_BITS {       // bit    description
   Uint16 TXFFIL:5;          // 4:0    Interrupt level
   Uint16 TXFFIENA:1;        // 5      Interrupt enable
   Uint16 TXFFINTCLR:1;      // 6      Clear INT flag
   Uint16 TXFFINT:1;         // 7      INT flag
   Uint16 TXFFST:5;          // 12:8   FIFO status
   Uint16 TXFIFOXRESET:1;    // 13     FIFO reset
   Uint16 SCIFFENA:1;        // 14     Enhancement enable
   Uint16 SCIRST:1;          // 15     SCI reset rx/tx channels 

}; 

union SCIFFTX_REG {
   Uint16               all;
   struct SCIFFTX_BITS  bit;
};

//------------------------------------------------
// SCI FIFO recieve register bit definitions:
// 
//
                                               
struct  SCIFFRX_BITS {       // bits   description
   Uint16 RXFFIL:5;          // 4:0    Interrupt level
   Uint16 RXFFIENA:1;        // 5      Interrupt enable
   Uint16 RXFFINTCLR:1;      // 6      Clear INT flag
   Uint16 RXFFINT:1;         // 7      INT flag
   Uint16 RXFFST:5;          // 12:8   FIFO status
   Uint16 RXFIFORESET:1;     // 13     FIFO reset
   Uint16 RXFFOVRCLR:1;      // 14     Clear overflow
   Uint16 RXFFOVF:1;         // 15     FIFO overflow

}; 

union SCIFFRX_REG {
   Uint16               all;
   struct SCIFFRX_BITS  bit;
};

// SCI FIFO control register bit definitions:
struct  SCIFFCT_BITS {     // bits   description
   Uint16 FFTXDLY:8;         // 7:0    FIFO transmit delay
   Uint16 rsvd:5;            // 12:8   reserved
   Uint16 CDC:1;             // 13     Auto baud mode enable
   Uint16 ABDCLR:1;          // 14     Auto baud clear
   Uint16 ABD:1;             // 15     Auto baud detect
};

union SCIFFCT_REG {
   Uint16               all;
   struct SCIFFCT_BITS  bit;
};

//---------------------------------------------------------------------------
// SCI Register File:
//
struct  SCI_REGS {
   union SCICCR_REG     SCICCR;     // Communications control register
   union SCICTL1_REG    SCICTL1;    // Control register 1
   Uint16               SCIHBAUD;   // Baud rate (high) register
   Uint16               SCILBAUD;   // Baud rate (low) register
   union SCICTL2_REG    SCICTL2;    // Control register 2
   union SCIRXST_REG    SCIRXST;    // Recieve status register
   Uint16               SCIRXEMU;   // Recieve emulation buffer register
   union SCIRXBUF_REG   SCIRXBUF;   // Recieve data buffer  
   Uint16               rsvd1;      // reserved
   Uint16               SCITXBUF;   // Transmit data buffer 
   union SCIFFTX_REG    SCIFFTX;    // FIFO transmit register
   union SCIFFRX_REG    SCIFFRX;    // FIFO recieve register
   union SCIFFCT_REG    SCIFFCT;    // FIFO control register
   Uint16               rsvd2;      // reserved
   Uint16               rsvd3;      // reserved
   union SCIPRI_REG     SCIPRI;     // FIFO Priority control   
};

//---------------------------------------------------------------------------
// SPI Individual Register Bit Definitions:
//
// SPI FIFO Transmit register bit    definitions:
struct  SPIFFTX_BITS {       // bit    description
   Uint16 TXFFIL:5;          // 4:0    Interrupt level
   Uint16 TXFFIENA:1;        // 5      Interrupt enable
   Uint16 TXFFINTCLR:1;      // 6      Clear INT flag
   Uint16 TXFFINT:1;         // 7      INT flag
   Uint16 TXFFST:5;          // 12:8   FIFO status
   Uint16 TXFIFO:1;          // 13     FIFO reset
   Uint16 SPIFFENA:1;        // 14     Enhancement enable
   Uint16 SPIRST:1;          // 15     Reset SPI
}; 

union SPIFFTX_REG {
   Uint16               all;
   struct SPIFFTX_BITS  bit;
};

//--------------------------------------------
// SPI FIFO recieve register bit definitions:
// 
//
struct  SPIFFRX_BITS {       // bits   description
   Uint16 RXFFIL:5;          // 4:0    Interrupt level
   Uint16 RXFFIENA:1;        // 5      Interrupt enable
   Uint16 RXFFINTCLR:1;      // 6      Clear INT flag
   Uint16 RXFFINT:1;         // 7      INT flag
   Uint16 RXFFST:5;          // 12:8   FIFO status
   Uint16 RXFIFORESET:1;     // 13     FIFO reset
   Uint16 RXFFOVFCLR:1;      // 14     Clear overflow
   Uint16 RXFFOVF:1;         // 15     FIFO overflow

}; 

union SPIFFRX_REG {
   Uint16               all;
   struct SPIFFRX_BITS  bit;
};

//--------------------------------------------
// SPI FIFO control register bit definitions:
// 
//
struct  SPIFFCT_BITS {       // bits   description
   Uint16 TXDLY:8;           // 7:0    FIFO transmit delay
   Uint16 rsvd:8;            // 15:8   reserved
};

union SPIFFCT_REG {
   Uint16               all;
   struct SPIFFCT_BITS  bit;
};
   
//---------------------------------------------
// SPI configuration register bit definitions:
// 
//
struct  SPICCR_BITS {        // bits   description
   Uint16 SPICHAR:4;         // 3:0    Character length control
   Uint16 SPILBK:1;          // 4      Loop-back enable/disable
   Uint16 rsvd1:1;           // 5      reserved
   Uint16 CLKPOLARITY:1;     // 6      Clock polarity
   Uint16 SPISWRESET:1;      // 7      SPI SW Reset
   Uint16 rsvd2:8;           // 15:8   reserved   
};

union SPICCR_REG {
   Uint16              all;
   struct SPICCR_BITS  bit;
};

//-------------------------------------------------
// SPI operation control register bit definitions:
// 
//
struct  SPICTL_BITS {        // bits   description
   Uint16 SPIINTENA:1;       // 0      Interrupt enable
   Uint16 TALK:1;            // 1      Master/Slave transmit enable
   Uint16 MASTER_SLAVE:1;    // 2      Network control mode
   Uint16 CLK_PHASE:1;       // 3      Clock phase select
   Uint16 OVERRUNINTENA:1;   // 4      Overrun interrupt enable
   Uint16 rsvd:11;           // 15:5   reserved      
};

union SPICTL_REG {
   Uint16              all;
   struct SPICTL_BITS  bit;
};

//--------------------------------------
// SPI status register bit definitions:
// 
//
struct  SPISTS_BITS {        // bits   description
   Uint16 rsvd1:5;           // 4:0    reserved
   Uint16 BUFFULL_FLAG:1;    // 5      SPI transmit buffer full flag
   Uint16 INT_FLAG:1;        // 6      SPI interrupt flag
   Uint16 OVERRUN_FLAG:1;    // 7      SPI reciever overrun flag
   Uint16 rsvd2:8;           // 15:8   reserved   
};

union SPISTS_REG {
   Uint16              all;
   struct SPISTS_BITS  bit;
}; 

//------------------------------------------------
// SPI priority control register bit definitions:
// 
//
struct  SPIPRI_BITS {        // bits   description
   Uint16 rsvd1:4;           // 3:0    reserved
   Uint16 FREE:1;            // 4      Free emulation mode control
   Uint16 SOFT:1;            // 5      Soft emulation mode control
   Uint16 PRIORITY:1;        // 6      Interrupt priority select
   Uint16 rsvd2:9;           // 15:7   reserved
};

union SPIPRI_REG {
   Uint16              all;
   struct SPIPRI_BITS  bit;
};       

//---------------------------------------------------------------------------
// SPI Register File:
//
struct  SPI_REGS { 
   union SPICCR_REG     SPICCR;      // Configuration register 
   union SPICTL_REG     SPICTL;      // Operation control register
   union SPISTS_REG     SPISTS;      // Status register
   Uint16               rsvd1;       // reserved
   Uint16               SPIBRR;      // Baud Rate
   Uint16               rsvd2;       // reserved
   Uint16               SPIRXEMU;    // Emulation buffer
   Uint16               SPIRXBUF;    // Serial input buffer
   Uint16               SPITXBUF;    // Serial output buffer
   Uint16               SPIDAT;      // Serial data 
   union SPIFFTX_REG    SPIFFTX;     // FIFO transmit register
   union SPIFFRX_REG    SPIFFRX;     // FIFO recieve register
   union SPIFFCT_REG    SPIFFCT;     // FIFO control register
   Uint16               rsvd3[2];    // reserved
   union SPIPRI_REG     SPIPRI;      // FIFO Priority control
};

//---------------------------------------------------------------------------
// System Control Individual Register Bit Definitions:
//


// PLL Status Register
struct PLLSTS_BITS   {    // bits  description
   Uint16 PLLLOCKS:1;     // 0     PLL lock status
   Uint16 rsvd1:1;        // 1     reserved
   Uint16 PLLOFF:1;       // 2     PLL off bit
   Uint16 MCLKSTS:1;      // 3     Missing clock status bit
   Uint16 MCLKCLR:1;      // 4     Missing clock clear bit
   Uint16 OSCOFF:1;       // 5     Oscillator clock off
   Uint16 MCLKOFF:1;      // 6     Missing clock detect
   Uint16 DIVSEL:2;       // 7     Divide Select
   Uint16 rsvd2:7;        // 15:7  reserved
};

union PLLSTS_REG {
   Uint16              all;
   struct PLLSTS_BITS  bit;
};

// High speed peripheral clock register bit definitions:
struct HISPCP_BITS  {   // bits  description
   Uint16 HSPCLK:3;     // 2:0   Rate relative to SYSCLKOUT
   Uint16 rsvd1:13;     // 15:3  reserved
};

union HISPCP_REG {
   Uint16              all;
   struct HISPCP_BITS  bit;
};

// Low speed peripheral clock register bit definitions:
struct LOSPCP_BITS  {   // bits  description
   Uint16 LSPCLK:3;     // 2:0   Rate relative to SYSCLKOUT
   Uint16 rsvd1:13;     // 15:3  reserved
};

union LOSPCP_REG {
   Uint16              all;
   struct LOSPCP_BITS  bit;
};

// Peripheral clock control register 0 bit definitions:
struct PCLKCR0_BITS  {   // bits  description
   Uint16 rsvd1:2;       // 1:0   reserved
   Uint16 TBCLKSYNC:1;   // 2     EWPM Module TBCLK enable/sync
   Uint16 ADCENCLK:1;    // 3     Enable high speed clk to ADC
   Uint16 I2CAENCLK:1;   // 4     Enable SYSCLKOUT to I2C-A
   Uint16 SCICENCLK:1;   // 5     Enalbe low speed clk to SCI-C
   Uint16 rsvd2:2;       // 7:6   reserved
   Uint16 SPIAENCLK:1;   // 8     Enable low speed clk to SPI-A
   Uint16 rsvd3:1;       // 9     reserved
   Uint16 SCIAENCLK:1;   // 10    Enable low speed clk to SCI-A
   Uint16 SCIBENCLK:1;   // 11    Enable low speed clk to SCI-B
   Uint16 MCBSPAENCLK:1; // 12    Enable low speed clk to McBSP-A
   Uint16 MCBSPBENCLK:1; // 13    Enable low speed clk to McBSP-B
   Uint16 ECANAENCLK:1;  // 14    Enable system clk to eCAN-A
   Uint16 ECANBENCLK:1;  // 15    Enable system clk to eCAN-B
};

union PCLKCR0_REG {
   Uint16              all;
   struct PCLKCR0_BITS bit;
};

// Peripheral clock control register 1 bit definitions:
struct PCLKCR1_BITS  {    // bits  description
   Uint16 EPWM1ENCLK:1;   // 0     Enable SYSCLKOUT to EPWM1
   Uint16 EPWM2ENCLK:1;   // 1     Enable SYSCLKOUT to EPWM2
   Uint16 EPWM3ENCLK:1;   // 2     Enable SYSCLKOUT to EPWM3
   Uint16 EPWM4ENCLK:1;   // 3     Enable SYSCLKOUT to EPWM4
   Uint16 EPWM5ENCLK:1;   // 4     Enable SYSCLKOUT to EPWM5
   Uint16 EPWM6ENCLK:1;   // 5     Enable SYSCLKOUT to EPWM6
   Uint16 rsvd1:2;        // 7:6   reserved
   Uint16 ECAP1ENCLK:1;   // 8     Enable SYSCLKOUT to ECAP1
   Uint16 ECAP2ENCLK:1;   // 9     Enable SYSCLKOUT to ECAP2
   Uint16 ECAP3ENCLK:1;   // 10    Enable SYSCLKOUT to ECAP3
   Uint16 ECAP4ENCLK:1;   // 11    Enable SYSCLKOUT to ECAP4
   Uint16 ECAP5ENCLK:1;   // 12    Enable SYSCLKOUT to ECAP5
   Uint16 ECAP6ENCLK:1;   // 13    Enable SYSCLKOUT to ECAP6
   Uint16 EQEP1ENCLK:1;   // 14    Enable SYSCLKOUT to EQEP1
   Uint16 EQEP2ENCLK:1;   // 15    Enable SYSCLKOUT to EQEP2
};

union PCLKCR1_REG {
   Uint16              all;
   struct PCLKCR1_BITS bit;
};


// Peripheral clock control register 2 bit definitions:
struct PCLKCR3_BITS  {        // bits  description
   Uint16 rsvd1:8;            // 7:0   reserved
   Uint16 CPUTIMER0ENCLK:1;   // 8     Enable SYSCLKOUT to CPU-Timer 0
   Uint16 CPUTIMER1ENCLK:1;   // 9     Enable SYSCLKOUT to CPU-Timer 1
   Uint16 CPUTIMER2ENCLK:1;   // 10    Enable SYSCLKOUT to CPU-Timer 2
   Uint16 DMAENCLK:1;         // 11    Enable the DMA clock
   Uint16 XINTFENCLK:1;       // 12    Enable SYSCLKOUT to XINTF
   Uint16 GPIOINENCLK:1;      //     Enable GPIO input clock
   Uint16 rsvd2:2;            // 15:14 reserved
};

union PCLKCR3_REG {
   Uint16              all;
   struct PCLKCR3_BITS bit;
};



// PLL control register bit definitions:
struct PLLCR_BITS {      // bits  description
   Uint16 DIV:4;         // 3:0   Set clock ratio for the PLL
   Uint16 rsvd1:12;      // 15:4  reserved
};

union PLLCR_REG {
   Uint16             all;
   struct PLLCR_BITS  bit;
};

// Low Power Mode 0 control register bit definitions:
struct LPMCR0_BITS {     // bits  description
   Uint16 LPM:2;         // 1:0   Set the low power mode
   Uint16 QUALSTDBY:6;   // 7:2   Qualification
   Uint16 rsvd1:7;       // 14:8  reserved
   Uint16 WDINTE:1;      // 15    Enables WD to wake the device from STANDBY
};

union LPMCR0_REG {
   Uint16              all;
   struct LPMCR0_BITS  bit;
};

//---------------------------------------------------------------------------
// System Control Register File:
//
struct SYS_CTRL_REGS {
   Uint16              rsvd7;     // 0
   union   PLLSTS_REG  PLLSTS;    // 1
   Uint16              rsvd1[8];  // 2-9
   union   HISPCP_REG  HISPCP;    // 10: High-speed peripheral clock pre-scaler
   union   LOSPCP_REG  LOSPCP;    // 11: Low-speed peripheral clock pre-scaler
   union   PCLKCR0_REG PCLKCR0;   // 12: Peripheral clock control register
   union   PCLKCR1_REG PCLKCR1;   // 13: Peripheral clock control register
   union   LPMCR0_REG  LPMCR0;    // 14: Low-power mode control register 0
   Uint16              rsvd2;     // 15: reserved
   union   PCLKCR3_REG PCLKCR3;   // 16: Peripheral clock control register
   union   PLLCR_REG   PLLCR;     // 17: PLL control register
   // No bit definitions are defined for SCSR because
   // a read-modify-write instruction can clear the WDOVERRIDE bit
   Uint16              SCSR;      // 18: System control and status register
   Uint16              WDCNTR;    // 19: WD counter register
   Uint16              rsvd4;     // 20
   Uint16              WDKEY;     // 21: WD reset key register
   Uint16              rsvd5[3];  // 22-24
   // No bit definitions are defined for WDCR because
   // the proper value must be written to the WDCHK field
   // whenever writing to this register.
   Uint16              WDCR;      // 25: WD timer control register
   Uint16              rsvd6[6];  // 26-31
};


/* --------------------------------------------------- */
/* CSM Registers                                       */
/*                                                     */
/* ----------------------------------------------------*/

/* CSM Status & Control register bit definitions */
struct  CSMSCR_BITS {      // bit   description
   Uint16     SECURE:1;    // 0     Secure flag
   Uint16     rsvd1:14;    // 14-1  reserved
   Uint16     FORCESEC:1;  // 15    Force Secure control bit

};

/* Allow access to the bit fields or entire register */
union CSMSCR_REG {
   Uint16             all;
   struct CSMSCR_BITS bit;
};

/* CSM Register File */
struct  CSM_REGS {
   Uint16           KEY0;    // KEY reg bits 15-0
   Uint16           KEY1;    // KEY reg bits 31-16
   Uint16           KEY2;    // KEY reg bits 47-32
   Uint16           KEY3;    // KEY reg bits 63-48
   Uint16           KEY4;    // KEY reg bits 79-64
   Uint16           KEY5;    // KEY reg bits 95-80
   Uint16           KEY6;    // KEY reg bits 111-96
   Uint16           KEY7;    // KEY reg bits 127-112
   Uint16           rsvd1;   // reserved
   Uint16           rsvd2;   // reserved
   Uint16           rsvd3;   // reserved
   Uint16           rsvd4;   // reserved
   Uint16           rsvd5;   // reserved
   Uint16           rsvd6;   // reserved
   Uint16           rsvd7;   // reserved
   union CSMSCR_REG CSMSCR;  // CSM Status & Control register
};

/* Password locations */
struct  CSM_PWL {
   Uint16   PSWD0;  // PSWD bits 15-0
   Uint16   PSWD1;  // PSWD bits 31-16
   Uint16   PSWD2;  // PSWD bits 47-32
   Uint16   PSWD3;  // PSWD bits 63-48
   Uint16   PSWD4;  // PSWD bits 79-64
   Uint16   PSWD5;  // PSWD bits 95-80
   Uint16   PSWD6;  // PSWD bits 111-96
   Uint16   PSWD7;  // PSWD bits 127-112
};

/* Flash Option Register bit definitions */
struct  FOPT_BITS {       // bit   description
   Uint16     ENPIPE:1;   // 0     Enable Pipeline Mode
   Uint16     rsvd:15;    // 1-15  reserved
};

/* Allow access to the bit fields or entire register */
union FOPT_REG {
   Uint16           all;
   struct FOPT_BITS bit;
};

/* Flash Power Modes Register bit definitions */
struct  FPWR_BITS {       // bit   description
   Uint16     PWR:2;      // 0-1   Power Mode bits
   Uint16     rsvd:14;    // 2-15  reserved
};

/* Allow access to the bit fields or entire register */
union FPWR_REG {
   Uint16           all;
   struct FPWR_BITS bit;
};


/* Flash Status Register bit definitions */
struct  FSTATUS_BITS {       // bit   description
   Uint16     PWRS:2;        // 0-1   Power Mode Status bits
   Uint16     STDBYWAITS:1;  // 2     Bank/Pump Sleep to Standby Wait Counter Status bits
   Uint16     ACTIVEWAITS:1; // 3     Bank/Pump Standby to Active Wait Counter Status bits
   Uint16     rsvd1:4;       // 4-7   reserved
   Uint16     V3STAT:1;      // 8     VDD3V Status Latch bit
   Uint16     rsvd2:7;       // 9-15  reserved
};

/* Allow access to the bit fields or entire register */
union FSTATUS_REG {
   Uint16              all;
   struct FSTATUS_BITS bit;
};

/* Flash Sleep to Standby Wait Counter Register bit definitions */
struct  FSTDBYWAIT_BITS {    // bit   description
   Uint16     STDBYWAIT:9;   // 0-8   Bank/Pump Sleep to Standby Wait Count bits
   Uint16     rsvd:7;        // 9-15  reserved
};

/* Allow access to the bit fields or entire register */
union FSTDBYWAIT_REG {
   Uint16                 all;
   struct FSTDBYWAIT_BITS bit;
};

/* Flash Standby to Active Wait Counter Register bit definitions */
struct  FACTIVEWAIT_BITS {   // bit   description
   Uint16     ACTIVEWAIT:9;  // 0-8   Bank/Pump Standby to Active Wait Count bits
   Uint16     rsvd:7;        // 9-15  reserved
};

/* Allow access to the bit fields or entire register */
union FACTIVEWAIT_REG {
   Uint16                  all;
   struct FACTIVEWAIT_BITS bit;
};

/* Bank Read Access Wait State Register bit definitions */
struct  FBANKWAIT_BITS {     // bit   description
   Uint16     RANDWAIT:4;    // 0-3   Flash Random Read Wait State bits
   Uint16     rsvd1:4;       // 4-7   reserved
   Uint16     PAGEWAIT:4;    // 8-11  Flash Paged Read Wait State bits
   Uint16     rsvd2:4;       // 12-15 reserved
};

/* Allow access to the bit fields or entire register */
union FBANKWAIT_REG {
   Uint16                all;
   struct FBANKWAIT_BITS bit;
};

/* OTP Read Access Wait State Register bit definitions */
struct  FOTPWAIT_BITS {      // bit   description
   Uint16     OTPWAIT:5;     // 0-4   OTP Read Wait State bits
   Uint16     rsvd:11;       // 5-15  reserved
};

/* Allow access to the bit fields or entire register */
union FOTPWAIT_REG {
   Uint16               all;
   struct FOTPWAIT_BITS bit;
};


struct FLASH_REGS {
   union FOPT_REG        FOPT;        // Option Register
   Uint16                rsvd1;       // reserved
   union FPWR_REG        FPWR;        // Power Modes Register
   union FSTATUS_REG     FSTATUS;     // Status Register
   union FSTDBYWAIT_REG  FSTDBYWAIT;  // Pump/Bank Sleep to Standby Wait State Register
   union FACTIVEWAIT_REG FACTIVEWAIT; // Pump/Bank Standby to Active Wait State Register
   union FBANKWAIT_REG   FBANKWAIT;   // Bank Read Access Wait State Register
   union FOTPWAIT_REG    FOTPWAIT;    // OTP Read Access Wait State Register
};

// XINTF timing register bit definitions:
struct XTIMING_BITS {    // bits  description
   Uint16 XWRTRAIL:2;    // 1:0   Write access trail timing
   Uint16 XWRACTIVE:3;   // 4:2   Write access active timing
   Uint16 XWRLEAD:2;     // 6:5   Write access lead timing
   Uint16 XRDTRAIL:2;    // 8:7   Read access trail timing
   Uint16 XRDACTIVE:3;   // 11:9  Read access active timing
   Uint16 XRDLEAD:2;     // 13:12 Read access lead timing
   Uint16 USEREADY:1;    // 14    Extend access using HW waitstates
   Uint16 READYMODE:1;   // 15    Ready mode
   Uint16 XSIZE:2;       // 17:16 XINTF bus width - must be written as 11b
   Uint16 rsvd1:4;       // 21:18 reserved
   Uint16 X2TIMING:1;    // 22    Double lead/active/trail timing
   Uint16 rsvd3:9;       // 31:23 reserved
};

union XTIMING_REG {
   Uint32               all;
   struct XTIMING_BITS  bit;
};

// XINTF control register bit definitions:
struct XINTCNF2_BITS {    // bits  description
   Uint16 WRBUFF:2;       // 1:0   Write buffer depth
   Uint16 CLKMODE:1;      // 2     Ratio for XCLKOUT with respect to XTIMCLK
   Uint16 CLKOFF:1;       // 3     Disable XCLKOUT
   Uint16 rsvd1:2;        // 5:4   reserved
   Uint16 WLEVEL:2;       // 7:6   Current level of the write buffer
   Uint16 rsvd2:1;        // 8     reserved
   Uint16 HOLD:1;         // 9     Hold enable/disable
   Uint16 HOLDS:1;        // 10    Current state of HOLDn input
   Uint16 HOLDAS:1;       // 11    Current state of HOLDAn output
   Uint16 rsvd3:4;        // 15:12 reserved
   Uint16 XTIMCLK:3;      // 18:16 Ratio for XTIMCLK
   Uint16 rsvd4:13;       // 31:19 reserved
};

union XINTCNF2_REG {
   Uint32                all;
   struct XINTCNF2_BITS  bit;
};

// XINTF bank switching register bit definitions:
struct XBANK_BITS {      // bits  description
   Uint16  BANK:3;       // 2:0   Zone for which banking is enabled
   Uint16  BCYC:3;       // 5:3   XTIMCLK cycles to add
   Uint16  rsvd:10;      // 15:6  reserved
};

union XBANK_REG {
   Uint16             all;
   struct XBANK_BITS  bit;
};

struct XRESET_BITS {
    Uint16  XHARDRESET:1;
    Uint16  rsvd1:15;
};

union XRESET_REG {
    Uint16            all;
    struct XRESET_BITS bit;
};

//---------------------------------------------------------------------------
// XINTF Register File:
//
struct XINTF_REGS {
   union XTIMING_REG XTIMING0;
   Uint32  rsvd1[5];
   union XTIMING_REG XTIMING6;
   union XTIMING_REG XTIMING7;
   Uint32  rsvd2[2];
   union XINTCNF2_REG XINTCNF2;
   Uint32  rsvd3;
   union XBANK_REG    XBANK;
   Uint16  rsvd4;
   Uint16  XREVISION;
   Uint16  rsvd5[2];
   union XRESET_REG   XRESET;
};

//---------------------------------------------------------------------------

struct XINTCR_BITS {
    Uint16   ENABLE:1;    // 0      enable/disable
    Uint16   rsvd1:1;     // 1      reserved
    Uint16   POLARITY:2;  // 3:2    pos/neg, both triggered
    Uint16   rsvd2:12;    //15:4    reserved
};

union XINTCR_REG {
   Uint16               all;
   struct XINTCR_BITS   bit;
};  

struct XNMICR_BITS {
    Uint16   ENABLE:1;    // 0      enable/disable
    Uint16   SELECT:1;    // 1      Timer 1 or XNMI connected to int13
    Uint16   POLARITY:2;  // 3:2    pos/neg, or both triggered
    Uint16   rsvd2:12;    // 15:4   reserved
};

union XNMICR_REG {
   Uint16               all;
   struct XNMICR_BITS   bit;
};  

//---------------------------------------------------------------------------
// External Interrupt Register File:
//
struct XINTRUPT_REGS {
   union XINTCR_REG XINT1CR;
   union XINTCR_REG XINT2CR;
   union XINTCR_REG XINT3CR;
   union XINTCR_REG XINT4CR;
   union XINTCR_REG XINT5CR;
   union XINTCR_REG XINT6CR;
   union XINTCR_REG XINT7CR;
   union XNMICR_REG XNMICR;
   Uint16           XINT1CTR;
   Uint16           XINT2CTR;
   Uint16           rsvd[5];
   Uint16           XNMICTR;
};



//===========================================================================
// PIE Interrupt Vector Table Definition:
//===========================================================================

// Create a user type called PINT (pointer to interrupt):

typedef interrupt void(*PINT)(void);

// Define Vector Table:
struct PIE_VECT_TABLE {

// Reset is never fetched from this table.
// It will always be fetched from 0x3FFFC0 in
// boot ROM

      PINT     PIE1_RESERVED;
      PINT     PIE2_RESERVED;
      PINT     PIE3_RESERVED;
      PINT     PIE4_RESERVED;
      PINT     PIE5_RESERVED;
      PINT     PIE6_RESERVED;
      PINT     PIE7_RESERVED;
      PINT     PIE8_RESERVED;
      PINT     PIE9_RESERVED;
      PINT     PIE10_RESERVED;
      PINT     PIE11_RESERVED;
      PINT     PIE12_RESERVED;
      PINT     PIE13_RESERVED;

// Non-Peripheral Interrupts:
      PINT     XINT13;    // XINT13 / CPU-Timer1
      PINT     TINT2;     // CPU-Timer2
      PINT     DATALOG;   // Datalogging interrupt
      PINT     RTOSINT;   // RTOS interrupt
      PINT     EMUINT;    // Emulation interrupt
      PINT     XNMI;      // Non-maskable interrupt
      PINT     ILLEGAL;   // Illegal operation TRAP
      PINT     USER1;     // User Defined trap 1
      PINT     USER2;     // User Defined trap 2
      PINT     USER3;     // User Defined trap 3
      PINT     USER4;     // User Defined trap 4
      PINT     USER5;     // User Defined trap 5
      PINT     USER6;     // User Defined trap 6
      PINT     USER7;     // User Defined trap 7
      PINT     USER8;     // User Defined trap 8
      PINT     USER9;     // User Defined trap 9
      PINT     USER10;    // User Defined trap 10
      PINT     USER11;    // User Defined trap 11
      PINT     USER12;    // User Defined trap 12

// Group 1 PIE Peripheral Vectors:
      PINT     SEQ1INT;
      PINT     SEQ2INT;
      PINT     rsvd1_3;
      PINT     XINT1;
      PINT     XINT2;
      PINT     ADCINT;    // ADC
      PINT     TINT0;     // Timer 0
      PINT     WAKEINT;   // WD

// Group 2 PIE Peripheral Vectors:
      PINT     EPWM1_TZINT; // EPWM-1
      PINT     EPWM2_TZINT; // EPWM-2
      PINT     EPWM3_TZINT; // EPWM-3
      PINT     EPWM4_TZINT; // EPWM-4
      PINT     EPWM5_TZINT; // EPWM-5
      PINT     EPWM6_TZINT; // EPWM-6
      PINT     rsvd2_7;
      PINT     rsvd2_8;

// Group 3 PIE Peripheral Vectors:
      PINT     EPWM1_INT;  // EPWM-1
      PINT     EPWM2_INT;  // EPWM-2
      PINT     EPWM3_INT;  // EPWM-3
      PINT     EPWM4_INT;  // EPWM-4
      PINT     EPWM5_INT;  // EPWM-5
      PINT     EPWM6_INT;  // EPWM-6
      PINT     rsvd3_7;
      PINT     rsvd3_8;

// Group 4 PIE Peripheral Vectors:
      PINT     ECAP1_INT; // ECAP-1
      PINT     ECAP2_INT; // ECAP-2
      PINT     ECAP3_INT; // ECAP-3
      PINT     ECAP4_INT; // ECAP-4
      PINT     ECAP5_INT; // ECAP-5
      PINT     ECAP6_INT; // ECAP-6
      PINT     rsvd4_7;
      PINT     rsvd4_8;

// Group 5 PIE Peripheral Vectors:
      PINT     EQEP1_INT; // EQEP-1
      PINT     EQEP2_INT; // EQEP-2
      PINT     rsvd5_3;
      PINT     rsvd5_4;
      PINT     rsvd5_5;
      PINT     rsvd5_6;
      PINT     rsvd5_7;
      PINT     rsvd5_8;

// Group 6 PIE Peripheral Vectors:
      PINT     SPIRXINTA; // SPI-A
      PINT     SPITXINTA; // SPI-A
      PINT     MRINTB;    // McBSP-B
      PINT     MXINTB;    // McBSP-B
	  PINT     MRINTA;    // McBSP-A
      PINT     MXINTA;    // McBSP-A
      PINT     rsvd6_7;
      PINT     rsvd6_8;

// Group 7 PIE Peripheral Vectors:
      PINT     DINTCH1;   // DMA
      PINT     DINTCH2;   // DMA
      PINT     DINTCH3;   // DMA
      PINT     DINTCH4;   // DMA
      PINT     DINTCH5;   // DMA
      PINT     DINTCH6;   // DMA
      PINT     rsvd7_7;
      PINT     rsvd7_8;

// Group 8 PIE Peripheral Vectors:
      PINT     I2CINT1A;  // I2C-A
      PINT     I2CINT2A;  // I2C-A
      PINT     rsvd8_3;
      PINT     rsvd8_4;
      PINT     SCIRXINTC;  // SCI-C
      PINT     SCITXINTC;  // SCI-C
      PINT     rsvd8_7;
      PINT     rsvd8_8;

// Group 9 PIE Peripheral Vectors:
      PINT     SCIRXINTA;  // SCI-A
      PINT     SCITXINTA;  // SCI-A
      PINT     SCIRXINTB;  // SCI-B
      PINT     SCITXINTB;  // SCI-B
      PINT     ECAN0INTA;  // eCAN-A
      PINT     ECAN1INTA;  // eCAN-A
      PINT     ECAN0INTB;  // eCAN-B
      PINT     ECAN1INTB;  // eCAN-B

// Group 10 PIE Peripheral Vectors:
      PINT     rsvd10_1;
      PINT     rsvd10_2;
      PINT     rsvd10_3;
      PINT     rsvd10_4;
      PINT     rsvd10_5;
      PINT     rsvd10_6;
      PINT     rsvd10_7;
      PINT     rsvd10_8;

// Group 11 PIE Peripheral Vectors:
      PINT     rsvd11_1;
      PINT     rsvd11_2;
      PINT     rsvd11_3;
      PINT     rsvd11_4;
      PINT     rsvd11_5;
      PINT     rsvd11_6;
      PINT     rsvd11_7;
      PINT     rsvd11_8;

// Group 12 PIE Peripheral Vectors:
      PINT     XINT3;      // External interrupt
      PINT     XINT4;
      PINT     XINT5;
      PINT     XINT6;
      PINT     XINT7;
      PINT     rsvd12_6;
      PINT     LVF;        // Latched overflow
      PINT     LUF;        // Latched underflow
};

//---------------------------------------------------------------------------
// Function prototypes and external definitions:
//
void InitCpuTimers(void);
void ConfigCpuTimer(struct CPUTIMER_VARS *Timer, float Freq, float Period);

//---------------------------------------------------------------------------
// Usefull Timer Operations:
//
// Start Timer:
#define StartCpuTimer0()   CpuTimer0Regs.TCR.bit.TSS = 0

// Stop Timer:
#define StopCpuTimer0()   CpuTimer0Regs.TCR.bit.TSS = 1

// Reload Timer With period Value:
#define ReloadCpuTimer0() CpuTimer0Regs.TCR.bit.TRB = 1

// Read 32-Bit Timer Value:
#define ReadCpuTimer0Counter() CpuTimer0Regs.TIM.all

// Read 32-Bit Period Value:
#define ReadCpuTimer0Period() CpuTimer0Regs.PRD.all

// CpuTimer 1 and CpuTimer2 are reserved for DSP BIOS & other RTOS
// Do not use these two timers if you ever plan on integrating
// DSP-BIOS or another realtime OS.
//
// For this reason, comment out the code to manipulate these two timers
// if using DSP-BIOS or another realtime OS.

// Start Timer:
#define StartCpuTimer1()   CpuTimer1Regs.TCR.bit.TSS = 0
#define StartCpuTimer2()   CpuTimer2Regs.TCR.bit.TSS = 0


// Stop Timer:
#define StopCpuTimer1()   CpuTimer1Regs.TCR.bit.TSS = 1
#define StopCpuTimer2()   CpuTimer2Regs.TCR.bit.TSS = 1

// Reload Timer With period Value:
#define ReloadCpuTimer1() CpuTimer1Regs.TCR.bit.TRB = 1
#define ReloadCpuTimer2() CpuTimer2Regs.TCR.bit.TRB = 1

// Read 32-Bit Timer Value:
#define ReadCpuTimer1Counter() CpuTimer1Regs.TIM.all
#define ReadCpuTimer2Counter() CpuTimer2Regs.TIM.all

// Read 32-Bit Period Value:
#define ReadCpuTimer1Period() CpuTimer1Regs.PRD.all
#define ReadCpuTimer2Period() CpuTimer2Regs.PRD.all


#ifdef __cplusplus
}
#endif /* extern "C" */

#endif  // end of LF28335_REGS_H definition

