/****************************************************************************
	File Name	: log_ref.h                   
	Project		: MSK2812 Motion Starter Kit
=============================================================================
	Target Sys  : DSP boards based on TMS320LF2812
	Description : Prototypes for logger and reference generator variales and 
                  functions 
	Originator/s: Technosoft Ltd.
	Status      : OK
=============================================================================
	Copyright � 2006 Technosoft
============================================================================*/
#ifndef __log_ref_h_
#define __log_ref_h_
/****************************************************************************
	Variables prototypes
*****************************************************************************/
/*-------- LOGGER variables -------*/
extern unsigned long log_indx;
extern unsigned long log_count;
extern unsigned long log_size;
extern unsigned long log_time;
extern int *log_p[17];
extern int log_table[];

/*------ REFERENCE variables ------*/
extern long ref_long;
extern unsigned int ref_indx;
extern unsigned int ref_time_val[100];
extern long ref_HL[100];
extern int pos_ref_high;
extern unsigned int pos_ref_low;
extern unsigned int ref_time;
extern unsigned int ref_cycles;
/****************************************************************************
	Functions prototypes
*****************************************************************************/
extern void init_logger(void);
extern void logger(void);
extern void init_reference(void);
extern int reference(void);
/*********************************************************/
#endif 	/* of ifdef __log_ref_h_ */



