% Ensayos para OMEGAFINA
% MRA 02-2023
close all; clear all; clc

%% Carga de los datos recogidos en los archivos de resultados



% velocidad wm seg�n interrupci�n EQEP
omega_isr = load('wm_log.txt');
% omega_isr2 = load('wm_log2.txt');
% contador de inicios de la interrupci�n EQEP 
% eqep_1 = load('eqep_1.txt');
% eqep_2 = load('eqep_2.txt');
% eqep = eqep_1 * 2^16  + eqep_2;
% % % valor QPOSLAT fuera de la interrupci�n EQEP 
% corr1 = load('delta_pos1.txt');
% corr2 = load('delta_pos2.txt');
% corr = corr1 * 2^16  + corr2;
% corr = corr - ((corr>(2^31))*2^32);
% 
% desescalado (paso de Uint16 a float)
omega_isr = omega_isr/65.534-500;
% omega_isr2 = omega_isr2/65.534-500;
% qposlat = qposlat/1638.35 -20;

% represent graf

figure
plot( omega_isr, 'r');
% hold on;
% plot( omega_isr2, 'g');
xlabel('k')
ylabel('\omega (rad/s)')
% ylim([0, 90])
xlim([0, 30000])


% grid
% figure
% stairs( eqep, 'g');
% 
% % plot( eqep_1( 1:2:end) , 'g');
% 
% grid
% figure
% stairs( corr, 'g');
% grid
% %figure
% %histogram(conta_isr, 500)
% 
% %%
% clc
% [1 min( omega_isr)  max( omega_isr) ]
% [2 min( conta_isr)  max( conta_isr) ]
% %[3 min( qposlat)  max( qposlat) ]
% 


fprintf('desviaci�n t�pica velocidad = sigma omega = %f (rad/s) \n', ...
    std( omega_isr) );


% figure;
% my_vel = eqep(2:end)- eqep(1:end -1);
% my_vel = my_vel + 10000 * (my_vel< -5000) - 10000 * (my_vel > 5000);
% stairs(my_vel);
% 
% const float tick_to_radps = (FRECUENCIA_MUESTREO_HZ * 2.0 * 3.141592)/(float) (QEPCNTMAX + 1);
% 
% 
% posCnt = EQep1Regs.QPOSCNT;
% Delta_posCnt = (int32)posCnt - (int32)posCnt_ant;
% if (Delta_posCnt < -QEPCNTMAX/2){
%      Delta_posCnt +=  QEPCNTMAX;
% } else if(Delta_posCnt > QEPCNTMAX/2){
%      Delta_posCnt -= QEPCNTMAX;
% } wm_k =  0.8 * wm_k + 0.2 * (float)(Delta_posCnt) * tick_to_radps;
% /*Calculo bueno de la velocidad*/
% 
% wm_log[logger] = (Uint16) (65.534 *(wm_k + 500));
% eqep_1[logger] = posCnt>>16;
% eqep_2[logger] = posCnt&0xFFFF; 
% delta_pos1[logger] = Delta_posCnt>>16;
% delta_pos2[logger] = Delta_posCnt&0xFFFF;