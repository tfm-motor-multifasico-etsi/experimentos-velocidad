/*=======================================================================================================================
    eQEP for Low-Speed and High-Speed Measuring with Capture Edge Unit and Unit
Time Out methods
=======================================================================================================================*/
// clang-format off

#define  UPEVENTDIV1   0x00
#define  UPEVENTDIV2   0x01
#define  UPEVENTDIV4   0x02
#define  UPEVENTDIV8   0x03
#define  UPEVENTDIV16  0x04

#define  SYSCLKDIV1    0x00
#define  SYSCLKDIV2    0x01
#define  SYSCLKDIV4    0x02
#define  SYSCLKDIV8    0x03
#define  SYSCLKDIV16   0x04
#define  SYSCLKDIV32   0x05
#define  SYSCLKDIV64   0x06
#define  SYSCLKDIV128  0x07

#define  QEPCNTMAX     (10000L - 1)               // EQEP Counter Maximum Value

// clang-format on

#define INITIAL_TIMEOUT 20000UL
// EQEP Interrupt Subroutine    
interrupt void PTC5Feqep_isr() {}

// clang-format off
void PTC5Feqep_start(){
    // GPIO Configure
    EALLOW;                                                        // Enable writing to EALLOW protected registers
    SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK        = 1;                // Enable the SYSCLKOUT to the GPIO
    SysCtrlRegs.PCLKCR1.bit.EQEP1ENCLK         = 1;                // EQEP1 Module is Clocked by the SYSCLKOUT
    GpioCtrlRegs.GPBMUX2.bit.GPIO50            = 1;                // JP3 #13 GPIO50 as EQEP1A(Input)
    GpioCtrlRegs.GPBMUX2.bit.GPIO51            = 1;                // JP3 #14 GPIO51 as EQEP1B(Input)
    GpioCtrlRegs.GPBMUX2.bit.GPIO53            = 1;                // JP3 #14 GPIO53 as EQEP1I(Input)
    EDIS;                                                          // Disable writing to EALLOW protected registers
    // End GPIO Configure
    // QDU Module Configuration
    EQep1Regs.QDECCTL.bit.QSRC                = 00;               // EQEP1 as Quadrature Count Mode
    EQep1Regs.QDECCTL.bit.QAP                 = 0;                // EQEP1A input polarity No Efect
    //EQep1Regs.QDECCTL.bit.QAP               = 1;                 // EQEP1A input negate polarity
    EQep1Regs.QDECCTL.bit.QBP                 = 0;                // EQEP1B input polarity No Efect
    //EQep1Regs.QDECCTL.bit.QBP               = 1;                 // EQEP1B input negate polarity
    EQep1Regs.QDECCTL.bit.QIP                 = 0;                // EQEP1I input polarity No Efect
    EQep1Regs.QDECCTL.bit.QSP                 = 0;                // EQEP1S polarity No Efect
    EQep1Regs.QDECCTL.bit.SWAP                = 0;                // Quadrature-clock inputs are not swaped
    EQep1Regs.QDECCTL.bit.IGATE               = 0;                // Disable gating of index pulse
    EQep1Regs.QDECCTL.bit.XCR                 = 0;                // 2x Resolution Count
    EQep1Regs.QDECCTL.bit.SOEN                = 0;                // Disable position-compare syn output
    EQep1Regs.QDECCTL.bit.SPSEL               = 0;                // Index pin is used for sync output
    // End QDU Module Configuration
    // PCCU Module Configuration
    EQep1Regs.QEPCTL.bit.WDE                = 0;                // Disable the EQEP watchdog timer
    EQep1Regs.QEPCTL.bit.QCLM               = 1;                // EQEP capture latch on Unit Time Out
    EQep1Regs.QEPCTL.bit.QPEN               = 1;                // Enable EQEP position counter
    EQep1Regs.QEPCTL.bit.PCRM               = 1;                // Position Counter Reset on Unit Time Event
    EQep1Regs.QEPCTL.bit.SEI                = 0;                // Strobe Event actions disable
    EQep1Regs.QEPCTL.bit.IEI                = 0;                // Index Event actions disable
    EQep1Regs.QEPCTL.bit.SWI                = 0;                // Software Initialization action enable
    EQep1Regs.QEPCTL.bit.IEL                = 0;                // Index Event Latch Reserved
    EQep1Regs.QEPCTL.bit.SWI                = 0;                // Enable Software initialization
    EQep1Regs.QEPCTL.bit.FREE_SOFT          = 0x2;             // Position Counter is Unaffected by emulation suspend
    // End PCCU Module Configuration
   // EQep1Regs.QPOSCNT                        = 0;
    EQep1Regs.QPOSINIT                       = 0;                // EQEP Counter Initial Position
    EQep1Regs.QPOSMAX                        = QEPCNTMAX;        // EQEP Counter Max Position
    EQep1Regs.QPOSCMP                        = QEPCNTMAX;        // EQEP Position Compare
    EQep1Regs.QCTMR                          = 0;                // EQEP Position Compare
    // Position-Compare Configuration
    EQep1Regs.QPOSCTL.bit.PCSHDW            = 0;                // EQEP Position-Compare Load Inmediate
    EQep1Regs.QPOSCTL.bit.PCLOAD            = 0;                // Position Compare Loads in QPOSCNT=0
    EQep1Regs.QPOSCTL.bit.PCPOL             = 0;                // Polarity of sync output Active High pulse output
    EQep1Regs.QPOSCTL.bit.PCE               = 0;                // Position Compare Disable
    // End Position-Compare Configuration
    // Edge Capture Unit Configuration w_measure = velocfactor * X / dT
    EQep1Regs.QCAPCTL.bit.UPPS                = UPEVENTDIV1;       // EQEP Unit Event /1 (X=1)
    EQep1Regs.QCAPCTL.bit.CCPS                = SYSCLKDIV32;       // EQEP Capture Timer Prescaler /32
    EQep1Regs.QCAPCTL.bit.CEN                 = 0;                 // EQEP Capture is Enable
    // End Edge Capture Unit Configuration
    //UTIME Configuration
    EQep1Regs.QUPRD                         = INITIAL_TIMEOUT;        // Unit Time Out Period
    EQep1Regs.QEPCTL.bit.UTE                = 0;                   // Enable the EQEP Unit Timer
    //End UTIME Configuration
    EALLOW;                                                        // This is needed to write to EALLOW protected registers
    PieVectTable.EQEP1_INT                    = &PTC5Feqep_isr;    // EQEP Interrupt Address
    EDIS;                                                          // Disable writing to EALLOW protected registers

    // Unit Time Out Interrupt
    // IER |= M_INT5;                                                 // Enable EQEP1 CPU-PIEIER5 for INT5 (Group 5)
    // PieCtrlRegs.PIEIER5.bit.INTx1              = 1;                // Enable the EQEP1_INT PIEIER5.1 to interrupt resquest sent to CPU Level
    // PieCtrlRegs.PIEACK.bit.ACK5                = 1;                // Clear the PIEACK of Group 5 for enables Interrupt Resquest at CPU Level
    // EQep1Regs.QEINT.bit.UTO                    = 1;                // Unit Time Out Interrupt Enable
    // End Unit Time Out Interrupt

}
