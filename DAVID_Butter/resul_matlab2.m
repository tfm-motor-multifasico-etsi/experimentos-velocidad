% Ensayos para OMEGAFINA
% MRA 02-2023
close all; clear all; clc

%% Carga de los datos recogidos en los archivos de resultados


file_list_fig1 = ["20", "50", "75"];

fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );

ax = nexttile(tiles);
ax.Box = "off";
ax.Title.String = "Distintas velocidades en r�gimen permanente";
ax.Title.FontSize =20;

for sec= 1:3
    hold on;
    omega = load(file_list_fig1(sec)+"/wm_log.txt");
    omega = omega/65.534-500;
    plt = plot(ax, omega);
    plt.LineWidth = 2;
    ax.YLim = [0, 90];
    ax.YLabel.String = "\omega (rad/s)";
    ax.YLabel.FontSize =20;
    if sec == 3
        ax.XLabel.String = "Instante";
        ax.XLabel.FontSize =20;
    end
    hold off;
end
saveas(fig1,"Reg_perm2",'epsc');
saveas(fig1,"Reg_perm2",'png');      

clear

fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );

ax = nexttile(tiles);
ax.Box = "off";
ax.Title.String = "Velocidad con fallo en el c�lculo";
ax.Title.FontSize =20;

hold on;
omega = load("res_malos/wm_log.txt");
omega = omega/65.534-500;
plt = plot(ax, omega);
plt.LineWidth = 2;
% ax.YLim = [0, 90];
ax.YLabel.String = "\omega (rad/s)";
ax.YLabel.FontSize =20;
ax.XLabel.String = "Instante";
ax.XLabel.FontSize =20;
hold off;
saveas(fig1,"malos",'epsc');
saveas(fig1,"malos",'png');  

clear;

Title_list_fig2 = ["Escal�n positivo","Escal�n negativo"];
file_list_fig2 = ["20_75_4", "30_-20_4"];
fig2 = figure;
fig2.Units = 'normalized';
fig2.Position = [0, 0, 1, 1];
tiles2 = tiledlayout( ...
    fig2, ...
    2, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
for sec= 1:2
    ax2 = nexttile(tiles2);
    omega = load(file_list_fig2(sec)+"/wm_log.txt");
    omega = omega(1:15000);
    omega = omega/65.534-500;
    plt = plot(ax2, omega);
    plt.LineWidth = 2;
    ax2.Title.String = Title_list_fig2(sec);
    ax2.Title.FontSize =20;
    ax2.YLabel.FontSize =20;
    ax2.YLabel.String = "\omega (rad/s)";
    ax2.Box = "off";
    if sec == 2
        ax2.XLabel.FontSize =20;
        ax2.XLabel.String = "Instante";
    end
end
saveas(fig2,"change_2",'epsc');
saveas(fig2,"change_2",'png');

clear

fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );

ax = nexttile(tiles);
ax.Box = "off";
ax.Title.String = "Velocidad con filtro";
ax.Title.FontSize =20;

hold on;
omega = load("20_25/wm_log.txt");
omega = omega/65.534-500;
plt = plot(ax, omega);
plt.LineWidth = 2;
% ax.YLim = [0, 90];
ax.YLabel.String = "\omega (rad/s)";
ax.YLabel.FontSize =20;
ax.XLabel.String = "Instante";
ax.XLabel.FontSize =20;
hold off;
saveas(fig1,"menos_filtro",'epsc');
saveas(fig1,"menos_filtro",'png');  

clear;

% A = 0.99;
% B = 1 - A;
% T = 1/20e3;
% a = tf([B, 0],[1, -A], T);
% b = d2c(a,'zoh');
% c = tf([B, B/T],[1, B/T]);
% d = tf([B/(2-B), (B*2)/(T*(2-B))],[1, (B*2)/(T*(2-B))]);
% f = d2c(a,'tustin');
% hold on;
% bode(b);
% bode(c);
% bode(d);
% legend;
% hold off;