/* ========================================================================================================================
                                      Control predictivo basdo en modelo para m�quina de 5 fases

==========================================================================================================================*/

/* ========================================================================================================================
Basado en el PTC_5F JARI. Dic. 2011
20 Feb.2012
HMGJ: Modificaciones para funcionamiento POST-FALTA m�quina de induccion de 5 fases.
24 Abr.2012
HMGJ: Modificaciones para funcionamiento PRE/POST-FALTA m�quina de induccion de 5 fases.
26 Jul.2012
HMGJ: Decimaci�n de la velocidad que se utiliza para el control de velocidad y el estimador de posici�n.
       wm_d3=wm_k/10
05/03/2015
CMT: Modificaciones para funcionamiento �nicamente en pre-falta.
28/04/2015
CMT: Modificadiones del modelo predictivo para adaptarlo al simulador de Manolo

==========================================================================================================================*/

/* Uso de los triggers:
     - 54: desde el inicio hasta el final de la adquisici�n de datos del datalogger. Tiempo de adquisici�n de resultados
     - 55: desde el inicio hasta el final del while(!stop). Duraci�n del bucle
     - 56: desde el inicio hasta el final de la medici�n de corrientes. Tiempo de medici�n
     - 57: desde el inicio hasta el final del while(timeout). Duraci�n del tiempo muerto
*/

#include "../DSP28335_Regs.h"
#include "../DSP28335_RegDef.h"
#include "../mon28335.h"
#include "math.h"

#include "PTC5F_parameters.c"
#include "PTC5F_eqep1.c"
#include "PTC5F_epwm.c"
#include "PTC5F_adc.c"
#include "PTC5F_tmr0.c"

const float tick_to_radps = (FRECUENCIA_MUESTREO_HZ * 2.0 * 3.141592)/(float) (QEPCNTMAX);

void main (){

     int sv;
     int fds = 1;
     int sopt, sapr;
int cuadr, fila;

    EALLOW;                                      // Enable writing to EALLOW protected registers

     SysCtrlRegs.HISPCP.bit.HSPCLK       = 0;     // HSPCLK = SYSCLKOUT / 1 = 150MHz
     SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 0;     // Stop all the TB clocks
     SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK = 1;     // Enable the SYSCLKOUT to the GPIO

     GpioCtrlRegs.GPBMUX2.bit.GPIO54            = 0;          // Configure GPIO54 (JP3-PIN 19) as digital I/O
     GpioCtrlRegs.GPBDIR.bit.GPIO54            = 1;          // Configure GPIO54 as digital Output (JP3-PIN 19)
     GpioDataRegs.GPBCLEAR.bit.GPIO54          = 1;          // CLEAR the TRIGGER SIGNAL (JP3-PIN 19)


     // LED_1 Configuration
     GpioCtrlRegs.GPAMUX2.bit.GPIO16   = 0;       // Configure JP4#16(GPIO 16) as digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO16    = 1;       //Configure JP4#16(GPIO 16) as digital Output
     GpioDataRegs.GPASET.bit.GPIO16      = 1;       // Turn-off LED_1
     // End LED_1 Configuration

     //salidas que controla rele (KSM)
     // Configuracion de J3-9
     GpioCtrlRegs.GPBMUX2.bit.GPIO48     = 0;   // Configure GPIO48 as digital I/O
     GpioCtrlRegs.GPBDIR.bit.GPIO48     = 1;          //Configure shared pins as digital Output (GPIO 48);
     GpioDataRegs.GPBCLEAR.bit.GPIO48     = 1;          //Enciende los ventiladores

     //salidas que controla rele (KP)
     // Configuracion de J3-10
     GpioCtrlRegs.GPBMUX2.bit.GPIO49     = 0;   // Configure GPIO49 as digital I/O
     GpioCtrlRegs.GPBDIR.bit.GPIO49     = 1;          //Configure shared pins as digital Output (GPIO 49);
     GpioDataRegs.GPBCLEAR.bit.GPIO49     = 1;          //habilita KP

     // LED_2 Configuration
     GpioCtrlRegs.GPAMUX2.bit.GPIO17   = 0;       // Configure JP4#18(GPIO 17) as digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO17    = 1;       //Configure JP4#18(GPIO 17) as digital Output
     GpioDataRegs.GPASET.bit.GPIO17      = 1;       // Turn-off LED_2
     // End LED_1 Configuration

     //############################### SA_UP & SA_DOWN ##########################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 0;       // Configure GPIO0 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO0         = 1;       // Configure GPIO0 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO0          = 1;       // Clear GPIO0

     GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 0;       // Configure GPIO1 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO1         = 1;       // Configure GPIO1 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO1          = 1;       // Clear GPIO1

     //####################################### SB_UP & SB_DOWN ####################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 0;       // Configure GPIO2 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO2         = 1;       // Configure GPIO2 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO2          = 1;       // Clear GPIO2

     GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 0;       // Configure GPIO3 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO3         = 1;       // Configure GPIO3 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO3          = 1;       // Clear GPIO3

     //##################################### SC_UP & SC_DOWN #####################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 0;       // Configure GPIO4 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO4         = 1;       // Configure GPIO4 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO4          = 1;       // Clear GPIO4

     GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 0;       // Configure GPIO5 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO5         = 1;       // Configure GPIO5 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO5          = 1;       // Clear GPIO5

     //################################### SD_UP & SD_DOWN ######################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 0;       // Configure GPIO6 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO6         = 1;       // Configure GPIO6 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO6          = 1;       // Clear GPIO6

     GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 0;       // Configure GPIO7 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO7         = 1;       // Configure GPIO7 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO7          = 1;       // Clear GPIO7

     //#################################### SE_UP & SE_DOWN #########################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 0;       // Configure GPIO8 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO8         = 1;       // Configure GPIO8 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO8          = 1;       // Clear GPIO8

     GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 0;       // Configure GPIO9 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO9         = 1;       // Configure GPIO9 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO9          = 1;       // Clear GPIO9

     //#################################### SF_UP & SF_DOWN #########################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 0;       // Configure GPIO10 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO10        = 1;       // Configure GPIO10 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO10            = 1;       // Clear GPIO10

     GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 0;       // Configure GPIO11 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO11        = 1;       // Configure GPIO11 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO11            = 1;       // Clear GPIO11
     //###########################################################################################

     //#################################### TRIGGER #########################################
     GpioCtrlRegs.GPBMUX2.bit.GPIO55            = 0;          // Configure GPIO54 (JP3-PIN 20) as digital I/O
     GpioCtrlRegs.GPBDIR.bit.GPIO55            = 1;          // Configure GPIO54 as digital Output (JP3-PIN 20)
     GpioDataRegs.GPBCLEAR.bit.GPIO55          = 1;          // CLEAR the TRIGGER SIGNAL (JP3-PIN 20)

     GpioCtrlRegs.GPBMUX2.bit.GPIO56            = 0;          // Configure GPIO54 (JP3-PIN 21) as digital I/O
     GpioCtrlRegs.GPBDIR.bit.GPIO56       = 1;          // Configure GPIO54 as digital Output (JP3-PIN 21)
     GpioDataRegs.GPBCLEAR.bit.GPIO56          = 1;          // CLEAR the TRIGGER SIGNAL (JP3-PIN 21)

     GpioCtrlRegs.GPBMUX2.bit.GPIO57            = 0;          // Configure GPIO54 (JP3-PIN 22) as digital I/O
     GpioCtrlRegs.GPBDIR.bit.GPIO57            = 1;          // Configure GPIO54 as digital Output (JP3-PIN 22)
     GpioDataRegs.GPBCLEAR.bit.GPIO57          = 1;          // CLEAR the TRIGGER SIGNAL (JP3-PIN 22)


     //##############################################################################################
     EDIS;                                        // Disable writing to EALLOW protected registers
   
     Fm          = FRECUENCIA_MUESTREO_HZ;        // Sampling Frequency (Hz)
    Tm          = 1/(float)Fm;                   // Sampling Period (s)
    pwm_period  = 37500000 / (2*Fm);             // timer de micro a 37.5 MHz y up/down counter


     // #################################################################################

     Ia_mir = 0;
     Ib_mir = 0;
     Id_mir = 0;
     Ie_mir = 0;
     Vdc_mir = 0;
     i_alpha = 0;
     i_beta = 0;
     i_x = 0;
     i_y = 0;
     i_alpha_p1 = 0;
     i_alpha_p2 = 0;
     i_beta_p = 0;
     i_x_p = 0;
     i_y_p = 0;
     i_alpha_med = 0;
     i_beta_med = 0;
     i_x_med = 0;
     i_y_med = 0;
     Werror = 0;
     e_km1 = 0;

     Kinte     = (float).5*Tm;

     wm_k     = 0;
     wm_km1 = 0;
     wr = 0;
     inte_k = 0;
     inte_km1 = 0;

     Isd = Isd_ref;
     Isq = 0;
     Iy_ref = 0;
     Ix_ref = 0;
     Ibeta_ref = 0;
     Ialfa_ref = 0;

     e_alpha = 0;
     e_beta = 0;
     e_x = 0;
     e_y = 0;

     wsl= 0;
     we= 0;
     thetae= 0;
     thetaenm1= 0;
     sinthetae= 0;
     costhetae= 1;

     Imax = Irated;

      // -------------Se a�ade para mi funci�n J de coste---------
           sinthetae_p= 0;
          costhetae_p= 1;
          Ibeta_ref_p = 0;
          Ialfa_ref_p = 0;
          thetae_p= 0;
      // ---------------------------------------------------------

     /*// A�adido para el cambio de referencia

     ia = 0;
     ib = 0;
     ia_p = 0;
     ib_p = 0;
     j = 0;
     cero = 0;*/

      // ###################################### INITALITIATION ###########################################


      Vdc          = (float)300;

      timeout = 0;
      mstart  = 0;


//  Start Peripherics

      PTC5Fepwm5F_start();
      PTC5Feqep_start();
      PTC5Fadc_start();
      PTC5Fcputmr0_start();

//      End Start Peripherics

     // Hist�ricos
     for (sv = 0; sv<CANTIDAD_LOG; sv++ )
     {
          //ias[sv] = 0;
          //ibs[sv] = 0;
          //ids[sv] = 0;
          //ies[sv] = 0;
          //ids_m[sv] = 0;
          //iqs_m[sv] = 0;
          //wmech1[sv] = 0;
          //we_m[sv] = 0;
//          svopt[sv] = 0;  //A�ADIDO POR MI
//          svapr[sv] = 0;  //A�ADIDO POR MI
          //ias_p1[sv] = 0;  //A�ADIDO POR MI
          //ias_p2[sv] = 0;  //A�ADIDO POR MI
          wm_log[sv] = 0;
          // eqep_1[sv] = 0;
          // eqep_2[sv] = 0;
          // delta_pos1[sv] = 0;
          // delta_pos2[sv] = 0;
//          vfila[sv] = 0;
     }


   // Parte del modelo predictivo correspondiente a las tensiones. Se ejecuta solo una vez al iniciar
   //for(sv = 0; sv<31; sv++)
   for(sv = 0; sv<11; sv++) // retocado para 10vv L + 2vv Z (o 10vv M + 2vv Z)
   {
          Usa[sv] = Tm*B11*Vdc*vsa[sv];
          Usb[sv] = Tm*B22*Vdc*vsb[sv];
          Usx[sv] = Tm*B33*Vdc*vsx[sv];
          Usy[sv] = Tm*B44*Vdc*vsy[sv];
   }

   // Multiplicaci�n de matriz A por Tm
         TmA11 = Tm*A11;
         TmA12 = Tm*A12;
         TmA21 = Tm*A21;
         TmA22 = Tm*A22;
         TmA33 = Tm*A33;
         TmA44 = Tm*A44;
// nuevas variables
UmTmA11 = 1 + TmA11;
UmTmA22 = 1 + TmA22;
H_alpha = 0;
H_beta  = 0;
g_alfa  = 0;
g_beta  = 0;
signoa  = 0;
signob  = 0;

U_r2   = 0.005;//0.01;//0.05;  // valor a retocar en el futuro
U_alfa = 0.0075;
U_tau1 = 0.3249;
U_tau2 = 1.3764;




   EALLOW;
     SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 0;                              // Stop all the TB clocks
     SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 1;                              // Start all the TB clocks
   EDIS; 
    
   EINT;                                                                                          // Enable interrupts to CPU-Level

     CpuTimer0Regs.TCR.bit.TRB = 1;                                             // Reload CPU-TIMER0 Period


     //Pesos ctes funcion de coste:
          K_xy=0;
          K_sc=0;
          SC_suma=0;

     //Hay que habilitar y desahbilitar el Kp segun la prueba de reversal que se quiera hacer. Se quita del bucle if fallo de mas adelante.
     GpioDataRegs.GPBSET.bit.GPIO49     = 1;

     // wm_ref = 0.75*wnom;
     // wm_ref = 0.50*wnom;
     //wm_ref = 0.20*wnom;

     wm_ref = 0.30*wnom;

     // C�lculo de coeficientes que dependen de wr
     wr = P*wm_ref;

     TmA12 = wr*TmA12;
     TmA21 = wr*TmA21;

     while(!stop)
          {
        TRIGGER_TOGGLE_54;

           ret_val_mon = (*callmon28335)(); // Arrancar 28335. MSK Execute Control
               GpioDataRegs.GPBSET.bit.GPIO48     = 1; //ventiladores encendidos

          //TRIGGER_TOGGLE_55;
          while(timeout) // Stand by the timeout Flag cleared in the EPWM1 interrupt
          {
        }
          //TRIGGER_TOGGLE_55;

        timeout = 1;                                                                      // Set PWM Time Out Flag

          TRIGGER_TOGGLE_57;
     // Deshabilitaci�n Kp de fase, en_fallo, se�al ctrl m�quina DC fallo
     //===========================================================================
          if (mstart2 != 0)
          {
               contfallo++;
               if (contfallo == 2.0*FRECUENCIA_MUESTREO_HZ)
               {
                    contfallo = 0;
                    mstart=1;
                    mstart2=0;
                    logger = 0;
               }
          }


//------------------------- PI Isq -------------------------

     // Control de Iq para el caso en el que la velocidad se quiere mantener constante

          /*if (logger >= 7000)
          {
               wm_d3 = wm_ref;
               //Isq = 1.54;
               //Isq = 1.4;
               //Isq = 1.42;
               Isq = 1.50;
               Isq_par = Isq;
          }
          else
          {*/

// Cálculo de la velocidad mecánica (rad/s) a partir de medida directa de QPOSCNT
posCnt = EQep1Regs.QPOSCNT;
Delta_posCnt = (int32)posCnt - (int32)posCnt_ant;
if (Delta_posCnt < -QEPCNTMAX/2){
     Delta_posCnt +=  QEPCNTMAX;
} else if(Delta_posCnt > QEPCNTMAX/2){
     Delta_posCnt -= QEPCNTMAX;
} 
wm_k =  0.99 * wm_k + 0.01 * ((float)(- Delta_posCnt) * tick_to_radps);
/*Calculo bueno de la velocidad*/
// wm_k = 0.1 * wnom;
posCnt_ant = posCnt;
               //TRIGGER_TOGGLE_55;
               wm_d3 = wm_k;
               // Bucle externo ctrl velocidad:
               Werror = wm_ref - wm_d3;
               inte_k = Kinte*(Werror + e_km1) + inte_km1;

               Isq = KPW*(Werror + KPIW*inte_k);

               //Saturaci�n del Par o Isq* en mi caso

               Isq_par = Isq;

               if (Isq >= Imax)
               {
                    Isq = Imax;
               }
               else if (Isq <= -Imax)
               {
                    Isq = -Imax;
               }
               else //Unicamente actualiza las var's si el ctrl no esta saturado, de lo contrario quedan al valor en t-1
               {
                    e_km1 = Werror;
                    inte_km1 = inte_k;
               }

          //}
//------------------------- End Isq -------------------------*/ 




// medida velocidad fuera de la ISR EQEP
//mi_qcprdlat = EQep1Regs.QCPRDLAT;
//mi_qposlat = EQep1Regs.QPOSLAT;





               //------------------------- dq - alfa-beta ----------------------*/

               // Transf. de las corrientes de referencia Isd-Isq:
               Ialfa_ref = Isd*costhetae - Isq*sinthetae;
               Ibeta_ref = Isd*sinthetae + Isq*costhetae;

               // ------------------Se a�ade para mi funci�n J de coste--------------
                    Ialfa_ref_p = Isd*costhetae_p - Isq*sinthetae_p;
                    Ibeta_ref_p = Isd*sinthetae_p + Isq*costhetae_p;
               // -------------------------------------------------------------------

               //------------------------- End dq - alfa-beta --------------------------*/

//------------- Control KP, Estimador, dq-alfa-beta, Referencias  -------------*/

               //---------------------------- Ref's Corriente -------------------------*/
               Ix_ref = 0;
               Iy_ref = 0;
               //-------------------------End Ref's Corriente -------------------------*/


//----------------- Med. y  acondicionamiento corrientes de fase - Trans. alfa-beta-x-y ------------------*/
            //TRIGGER_TOGGLE_56;
          // ADC converter SEQ 1 is Busy
          while(AdcRegs.ADCST.bit.SEQ1_BSY)
          {
               LED2_TOGGLE;
          }

               /*
               Ia_mir = (float) AdcMirror.ADCRESULT0;
               Ib_mir = (float) AdcMirror.ADCRESULT1;
               Id_mir = (float) AdcMirror.ADCRESULT2;
               Ie_mir = (float) AdcMirror.ADCRESULT3;
               Vdc_mir= (float) AdcMirror.ADCRESULT4;

               //TRIGGER_TOGGLE_56;

               Ia_medido = (Ia_mir - CONST_OFFSET_IA)*CONST_GAINFACT_IA;
               Ib_medido = (Ib_mir - CONST_OFFSET_IB)*CONST_GAINFACT_IB;
               Id_medido = (Id_mir - CONST_OFFSET_ID)*CONST_GAINFACT_ID;
               Ie_medido = (Ie_mir - CONST_OFFSET_IE)*CONST_GAINFACT_IE;
               Vdc_medido= (Vdc_mir - CONST_OFFSET_VDCLINK)*CONST_GAINFACT_VDCLINK;

               Ic_medido = (float)0-((float)Ia_medido + (float)Ib_medido + (float)Id_medido + (float)Ie_medido);
               */

               //TRIGGER_TOGGLE_56;
               TRIGGER_TOGGLE_55;

               Ia_medido = ((float) AdcMirror.ADCRESULT0 - CONST_OFFSET_IA)*CONST_GAINFACT_IA;
               Ib_medido = ((float) AdcMirror.ADCRESULT1 - CONST_OFFSET_IB)*CONST_GAINFACT_IB;
               Id_medido = ((float) AdcMirror.ADCRESULT2 - CONST_OFFSET_ID)*CONST_GAINFACT_ID;
               Ie_medido = ((float) AdcMirror.ADCRESULT3 - CONST_OFFSET_IE)*CONST_GAINFACT_IE;
               Ic_medido = -Ia_medido -Ib_medido -Id_medido -Ie_medido;


//------------- End Med. y  acondicionamiento corrientes de fase - Trans. alfa-beta-x-y ------------------*/

        // -------------------------- abcde to alpha-beta-x-y --------------------------------------------

               i_alpha_med = T11*Ia_medido + T12*Ib_medido + T13*Ic_medido + T14*Id_medido + T15*Ie_medido;
               i_beta_med  =                      T22*Ib_medido + T23*Ic_medido + T24*Id_medido + T25*Ie_medido;
               i_x_med         = T31*Ia_medido + T32*Ib_medido + T33*Ic_medido + T34*Id_medido + T35*Ie_medido;
               i_y_med         =                      T42*Ib_medido + T43*Ic_medido + T44*Id_medido + T45*Ie_medido;

               TRIGGER_TOGGLE_55;

               /*
               Tcarga = CCarga*(Isd*Isq_par);
               Tcarga_pre=Tcarga;

               // Transf. de las corrientes de medida a Isd-Isq:
               Id_med =  i_alpha_med*costhetae + i_beta_med*sinthetae;
               Iq_med = -i_alpha_med*sinthetae + i_beta_med*costhetae;
               */

          // ------------------------ End abcde to alpha-beta-x-y ----------------------------*/


//-------- End Med. y  acondicionamiento corrientes de fase - Trans. alfa-beta-x-y ------------------

// --------------------------------------- PREDICTIVE MODEL -----------------------------------------------------
          // C�lculo de la funci�n G (ya va multiplicada por Tm)
               G1 = i_alpha_med - H_alpha;
               G2 = i_beta_med  - H_beta;
          // Predicci�n para k+1
               H_alpha = UmTmA11*i_alpha_med + TmA12*i_beta_med   + Usa[sopt];
               H_beta  = TmA21*i_alpha_med   + UmTmA22*i_beta_med + Usb[sopt];
               i_alpha_p1 = H_alpha  + G1;
               i_beta_p   = H_beta   + G2;


// ----------------------------------------- OPTIMIZATION -------------------------------------------------------
               // T�rminos indep del sv:
                    g_alfa = Ialfa_ref_p - (UmTmA11*i_alpha_p1 + TmA12*i_beta_p);
                    g_beta = Ibeta_ref_p - (TmA21*i_alpha_p1   + UmTmA22*i_beta_p);

               sopt = 0;
               Jopt = 500000;

                    TRIGGER_TOGGLE_56;
                    for (sv=0; sv<11 ; sv++)  // retocado para 10vv L + 2vv Z
                    {
                         e_alpha = g_alfa - Usa[sv];
                         e_beta  = g_beta - Usb[sv];
                         Jsv = e_alpha*e_alpha + e_beta*e_beta;
                         if(Jopt>Jsv)
                         {
                              sopt = sv;
                              Jopt = Jsv;
                         }
                    }
                    TRIGGER_TOGGLE_56;



                    // Transf. de las corrientes de medida a Isd-Isq:
                    Id_med =  i_alpha_med*costhetae + i_beta_med*sinthetae;
                    Iq_med = -i_alpha_med*sinthetae + i_beta_med*costhetae;

               //----------------------- Estimador de Posici�n ---------------------------
                    wsl = invTau_r*Isq/Isd;
                    we  = P*wm_d3 + wsl;

                    thetae = thetaenm1 + we*Tm;

                    // ---------A�adido para mi funci�n J de coste--------------
                         thetae_p = thetaenm1 + we*Tm*3;
                    // ---------------------------------------------------------

                    if (thetae > PI2)
                    {
                         thetae -= PI2;
                    }
                    else if (thetae<0)
                    {
                         thetae += PI2;
                    }

                    // ---------A�adido para mi funci�n J de coste--------------
                         if (thetae_p > PI2)
                         {
                              thetae_p -= PI2;
                         }
                         else if (thetae_p<0)
                         {
                              thetae_p += PI2;
                         }
                    // ---------------------------------------------------------
                    sinthetae = sin(thetae);
                    costhetae = cos(thetae);
                    // ---------A�adido para mi funci�n J de coste--------------
                         sinthetae_p = sin(thetae_p);
                         costhetae_p = cos(thetae_p);
                    // ---------------------------------------------------------
                    thetaenm1 = thetae;


                    //TRIGGER_TOGGLE_55;
               //----------------------- End Estimador de Posici�n ---------------------------



if ((dsampled >= DOWNSAMP)&&(mstart))
               {
                    fds = 1;
                    dsampled=0;
               }
else{
     dsampled++;     
}


//Medida decimada:

                    if ((logger < CANTIDAD_LOG)&&(fds)&&(mstart)) // (mstart2==1)
                    {

// Para pruebas OMEGAFINA MRA FEB 2023
                         //svopt[logger] = (Uint16) (65.534 *(wm_k + 500));
                         //svapr[logger] = global_cont_eqep_interr;
                         //vfila[logger] =  (Uint16) (1638.35 * (veloc_radsec + 20));
                         wm_log[logger] = (Uint16) (65.534 *(wm_k + 500));
                         // wm_log2[logger] = (Uint16) (65.534 *(wm_k2 + 500));
                         // eqep_1[logger] = posCnt>>16;
                         // eqep_2[logger] = posCnt&0xFFFF; 
                         delta_pos1[logger] = Delta_posCnt>>16;
                         delta_pos2[logger] = Delta_posCnt&0xFFFF;
                         if (logger==1000){
                              wm_ref = -0.2*wnom;
                         }
                         logger++;
                         fds=0;
             }


                    // la actuaci�n debe ir al final del todo
                    //------------------------------- Generaci�n de disparos ----------------------------------*/
                    EPwm1Regs.CMPA.half.CMPA = (1-SCMP[sopt][0])*pwm_period;    // Load EPWM1 CMPA
                    EPwm2Regs.CMPA.half.CMPA = (1-SCMP[sopt][1])*pwm_period;    // Load EPWM2 CMPA
                    EPwm3Regs.CMPA.half.CMPA = (1-SCMP[sopt][2])*pwm_period;    // Load EPWM3 CMPA
                    EPwm4Regs.CMPA.half.CMPA = (1-SCMP[sopt][3])*pwm_period;    // Load EPWM4 CMPA
                    EPwm5Regs.CMPA.half.CMPA = (1-SCMP[sopt][4])*pwm_period;    // Load EPWM5 CMPA

//TRIGGER_TOGGLE_54;
TRIGGER_TOGGLE_57;


     }// while(!stop)

     DINT;

     EALLOW;                                          // Enable writing to EALLOW protected registers

     GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 0;       // Configure GPIO0 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO0         = 1;       // Configure GPIO0 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO0          = 1;       // Clear GPIO0

     GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 0;       // Configure GPIO1 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO1         = 1;       // Configure GPIO1 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO1          = 1;       // Clear GPIO1
     //###########################################################################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 0;       // Configure GPIO2 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO2         = 1;       // Configure GPIO2 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO2          = 1;       // Clear GPIO2

     GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 0;       // Configure GPIO3 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO3         = 1;       // Configure GPIO3 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO3          = 1;       // Clear GPIO3
     //###########################################################################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 0;       // Configure GPIO4 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO4         = 1;       // Configure GPIO4 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO4          = 1;       // Clear GPIO4

     GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 0;       // Configure GPIO5 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO5         = 1;       // Configure GPIO5 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO5          = 1;       // Clear GPIO5
     //###########################################################################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 0;       // Configure GPIO6 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO6         = 1;       // Configure GPIO6 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO6          = 1;       // Clear GPIO6

     GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 0;       // Configure GPIO7 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO7         = 1;       // Configure GPIO7 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO7          = 1;       // Clear GPIO7
     //###########################################################################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 0;       // Configure GPIO8 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO8         = 1;       // Configure GPIO8 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO8          = 1;       // Clear GPIO8

     GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 0;       // Configure GPIO9 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO9         = 1;       // Configure GPIO9 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO9          = 1;       // Clear GPIO9
     //###########################################################################################

     GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 0;       // Configure GPIO10 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO10        = 1;       // Configure GPIO10 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO10            = 1;       // Clear GPIO10

     GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 0;       // Configure GPIO11 as Digital I/O
     GpioCtrlRegs.GPADIR.bit.GPIO11        = 1;       // Configure GPIO11 as digital Output
     GpioDataRegs.GPACLEAR.bit.GPIO11          = 1;       // Clear GPIO11
     //###########################################################################################
                                   // Enable writing to EALLOW protected registers
     SysCtrlRegs.WDCR = 0x000F;            // Enable Watchdog and write incorrect WD Check Bits
     asm(" NOP");
     asm(" NOP");
     asm(" NOP");
}// void main ()
//##########################################################################################################3

