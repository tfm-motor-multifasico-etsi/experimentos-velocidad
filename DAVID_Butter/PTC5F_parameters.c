/*-----------------------------------------------------------------------------
DEFINICION DE VARIABLES
-----------------------------------------------------------------------------*/

#define DEADBAND_VALUE 0 				//valor del delay para generar el deadband
										// 37.5 equivale a 1 us

#define FRECUENCIA_MUESTREO_HZ  20000 //20000 //15000 //12500	//frecuencia muestreo en Hz

#define FRECUENCIA_CLK_PWM      37500000		//preescaler /4.. entonces 150.000.000/4
#define FRECUENCIA_SYSCLKOUT    150000000 // el clock esta a 150 MHz

#define FCPUTIMER0_HZ		10				// Frecuencia de desborde del CPU-Timer0

#define PROBEJ1_23_SET		GpioDataRegs.GPBSET.bit.GPIO35=1			// GPIO35
#define PROBEJ1_23_CLEAR	GpioDataRegs.GPBCLEAR.bit.GPIO35=1			// GPIO35
#define PROBEJ1_23_TOGGLE	GpioDataRegs.GPBTOGGLE.bit.GPIO35=1			// GPIO35

#define TRIGGER_SET_54			GpioDataRegs.GPBSET.bit.GPIO54    = 1;		// SET    the TRIGGER SIGNAL (JP3-PIN 19)
#define TRIGGER_CLEAR_54		GpioDataRegs.GPBCLEAR.bit.GPIO54	 = 1;		// CLEAR  the TRIGGER SIGNAL (JP3-PIN 19)
#define TRIGGER_TOGGLE_54		GpioDataRegs.GPBTOGGLE.bit.GPIO54 = 1;		// TOFFLE the TRIGGER SIGNAL (JP3-PIN 19)

//Monitorizaci�n tiempos de ejecuci�n del c�digo en el DSP

#define TRIGGER_SET_55			GpioDataRegs.GPBSET.bit.GPIO55    = 1;		// SET    the TRIGGER SIGNAL (JP3-PIN 20)
#define TRIGGER_CLEAR_55		GpioDataRegs.GPBCLEAR.bit.GPIO55	 = 1;		// CLEAR  the TRIGGER SIGNAL (JP3-PIN 20)
#define TRIGGER_TOGGLE_55		GpioDataRegs.GPBTOGGLE.bit.GPIO55 = 1;		// TOFFLE the TRIGGER SIGNAL (JP3-PIN 20)

#define TRIGGER_SET_56			GpioDataRegs.GPBSET.bit.GPIO56    = 1;		// SET    the TRIGGER SIGNAL (JP3-PIN 21)
#define TRIGGER_CLEAR_56		GpioDataRegs.GPBCLEAR.bit.GPIO56	 = 1;		// CLEAR  the TRIGGER SIGNAL (JP3-PIN 21)
#define TRIGGER_TOGGLE_56		GpioDataRegs.GPBTOGGLE.bit.GPIO56 = 1;		// TOFFLE the TRIGGER SIGNAL (JP3-PIN 21)

#define TRIGGER_SET_57		GpioDataRegs.GPBSET.bit.GPIO57    = 1;		// SET    the TRIGGER SIGNAL (JP3-PIN 22)
#define TRIGGER_CLEAR_57		GpioDataRegs.GPBCLEAR.bit.GPIO57	 = 1;		// CLEAR  the TRIGGER SIGNAL (JP3-PIN 22)
#define TRIGGER_TOGGLE_57	GpioDataRegs.GPBTOGGLE.bit.GPIO57 = 1;		// TOFFLE the TRIGGER SIGNAL (JP3-PIN 22)

#define PROBEJ1_17_SET		GpioDataRegs.GPBSET.bit.GPIO36=1			// GPIO36
#define PROBEJ1_17_CLEAR	GpioDataRegs.GPBCLEAR.bit.GPIO36=1			// GPIO36
#define PROBEJ1_17_TOGGLE	GpioDataRegs.GPBTOGGLE.bit.GPIO36=1			// GPIO36

#define LED1_OFF			GpioDataRegs.GPASET.bit.GPIO16=1			// GPIO16
#define LED1_ON				GpioDataRegs.GPACLEAR.bit.GPIO16=1			// GPIO16
#define LED1_TOGGLE			GpioDataRegs.GPATOGGLE.bit.GPIO16=1			// GPIO16


#define LED2_OFF		GpioDataRegs.GPASET.bit.GPIO17=1			// GPIO17
#define LED2_ON			GpioDataRegs.GPACLEAR.bit.GPIO17=1			// GPIO17
#define LED2_TOGGLE		GpioDataRegs.GPATOGGLE.bit.GPIO17=1			// GPIO17

#define RELE_KP_SET			GpioDataRegs.GPBSET.bit.GPIO49=1			// GPIO49
#define RELE_KP_CLEAR			GpioDataRegs.GPBCLEAR.bit.GPIO49=1			// GPIO49


//ojo, en la funcion de configuracion de la interrupcion se hace manualmente, no con defines
// gpio 24 ----> j4 - pin 13
#define INTSPI_pin_SET GpioDataRegs.GPASET.bit.GPIO24=1
#define INTSPI_pin_CLEAR GpioDataRegs.GPACLEAR.bit.GPIO24=1
#define INTSPI_pin_TOGGLE GpioDataRegs.GPATOGGLE.bit.GPIO24=1


#define ENCODER_SLOTS 2500	//ranuras del encoder por vuelta


//OJO, ESTAS CONSTANTES SON CALCULADAS DE ACUERDO A QEP_UNIT_TIMEOUT=1.500.000 - ENCODER_SLOTS 2500
#define CONST_VELOC_RADSEC 0.06283185
#define CONST_VELOC_RPM    0.6
#define CONST_VELOC_HZ    0.01

#define  RADPSTORPM   (float) 9.549296          // Convertion Constant from rad/s to rpm
#define  RADPSTOHZ    (float) 0.159155          // Convertion Constant from rad/s to Hz
#define  HZTORADPS    (float) 6.283185          // Convertion Constant from Hz to rad/s


//####################################### MODULO DE 21A ###################################################

#define CONST_GAINFACT_IA			(float)  0.013106
#define CONST_GAINFACT_IB			(float)  0.013280
#define CONST_GAINFACT_ID			(float)  0.013125
#define CONST_GAINFACT_IE			(float)  0.013307
#define CONST_GAINFACT_VDCLINK	(float) 1

#define CONST_OFFSET_IA			(float) 2105.8066
#define CONST_OFFSET_IB			(float) 2083.8260
#define CONST_OFFSET_ID			(float) 2099.3768
#define CONST_OFFSET_IE			(float) 2084.7112
#define CONST_OFFSET_VDCLINK	(float) 0

#define Irated   (float) 2.43			// (A) Converter Nominal Current

float Iq_med;
float Isq_par;
float Id_med;

#define Isd_ref (float) 0.57

#define alpha  (float)    1.2566371
#define alpha2 (float)    2.5132741
#define alpha3 (float)    3.7699111
#define alpha4 (float)    5.0265482
#define PI2		(float)	  6.28318



//############################################## MACHINE PARAMETERS 5F SVQ ###############3######################################                                                                                                                                     
// ELECTRIC MACHINE PARAMETERS

// MECHANICAL PARAMETERS
#define P        (float) 3				// Pole pairs
#define Bm       (float) 0.0221		// (N*m*s/rad) Friction Coefficient
#define Jm       (float) 0.109		// (kg*m^2) Rotational Inertia of the rotor
#define Trated   (float) 8.0			// (N*m) Torque Rated 
#define wnom     (float) 104.7197	// (rad/s) Nominal Speed
#define IFASEMAX (float) 6.5			// Fase current rated (Vdc = 390)

#define FSNOM	  (float) 0.80			// (Wb) Nominal Stator Flux

//############################################### END MACHINE PARAMETERS ##################################################


// ############################################## PI - CONTROLLER ######################################################

float KPW = 0.08;
float KPIW = 6;

#define EWMIN		(float) 1
#define FSREF		(float) 0.37

// ############################################## END PI - CONTROLLER ###################################################


//####################################################### WEIGHT FACTORS ########################################################


#define Kxy		(float)  3.5
#define KFS		(float)	41.5
#define KTJ		(float)	1.875
#define Kab		(float)  100000
#define ISABMAX  (float) 9.2		// (A) Nominal alpha-beta current
#define ISXYMAX  (float) 0.10			// (A) Nominal x-y current max

#define DOWNSAMP  (Uint16) 4

//####################################################  END WEIGHT FACTORS ######################################################

//#################################################### ANGLES ##################################################

#define deltad   (float)   0.5236
#define deltab   (float)   2.0944
#define deltae   (float)   2.6180
#define deltac   (float)   4.1888
#define deltaf   (float)   4.7124

//################################################## End ANGLES ################################################



// ################################################ TRANSFORMATION MATRIX ############################################

#define T11		(float)	 0.4
#define T12		(float)	 0.12361
#define T13		(float)	-0.32361
#define T14		(float)	-0.32361
#define T15		(float)	 0.12361

#define T22		(float)	 0.38042
#define T23		(float)	 0.23511
#define T24		(float)	-0.23511
#define T25		(float)	-0.38042

#define T31		(float)	 0.4	 
#define T32		(float)	-0.32361	 
#define T33		(float)	 0.12361	
#define T34		(float)	 0.12361
#define T35		(float)	-0.32361

#define T42		(float)	 0.23511
#define T43		(float)	-0.38042	 
#define T44		(float)	 0.38042	
#define T45		(float)	-0.23511

// ############################################## END TRANSFORMATION MATRIX ############################################


// ################################################ INVERSE TRANSFORMATION MATRIX ############################################
#define invT11	(float)	 1
#define invT13	(float)  1

#define invT21	(float)	 0.30901
#define invT22	(float)	 0.95105
#define invT23	(float)	-0.80901
#define invT24	(float)	 0.58778

#define invT31	(float)	-0.80901
#define invT32	(float)	 0.58778
#define invT33	(float)	 0.30901
#define invT34	(float)	-0.95105

#define invT41	(float)	-0.80901
#define invT42	(float)	-0.58778
#define invT43	(float)	 0.30901
#define invT44	(float)	 0.95105

#define invT51 (float)	 0.30901
#define invT52	(float)	-0.95105
#define invT53	(float)	-0.80901
#define invT54 (float)	-0.58778

// ################################################ END INVERSE TRANSFORMATION MATRIX ############################################


// ################################################ MACHINE PARAMETERS 10kHz ###################################################

#define Rs       (float) 19.45			// (Ohm) Stator Resistance
#define Rr       (float) 6.77			// (Ohm) Rotor Resistance
#define Lls      (float) 0.1007			// (H) Stator Leakage Inductance
#define Llr      (float) 0.0386			// (H) Rotor Leakage Inductance
#define M        (float) 0.6565			// (H) Mutue Inductance Tranformation M=2.5*Lm
#define Lm       (float) 0.6565
#define Ls       (float) 0.7572			// (H) Stator Inductance Transformation Ls= Lls+M
#define Lr       (float) 0.6951			// (H) Rotor Inductance Transformation Lr=Llr+M
#define Tau_r    (float) 0.1027			// (s) Rotor's Time Constant Lr/Rr
#define invTau_r (float) 9.7396			// (1/s) Rotor's Time Constant invert (correct)
#define Tau_s    (float) 0.0389			// (s) Stator's Time Constant Ls/Rs
#define Sigma    (float) 0.1811			// Sigma=1-M^2/(Ls*Lr)
#define sLs		 (float) 0.1372			// Sigma*Ls
#define Kr       (float) 0.9445			// Kr=M/Lr
#define Kpar	 (float) 7.5			// Par constant 5*P/2
#define Kwsl	 (float) 6.3941			// constante de wsl (M/tau_r)
#define KEQ		 (float) 1.1534			// Ls/M for the decouppling factor
#define KTISQ	 (float) 0.1412			// 2*Lr/(5*P*M)
#define KTFI	 (float) 7.0835		// (5*P*M)/(2*Lr)*/

//################################################ A Matrix ################################################
#define	A11		(float)	-141.8088292
#define	A12		(float)	   4.5207016
//#define	A13		(float)	0
//#define	A14		(float)	0
#define	A15		(float)	  46.6186590
#define	A16		(float)	   4.7865037

#define	A21		(float) -A12	
#define	A22		(float)	 A11
//#define	A23		(float)	0
//#define	A24		(float)	0
#define	A25		(float)	-A16
#define	A26		(float)	 A15

//#define	A31		(float)	0
//#define	A32		(float)	0
#define	A33		(float)	-193.1479643
//#define	A34		(float)	0
//#define	A35		(float)	0
//#define	A36		(float)	0

//#define	A41		(float)	0
//#define	A42		(float)	0
//#define	A43		(float)	0
#define	A44		(float)	A33
//#define	A45		(float)	0
//#define	A46		(float)	0

#define	A51		(float)	 133.9339611
#define	A52		(float)	  -5.2141283
//#define	A53		(float)	0
//#define	A54		(float)	0
#define	A55		(float)	 -53.7694571
#define	A56		(float)	  -5.5207016

#define	A61		(float)	-A52
#define	A62		(float)	 A51
//#define	A63		(float)	0
//#define	A64		(float)	0
#define	A65		(float)	-A56
#define	A66		(float)	 A55

//############################################## END A Matrix ##############################################
//################################################ B Matrix ################################################
#define	B11		(float)	7.2909424
#define	B22		(float)	B11
#define	B33		(float) 9.9304866
#define	B44		(float)	B33
#define	B51		(float)	-6.8860648
#define	B62		(float)	B51
//############################################### END B Matrix ##############################################


/*-----------------------------------------------------------------------------
Variables Globales
-----------------------------------------------------------------------------*/
//tiempo de muestreo, frecuencia de muestreo y frecuencia de la se�al (modulador)
float Tm; //, frecuencia;
Uint16 Fm;
Uint16 pwm_period;         // TBPRD Register value
int   timeout = 1;      // PWM CNTR overflow flag

// value returned by the monitor 
volatile Uint16 stop = 0;
volatile Uint16 ret_val_mon;
/*
float SCMP[32][5] = {{0,0,0,0,0},	// 00
					 {0,0,0,0,1},	// 01
					 {0,0,0,1,0},	// 02
					 {0,0,0,1,1},	// 03
					 {0,0,1,0,0},	// 04
					 {0,0,1,0,1},	// 05
					 {0,0,1,1,0},	// 06
					 {0,0,1,1,1},	// 07
					 {0,1,0,0,0},	// 08
					 {0,1,0,0,1},	// 09
					 {0,1,0,1,0},	// 10
					 {0,1,0,1,1},	// 11
					 {0,1,1,0,0},	// 12
					 {0,1,1,0,1},	// 13
					 {0,1,1,1,0},	// 14
					 {0,1,1,1,1},	// 15
					 {1,0,0,0,0},	// 16
					 {1,0,0,0,1},	// 17
					 {1,0,0,1,0},	// 18
					 {1,0,0,1,1},	// 19
					 {1,0,1,0,0},	// 20
					 {1,0,1,0,1},	// 21
					 {1,0,1,1,0},	// 22
					 {1,0,1,1,1},	// 23
					 {1,1,0,0,0},	// 24
					 {1,1,0,0,1},	// 25
					 {1,1,0,1,0},	// 26
					 {1,1,0,1,1},	// 27
					 {1,1,1,0,0},	// 28
					 {1,1,1,0,1},	// 29
					 {1,1,1,1,0},	// 30
					 {1,1,1,1,1}};	// 31
*/

// Retocado para usar 10VV L + 2VV Z
float SCMP[12][5] = {{0,0,0,0,0},	// 00
//float SCMP[32][5] = {{0,0,0,0,0},	// 00
				//	 {0,0,0,0,1},	// 01
				//	 {0,0,0,1,0},	// 02
					 {0,0,0,1,1},	// 03
				//	 {0,0,1,0,0},	// 04
				//	 {0,0,1,0,1},	// 05
					 {0,0,1,1,0},	// 06
					 {0,0,1,1,1},	// 07
				//	 {0,1,0,0,0},	// 08
				//	 {0,1,0,0,1},	// 09
				//	 {0,1,0,1,0},	// 10
				//	 {0,1,0,1,1},	// 11
					 {0,1,1,0,0},	// 12
				//	 {0,1,1,0,1},	// 13
					 {0,1,1,1,0},	// 14
				//	 {0,1,1,1,1},	// 15
				//	 {1,0,0,0,0},	// 16
					 {1,0,0,0,1},	// 17
				//	 {1,0,0,1,0},	// 18
					 {1,0,0,1,1},	// 19
				//	 {1,0,1,0,0},	// 20
				//	 {1,0,1,0,1},	// 21
				//	 {1,0,1,1,0},	// 22
				//	 {1,0,1,1,1},	// 23
					 {1,1,0,0,0},	// 24
					 {1,1,0,0,1},	// 25
				//	 {1,1,0,1,0},	// 26
				//	 {1,1,0,1,1},	// 27
					 {1,1,1,0,0},	// 28
				//	 {1,1,1,0,1},	// 29
				//	 {1,1,1,1,0},	// 30
					 {1,1,1,1,1}};	// 31


/*
// Retocado para usar 10VV M + 2VV Z
float SCMP[12][5] = {{0,0,0,0,0},	// 00
//float SCMP[32][5] = {{0,0,0,0,0},	// 00
					 {0,0,0,0,1},	// 01
					 {0,0,0,1,0},	// 02
				//	 {0,0,0,1,1},	// 03
					 {0,0,1,0,0},	// 04
				//	 {0,0,1,0,1},	// 05
				//	 {0,0,1,1,0},	// 06
				//	 {0,0,1,1,1},	// 07
					 {0,1,0,0,0},	// 08
				//	 {0,1,0,0,1},	// 09
				//	 {0,1,0,1,0},	// 10
				//	 {0,1,0,1,1},	// 11
				//	 {0,1,1,0,0},	// 12
				//	 {0,1,1,0,1},	// 13
				//	 {0,1,1,1,0},	// 14
					 {0,1,1,1,1},	// 15
					 {1,0,0,0,0},	// 16
				//	 {1,0,0,0,1},	// 17
				//	 {1,0,0,1,0},	// 18
				//	 {1,0,0,1,1},	// 19
				//	 {1,0,1,0,0},	// 20
				//	 {1,0,1,0,1},	// 21
				//	 {1,0,1,1,0},	// 22
					 {1,0,1,1,1},	// 23
				//	 {1,1,0,0,0},	// 24
				//	 {1,1,0,0,1},	// 25
				//	 {1,1,0,1,0},	// 26
					 {1,1,0,1,1},	// 27
				//	 {1,1,1,0,0},	// 28
					 {1,1,1,0,1},	// 29
					 {1,1,1,1,0},	// 30
					 {1,1,1,1,1}};	// 31
*/
/*
float vsa[32] = {0,	 0.12361,  -0.32361,	-0.2,	    -0.32361,	-0.2,	    -0.64721,	-0.52361,	0.12361,	 0.24721, -0.2,	  -0.0764,	-0.2,	    -0.0764,  -0.52361,	 -0.4, 0.4,	0.52361,	  0.0764,	0.2,	    0.0764,	   0.2,	  -0.24721,	 -0.12361,	0.52361,	  0.64721,  0.2,		 0.32361,	0.2,		 0.32361,  -0.12361,	 0};
float vsb[32] = {0,	-0.38042,  -0.23511,	-0.61554,  0.23511,	-0.14531,  0,		   -0.38042,   0.38042,	 0,	     0.14531, -0.235114, 0.61554,  0.23511,	0.38042,	  0,	 0,  -0.38042,	 -0.23511, -0.61554,	 0.235114, -0.14531,	0,			 -0.380422,	0.38042,	  0,		   0.14531,	-0.235114,	0.61554,	 0.235114,	0.380423, 0};
float vsx[32] = {0,	-0.32361,	0.12361,	-0.2,	     0.12361,	-0.2,	     0.247214,	-0.076393, -0.32361,	-0.64721, -0.2,	  -0.52361,	-0.2,	    -0.52361, -0.076393, -0.4, 0.4,	0.076393,  0.52361,	0.2,	    0.52361,	0.2,	   0.647214,  0.32361,	0.076393, -0.247214,	0.2,		-0.12361,	0.2,		-0.12361,	0.32361,	 0};
float vsy[32] = {0,	-0.23511,	0.38042,	 0.14531, -0.38042,	-0.61554,  0,			-0.235114,	0.235114, 0,		  0.61554,	0.38042,	-0.14531, -0.38042,	0.235114,  0,	 0,  -0.2351141, 0.380423,	0.14531,	-0.380423, -0.61554,	0,			 -0.235114,	0.235114,  0,		   0.61554,	 0.380423, -0.14531,	-0.380423,	0.235114, 0};
*/

// Retocado para usar 10VV L + 2VV Z
float vsa[12] = {0,	 -0.2, -0.64721, -0.52361, -0.2, -0.52361, 0.52361,	 0.2, 0.52361, 0.64721, 0.2, 0};
float vsb[12] = {0,	-0.61554,   0,	-0.38042,  0.61554,	0.38042, -0.38042, -0.61554, 0.38042,  0, 0.61554,	 0};
float vsx[12] = {0,	-0.2,     0.247214,	-0.076393, 	-0.2, -0.076393, 0.076393, 0.2,  0.076393, -0.247214, 0.2, 0};
float vsy[12] = {0,	 0.14531,    0,	-0.235114,	 -0.14531,  0.235114,  -0.2351141, 0.14531,	0.235114,  0, -0.14531,	 0};


/*
// Retocado para usar 10VV M + 2VV Z
float vsa[12] = {0,	 0.12361, -0.32361, -0.32361,  0.12361, -0.4, 0.4,  -0.12361,   0.32361,   0.32361, -0.12361, 0};
float vsb[12] = {0,	-0.38042, -0.23511,	 0.23511,  0.38042,    0,   0, -0.380422, -0.235114,  0.235114, 0.380423, 0};
float vsx[12] = {0,	-0.32361,  0.12361,  0.12361, -0.32361, -0.4, 0.4,   0.32361,  -0.12361,  -0.12361,  0.32361, 0};
float vsy[12] = {0,	-0.23511,  0.38042,	-0.38042, 0.235114,    0,   0, -0.235114,  0.380423, -0.380423,	0.235114, 0};
*/

//float Usa[32], Usb[32], Usx[32], Usy[32];

// Retocado para usar 10VV L + 2VV Z (o 10VV M + 2VV Z)
float Usa[12], Usb[12], Usx[12], Usy[12];


float K_xy;
float K_sc;

float SC_suma;

unsigned int Vdclink_medido;
float Ia_medido, Ia_mir, i_alpha;
float Ib_medido, Ib_mir, i_beta;
float Ic_medido, i_x;
float Id_medido, Id_mir, i_y;
float Ie_medido, Ie_mir;
float If_medido;
float Vdc_medido, Vdc_mir;

// Definicion var's calculo corriente post-falta
float Isd;
float Isq;
float Iy_ref;
float Ix_ref;
float Ibeta_ref;
float Ialfa_ref;

// -----------------------Se a�ade para mi funci�n de coste--------------------
	float Ibeta_ref_p;
	float Ialfa_ref_p;
	float thetae_p; 
	float sinthetae_p;
	float costhetae_p;
// ----------------------------------------------------------------------------

float wsl;
float we;
float thetae;

float thetaenm1;
float sinthetae;
float costhetae;

float Tcarga;
float Tcarga_pre;
float CCarga = 4.88; //Para Tcarga=CCarga(Id*Iq)

float Imax = 0;

float mstart = 0;
float mstart2 = 0;

float ThetaI = 0;

float contfallo = 0;

float Vdc;  // DC-Link

float w1 = 0;
float w2 = 0;
float wm_d3 = 0;
// Variables segundo metodo de dec. velocidad. i.e. el que no funca
float wm_k, wm_ref, wr, Werror, e_km1, Ke, Kinte, inte_k, inte_km1;
float wm_km1 = 0;
Uint32 posCnt_ant = 0;
Uint32 posCnt = 0;
int32 Delta_posCnt = 0;
// Variables que pasan a ser globales para pruebas OMEGAFINA MRA 02-2023
float  eqeptmr	= 1;
float  hdx		= 0;
float  veloc_radsec;
float mi_qcprdlat = 1.1111;
float mi_qposlat = 2.2222;


unsigned int global_cont_eqep_interr=0;

float TmA11, TmA12, TmA21, TmA22, TmA33, TmA44;
float G1, G2, G3, G4;
float e_alpha, e_beta, e_x, e_y;
float Jopt, Jsv;
float i_alpha_cte, i_beta_cte, i_x_cte, i_y_cte;
float i_alpha_p1, i_alpha_p2, i_beta_p, i_x_p, i_y_p;
float i_alpha_med, i_beta_med, i_x_med, i_y_med;

float w_calc;

// Variables nuevas 2022-02 MRA
float UmTmA11;
float UmTmA22;
float H_alpha;
float H_beta;
float g_alfa;
float g_beta;
float mi_r2;
float U_r2;
float signoa;
float signob;
float U_alfa;
float tau_g;
float U_tau1;
float U_tau2;
int Tabla_VV[4][4] = {{ 10, 4, 1, 7}, { 9, 2, 2, 9}, { 8, 5, 3, 6}, { 10, 4, 1, 7}};


//A�adido para el cambio de referencia
float ia, ib, ia_p, ib_p, j, cero, Ialfa_ref_ant;

float metodo=2;

Uint16 dsampled = 0;

//############################################
Uint16 logger=0;
Uint16 logger1=0;
Uint16 logger2=0;
Uint16 logger3=0;
Uint16 logger4=0;
Uint16 logger5=0;
Uint16 logger6=0;
Uint16 logger7=0;
Uint16 logger8=0;
Uint16 loggaux=0;
Uint16 clogger=0;
Uint16 cloggaux=0;
//############################################

#define CANTIDAD_LOG 30000
//Uint16 ias[CANTIDAD_LOG];
//Uint16 ibs[CANTIDAD_LOG];
//Uint16 ids[CANTIDAD_LOG];
//Uint16 ies[CANTIDAD_LOG];

//Uint16 iq_ref[CANTIDAD_LOG];
//Uint16 we_m[CANTIDAD_LOG];
//Uint16 ids_m[CANTIDAD_LOG];
//Uint16 iqs_m[CANTIDAD_LOG];

//Uint16 wmech1[CANTIDAD_LOG];

// Uint16 svopt[CANTIDAD_LOG];
// Uint16 svapr[CANTIDAD_LOG];
// Uint16 ialpha_k[CANTIDAD_LOG]; 
// Uint16 ialpha_ref[CANTIDAD_LOG]; 
// Uint16 ix_k[CANTIDAD_LOG];
// Uint16 vfila[CANTIDAD_LOG]; 
Uint16 wm_log[CANTIDAD_LOG];
// Uint16 eqep_1[CANTIDAD_LOG];
// Uint16 eqep_2[CANTIDAD_LOG];
Uint16 delta_pos1[CANTIDAD_LOG];
Uint16 delta_pos2[CANTIDAD_LOG];
// Uint16 eqep_1[CANTIDAD_LOG]; 
// Uint16 eqep_2[CANTIDAD_LOG]; 
// Uint16 delta_pos1[CANTIDAD_LOG];
// Uint16 corr2[CANTIDAD_LOG];


//#pragma DATA_SECTION(ias,"LOGGER");
//#pragma DATA_SECTION(ibs,"LOGGER");
//#pragma DATA_SECTION(ids,"LOGGER");
//#pragma DATA_SECTION(ies,"LOGGER");

//#pragma DATA_SECTION(ids_m,"LOGGER");
//#pragma DATA_SECTION(iqs_m,"LOGGER");
//#pragma DATA_SECTION(iq_ref,"LOGGER");
//#pragma DATA_SECTION(we_m,"LOGGER");
//#pragma DATA_SECTION(wmech1,"LOGGER");

//#pragma DATA_SECTION(svopt,"LOGGER"); 
//#pragma DATA_SECTION(svapr,"LOGGER"); 
//#pragma DATA_SECTION(ias_p1,"LOGGER");
//#pragma DATA_SECTION(ias_p2,"LOGGER");
// #pragma DATA_SECTION(ialpha_k,"LOGGER"); 
// #pragma DATA_SECTION(ialpha_ref,"LOGGER"); 
// #pragma DATA_SECTION(ix_k,"LOGGER");
//#pragma DATA_SECTION(vfila,"LOGGER");
#pragma DATA_SECTION(wm_log,"LOGGER");
// #pragma DATA_SECTION(eqep_1,"LOGGER");
// #pragma DATA_SECTION(eqep_2,"LOGGER");
#pragma DATA_SECTION(delta_pos1,"LOGGER");
#pragma DATA_SECTION(delta_pos2,"LOGGER");
// #pragma DATA_SECTION(corr2,"LOGGER"); 


extern void epwmmodule_config(volatile struct EPWM_REGS *ePWM_Regs);
