/*=======================================================================================================================
    eQEP for Low-Speed and High-Speed Measuring with Capture Edge Unit and Unit
Time Out methods
=======================================================================================================================*/
// clang-format off

#define  UPEVENTDIV1   0x00
#define  UPEVENTDIV2   0x01
#define  UPEVENTDIV4   0x02
#define  UPEVENTDIV8   0x03
#define  UPEVENTDIV16  0x04

#define  SYSCLKDIV1    0x00
#define  SYSCLKDIV2    0x01
#define  SYSCLKDIV4    0x02
#define  SYSCLKDIV8    0x03
#define  SYSCLKDIV16   0x04
#define  SYSCLKDIV32   0x05
#define  SYSCLKDIV64   0x06
#define  SYSCLKDIV128  0x07

#define  QEPCNTMAX     10000L               // EQEP Counter Maximum Value

// clang-format on

/** A macro to calculate the hvfactor given a timeout_period and assuming a
 * SYSCLKOUT= 150 MHz.
 *
 * @param[in] timeout_period: the time between interrupts
 * @result: the hvfactor that allows for speed calculations.
 * 
 * It's based on the following calculation
 *
 *                   2 * pi            2 * pi
 *               ---------------      --------
 *               num_franjas * 4      2500 * 4       pi * 30_000
 * hv_factor = ----------------- = -------------- = --------------
 *                    AT           timeout_period   timeout_period
 *                                 --------------
 *                                    SYSCLKOUT
 *
 */
#define hvfactor_macro(timeout_period) (94247.7796077 / ((float)timeout_period))

#define INITIAL_TIMEOUT 20000UL
unsigned long eqep_timeout_period = INITIAL_TIMEOUT;
unsigned corrections = 0;
// EQEP Interrupt Subroutine    
interrupt void PTC5Feqep_isr() {
    const float ACCEPTABLE_ERR = 0.01;
    const float LOWEST_ACCEPTABLE_ERR = 0.005;
    const float MARGIN = 0.05; // percent bounds
    const float MARGIN_UP = 1 + MARGIN;
    const float MARGIN_DOWN = 1 - MARGIN;
    const unsigned long MAX_TIMEOUT_TRANS = 750000; // 5ms 200Hz 
    const unsigned long MIN_TIMEOUT_TRANS = 7500; // The current freq 20kHz
    const unsigned long MAX_TIMEOUT = 3000000; // 20ms 50Hz 
    static unsigned char max_timeout_hit = 0;
    static unsigned char min_timeout_hit = 0;
    static float hvfactor = hvfactor_macro(INITIAL_TIMEOUT);
    float abs_wm_k = fabs(wm_k);
    unsigned char is_in_perm = wm_ref > 0? 
                                (wm_k >= (MARGIN_DOWN) * wm_ref) 
                                && (wm_k <= (MARGIN_UP) *wm_ref)
                                : (wm_k <= (MARGIN_DOWN) * wm_ref) 
                                && (wm_k >= (MARGIN_UP) *wm_ref);
    
    // Edge Capture and Unit Time Out Direction Speed
    // The position of the encoder is reverse-> forwards is actually backwards
    if (EQep1Regs.QEPSTS.bit.QDF) { // Forward Diretcion QDF=1,
        eqeptmr = 0 - (float)EQep1Regs.QCPRDLAT;
        hdx = 0 - (float)EQep1Regs.QPOSLAT;
    } else { // Reverse Direction QDF=0
        eqeptmr = (float)EQep1Regs.QCPRDLAT;
        hdx = (float)QEPCNTMAX - (float)EQep1Regs.QPOSLAT;
    }
    // Capture Module error
    if (EQep1Regs.QEPSTS.bit.COEF) {
        EQep1Regs.QEPSTS.bit.COEF = 1;
    } else {
        wm_k = (float)hdx * hvfactor;
    }
    if (is_in_perm){
        if (eqep_timeout_period < MAX_TIMEOUT){        
            corrections = 1;
            eqep_timeout_period = MAX_TIMEOUT;
            EQep1Regs.QUPRD = eqep_timeout_period;
            hvfactor = hvfactor_macro(eqep_timeout_period);
        }
    } else {
        float bounds =  abs_wm_k * ACCEPTABLE_ERR;
        if ( !max_timeout_hit && (hvfactor > bounds)){
            corrections = 2;
            min_timeout_hit = 0;
            eqep_timeout_period *= (hvfactor/bounds);
            if (eqep_timeout_period > MAX_TIMEOUT_TRANS){
                eqep_timeout_period = MAX_TIMEOUT_TRANS;
                max_timeout_hit = 1;
            }
            EQep1Regs.QUPRD = eqep_timeout_period;
            hvfactor = hvfactor_macro(eqep_timeout_period);
        } else if (!min_timeout_hit && (hvfactor < abs_wm_k * LOWEST_ACCEPTABLE_ERR)){
            corrections = 3;
            max_timeout_hit = 0;
            eqep_timeout_period *= (hvfactor/bounds);
            if (eqep_timeout_period < MIN_TIMEOUT_TRANS){
                min_timeout_hit = 1;
                eqep_timeout_period = MIN_TIMEOUT_TRANS;
            }
            EQep1Regs.QUPRD = eqep_timeout_period;
            hvfactor = hvfactor_macro(eqep_timeout_period);
        }
    }
    
    EQep1Regs.QCLR.bit.UTO = 1;      // Clears Unit Time Out Interrupt Flag
    EQep1Regs.QCLR.bit.INT = 1;      // Clears Global EQEP1 Interrupt Flag
    PieCtrlRegs.PIEACK.bit.ACK5 = 1; // Clear the PIEACK of Group 5 for enables
                                     // Interrupt Resquest at CPU Level
}

// clang-format off
void PTC5Feqep_start(){
    // GPIO Configure
    EALLOW;                                                        // Enable writing to EALLOW protected registers
    SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK        = 1;                // Enable the SYSCLKOUT to the GPIO
    SysCtrlRegs.PCLKCR1.bit.EQEP1ENCLK         = 1;                // EQEP1 Module is Clocked by the SYSCLKOUT
    GpioCtrlRegs.GPBMUX2.bit.GPIO50            = 1;                // JP3 #13 GPIO50 as EQEP1A(Input)
    GpioCtrlRegs.GPBMUX2.bit.GPIO51            = 1;                // JP3 #14 GPIO51 as EQEP1B(Input)
    GpioCtrlRegs.GPBMUX2.bit.GPIO53            = 1;                // JP3 #14 GPIO53 as EQEP1I(Input)
    EDIS;                                                          // Disable writing to EALLOW protected registers
    // End GPIO Configure
    // QDU Module Configuration
    EQep1Regs.QDECCTL.bit.QSRC                = 00;               // EQEP1 as Quadrature Count Mode
    EQep1Regs.QDECCTL.bit.QAP                 = 0;                // EQEP1A input polarity No Efect
    //EQep1Regs.QDECCTL.bit.QAP               = 1;                 // EQEP1A input negate polarity
    EQep1Regs.QDECCTL.bit.QBP                 = 0;                // EQEP1B input polarity No Efect
    //EQep1Regs.QDECCTL.bit.QBP               = 1;                 // EQEP1B input negate polarity
    EQep1Regs.QDECCTL.bit.QIP                 = 0;                // EQEP1I input polarity No Efect
    EQep1Regs.QDECCTL.bit.QSP                 = 0;                // EQEP1S polarity No Efect
    EQep1Regs.QDECCTL.bit.SWAP                = 0;                // Quadrature-clock inputs are not swaped
    EQep1Regs.QDECCTL.bit.IGATE               = 0;                // Disable gating of index pulse
    EQep1Regs.QDECCTL.bit.XCR                 = 0;                // 2x Resolution Count
    EQep1Regs.QDECCTL.bit.SOEN                = 0;                // Disable position-compare syn output
    EQep1Regs.QDECCTL.bit.SPSEL               = 0;                // Index pin is used for sync output
    // End QDU Module Configuration
    // PCCU Module Configuration
    EQep1Regs.QEPCTL.bit.WDE                = 0;                // Disable the EQEP watchdog timer
    EQep1Regs.QEPCTL.bit.QCLM               = 1;                // EQEP capture latch on Unit Time Out
    EQep1Regs.QEPCTL.bit.QPEN               = 1;                // Enable EQEP position counter
    EQep1Regs.QEPCTL.bit.PCRM               = 3;                // Position Counter Reset on Unit Time Event
    EQep1Regs.QEPCTL.bit.SEI                = 0;                // Strobe Event actions disable
    EQep1Regs.QEPCTL.bit.IEI                = 0;                // Index Event actions disable
    EQep1Regs.QEPCTL.bit.SWI                = 0;                // Software Initialization action enable
    EQep1Regs.QEPCTL.bit.IEL                = 0;                // Index Event Latch Reserved
    EQep1Regs.QEPCTL.bit.SWI                = 0;                // Enable Software initialization
    EQep1Regs.QEPCTL.bit.FREE_SOFT          = 0x10;             // Position Counter is Unaffected by emulation suspend
    // End PCCU Module Configuration
    EQep1Regs.QPOSINIT                       = 0;                // EQEP Counter Initial Position
    EQep1Regs.QPOSMAX                        = QEPCNTMAX;        // EQEP Counter Max Position
    EQep1Regs.QPOSCMP                        = QEPCNTMAX;        // EQEP Position Compare
    EQep1Regs.QCTMR                          = 0;                // EQEP Position Compare
    // Position-Compare Configuration
    EQep1Regs.QPOSCTL.bit.PCSHDW            = 0;                // EQEP Position-Compare Load Inmediate
    EQep1Regs.QPOSCTL.bit.PCLOAD            = 0;                // Position Compare Loads in QPOSCNT=0
    EQep1Regs.QPOSCTL.bit.PCPOL             = 0;                // Polarity of sync output Active High pulse output
    EQep1Regs.QPOSCTL.bit.PCE               = 0;                // Position Compare Disable
    // End Position-Compare Configuration
    // Edge Capture Unit Configuration w_measure = velocfactor * X / dT
    EQep1Regs.QCAPCTL.bit.UPPS                = UPEVENTDIV1;       // EQEP Unit Event /1 (X=1)
    EQep1Regs.QCAPCTL.bit.CCPS                = SYSCLKDIV32;       // EQEP Capture Timer Prescaler /32
    EQep1Regs.QCAPCTL.bit.CEN                 = 1;                 // EQEP Capture is Enable
    // End Edge Capture Unit Configuration
    //UTIME Configuration
    EQep1Regs.QUPRD                            = INITIAL_TIMEOUT;        // Unit Time Out Period
    EQep1Regs.QEPCTL.bit.UTE                = 1;                   // Enable the EQEP Unit Timer
    //End UTIME Configuration
    EALLOW;                                                        // This is needed to write to EALLOW protected registers
    PieVectTable.EQEP1_INT                    = &PTC5Feqep_isr;    // EQEP Interrupt Address
    EDIS;                                                          // Disable writing to EALLOW protected registers

    // Unit Time Out Interrupt
    IER |= M_INT5;                                                 // Enable EQEP1 CPU-PIEIER5 for INT5 (Group 5)
    PieCtrlRegs.PIEIER5.bit.INTx1              = 1;                // Enable the EQEP1_INT PIEIER5.1 to interrupt resquest sent to CPU Level
    PieCtrlRegs.PIEACK.bit.ACK5                = 1;                // Clear the PIEACK of Group 5 for enables Interrupt Resquest at CPU Level
    EQep1Regs.QEINT.bit.UTO                    = 1;                // Unit Time Out Interrupt Enable
    // End Unit Time Out Interrupt

}
