% Ensayos para OMEGAFINA
% MRA 02-2023

close all; clear all; clc

%% Carga de los datos recogidos en los archivos de resultados



% velocidad wm seg�n interrupci�n EQEP
omega_isr = load('wm_log.txt');
% contador de inicios de la interrupci�n EQEP 
eqep_1 = load('eqep_1.txt');
eqep_2 = load('eqep_2.txt');
eqep = eqep_1 * 2^16  + eqep_2;
% valor QPOSLAT fuera de la interrupci�n EQEP 
correcciones = load('corr.txt');

% desescalado (paso de Uint16 a float)
omega_isr = omega_isr/65.534-500;
% qposlat = qposlat/1638.35 -20;

% represent graf

figure
plot( omega_isr, 'r');
xlabel('k')
ylabel('\omega (rad/s)')
%ylim([0, 90])
%xlim([0, 30000])
grid
figure
stairs( eqep, 'g');
grid
figure
stairs( correcciones, 'g');
grid
%figure
%histogram(conta_isr, 500)

%%
clc
[1 min( omega_isr)  max( omega_isr) ]
[2 min( conta_isr)  max( conta_isr) ]
%[3 min( qposlat)  max( qposlat) ]



fprintf('desviaci�n t�pica velocidad = sigma omega = %f (rad/s) \n', ...
    std( omega_isr) );




