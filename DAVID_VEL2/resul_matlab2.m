close all; clear all; clc

file_list_fig1 = ["20", "50", "75"];
fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );

ax = nexttile(tiles);
ax.Box = "off";
ax.Title.String = "Distintas velocidades en r�gimen permanente";
ax.Title.FontSize =20;

for sec= 1:3
    hold on;
    omega = load(file_list_fig1(sec)+"/wm_log.txt");
    omega = omega/65.534-500;
    plt = plot(ax, omega);
    plt.LineWidth = 2;
    ax.YLim = [0, 90];
    ax.YLabel.String = "\omega (rad/s)";
    ax.YLabel.FontSize =20;
    if sec == 3
        ax.XLabel.String = "Instante";
        ax.XLabel.FontSize =20;
    end
    hold off;
end
saveas(fig1,"Reg_perm2",'epsc');
saveas(fig1,"Reg_perm2",'png');            
%% fig change
clear;

Title_list_fig2 = ["Con cambio en el r�gimen permanente","Sin cambio en r�gimen permanente"];
file_list_fig2 = ["20_25", "20_25_no_perm"];
fig2 = figure;
fig2.Units = 'normalized';
fig2.Position = [0, 0, 1, 1];
tiles2 = tiledlayout( ...
    fig2, ...
    2, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
for sec= 1:2
    ax2 = nexttile(tiles2);
    omega = load(file_list_fig2(sec)+"/wm_log.txt");
    omega = omega/65.534-500;
    plt = plot(ax2, omega);
    plt.LineWidth = 2;
    ax2.Title.String = Title_list_fig2(sec);
    ax2.Title.FontSize =20;
    ax2.YLabel.FontSize =20;
    ax2.YLabel.String = "\omega (rad/s)";
    ax2.Box = "off";
    if sec == 2
        ax2.XLabel.FontSize =20;
        ax2.XLabel.String = "Instante";
    end
end
saveas(fig2,"change_compare",'epsc');
saveas(fig2,"change_compare",'png');

clear;
Title_list_fig2 = ["Escal�n positivo","Escal�n negativo"];
file_list_fig2 = ["25_70_4", "30_-20_4"];
fig2 = figure;
fig2.Units = 'normalized';
fig2.Position = [0, 0, 1, 1];
tiles2 = tiledlayout( ...
    fig2, ...
    2, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
for sec= 1:2
    ax2 = nexttile(tiles2);
    omega = load(file_list_fig2(sec)+"/wm_log.txt");
    omega = omega(1:15000);
    omega = omega/65.534-500;
    plt = plot(ax2, omega);
    plt.LineWidth = 2;
    ax2.Title.String = Title_list_fig2(sec);
    ax2.Title.FontSize =20;
    ax2.YLabel.FontSize =20;
    ax2.YLabel.String = "\omega (rad/s)";
    ax2.Box = "off";
    if sec == 2
        ax2.XLabel.FontSize =20;
        ax2.XLabel.String = "Instante";
    end
end
saveas(fig2,"change_2",'epsc');
saveas(fig2,"change_2",'png');