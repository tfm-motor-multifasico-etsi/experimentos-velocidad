clear; close all;

CONV_FACT = 94247.7796077;
err_from_time = @(timeout_period) CONV_FACT / timeout_period;


w_space = linspace(0.001, 1000*2*pi/60, 1000);
err = arrayfun(@(w) err_calc(w, 0.01)/ w, w_space);
err2 = arrayfun(@(w) err_calc(w, 0.005)/ w, w_space);
err3 = arrayfun(@(w) err_from_time(3000000)/ w, w_space);

fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );

ax = nexttile(tiles);
hold on;
plt1 = plot(ax, w_space,err);
plt2 = plot(ax, w_space, err2);
plt3 = plot(ax, w_space, err3);
plt1.LineWidth = 2;
plt2.LineWidth = 2;
plt3.LineWidth = 2;
lgnd = legend(ax, [ "Transient upper error", ...
                    "Transient lower error", ...
                    "Steady state error"]);
lgnd.FontSize = 20;
ax.Box = "off";
ax.Title.String = "Error rate in the motor's speed range";
ax.Title.FontSize = 20;
ax.XLim = [0, 1000*2*pi/60];
ax.XLabel.String = "\omega (rad/s)";
ax.XLabel.FontSize =20;
ax.YScale = 'log';
ax.YLabel.String = "Relative error (/1)";
ax.YLabel.FontSize = 20;


%[12.566370614359998, 25.132741228719997]

saveas(fig1,"err",'epsc');
saveas(fig1,"err",'svg');
saveas(fig1,"err",'png');
