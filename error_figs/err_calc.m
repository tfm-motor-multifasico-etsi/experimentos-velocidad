function [err] = err_calc(w,percent)
    MIN_TIME = 7500;
    MAX_TIME = 750000;
    CONV_FACT = 94247.7796077;
    err = percent * abs(w);
    t = CONV_FACT / err;
    if t > MAX_TIME
        err = CONV_FACT / MAX_TIME;
    end
    if t < MIN_TIME
        err = CONV_FACT / MIN_TIME;
    end
end

