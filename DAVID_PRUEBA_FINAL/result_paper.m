
%% Corrientes por cero
clear;

i_alpha = load('Filtro2/ialpha_k_log.txt');
i_alpha_ref = load('Filtro2/ialpha_ref_log.txt');
i_beta = load('Filtro2/ibeta_k_log.txt');
i_beta_ref = load('Filtro2/ibeta_ref_log.txt');

i_alpha = i_alpha/1638.35-20;
i_alpha_ref = i_alpha_ref/1638.35-20;
i_beta = i_beta/1638.35-20;
i_beta_ref = i_beta_ref/1638.35-20;

fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
ax = nexttile(tiles);
hold on
plt1 = plot(ax,(0:2800-1200)*4/20000, i_alpha(1200: 2800));
plt1.LineWidth = 2;

plt2 = plot(ax,(0:2800-1200)*4/20000, i_beta(1200: 2800));
plt2.LineWidth = 2;
ax.XLim = [0, (2800-1200) * 4/20000];
% plt1 = plot(ax, omega);
% plt.LineWidth = 2;
% 
% plt1 = plot(ax, omega);
% plt.LineWidth = 2;
ax.Box = "off";
ax.YLabel.String = "I (A)";
ax.YLabel.FontSize =20;
ax.XLabel.String = "t (s)";
ax.XLabel.FontSize =20;
ax.Title.String = "Zero cross";
ax.Title.FontSize =20;
lgnd = legend(ax, ["i_{\alpha}", "i_{\beta}"]);
lgnd.FontSize = 20;

hold off

saveas(fig1,"figuras/paso_por_cero",'epsc');
saveas(fig1,"figuras/paso_por_cero",'svg');
saveas(fig1,"figuras/paso_por_cero",'png'); 

%% Tres corrientes

clear;close all;

list = [["ialpha_k_log", "ialpha_ref_log"]; ["ibeta_k_log", "ibeta_ref_log"]];

Titles = ["High Load","Transient Load", "Low Load"];
Titles2 = ["i_{\alpha} ","i_{\beta} "];

Interval = [[900,900 + 2/25*20000/4];
            [2400, 2400 + 2/25*20000/4];
            [25000, 25000 + 2/25*20000/4]];
fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    2, ...
    3, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
for idx = 1:2
    i = load('Filter/'+list(idx, 1)+'.txt');
    i_ref = load('Filter/'+list(idx, 2)+'.txt');
    i = i/1638.35-20;
    i_ref = i_ref/1638.35-20;
    for jdx = 1:3
        ax = nexttile(tiles);
        hold on
        x_vals =(0:(Interval(jdx, 2)-Interval(jdx, 1)))*4/20000;
        plt1 = plot(ax,x_vals , i(Interval(jdx, 1):Interval(jdx, 2)));
        plt1.LineWidth = 2;
        plt2 = plot(ax,x_vals, i_ref(Interval(jdx, 1):Interval(jdx, 2)));
        plt2.LineWidth = 2;
        ax.Box = "off";
        ax.XLim = [0, x_vals(length(x_vals))];
        if jdx == 1
            ax.YLabel.String = "I [A]";
            ax.YLabel.FontSize =20;
        end
        if idx == 2
            ax.XLabel.String = "t (s)";
            ax.XLabel.FontSize =20;
        end
        ax.Title.String = Titles2(idx) + Titles(jdx);
        ax.Title.FontSize =20;
    end
    hold off
end

saveas(fig1,"figuras/distintas_cargas",'epsc');
saveas(fig1,"figuras/distintas_cargas",'svg');
saveas(fig1,"figuras/distintas_cargas",'png'); 


%% Todo junto_paper

folder_list = ["vel2", "Filtro2"];

x_vals = (1:30000)*4/20000;
fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
ax = nexttile(tiles);
hold on
for folder_idx = 1: length(folder_list)
    omega = load(folder_list(folder_idx)+"/wm_log.txt");
    omega = omega/65.534-500;
    plt = plot(ax, x_vals, omega);
    plt.LineWidth = 2;
end
lgnd = legend(ax, ["Nombre método", "Filter"]);
lgnd.FontSize = 20;
ax.Box = "off";
ax.YLabel.String = "\omega (rad/s)";
ax.YLabel.FontSize =20;
ax.XLabel.String = "t (s)";
ax.XLabel.FontSize =20;
ax.Title.String = "Step";
ax.Title.FontSize =20;
hold off
saveas(fig1,"figuras/2",'svg');
saveas(fig1,"figuras/2",'epsc');
saveas(fig1,"figuras/2",'png'); 

