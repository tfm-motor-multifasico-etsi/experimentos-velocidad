% Ensayos para OMEGAFINA
% MRA 02-2023
close all; clear all; clc

%% Carga de los datos recogidos en los archivos de resultados



% velocidad wm seg�n interrupci�n EQEP
omega_isr = load('wm_log.txt');
i_alpha = load('ialpha_k_log.txt');
i_alpha_ref = load('ialpha_ref_log.txt');

i_beta = load('ibeta_k_log.txt');
i_beta_ref = load('ibeta_ref_log.txt');

omega_isr = omega_isr/65.534-500;
i_alpha = i_alpha/1638.35-20;
i_alpha_ref = i_alpha_ref/1638.35-20;
i_beta = i_beta/1638.35-20;
i_beta_ref = i_beta_ref/1638.35-20;

figure
plot( omega_isr, 'r');
% hold on;
% plot( omega_isr2, 'g');
xlabel('k')
ylabel('\omega (rad/s)')
% ylim([0, 90])
xlim([0, 30000])
figure;
hold on;
plot( i_alpha, 'g');
plot( i_alpha_ref, 'b');
hold off;
figure;
hold on;
plot( i_beta, 'g');
plot( i_beta_ref, 'b');
hold off;