close all; clear; clc

%% Todo junto

folder_list = ["nada", "VEL1", "vel2", "Filtro2"];

x_vals = (1:30000)*4/20000;
fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
ax = nexttile(tiles);
hold on
for folder_idx = 1: length(folder_list)
    omega = load(folder_list(folder_idx)+"/wm_log.txt");
    omega = omega/65.534-500;
    plt = plot(ax, x_vals, omega);
    plt.LineWidth = 2;
end
lgnd = legend(ax, ["Original", "Con Compromiso", "Sin Compromiso", "Filtro"]);
lgnd.FontSize = 20;
ax.Box = "off";
ax.YLabel.String = "\omega (rad/s)";
ax.YLabel.FontSize =20;
ax.XLabel.String = "t (s)";
ax.XLabel.FontSize =20;
ax.Title.String = "Escal�n de 50% a -50%";
ax.Title.FontSize =20;
hold off

saveas(fig1,"figuras/cuatro",'epsc');
saveas(fig1,"figuras/cuatro",'png'); 
%% Tiled
clear;
x_vals = (1: 30000)*4/20000;
folder_list = ["nada", "VEL1", "vel2", "Filtro2"];
title_list = ["Original", "Con Compromiso", "Sin Compromiso", "Filtro"];
fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    2, ...
    2, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
for folder_idx = 1: length(folder_list)
    ax = nexttile(tiles);
    omega = load(folder_list(folder_idx)+"/wm_log.txt");
    omega = omega/65.534-500;
    plt = plot(ax,x_vals, omega);
    plt.LineWidth = 2;
    ax.Box = "off";
    ax.YLabel.String = "\omega (rad/s)";
    ax.YLabel.FontSize =20;
    ax.XLabel.String = "t (s)";
    ax.XLabel.FontSize = 20;
    ax.YLim = [-70, 60];

    ax.Title.String = title_list(folder_idx);
    ax.Title.FontSize =20;
end
saveas(fig1,"figuras/tiled",'epsc');
saveas(fig1,"figuras/tiled",'png'); 

