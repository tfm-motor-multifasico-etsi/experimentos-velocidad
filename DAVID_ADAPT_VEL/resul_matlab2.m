close all; clear all; clc

Title_list_fig1 = ["Error aceptable: [0, 0.5]%", "Error aceptable: [0, 1]%", "Error aceptable: [2, 5]%"];
file_list_fig1 = [["RES_0.5_20", "RES_0.5_50", "RES_0.5_75"];["RES_1_20", "RES_1_50", "RES_1_75"];["RES_5_20", "RES_5_50", "RES_5_75"]];
fig1 = figure;
fig1.Units = 'normalized';
fig1.Position = [0, 0, 1, 1];
tiles = tiledlayout( ...
    fig1, ...
    3, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
for sec= 1:3
    ax = nexttile(tiles);
    ax.Box = "off";
    hold on;
    for vel= 1:3
        omega = load(file_list_fig1(sec, vel)+"/wm_log.txt");
        omega = omega/65.534-500;
        ax.Title.String = Title_list_fig1(sec);
        ax.Title.FontSize =20;
        plt = plot(ax, omega);
        plt.LineWidth = 2;
        ax.YLim = [0, 90];
        ax.YLabel.String = "\omega (rad/s)";
        ax.YLabel.FontSize =20;
    end
    if sec == 3
        ax.XLabel.String = "Instante";
        ax.XLabel.FontSize =20;
    end
    hold off;
end
saveas(fig1,"Reg_perm",'epsc');
saveas(fig1,"Reg_perm",'png');            
%% fig change
clear;

Title_list_fig2 = ["Error aceptable: [0, 1]%", "Error aceptable: [2, 5]%"];
file_list_fig2 = ["RES_25_75", "RES_30_-20"];
fig2 = figure;
fig2.Units = 'normalized';
fig2.Position = [0, 0, 1, 1];
tiles2 = tiledlayout( ...
    fig2, ...
    2, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
for sec= 1:2
    ax2 = nexttile(tiles2);
    omega = load(file_list_fig2(sec)+"/wm_log.txt");
    omega = omega/65.534-500;
    plt = plot(ax2, omega);
    plt.LineWidth = 2;
    ax2.Title.String = Title_list_fig2(sec);
    ax2.Title.FontSize =20;
    ax2.YLabel.FontSize =20;
    ax2.YLabel.String = "\omega (rad/s)";
    ax2.Box = "off";
    if sec == 2
        ax2.XLabel.FontSize =20;
        ax2.XLabel.String = "Instante";
    end
end
saveas(fig2,"change",'epsc');
saveas(fig2,"change",'png');

clear;
%% fig neg
fig = figure;
tile = tiledlayout( ...
    fig, ...
    1, ...
    1, ...
    'Padding', 'none', ...
    'TileSpacing', 'compact' ...
    );
ax = nexttile(tile);
fig.Units = 'normalized';
fig.Position = [0, 0, 1, 1];
omega = load("RES_5_-50/wm_log.txt");
omega = omega/65.534-500;
plt = plot(ax, omega);
plt.LineWidth=2;
ax.YLim = [-100, 0];
ax.Box = "off";
ax.Title.String = "Error aceptable [2, 5]%";
ax.Title.FontSize =20;
ax.YLabel.FontSize =20;
ax.YLabel.String = "\omega (rad/s)";
ax.XLabel.String = "Instante";
ax.XLabel.FontSize =20;
saveas(fig, "negative", "epsc");
saveas(fig, "negative", "png");
clear; close all;