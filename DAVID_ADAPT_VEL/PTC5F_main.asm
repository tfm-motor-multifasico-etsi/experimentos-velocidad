;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v5.0.1 *
;* Date/Time created: Thu Mar 30 13:39:40 2023                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --quiet --silicon_version=28 --symdebug:coff 
FP	.set	XAR2
	.file	"C:\CCStudio_v3.3\MyProjects\DAVID_ADAPT_VEL\PTC5F_main.c"
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger1+0,32
	.field  	0,16			; _logger1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger2+0,32
	.field  	0,16			; _logger2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_dsampled+0,32
	.field  	0,16			; _dsampled @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger4+0,32
	.field  	0,16			; _logger4 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger5+0,32
	.field  	0,16			; _logger5 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger+0,32
	.field  	0,16			; _logger @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger3+0,32
	.field  	0,16			; _logger3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_global_cont_eqep_interr+0,32
	.field  	0,16			; _global_cont_eqep_interr @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_stop+0,32
	.field  	0,16			; _stop @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_timeout+0,32
	.field  	1,16			; _timeout @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_secntr+0,32
	.field  	0,16			; _secntr @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_corrections+0,32
	.field  	0,16			; _corrections @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_imcntr+0,32
	.field  	0,16			; _imcntr @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_max_timeout_hit$1+0,32
	.field  	0,16			; _max_timeout_hit$1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_min_timeout_hit$2+0,32
	.field  	0,16			; _min_timeout_hit$2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger8+0,32
	.field  	0,16			; _logger8 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger7+0,32
	.field  	0,16			; _logger7 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_loggaux+0,32
	.field  	0,16			; _loggaux @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_cloggaux+0,32
	.field  	0,16			; _cloggaux @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_clogger+0,32
	.field  	0,16			; _clogger @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger6+0,32
	.field  	0,16			; _logger6 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mi_qposlat+0,32
	.xfloat	2.22219991683959960938e+00		; _mi_qposlat @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mi_qcprdlat+0,32
	.xfloat	1.11109995841979980469e+00		; _mi_qcprdlat @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_eqeptmr+0,32
	.xfloat	1.00000000000000000000e+00		; _eqeptmr @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_wm_km1+0,32
	.xfloat	0.00000000000000000000e+00		; _wm_km1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_hdx+0,32
	.xfloat	0.00000000000000000000e+00		; _hdx @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mstart2+0,32
	.xfloat	0.00000000000000000000e+00		; _mstart2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mstart+0,32
	.xfloat	0.00000000000000000000e+00		; _mstart @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ThetaI+0,32
	.xfloat	0.00000000000000000000e+00		; _ThetaI @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_contfallo+0,32
	.xfloat	0.00000000000000000000e+00		; _contfallo @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_w1+0,32
	.xfloat	0.00000000000000000000e+00		; _w1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_CCarga+0,32
	.xfloat	4.88000011444091796875e+00		; _CCarga @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_Imax+0,32
	.xfloat	0.00000000000000000000e+00		; _Imax @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_wm_d3+0,32
	.xfloat	0.00000000000000000000e+00		; _wm_d3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_w2+0,32
	.xfloat	0.00000000000000000000e+00		; _w2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_eqep_timeout_period+0,32
	.field  	20000,32			; _eqep_timeout_period @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_metodo+0,32
	.xfloat	2.00000000000000000000e+00		; _metodo @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_hvfactor$3+0,32
	.xfloat	4.71238899230957031250e+00		; _hvfactor$3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_KPIW+0,32
	.xfloat	6.00000000000000000000e+00		; _KPIW @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_KPW+0,32
	.xfloat	7.99999982118606567383e-02		; _KPW @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_1,16
	.field  	_Tabla_VV+0,32
	.field  	10,16			; _Tabla_VV[0][0] @ 0
	.field  	4,16			; _Tabla_VV[0][1] @ 16
	.field  	1,16			; _Tabla_VV[0][2] @ 32
	.field  	7,16			; _Tabla_VV[0][3] @ 48
	.field  	9,16			; _Tabla_VV[1][0] @ 64
	.field  	2,16			; _Tabla_VV[1][1] @ 80
	.field  	2,16			; _Tabla_VV[1][2] @ 96
	.field  	9,16			; _Tabla_VV[1][3] @ 112
	.field  	8,16			; _Tabla_VV[2][0] @ 128
	.field  	5,16			; _Tabla_VV[2][1] @ 144
	.field  	3,16			; _Tabla_VV[2][2] @ 160
	.field  	6,16			; _Tabla_VV[2][3] @ 176
	.field  	10,16			; _Tabla_VV[3][0] @ 192
	.field  	4,16			; _Tabla_VV[3][1] @ 208
	.field  	1,16			; _Tabla_VV[3][2] @ 224
	.field  	7,16			; _Tabla_VV[3][3] @ 240
$C$IR_1:	.set	16

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_2,16
	.field  	_vsy+0,32
	.xfloat	0.00000000000000000000e+00		; _vsy[0] @ 0
	.xfloat	1.45309999585151672363e-01		; _vsy[1] @ 32
	.xfloat	0.00000000000000000000e+00		; _vsy[2] @ 64
	.xfloat	-2.35113993287086486816e-01		; _vsy[3] @ 96
	.xfloat	-1.45309999585151672363e-01		; _vsy[4] @ 128
	.xfloat	2.35113993287086486816e-01		; _vsy[5] @ 160
	.xfloat	-2.35114097595214843750e-01		; _vsy[6] @ 192
	.xfloat	1.45309999585151672363e-01		; _vsy[7] @ 224
	.xfloat	2.35113993287086486816e-01		; _vsy[8] @ 256
	.xfloat	0.00000000000000000000e+00		; _vsy[9] @ 288
	.xfloat	-1.45309999585151672363e-01		; _vsy[10] @ 320
	.xfloat	0.00000000000000000000e+00		; _vsy[11] @ 352
$C$IR_2:	.set	24

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_3,16
	.field  	_vsa+0,32
	.xfloat	0.00000000000000000000e+00		; _vsa[0] @ 0
	.xfloat	-2.00000002980232238770e-01		; _vsa[1] @ 32
	.xfloat	-6.47210001945495605469e-01		; _vsa[2] @ 64
	.xfloat	-5.23609995841979980469e-01		; _vsa[3] @ 96
	.xfloat	-2.00000002980232238770e-01		; _vsa[4] @ 128
	.xfloat	-5.23609995841979980469e-01		; _vsa[5] @ 160
	.xfloat	5.23609995841979980469e-01		; _vsa[6] @ 192
	.xfloat	2.00000002980232238770e-01		; _vsa[7] @ 224
	.xfloat	5.23609995841979980469e-01		; _vsa[8] @ 256
	.xfloat	6.47210001945495605469e-01		; _vsa[9] @ 288
	.xfloat	2.00000002980232238770e-01		; _vsa[10] @ 320
	.xfloat	0.00000000000000000000e+00		; _vsa[11] @ 352
$C$IR_3:	.set	24

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_4,16
	.field  	_vsb+0,32
	.xfloat	0.00000000000000000000e+00		; _vsb[0] @ 0
	.xfloat	-6.15540027618408203125e-01		; _vsb[1] @ 32
	.xfloat	0.00000000000000000000e+00		; _vsb[2] @ 64
	.xfloat	-3.80419999361038208008e-01		; _vsb[3] @ 96
	.xfloat	6.15540027618408203125e-01		; _vsb[4] @ 128
	.xfloat	3.80419999361038208008e-01		; _vsb[5] @ 160
	.xfloat	-3.80419999361038208008e-01		; _vsb[6] @ 192
	.xfloat	-6.15540027618408203125e-01		; _vsb[7] @ 224
	.xfloat	3.80419999361038208008e-01		; _vsb[8] @ 256
	.xfloat	0.00000000000000000000e+00		; _vsb[9] @ 288
	.xfloat	6.15540027618408203125e-01		; _vsb[10] @ 320
	.xfloat	0.00000000000000000000e+00		; _vsb[11] @ 352
$C$IR_4:	.set	24

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_5,16
	.field  	_vsx+0,32
	.xfloat	0.00000000000000000000e+00		; _vsx[0] @ 0
	.xfloat	-2.00000002980232238770e-01		; _vsx[1] @ 32
	.xfloat	2.47214004397392272949e-01		; _vsx[2] @ 64
	.xfloat	-7.63930007815361022949e-02		; _vsx[3] @ 96
	.xfloat	-2.00000002980232238770e-01		; _vsx[4] @ 128
	.xfloat	-7.63930007815361022949e-02		; _vsx[5] @ 160
	.xfloat	7.63930007815361022949e-02		; _vsx[6] @ 192
	.xfloat	2.00000002980232238770e-01		; _vsx[7] @ 224
	.xfloat	7.63930007815361022949e-02		; _vsx[8] @ 256
	.xfloat	-2.47214004397392272949e-01		; _vsx[9] @ 288
	.xfloat	2.00000002980232238770e-01		; _vsx[10] @ 320
	.xfloat	0.00000000000000000000e+00		; _vsx[11] @ 352
$C$IR_5:	.set	24

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_6,16
	.field  	_SCMP+0,32
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][0] @ 0
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][1] @ 32
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][2] @ 64
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][3] @ 96
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][4] @ 128
	.xfloat	0.00000000000000000000e+00		; _SCMP[1][0] @ 160
	.xfloat	0.00000000000000000000e+00		; _SCMP[1][1] @ 192
	.xfloat	0.00000000000000000000e+00		; _SCMP[1][2] @ 224
	.xfloat	1.00000000000000000000e+00		; _SCMP[1][3] @ 256
	.xfloat	1.00000000000000000000e+00		; _SCMP[1][4] @ 288
	.xfloat	0.00000000000000000000e+00		; _SCMP[2][0] @ 320
	.xfloat	0.00000000000000000000e+00		; _SCMP[2][1] @ 352
	.xfloat	1.00000000000000000000e+00		; _SCMP[2][2] @ 384
	.xfloat	1.00000000000000000000e+00		; _SCMP[2][3] @ 416
	.xfloat	0.00000000000000000000e+00		; _SCMP[2][4] @ 448
	.xfloat	0.00000000000000000000e+00		; _SCMP[3][0] @ 480
	.xfloat	0.00000000000000000000e+00		; _SCMP[3][1] @ 512
	.xfloat	1.00000000000000000000e+00		; _SCMP[3][2] @ 544
	.xfloat	1.00000000000000000000e+00		; _SCMP[3][3] @ 576
	.xfloat	1.00000000000000000000e+00		; _SCMP[3][4] @ 608
	.xfloat	0.00000000000000000000e+00		; _SCMP[4][0] @ 640
	.xfloat	1.00000000000000000000e+00		; _SCMP[4][1] @ 672
	.xfloat	1.00000000000000000000e+00		; _SCMP[4][2] @ 704
	.xfloat	0.00000000000000000000e+00		; _SCMP[4][3] @ 736
	.xfloat	0.00000000000000000000e+00		; _SCMP[4][4] @ 768
	.xfloat	0.00000000000000000000e+00		; _SCMP[5][0] @ 800
	.xfloat	1.00000000000000000000e+00		; _SCMP[5][1] @ 832
	.xfloat	1.00000000000000000000e+00		; _SCMP[5][2] @ 864
	.xfloat	1.00000000000000000000e+00		; _SCMP[5][3] @ 896
	.xfloat	0.00000000000000000000e+00		; _SCMP[5][4] @ 928
	.xfloat	1.00000000000000000000e+00		; _SCMP[6][0] @ 960
	.xfloat	0.00000000000000000000e+00		; _SCMP[6][1] @ 992
	.xfloat	0.00000000000000000000e+00		; _SCMP[6][2] @ 1024
	.xfloat	0.00000000000000000000e+00		; _SCMP[6][3] @ 1056
	.xfloat	1.00000000000000000000e+00		; _SCMP[6][4] @ 1088
	.xfloat	1.00000000000000000000e+00		; _SCMP[7][0] @ 1120
	.xfloat	0.00000000000000000000e+00		; _SCMP[7][1] @ 1152
	.xfloat	0.00000000000000000000e+00		; _SCMP[7][2] @ 1184
	.xfloat	1.00000000000000000000e+00		; _SCMP[7][3] @ 1216
	.xfloat	1.00000000000000000000e+00		; _SCMP[7][4] @ 1248
	.xfloat	1.00000000000000000000e+00		; _SCMP[8][0] @ 1280
	.xfloat	1.00000000000000000000e+00		; _SCMP[8][1] @ 1312
	.xfloat	0.00000000000000000000e+00		; _SCMP[8][2] @ 1344
	.xfloat	0.00000000000000000000e+00		; _SCMP[8][3] @ 1376
	.xfloat	0.00000000000000000000e+00		; _SCMP[8][4] @ 1408
	.xfloat	1.00000000000000000000e+00		; _SCMP[9][0] @ 1440
	.xfloat	1.00000000000000000000e+00		; _SCMP[9][1] @ 1472
	.xfloat	0.00000000000000000000e+00		; _SCMP[9][2] @ 1504
	.xfloat	0.00000000000000000000e+00		; _SCMP[9][3] @ 1536
	.xfloat	1.00000000000000000000e+00		; _SCMP[9][4] @ 1568
	.xfloat	1.00000000000000000000e+00		; _SCMP[10][0] @ 1600
	.xfloat	1.00000000000000000000e+00		; _SCMP[10][1] @ 1632
	.xfloat	1.00000000000000000000e+00		; _SCMP[10][2] @ 1664
	.xfloat	0.00000000000000000000e+00		; _SCMP[10][3] @ 1696
	.xfloat	0.00000000000000000000e+00		; _SCMP[10][4] @ 1728
	.xfloat	1.00000000000000000000e+00		; _SCMP[11][0] @ 1760
	.xfloat	1.00000000000000000000e+00		; _SCMP[11][1] @ 1792
	.xfloat	1.00000000000000000000e+00		; _SCMP[11][2] @ 1824
	.xfloat	1.00000000000000000000e+00		; _SCMP[11][3] @ 1856
	.xfloat	1.00000000000000000000e+00		; _SCMP[11][4] @ 1888
$C$IR_6:	.set	120

	.global	_logger1
_logger1:	.usect	".ebss",1,1,0
	.sym	_logger1,_logger1, 14, 2, 16
	.global	_logger2
_logger2:	.usect	".ebss",1,1,0
	.sym	_logger2,_logger2, 14, 2, 16
	.global	_dsampled
_dsampled:	.usect	".ebss",1,1,0
	.sym	_dsampled,_dsampled, 14, 2, 16
	.global	_logger4
_logger4:	.usect	".ebss",1,1,0
	.sym	_logger4,_logger4, 14, 2, 16
	.global	_logger5
_logger5:	.usect	".ebss",1,1,0
	.sym	_logger5,_logger5, 14, 2, 16
	.global	_logger
_logger:	.usect	".ebss",1,1,0
	.sym	_logger,_logger, 14, 2, 16
	.global	_logger3
_logger3:	.usect	".ebss",1,1,0
	.sym	_logger3,_logger3, 14, 2, 16
	.global	_global_cont_eqep_interr
_global_cont_eqep_interr:	.usect	".ebss",1,1,0
	.sym	_global_cont_eqep_interr,_global_cont_eqep_interr, 14, 2, 16
	.global	_pwm_period
_pwm_period:	.usect	".ebss",1,1,0
	.sym	_pwm_period,_pwm_period, 14, 2, 16
	.global	_Fm
_Fm:	.usect	".ebss",1,1,0
	.sym	_Fm,_Fm, 14, 2, 16
	.global	_stop
_stop:	.usect	".ebss",1,1,0
	.sym	_stop,_stop, 14, 2, 16
	.global	_ret_val_mon
_ret_val_mon:	.usect	".ebss",1,1,0
	.sym	_ret_val_mon,_ret_val_mon, 14, 2, 16
	.global	_Vdclink_medido
_Vdclink_medido:	.usect	".ebss",1,1,0
	.sym	_Vdclink_medido,_Vdclink_medido, 14, 2, 16
	.global	_timeout
_timeout:	.usect	".ebss",1,1,0
	.sym	_timeout,_timeout, 4, 2, 16
	.global	_secntr
_secntr:	.usect	".ebss",1,1,0
	.sym	_secntr,_secntr, 14, 2, 16
	.global	_corrections
_corrections:	.usect	".ebss",1,1,0
	.sym	_corrections,_corrections, 14, 2, 16
	.global	_imcntr
_imcntr:	.usect	".ebss",1,1,0
	.sym	_imcntr,_imcntr, 14, 2, 16
_max_timeout_hit$1:	.usect	".ebss",1,1,0
_min_timeout_hit$2:	.usect	".ebss",1,1,0
	.global	_logger8
_logger8:	.usect	".ebss",1,1,0
	.sym	_logger8,_logger8, 14, 2, 16
	.global	_logger7
_logger7:	.usect	".ebss",1,1,0
	.sym	_logger7,_logger7, 14, 2, 16
	.global	_loggaux
_loggaux:	.usect	".ebss",1,1,0
	.sym	_loggaux,_loggaux, 14, 2, 16
	.global	_cloggaux
_cloggaux:	.usect	".ebss",1,1,0
	.sym	_cloggaux,_cloggaux, 14, 2, 16
	.global	_clogger
_clogger:	.usect	".ebss",1,1,0
	.sym	_clogger,_clogger, 14, 2, 16
	.global	_logger6
_logger6:	.usect	".ebss",1,1,0
	.sym	_logger6,_logger6, 14, 2, 16
	.global	_mi_qposlat
_mi_qposlat:	.usect	".ebss",2,1,1
	.sym	_mi_qposlat,_mi_qposlat, 6, 2, 32
	.global	_mi_qcprdlat
_mi_qcprdlat:	.usect	".ebss",2,1,1
	.sym	_mi_qcprdlat,_mi_qcprdlat, 6, 2, 32
	.global	_TmA11
_TmA11:	.usect	".ebss",2,1,1
	.sym	_TmA11,_TmA11, 6, 2, 32
	.global	_eqeptmr
_eqeptmr:	.usect	".ebss",2,1,1
	.sym	_eqeptmr,_eqeptmr, 6, 2, 32
	.global	_wm_km1
_wm_km1:	.usect	".ebss",2,1,1
	.sym	_wm_km1,_wm_km1, 6, 2, 32
	.global	_veloc_radsec
_veloc_radsec:	.usect	".ebss",2,1,1
	.sym	_veloc_radsec,_veloc_radsec, 6, 2, 32
	.global	_hdx
_hdx:	.usect	".ebss",2,1,1
	.sym	_hdx,_hdx, 6, 2, 32
	.global	_e_alpha
_e_alpha:	.usect	".ebss",2,1,1
	.sym	_e_alpha,_e_alpha, 6, 2, 32
	.global	_G1
_G1:	.usect	".ebss",2,1,1
	.sym	_G1,_G1, 6, 2, 32
	.global	_TmA44
_TmA44:	.usect	".ebss",2,1,1
	.sym	_TmA44,_TmA44, 6, 2, 32
	.global	_G3
_G3:	.usect	".ebss",2,1,1
	.sym	_G3,_G3, 6, 2, 32
	.global	_G2
_G2:	.usect	".ebss",2,1,1
	.sym	_G2,_G2, 6, 2, 32
	.global	_TmA21
_TmA21:	.usect	".ebss",2,1,1
	.sym	_TmA21,_TmA21, 6, 2, 32
	.global	_TmA12
_TmA12:	.usect	".ebss",2,1,1
	.sym	_TmA12,_TmA12, 6, 2, 32
	.global	_TmA33
_TmA33:	.usect	".ebss",2,1,1
	.sym	_TmA33,_TmA33, 6, 2, 32
	.global	_TmA22
_TmA22:	.usect	".ebss",2,1,1
	.sym	_TmA22,_TmA22, 6, 2, 32
	.global	_mstart2
_mstart2:	.usect	".ebss",2,1,1
	.sym	_mstart2,_mstart2, 6, 2, 32
	.global	_mstart
_mstart:	.usect	".ebss",2,1,1
	.sym	_mstart,_mstart, 6, 2, 32
	.global	_ThetaI
_ThetaI:	.usect	".ebss",2,1,1
	.sym	_ThetaI,_ThetaI, 6, 2, 32
	.global	_Vdc
_Vdc:	.usect	".ebss",2,1,1
	.sym	_Vdc,_Vdc, 6, 2, 32
	.global	_contfallo
_contfallo:	.usect	".ebss",2,1,1
	.sym	_contfallo,_contfallo, 6, 2, 32
	.global	_w1
_w1:	.usect	".ebss",2,1,1
	.sym	_w1,_w1, 6, 2, 32
	.global	_costhetae
_costhetae:	.usect	".ebss",2,1,1
	.sym	_costhetae,_costhetae, 6, 2, 32
	.global	_sinthetae
_sinthetae:	.usect	".ebss",2,1,1
	.sym	_sinthetae,_sinthetae, 6, 2, 32
	.global	_Tcarga
_Tcarga:	.usect	".ebss",2,1,1
	.sym	_Tcarga,_Tcarga, 6, 2, 32
	.global	_CCarga
_CCarga:	.usect	".ebss",2,1,1
	.sym	_CCarga,_CCarga, 6, 2, 32
	.global	_Tcarga_pre
_Tcarga_pre:	.usect	".ebss",2,1,1
	.sym	_Tcarga_pre,_Tcarga_pre, 6, 2, 32
	.global	_Imax
_Imax:	.usect	".ebss",2,1,1
	.sym	_Imax,_Imax, 6, 2, 32
	.global	_Ke
_Ke:	.usect	".ebss",2,1,1
	.sym	_Ke,_Ke, 6, 2, 32
	.global	_e_km1
_e_km1:	.usect	".ebss",2,1,1
	.sym	_e_km1,_e_km1, 6, 2, 32
	.global	_Kinte
_Kinte:	.usect	".ebss",2,1,1
	.sym	_Kinte,_Kinte, 6, 2, 32
	.global	_inte_km1
_inte_km1:	.usect	".ebss",2,1,1
	.sym	_inte_km1,_inte_km1, 6, 2, 32
	.global	_inte_k
_inte_k:	.usect	".ebss",2,1,1
	.sym	_inte_k,_inte_k, 6, 2, 32
	.global	_G4
_G4:	.usect	".ebss",2,1,1
	.sym	_G4,_G4, 6, 2, 32
	.global	_wm_d3
_wm_d3:	.usect	".ebss",2,1,1
	.sym	_wm_d3,_wm_d3, 6, 2, 32
	.global	_w2
_w2:	.usect	".ebss",2,1,1
	.sym	_w2,_w2, 6, 2, 32
	.global	_wm_k
_wm_k:	.usect	".ebss",2,1,1
	.sym	_wm_k,_wm_k, 6, 2, 32
	.global	_wr
_wr:	.usect	".ebss",2,1,1
	.sym	_wr,_wr, 6, 2, 32
	.global	_wm_ref
_wm_ref:	.usect	".ebss",2,1,1
	.sym	_wm_ref,_wm_ref, 6, 2, 32
	.global	_Werror
_Werror:	.usect	".ebss",2,1,1
	.sym	_Werror,_Werror, 6, 2, 32
	.global	_signob
_signob:	.usect	".ebss",2,1,1
	.sym	_signob,_signob, 6, 2, 32
	.global	_signoa
_signoa:	.usect	".ebss",2,1,1
	.sym	_signoa,_signoa, 6, 2, 32
	.global	_U_alfa
_U_alfa:	.usect	".ebss",2,1,1
	.sym	_U_alfa,_U_alfa, 6, 2, 32
	.global	_U_tau1
_U_tau1:	.usect	".ebss",2,1,1
	.sym	_U_tau1,_U_tau1, 6, 2, 32
	.global	_tau_g
_tau_g:	.usect	".ebss",2,1,1
	.sym	_tau_g,_tau_g, 6, 2, 32
	.global	_g_alfa
_g_alfa:	.usect	".ebss",2,1,1
	.sym	_g_alfa,_g_alfa, 6, 2, 32
	.global	_H_beta
_H_beta:	.usect	".ebss",2,1,1
	.sym	_H_beta,_H_beta, 6, 2, 32
	.global	_g_beta
_g_beta:	.usect	".ebss",2,1,1
	.sym	_g_beta,_g_beta, 6, 2, 32
	.global	_U_r2
_U_r2:	.usect	".ebss",2,1,1
	.sym	_U_r2,_U_r2, 6, 2, 32
	.global	_mi_r2
_mi_r2:	.usect	".ebss",2,1,1
	.sym	_mi_r2,_mi_r2, 6, 2, 32
	.global	_cero
_cero:	.usect	".ebss",2,1,1
	.sym	_cero,_cero, 6, 2, 32
	.global	_j
_j:	.usect	".ebss",2,1,1
	.sym	_j,_j, 6, 2, 32
	.global	_Ialfa_ref_ant
_Ialfa_ref_ant:	.usect	".ebss",2,1,1
	.sym	_Ialfa_ref_ant,_Ialfa_ref_ant, 6, 2, 32
	.global	_eqep_timeout_period
_eqep_timeout_period:	.usect	".ebss",2,1,1
	.sym	_eqep_timeout_period,_eqep_timeout_period, 15, 2, 32
	.global	_metodo
_metodo:	.usect	".ebss",2,1,1
	.sym	_metodo,_metodo, 6, 2, 32
	.global	_ia
_ia:	.usect	".ebss",2,1,1
	.sym	_ia,_ia, 6, 2, 32
	.global	_U_tau2
_U_tau2:	.usect	".ebss",2,1,1
	.sym	_U_tau2,_U_tau2, 6, 2, 32
	.global	_ib
_ib:	.usect	".ebss",2,1,1
	.sym	_ib,_ib, 6, 2, 32
	.global	_ib_p
_ib_p:	.usect	".ebss",2,1,1
	.sym	_ib_p,_ib_p, 6, 2, 32
	.global	_ia_p
_ia_p:	.usect	".ebss",2,1,1
	.sym	_ia_p,_ia_p, 6, 2, 32
_hvfactor$3:	.usect	".ebss",2,1,1
	.global	_i_beta_cte
_i_beta_cte:	.usect	".ebss",2,1,1
	.sym	_i_beta_cte,_i_beta_cte, 6, 2, 32
	.global	_i_alpha_cte
_i_alpha_cte:	.usect	".ebss",2,1,1
	.sym	_i_alpha_cte,_i_alpha_cte, 6, 2, 32
	.global	_i_x_cte
_i_x_cte:	.usect	".ebss",2,1,1
	.sym	_i_x_cte,_i_x_cte, 6, 2, 32
	.global	_i_alpha_p1
_i_alpha_p1:	.usect	".ebss",2,1,1
	.sym	_i_alpha_p1,_i_alpha_p1, 6, 2, 32
	.global	_i_y_cte
_i_y_cte:	.usect	".ebss",2,1,1
	.sym	_i_y_cte,_i_y_cte, 6, 2, 32
	.global	_e_x
_e_x:	.usect	".ebss",2,1,1
	.sym	_e_x,_e_x, 6, 2, 32
	.global	_e_beta
_e_beta:	.usect	".ebss",2,1,1
	.sym	_e_beta,_e_beta, 6, 2, 32
	.global	_e_y
_e_y:	.usect	".ebss",2,1,1
	.sym	_e_y,_e_y, 6, 2, 32
	.global	_Jsv
_Jsv:	.usect	".ebss",2,1,1
	.sym	_Jsv,_Jsv, 6, 2, 32
	.global	_Jopt
_Jopt:	.usect	".ebss",2,1,1
	.sym	_Jopt,_Jopt, 6, 2, 32
	.global	_i_x_med
_i_x_med:	.usect	".ebss",2,1,1
	.sym	_i_x_med,_i_x_med, 6, 2, 32
	.global	_i_beta_med
_i_beta_med:	.usect	".ebss",2,1,1
	.sym	_i_beta_med,_i_beta_med, 6, 2, 32
	.global	_i_y_med
_i_y_med:	.usect	".ebss",2,1,1
	.sym	_i_y_med,_i_y_med, 6, 2, 32
	.global	_UmTmA22
_UmTmA22:	.usect	".ebss",2,1,1
	.sym	_UmTmA22,_UmTmA22, 6, 2, 32
	.global	_UmTmA11
_UmTmA11:	.usect	".ebss",2,1,1
	.sym	_UmTmA11,_UmTmA11, 6, 2, 32
	.global	_i_beta_p
_i_beta_p:	.usect	".ebss",2,1,1
	.sym	_i_beta_p,_i_beta_p, 6, 2, 32
	.global	_i_alpha_p2
_i_alpha_p2:	.usect	".ebss",2,1,1
	.sym	_i_alpha_p2,_i_alpha_p2, 6, 2, 32
	.global	_i_x_p
_i_x_p:	.usect	".ebss",2,1,1
	.sym	_i_x_p,_i_x_p, 6, 2, 32
	.global	_i_alpha_med
_i_alpha_med:	.usect	".ebss",2,1,1
	.sym	_i_alpha_med,_i_alpha_med, 6, 2, 32
	.global	_i_y_p
_i_y_p:	.usect	".ebss",2,1,1
	.sym	_i_y_p,_i_y_p, 6, 2, 32
	.global	_H_alpha
_H_alpha:	.usect	".ebss",2,1,1
	.sym	_H_alpha,_H_alpha, 6, 2, 32
	.global	_Ib_medido
_Ib_medido:	.usect	".ebss",2,1,1
	.sym	_Ib_medido,_Ib_medido, 6, 2, 32
	.global	_Ib_mir
_Ib_mir:	.usect	".ebss",2,1,1
	.sym	_Ib_mir,_Ib_mir, 6, 2, 32
	.global	_i_alpha
_i_alpha:	.usect	".ebss",2,1,1
	.sym	_i_alpha,_i_alpha, 6, 2, 32
	.global	_Ia_medido
_Ia_medido:	.usect	".ebss",2,1,1
	.sym	_Ia_medido,_Ia_medido, 6, 2, 32
	.global	_Ia_mir
_Ia_mir:	.usect	".ebss",2,1,1
	.sym	_Ia_mir,_Ia_mir, 6, 2, 32
	.global	_Id_medido
_Id_medido:	.usect	".ebss",2,1,1
	.sym	_Id_medido,_Id_medido, 6, 2, 32
	.global	_Id_mir
_Id_mir:	.usect	".ebss",2,1,1
	.sym	_Id_mir,_Id_mir, 6, 2, 32
	.global	_i_x
_i_x:	.usect	".ebss",2,1,1
	.sym	_i_x,_i_x, 6, 2, 32
	.global	_i_beta
_i_beta:	.usect	".ebss",2,1,1
	.sym	_i_beta,_i_beta, 6, 2, 32
	.global	_Ic_medido
_Ic_medido:	.usect	".ebss",2,1,1
	.sym	_Ic_medido,_Ic_medido, 6, 2, 32
	.global	_SC_suma
_SC_suma:	.usect	".ebss",2,1,1
	.sym	_SC_suma,_SC_suma, 6, 2, 32
	.global	_KPIW
_KPIW:	.usect	".ebss",2,1,1
	.sym	_KPIW,_KPIW, 6, 2, 32
	.global	_Tm
_Tm:	.usect	".ebss",2,1,1
	.sym	_Tm,_Tm, 6, 2, 32
	.global	_KPW
_KPW:	.usect	".ebss",2,1,1
	.sym	_KPW,_KPW, 6, 2, 32
	.global	_Isq_par
_Isq_par:	.usect	".ebss",2,1,1
	.sym	_Isq_par,_Isq_par, 6, 2, 32
	.global	_Id_med
_Id_med:	.usect	".ebss",2,1,1
	.sym	_Id_med,_Id_med, 6, 2, 32
	.global	_K_xy
_K_xy:	.usect	".ebss",2,1,1
	.sym	_K_xy,_K_xy, 6, 2, 32
	.global	_K_sc
_K_sc:	.usect	".ebss",2,1,1
	.sym	_K_sc,_K_sc, 6, 2, 32
	.global	_thetae_p
_thetae_p:	.usect	".ebss",2,1,1
	.sym	_thetae_p,_thetae_p, 6, 2, 32
	.global	_thetaenm1
_thetaenm1:	.usect	".ebss",2,1,1
	.sym	_thetaenm1,_thetaenm1, 6, 2, 32
	.global	_Ialfa_ref_p
_Ialfa_ref_p:	.usect	".ebss",2,1,1
	.sym	_Ialfa_ref_p,_Ialfa_ref_p, 6, 2, 32
	.global	_Ialfa_ref
_Ialfa_ref:	.usect	".ebss",2,1,1
	.sym	_Ialfa_ref,_Ialfa_ref, 6, 2, 32
	.global	_Ibeta_ref_p
_Ibeta_ref_p:	.usect	".ebss",2,1,1
	.sym	_Ibeta_ref_p,_Ibeta_ref_p, 6, 2, 32
	.global	_costhetae_p
_costhetae_p:	.usect	".ebss",2,1,1
	.sym	_costhetae_p,_costhetae_p, 6, 2, 32
	.global	_sinthetae_p
_sinthetae_p:	.usect	".ebss",2,1,1
	.sym	_sinthetae_p,_sinthetae_p, 6, 2, 32
	.global	_wsl
_wsl:	.usect	".ebss",2,1,1
	.sym	_wsl,_wsl, 6, 2, 32
	.global	_thetae
_thetae:	.usect	".ebss",2,1,1
	.sym	_thetae,_thetae, 6, 2, 32
	.global	_we
_we:	.usect	".ebss",2,1,1
	.sym	_we,_we, 6, 2, 32
	.global	_Ibeta_ref
_Ibeta_ref:	.usect	".ebss",2,1,1
	.sym	_Ibeta_ref,_Ibeta_ref, 6, 2, 32
	.global	_If_medido
_If_medido:	.usect	".ebss",2,1,1
	.sym	_If_medido,_If_medido, 6, 2, 32
	.global	_Vdc_medido
_Vdc_medido:	.usect	".ebss",2,1,1
	.sym	_Vdc_medido,_Vdc_medido, 6, 2, 32
	.global	_Ie_mir
_Ie_mir:	.usect	".ebss",2,1,1
	.sym	_Ie_mir,_Ie_mir, 6, 2, 32
	.global	_i_y
_i_y:	.usect	".ebss",2,1,1
	.sym	_i_y,_i_y, 6, 2, 32
	.global	_Ie_medido
_Ie_medido:	.usect	".ebss",2,1,1
	.sym	_Ie_medido,_Ie_medido, 6, 2, 32
	.global	_Iq_med
_Iq_med:	.usect	".ebss",2,1,1
	.sym	_Iq_med,_Iq_med, 6, 2, 32
	.global	_Iy_ref
_Iy_ref:	.usect	".ebss",2,1,1
	.sym	_Iy_ref,_Iy_ref, 6, 2, 32
	.global	_Ix_ref
_Ix_ref:	.usect	".ebss",2,1,1
	.sym	_Ix_ref,_Ix_ref, 6, 2, 32
	.global	_Isq
_Isq:	.usect	".ebss",2,1,1
	.sym	_Isq,_Isq, 6, 2, 32
	.global	_Vdc_mir
_Vdc_mir:	.usect	".ebss",2,1,1
	.sym	_Vdc_mir,_Vdc_mir, 6, 2, 32
	.global	_Isd
_Isd:	.usect	".ebss",2,1,1
	.sym	_Isd,_Isd, 6, 2, 32
	.global	_CpuTimer1Regs
_CpuTimer1Regs:	.usect	"CpuTimer1RegsFile",8,1,1
	.sym	_CpuTimer1Regs,_CpuTimer1Regs, 8, 2, 128, _CPUTIMER_REGS
	.global	_CpuTimer0Regs
_CpuTimer0Regs:	.usect	"CpuTimer0RegsFile",8,1,1
	.sym	_CpuTimer0Regs,_CpuTimer0Regs, 8, 2, 128, _CPUTIMER_REGS
	.global	_CpuTimer2Regs
_CpuTimer2Regs:	.usect	"CpuTimer2RegsFile",8,1,1
	.sym	_CpuTimer2Regs,_CpuTimer2Regs, 8, 2, 128, _CPUTIMER_REGS
	.global	_FlashRegs
_FlashRegs:	.usect	"FlashRegsFile",8,1,0
	.sym	_FlashRegs,_FlashRegs, 8, 2, 128, _FLASH_REGS
	.global	_CsmPwl
_CsmPwl:	.usect	"CsmPwlFile",8,1,0
	.sym	_CsmPwl,_CsmPwl, 8, 2, 128, _CSM_PWL
	.global	_GpioIntRegs
_GpioIntRegs:	.usect	"GpioIntRegsFile",10,1,1
	.sym	_GpioIntRegs,_GpioIntRegs, 8, 2, 160, _GPIO_INT_REGS
	.global	_SciaRegs
_SciaRegs:	.usect	"SciaRegsFile",16,1,0
	.sym	_SciaRegs,_SciaRegs, 8, 2, 256, _SCI_REGS
	.global	_ScicRegs
_ScicRegs:	.usect	"ScicRegsFile",16,1,0
	.sym	_ScicRegs,_ScicRegs, 8, 2, 256, _SCI_REGS
	.global	_ScibRegs
_ScibRegs:	.usect	"ScibRegsFile",16,1,0
	.sym	_ScibRegs,_ScibRegs, 8, 2, 256, _SCI_REGS
	.global	_Tabla_VV
_Tabla_VV:	.usect	".ebss",16,1,0
	.sym	_Tabla_VV,_Tabla_VV, 244, 2, 256,, 4, 4
	.global	_AdcMirror
_AdcMirror:	.usect	"AdcMirrorFile",16,1,0
	.sym	_AdcMirror,_AdcMirror, 8, 2, 256, _ADC_RESULT_MIRROR_REGS
	.global	_XIntruptRegs
_XIntruptRegs:	.usect	"XIntruptRegsFile",16,1,0
	.sym	_XIntruptRegs,_XIntruptRegs, 8, 2, 256, _XINTRUPT_REGS
	.global	_SpiaRegs
_SpiaRegs:	.usect	"SpiaRegsFile",16,1,0
	.sym	_SpiaRegs,_SpiaRegs, 8, 2, 256, _SPI_REGS
	.global	_CsmRegs
_CsmRegs:	.usect	"CsmRegsFile",16,1,0
	.sym	_CsmRegs,_CsmRegs, 8, 2, 256, _CSM_REGS
	.global	_Usa
_Usa:	.usect	".ebss",24,1,1
	.sym	_Usa,_Usa, 54, 2, 384,, 12
	.global	_Usx
_Usx:	.usect	".ebss",24,1,1
	.sym	_Usx,_Usx, 54, 2, 384,, 12
	.global	_vsy
_vsy:	.usect	".ebss",24,1,1
	.sym	_vsy,_vsy, 54, 2, 384,, 12
	.global	_Usb
_Usb:	.usect	".ebss",24,1,1
	.sym	_Usb,_Usb, 54, 2, 384,, 12
	.global	_Usy
_Usy:	.usect	".ebss",24,1,1
	.sym	_Usy,_Usy, 54, 2, 384,, 12
	.global	_vsa
_vsa:	.usect	".ebss",24,1,1
	.sym	_vsa,_vsa, 54, 2, 384,, 12
	.global	_vsb
_vsb:	.usect	".ebss",24,1,1
	.sym	_vsb,_vsb, 54, 2, 384,, 12
	.global	_vsx
_vsx:	.usect	".ebss",24,1,1
	.sym	_vsx,_vsx, 54, 2, 384,, 12
	.global	_PieCtrlRegs
_PieCtrlRegs:	.usect	"PieCtrlRegsFile",26,1,0
	.sym	_PieCtrlRegs,_PieCtrlRegs, 8, 2, 416, _PIE_CTRL_REGS
	.global	_XintfRegs
_XintfRegs:	.usect	"XintfRegsFile",30,1,1
	.sym	_XintfRegs,_XintfRegs, 8, 2, 480, _XINTF_REGS
	.global	_AdcRegs
_AdcRegs:	.usect	"AdcRegsFile",30,1,0
	.sym	_AdcRegs,_AdcRegs, 8, 2, 480, _ADC_REGS
	.global	_ECap2Regs
_ECap2Regs:	.usect	"ECap2RegsFile",32,1,1
	.sym	_ECap2Regs,_ECap2Regs, 8, 2, 512, _ECAP_REGS
	.global	_ECap1Regs
_ECap1Regs:	.usect	"ECap1RegsFile",32,1,1
	.sym	_ECap1Regs,_ECap1Regs, 8, 2, 512, _ECAP_REGS
	.global	_SysCtrlRegs
_SysCtrlRegs:	.usect	"SysCtrlRegsFile",32,1,0
	.sym	_SysCtrlRegs,_SysCtrlRegs, 8, 2, 512, _SYS_CTRL_REGS
	.global	_GpioDataRegs
_GpioDataRegs:	.usect	"GpioDataRegsFile",32,1,1
	.sym	_GpioDataRegs,_GpioDataRegs, 8, 2, 512, _GPIO_DATA_REGS
	.global	_ECap6Regs
_ECap6Regs:	.usect	"ECap6RegsFile",32,1,1
	.sym	_ECap6Regs,_ECap6Regs, 8, 2, 512, _ECAP_REGS
	.global	_ECap3Regs
_ECap3Regs:	.usect	"ECap3RegsFile",32,1,1
	.sym	_ECap3Regs,_ECap3Regs, 8, 2, 512, _ECAP_REGS
	.global	_ECap4Regs
_ECap4Regs:	.usect	"ECap4RegsFile",32,1,1
	.sym	_ECap4Regs,_ECap4Regs, 8, 2, 512, _ECAP_REGS
	.global	_ECap5Regs
_ECap5Regs:	.usect	"ECap5RegsFile",32,1,1
	.sym	_ECap5Regs,_ECap5Regs, 8, 2, 512, _ECAP_REGS
	.global	_EPwm2Regs
_EPwm2Regs:	.usect	"EPwm2RegsFile",34,1,1
	.sym	_EPwm2Regs,_EPwm2Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm1Regs
_EPwm1Regs:	.usect	"EPwm1RegsFile",34,1,1
	.sym	_EPwm1Regs,_EPwm1Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm4Regs
_EPwm4Regs:	.usect	"EPwm4RegsFile",34,1,1
	.sym	_EPwm4Regs,_EPwm4Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm5Regs
_EPwm5Regs:	.usect	"EPwm5RegsFile",34,1,1
	.sym	_EPwm5Regs,_EPwm5Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm3Regs
_EPwm3Regs:	.usect	"EPwm3RegsFile",34,1,1
	.sym	_EPwm3Regs,_EPwm3Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm6Regs
_EPwm6Regs:	.usect	"EPwm6RegsFile",34,1,1
	.sym	_EPwm6Regs,_EPwm6Regs, 8, 2, 544, _EPWM_REGS
	.global	_I2caRegs
_I2caRegs:	.usect	"I2caRegsFile",34,1,0
	.sym	_I2caRegs,_I2caRegs, 8, 2, 544, _I2C_REGS
	.global	_McbspbRegs
_McbspbRegs:	.usect	"McbspbRegsFile",37,1,0
	.sym	_McbspbRegs,_McbspbRegs, 8, 2, 592, _MCBSP_REGS
	.global	_McbspaRegs
_McbspaRegs:	.usect	"McbspaRegsFile",37,1,0
	.sym	_McbspaRegs,_McbspaRegs, 8, 2, 592, _MCBSP_REGS
	.global	_GpioCtrlRegs
_GpioCtrlRegs:	.usect	"GpioCtrlRegsFile",46,1,1
	.sym	_GpioCtrlRegs,_GpioCtrlRegs, 8, 2, 736, _GPIO_CTRL_REGS
	.global	_ECanaRegs
_ECanaRegs:	.usect	"ECanaRegsFile",52,1,1
	.sym	_ECanaRegs,_ECanaRegs, 8, 2, 832, _ECAN_REGS
	.global	_ECanbRegs
_ECanbRegs:	.usect	"ECanbRegsFile",52,1,1
	.sym	_ECanbRegs,_ECanbRegs, 8, 2, 832, _ECAN_REGS
	.global	_ECanaMOTORegs
_ECanaMOTORegs:	.usect	"ECanaMOTORegsFile",64,1,1
	.sym	_ECanaMOTORegs,_ECanaMOTORegs, 8, 2, 1024, _MOTO_REGS
	.global	_ECanaMOTSRegs
_ECanaMOTSRegs:	.usect	"ECanaMOTSRegsFile",64,1,1
	.sym	_ECanaMOTSRegs,_ECanaMOTSRegs, 8, 2, 1024, _MOTS_REGS
	.global	_ECanbLAMRegs
_ECanbLAMRegs:	.usect	"ECanbLAMRegsFile",64,1,1
	.sym	_ECanbLAMRegs,_ECanbLAMRegs, 8, 2, 1024, _LAM_REGS
	.global	_EQep1Regs
_EQep1Regs:	.usect	"EQep1RegsFile",64,1,1
	.sym	_EQep1Regs,_EQep1Regs, 8, 2, 1024, _EQEP_REGS
	.global	_EQep2Regs
_EQep2Regs:	.usect	"EQep2RegsFile",64,1,1
	.sym	_EQep2Regs,_EQep2Regs, 8, 2, 1024, _EQEP_REGS
	.global	_ECanaLAMRegs
_ECanaLAMRegs:	.usect	"ECanaLAMRegsFile",64,1,1
	.sym	_ECanaLAMRegs,_ECanaLAMRegs, 8, 2, 1024, _LAM_REGS
	.global	_ECanbMOTSRegs
_ECanbMOTSRegs:	.usect	"ECanbMOTSRegsFile",64,1,1
	.sym	_ECanbMOTSRegs,_ECanbMOTSRegs, 8, 2, 1024, _MOTS_REGS
	.global	_ECanbMOTORegs
_ECanbMOTORegs:	.usect	"ECanbMOTORegsFile",64,1,1
	.sym	_ECanbMOTORegs,_ECanbMOTORegs, 8, 2, 1024, _MOTO_REGS
	.global	_SCMP
_SCMP:	.usect	".ebss",120,1,1
	.sym	_SCMP,_SCMP, 246, 2, 1920,, 12, 5
	.global	_DevEmuRegs
_DevEmuRegs:	.usect	"DevEmuRegsFile",208,1,1
	.sym	_DevEmuRegs,_DevEmuRegs, 8, 2, 3328, _DEV_EMU_REGS
	.global	_DmaRegs
_DmaRegs:	.usect	"DmaRegsFile",224,1,1
	.sym	_DmaRegs,_DmaRegs, 8, 2, 3584, _DMA_REGS
	.global	_ECanbMboxes
_ECanbMboxes:	.usect	"ECanbMboxesFile",256,1,1
	.sym	_ECanbMboxes,_ECanbMboxes, 8, 2, 4096, _ECAN_MBOXES
	.global	_ECanaMboxes
_ECanaMboxes:	.usect	"ECanaMboxesFile",256,1,1
	.sym	_ECanaMboxes,_ECanaMboxes, 8, 2, 4096, _ECAN_MBOXES
	.global	_PieVectTable
_PieVectTable:	.usect	"PieVectTableFile",256,1,1
	.sym	_PieVectTable,_PieVectTable, 8, 2, 4096, _PIE_VECT_TABLE
	.global	_eqep_1
_eqep_1:	.usect	"LOGGER",15000,1,0
	.sym	_eqep_1,_eqep_1, 62, 2, 240000,, 15000
	.global	_wm_log
_wm_log:	.usect	"LOGGER",15000,1,0
	.sym	_wm_log,_wm_log, 62, 2, 240000,, 15000
	.global	_corr
_corr:	.usect	"LOGGER",15000,1,0
	.sym	_corr,_corr, 62, 2, 240000,, 15000
	.global	_eqep_2
_eqep_2:	.usect	"LOGGER",15000,1,0
	.sym	_eqep_2,_eqep_2, 62, 2, 240000,, 15000
;	opt2000 C:\\Users\\usuario\\AppData\\Local\\Temp\\035323 C:\\Users\\usuario\\AppData\\Local\\Temp\\035325 
;	ac2000 -@C:\Users\usuario\AppData\Local\Temp\0353212 
	.sect	".text"
	.global	_PTC5Feqep_isr
	.file	"C:\CCStudio_v3.3\MyProjects\DAVID_ADAPT_VEL\PTC5F_eqep1.c"
	.sym	_PTC5Feqep_isr,_PTC5Feqep_isr, 32, 2, 0
	.func	58
	.sym	_max_timeout_hit,_max_timeout_hit$1, 12, 3, 16
	.sym	_min_timeout_hit,_min_timeout_hit$2, 12, 3, 16
	.sym	_hvfactor,_hvfactor$3, 6, 3, 32

;***************************************************************
;* FNAME: _PTC5Feqep_isr                FR SIZE:  32           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto, 32 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Feqep_isr:
	.line	1
;----------------------------------------------------------------------
;  58 | interrupt void PTC5Feqep_isr() {                                       
;----------------------------------------------------------------------
;* AL    assigned to $O$S1
;* AL    assigned to $O$S2
;* R1HL  assigned to _ACCEPTABLE_ERR
	.sym	_ACCEPTABLE_ERR,47, 6, 4, 32
;* R2HL  assigned to _LOWEST_ACCEPTABLE_ERR
	.sym	_LOWEST_ACCEPTABLE_ERR,51, 6, 4, 32
;* R0HL  assigned to _MARGIN
	.sym	_MARGIN,43, 6, 4, 32
;* R4HL  assigned to _MARGIN_UP
	.sym	_MARGIN_UP,59, 6, 4, 32
;* R3HL  assigned to _MARGIN_DOWN
	.sym	_MARGIN_DOWN,55, 6, 4, 32
;* AR3   assigned to _MAX_TIMEOUT
	.sym	_MAX_TIMEOUT,10, 15, 4, 32
;* AR2   assigned to _MIN_TIMEOUT
	.sym	_MIN_TIMEOUT,8, 15, 4, 32
;* R0HL  assigned to _abs_wm_k
	.sym	_abs_wm_k,43, 6, 4, 32
;* AL    assigned to _is_ready
	.sym	_is_ready,0, 12, 4, 16
;* R1HL  assigned to _bounds
	.sym	_bounds,47, 6, 4, 32
        ASP
        PUSH      RB
        PUSH      AR1H:AR0H
        MOVL      *SP++,XAR2
        MOVL      *SP++,XAR3
        MOVL      *SP++,XAR4
        MOVL      *SP++,XAR5
        MOVL      *SP++,XAR6
        MOVL      *SP++,XAR7
        MOVL      *SP++,XT
        MOV32     *SP++,STF
        MOV32     *SP++,R0H
        MOV32     *SP++,R1H
        MOV32     *SP++,R2H
        MOV32     *SP++,R3H
        MOV32     *SP++,R4H
        MOV32     *SP++,R5H
        SETFLG    RNDF32=1, RNDF64=1
        SPM       0
        CLRC      PAGE0,OVM
        CLRC      AMODE
	.line	2
;----------------------------------------------------------------------
;  59 | const float ACCEPTABLE_ERR = 0.01;                                     
;----------------------------------------------------------------------
        MOVIZ     R1H,#15395            ; |59| 
        MOVXI     R1H,#55050            ; |59| 
	.line	3
;----------------------------------------------------------------------
;  60 | const float LOWEST_ACCEPTABLE_ERR = 0.005;                             
;  61 | const float MARGIN = 0.3;                                              
;----------------------------------------------------------------------
        MOVIZ     R2H,#15267            ; |60| 
        MOVXI     R2H,#55050            ; |60| 
	.line	5
;----------------------------------------------------------------------
;  62 | const float MARGIN_UP = 1 + MARGIN;                                    
;----------------------------------------------------------------------
        MOVIZ     R0H,#16025            ; |62| 
        MOVXI     R0H,#39322            ; |62| 
        ADDF32    R4H,R0H,#16256        ; |62| 
	.line	6
;----------------------------------------------------------------------
;  63 | const float MARGIN_DOWN = 1 - MARGIN;                                  
;----------------------------------------------------------------------
        SUBF32    R3H,#16256,R0H        ; |63| 
	.line	7
;----------------------------------------------------------------------
;  64 | const unsigned long MAX_TIMEOUT = 750000;                              
;----------------------------------------------------------------------
        MOVL      XAR4,#750000          ; |64| 
        MOVL      XAR3,XAR4             ; |64| 
	.line	8
;----------------------------------------------------------------------
;  65 | const unsigned long MIN_TIMEOUT = 10000;                               
;  66 | static unsigned char max_timeout_hit = 0;                              
;  67 | static unsigned char min_timeout_hit = 0;                              
;  68 | static float hvfactor = hvfactor_macro(INITIAL_TIMEOUT);               
;----------------------------------------------------------------------
        MOVL      XAR2,#10000           ; |65| 
	.line	12
;----------------------------------------------------------------------
;  69 | float abs_wm_k = fabs(wm_k);                                           
;----------------------------------------------------------------------
        MOVW      DP,#_wm_k
        MOV32     R0H,@_wm_k
        ABSF32    R0H,R0H               ; |69| 
	.line	13
;----------------------------------------------------------------------
;  70 | unsigned char is_ready = wm_ref > 0?                                   
;  71 |                             (wm_k >= (MARGIN_DOWN) * wm_ref)           
;  72 |                             && (wm_k <= (MARGIN_UP) *wm_ref)           
;  73 |                             : (wm_k <= (MARGIN_DOWN) * wm_ref)         
;  74 |                             && (wm_k >= (MARGIN_UP) *wm_ref);          
;----------------------------------------------------------------------
        MOV32     R5H,@_wm_ref
        CMPF32    R5H,#0                ; |70| 
        MOVST0    ZF, NF                ; |70| 
        B         $C$L1,GT              ; |70| 
        ; branchcc occurs ; |70| 
        MPYF32    R3H,R5H,R3H           ; |70| 
        MOVB      AL,#0
        MOV32     R5H,@_wm_k
        CMPF32    R5H,R3H               ; |70| 
        MOVST0    ZF, NF                ; |70| 
        B         $C$L2,GT              ; |70| 
        ; branchcc occurs ; |70| 
        MOV32     R3H,@_wm_ref
        MPYF32    R3H,R3H,R4H           ; |70| 
        MOV32     R4H,@_wm_k
        CMPF32    R4H,R3H               ; |70| 
        MOVST0    ZF, NF                ; |70| 
        MOVB      AL,#1,GEQ             ; |70| 
        B         $C$L2,UNC             ; |70| 
        ; branch occurs ; |70| 
$C$L1:    
        MPYF32    R3H,R5H,R3H           ; |70| 
        MOVB      AL,#0
        MOV32     R5H,@_wm_k
        CMPF32    R5H,R3H               ; |70| 
        MOVST0    ZF, NF                ; |70| 
        B         $C$L2,LT              ; |70| 
        ; branchcc occurs ; |70| 
        MOV32     R3H,@_wm_ref
        MPYF32    R3H,R3H,R4H           ; |70| 
        MOV32     R4H,@_wm_k
        CMPF32    R4H,R3H               ; |70| 
        MOVST0    ZF, NF                ; |70| 
        MOVB      AL,#1,LEQ             ; |70| 
$C$L2:    
	.line	18
;----------------------------------------------------------------------
;  75 | float bounds =  abs_wm_k * ACCEPTABLE_ERR;                             
;  76 | // Edge Capture and Unit Time Out Direction Speed                      
;  77 | // The position of the encoder is reverse-> forwards is actually backwa
;     | rds                                                                    
;----------------------------------------------------------------------
        MPYF32    R1H,R1H,R0H           ; |75| 
	.line	21
;----------------------------------------------------------------------
;  78 | if (EQep1Regs.QEPSTS.bit.QDF) { // Forward Diretcion QDF=1,            
;  79 |     eqeptmr = 0 - (float)EQep1Regs.QCPRDLAT;                           
;  80 |     hdx = 0 - (float)EQep1Regs.QPOSLAT;                                
;  81 | } else { // Reverse Direction QDF=0                                    
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+28
        TBIT      @_EQep1Regs+28,#5     ; |78| 
        BF        $C$L3,TC              ; |78| 
        ; branchcc occurs ; |78| 
	.line	25
;----------------------------------------------------------------------
;  82 | eqeptmr = (float)EQep1Regs.QCPRDLAT;                                   
;----------------------------------------------------------------------
        UI16TOF32 R3H,@_EQep1Regs+32    ; |82| 
        MOVW      DP,#_eqeptmr
        MOV32     @_eqeptmr,R3H
	.line	26
;----------------------------------------------------------------------
;  83 | hdx = (float)QEPCNTMAX - (float)EQep1Regs.QPOSLAT;                     
;  85 | // Capture Module error                                                
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+12
        MOVIZ     R3H,#17948            ; |83| 
        UI32TOF32 R4H,@_EQep1Regs+12    ; |83| 
        MOVXI     R3H,#16384            ; |83| 
        SUBF32    R3H,R3H,R4H           ; |83| 
        MOVW      DP,#_hdx
        MOV32     @_hdx,R3H
        B         $C$L4,UNC             ; |83| 
        ; branch occurs ; |83| 
$C$L3:    
	.line	22
        UI16TOF32 R3H,@_EQep1Regs+32    ; |79| 
        NOP
        SUBF32    R3H,#0,R3H            ; |79| 
        MOVW      DP,#_eqeptmr
        MOV32     @_eqeptmr,R3H
	.line	23
        MOVW      DP,#_EQep1Regs+12
        UI32TOF32 R3H,@_EQep1Regs+12    ; |80| 
        NOP
        SUBF32    R3H,#0,R3H            ; |80| 
        MOVW      DP,#_hdx
        MOV32     @_hdx,R3H
$C$L4:    
	.line	29
;----------------------------------------------------------------------
;  86 | if (EQep1Regs.QEPSTS.bit.COEF) {                                       
;  87 |     EQep1Regs.QEPSTS.bit.COEF = 1;                                     
;  88 | } else {                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+28
        TBIT      @_EQep1Regs+28,#3     ; |86| 
        BF        $C$L5,TC              ; |86| 
        ; branchcc occurs ; |86| 
	.line	32
;----------------------------------------------------------------------
;  89 | wm_k = (float)hdx * hvfactor;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_hvfactor$3
        MOV32     R4H,@_hvfactor$3
        MPYF32    R3H,R4H,R3H           ; |89| 
        MOVW      DP,#_wm_k
        MOV32     @_wm_k,R3H
        B         $C$L6,UNC             ; |89| 
        ; branch occurs ; |89| 
$C$L5:    
	.line	30
        AND       AH,@_EQep1Regs+28,#0xfff7 ; |87| 
        ORB       AH,#0x08              ; |87| 
        MOV       @_EQep1Regs+28,AH     ; |87| 
$C$L6:    
	.line	34
;----------------------------------------------------------------------
;  91 | if ( !max_timeout_hit && is_ready && (hvfactor > bounds)){             
;----------------------------------------------------------------------
        MOVW      DP,#_max_timeout_hit$1
        MOV       AH,@_max_timeout_hit$1 ; |91| 
        BF        $C$L8,NEQ             ; |91| 
        ; branchcc occurs ; |91| 
        CMPB      AL,#0                 ; |91| 
        BF        $C$L8,EQ              ; |91| 
        ; branchcc occurs ; |91| 
        MOVW      DP,#_hvfactor$3
        MOV32     R3H,@_hvfactor$3
        CMPF32    R3H,R1H               ; |91| 
        MOVST0    ZF, NF                ; |91| 
        B         $C$L8,LEQ             ; |91| 
        ; branchcc occurs ; |91| 
	.line	35
;----------------------------------------------------------------------
;  92 | min_timeout_hit = 0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_min_timeout_hit$2
        MOV       @_min_timeout_hit$2,#0 ; |92| 
	.line	36
;----------------------------------------------------------------------
;  93 | corrections++;                                                         
;----------------------------------------------------------------------
        INC       @_corrections         ; |93| 
	.line	37
;----------------------------------------------------------------------
;  94 | eqep_timeout_period *= (hvfactor/bounds);                              
;----------------------------------------------------------------------
        MOVW      DP,#_hvfactor$3
        MOV32     R0H,@_hvfactor$3
        LCR       #FS$$DIV              ; |94| 
        ; call occurs [#FS$$DIV] ; |94| 
        MOVW      DP,#_eqep_timeout_period
        UI32TOF32 R1H,@_eqep_timeout_period ; |94| 
        NOP
        MPYF32    R0H,R0H,R1H           ; |94| 
        NOP
        F32TOUI32 R0H,R0H               ; |94| 
        NOP
        MOV32     @_eqep_timeout_period,R0H
	.line	38
;----------------------------------------------------------------------
;  95 | if (eqep_timeout_period > MAX_TIMEOUT){                                
;----------------------------------------------------------------------
        MOVL      ACC,XAR3
        CMPL      ACC,@_eqep_timeout_period ; |95| 
        B         $C$L7,HIS             ; |95| 
        ; branchcc occurs ; |95| 
	.line	39
;----------------------------------------------------------------------
;  96 | eqep_timeout_period = MAX_TIMEOUT;                                     
;----------------------------------------------------------------------
        MOVL      @_eqep_timeout_period,XAR3 ; |96| 
	.line	40
;----------------------------------------------------------------------
;  97 | max_timeout_hit = 1;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_max_timeout_hit$1
        MOVB      @_max_timeout_hit$1,#1,UNC ; |97| 
$C$L7:    
	.line	42
;----------------------------------------------------------------------
;  99 | EQep1Regs.QUPRD = eqep_timeout_period;                                 
;----------------------------------------------------------------------
        MOVW      DP,#_eqep_timeout_period
        MOVL      ACC,@_eqep_timeout_period ; |99| 
        MOVW      DP,#_EQep1Regs+16
        MOVL      @_EQep1Regs+16,ACC    ; |99| 
	.line	43
;----------------------------------------------------------------------
; 100 | hvfactor = hvfactor_macro(eqep_timeout_period);                        
;----------------------------------------------------------------------
        MOVIZ     R0H,#18360            ; |100| 
        MOVW      DP,#_eqep_timeout_period
        MOVXI     R0H,#5092             ; |100| 
        UI32TOF32 R1H,@_eqep_timeout_period ; |100| 
        LCR       #FS$$DIV              ; |100| 
        ; call occurs [#FS$$DIV] ; |100| 
        MOVW      DP,#_hvfactor$3
        MOV32     @_hvfactor$3,R0H
	.line	44
;----------------------------------------------------------------------
; 101 | } else if (!min_timeout_hit && is_ready && (hvfactor < abs_wm_k * LOWES
;     | T_ACCEPTABLE_ERR)){                                                    
;----------------------------------------------------------------------
        B         $C$L10,UNC            ; |101| 
        ; branch occurs ; |101| 
$C$L8:    
        MOVW      DP,#_min_timeout_hit$2
        MOV       AH,@_min_timeout_hit$2 ; |101| 
        BF        $C$L10,NEQ            ; |101| 
        ; branchcc occurs ; |101| 
        CMPB      AL,#0                 ; |101| 
        BF        $C$L10,EQ             ; |101| 
        ; branchcc occurs ; |101| 
        MPYF32    R0H,R2H,R0H           ; |101| 
        MOVW      DP,#_hvfactor$3
        MOV32     R2H,@_hvfactor$3
        CMPF32    R2H,R0H               ; |101| 
        MOVST0    ZF, NF                ; |101| 
        B         $C$L10,GEQ            ; |101| 
        ; branchcc occurs ; |101| 
	.line	45
;----------------------------------------------------------------------
; 102 | max_timeout_hit = 0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_max_timeout_hit$1
        MOV       @_max_timeout_hit$1,#0 ; |102| 
	.line	46
;----------------------------------------------------------------------
; 103 | corrections++;                                                         
;----------------------------------------------------------------------
        INC       @_corrections         ; |103| 
	.line	47
;----------------------------------------------------------------------
; 104 | eqep_timeout_period *= (hvfactor/bounds);                              
;----------------------------------------------------------------------
        MOVW      DP,#_hvfactor$3
        MOV32     R0H,@_hvfactor$3
        LCR       #FS$$DIV              ; |104| 
        ; call occurs [#FS$$DIV] ; |104| 
        MOVW      DP,#_eqep_timeout_period
        UI32TOF32 R1H,@_eqep_timeout_period ; |104| 
        NOP
        MPYF32    R0H,R0H,R1H           ; |104| 
        NOP
        F32TOUI32 R0H,R0H               ; |104| 
        NOP
        MOV32     @_eqep_timeout_period,R0H
	.line	48
;----------------------------------------------------------------------
; 105 | if (eqep_timeout_period < MIN_TIMEOUT){                                
;----------------------------------------------------------------------
        MOVL      ACC,XAR2
        CMPL      ACC,@_eqep_timeout_period ; |105| 
        B         $C$L9,LOS             ; |105| 
        ; branchcc occurs ; |105| 
	.line	49
;----------------------------------------------------------------------
; 106 | min_timeout_hit = 1;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_min_timeout_hit$2
        MOVB      @_min_timeout_hit$2,#1,UNC ; |106| 
	.line	50
;----------------------------------------------------------------------
; 107 | eqep_timeout_period = MIN_TIMEOUT;                                     
;----------------------------------------------------------------------
        MOVW      DP,#_eqep_timeout_period
        MOVL      @_eqep_timeout_period,XAR2 ; |107| 
$C$L9:    
	.line	52
;----------------------------------------------------------------------
; 109 | EQep1Regs.QUPRD = eqep_timeout_period;                                 
;----------------------------------------------------------------------
        MOVL      ACC,@_eqep_timeout_period ; |109| 
        MOVW      DP,#_EQep1Regs+16
        MOVL      @_EQep1Regs+16,ACC    ; |109| 
	.line	53
;----------------------------------------------------------------------
; 110 | hvfactor = hvfactor_macro(eqep_timeout_period);                        
;----------------------------------------------------------------------
        MOVIZ     R0H,#18360            ; |110| 
        MOVW      DP,#_eqep_timeout_period
        MOVXI     R0H,#5092             ; |110| 
        UI32TOF32 R1H,@_eqep_timeout_period ; |110| 
        LCR       #FS$$DIV              ; |110| 
        ; call occurs [#FS$$DIV] ; |110| 
        MOVW      DP,#_hvfactor$3
        MOV32     @_hvfactor$3,R0H
$C$L10:    
	.line	55
;----------------------------------------------------------------------
; 112 | EQep1Regs.QCLR.bit.UTO = 1;      // Clears Unit Time Out Interrupt Flag
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+26
        AND       AL,@_EQep1Regs+26,#0xf7ff ; |112| 
        OR        AL,#0x0800            ; |112| 
        MOV       @_EQep1Regs+26,AL     ; |112| 
	.line	56
;----------------------------------------------------------------------
; 113 | EQep1Regs.QCLR.bit.INT = 1;      // Clears Global EQEP1 Interrupt Flag 
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+26,#0xfffe ; |113| 
        ORB       AL,#0x01              ; |113| 
        MOV       @_EQep1Regs+26,AL     ; |113| 
	.line	57
;----------------------------------------------------------------------
; 114 | PieCtrlRegs.PIEACK.bit.ACK5 = 1; // Clear the PIEACK of Group 5 for ena
;     | bles                                                                   
; 115 |                                  // Interrupt Resquest at CPU Level    
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1
        AND       AL,@_PieCtrlRegs+1,#0xffef ; |114| 
        ORB       AL,#0x10              ; |114| 
        MOV       @_PieCtrlRegs+1,AL    ; |114| 
	.line	59
        MOV32     R5H,*--SP
        MOV32     R4H,*--SP
        MOV32     R3H,*--SP
        MOV32     R2H,*--SP
        MOV32     R1H,*--SP
        MOV32     R0H,*--SP
        MOV32     STF,*--SP
        MOVL      XT,*--SP
        MOVL      XAR7,*--SP
        MOVL      XAR6,*--SP
        MOVL      XAR5,*--SP
        MOVL      XAR4,*--SP
        MOVL      XAR3,*--SP
        MOVL      XAR2,*--SP
        POP       AR1H:AR0H
        POP       RB
        NASP
        IRET
        ; return occurs
	.endfunc	116,0033ff7e5h,32
	.sect	".text"
	.global	_PTC5Feqep_start
	.sym	_PTC5Feqep_start,_PTC5Feqep_start, 32, 2, 0
	.func	119

;***************************************************************
;* FNAME: _PTC5Feqep_start              FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Feqep_start:
	.line	1
;----------------------------------------------------------------------
; 119 | void PTC5Feqep_start(){                                                
; 120 | // GPIO Configure                                                      
;----------------------------------------------------------------------
	.line	3
;----------------------------------------------------------------------
; 121 | EALLOW;                                                        // Enabl
;     | e writing to EALLOW protected registers                                
;----------------------------------------------------------------------
 EALLOW
	.line	4
;----------------------------------------------------------------------
; 122 | SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK        = 1;                // Enabl
;     | e the SYSCLKOUT to the GPIO                                            
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+16
        AND       AL,@_SysCtrlRegs+16,#0xdfff ; |122| 
        OR        AL,#0x2000            ; |122| 
        MOV       @_SysCtrlRegs+16,AL   ; |122| 
	.line	5
;----------------------------------------------------------------------
; 123 | SysCtrlRegs.PCLKCR1.bit.EQEP1ENCLK         = 1;                // EQEP1
;     |  Module is Clocked by the SYSCLKOUT                                    
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xbfff ; |123| 
        OR        AL,#0x4000            ; |123| 
        MOV       @_SysCtrlRegs+13,AL   ; |123| 
	.line	6
;----------------------------------------------------------------------
; 124 | GpioCtrlRegs.GPBMUX2.bit.GPIO50            = 1;                // JP3 #
;     | 13 GPIO50 as EQEP1A(Input)                                             
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       AL,@_GpioCtrlRegs+24,#0xffcf ; |124| 
        ORB       AL,#0x10              ; |124| 
        MOV       @_GpioCtrlRegs+24,AL  ; |124| 
	.line	7
;----------------------------------------------------------------------
; 125 | GpioCtrlRegs.GPBMUX2.bit.GPIO51            = 1;                // JP3 #
;     | 14 GPIO51 as EQEP1B(Input)                                             
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+24,#0xff3f ; |125| 
        ORB       AL,#0x40              ; |125| 
        MOV       @_GpioCtrlRegs+24,AL  ; |125| 
	.line	8
;----------------------------------------------------------------------
; 126 | GpioCtrlRegs.GPBMUX2.bit.GPIO53            = 1;                // JP3 #
;     | 14 GPIO53 as EQEP1I(Input)                                             
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+24,#0xf3ff ; |126| 
        OR        AL,#0x0400            ; |126| 
        MOV       @_GpioCtrlRegs+24,AL  ; |126| 
	.line	9
;----------------------------------------------------------------------
; 127 | EDIS;                                                          // Disab
;     | le writing to EALLOW protected registers                               
; 128 | // End GPIO Configure                                                  
; 129 | // QDU Module Configuration                                            
;----------------------------------------------------------------------
 EDIS
	.line	12
;----------------------------------------------------------------------
; 130 | EQep1Regs.QDECCTL.bit.QSRC                = 00;               // EQEP1
;     | as Quadrature Count Mode                                               
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+20
        AND       @_EQep1Regs+20,#0x3fff ; |130| 
	.line	13
;----------------------------------------------------------------------
; 131 | EQep1Regs.QDECCTL.bit.QAP                 = 0;                // EQEP1A
;     |  input polarity No Efect                                               
; 132 | //EQep1Regs.QDECCTL.bit.QAP               = 1;                 // EQEP1
;     | A input negate polarity                                                
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xfeff ; |131| 
	.line	15
;----------------------------------------------------------------------
; 133 | EQep1Regs.QDECCTL.bit.QBP                 = 0;                // EQEP1B
;     |  input polarity No Efect                                               
; 134 | //EQep1Regs.QDECCTL.bit.QBP               = 1;                 // EQEP1
;     | B input negate polarity                                                
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xff7f ; |133| 
	.line	17
;----------------------------------------------------------------------
; 135 | EQep1Regs.QDECCTL.bit.QIP                 = 0;                // EQEP1I
;     |  input polarity No Efect                                               
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xffbf ; |135| 
	.line	18
;----------------------------------------------------------------------
; 136 | EQep1Regs.QDECCTL.bit.QSP                 = 0;                // EQEP1S
;     |  polarity No Efect                                                     
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xffdf ; |136| 
	.line	19
;----------------------------------------------------------------------
; 137 | EQep1Regs.QDECCTL.bit.SWAP                = 0;                // Quadra
;     | ture-clock inputs are not swaped                                       
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xfbff ; |137| 
	.line	20
;----------------------------------------------------------------------
; 138 | EQep1Regs.QDECCTL.bit.IGATE               = 0;                // Disabl
;     | e gating of index pulse                                                
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xfdff ; |138| 
	.line	21
;----------------------------------------------------------------------
; 139 | EQep1Regs.QDECCTL.bit.XCR                 = 0;                // 2x Res
;     | olution Count                                                          
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xf7ff ; |139| 
	.line	22
;----------------------------------------------------------------------
; 140 | EQep1Regs.QDECCTL.bit.SOEN                = 0;                // Disabl
;     | e position-compare syn output                                          
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xdfff ; |140| 
	.line	23
;----------------------------------------------------------------------
; 141 | EQep1Regs.QDECCTL.bit.SPSEL               = 0;                // Index
;     | pin is used for sync output                                            
; 142 | // End QDU Module Configuration                                        
; 143 | // PCCU Module Configuration                                           
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xefff ; |141| 
	.line	26
;----------------------------------------------------------------------
; 144 | EQep1Regs.QEPCTL.bit.WDE                = 0;                // Disable
;     | the EQEP watchdog timer                                                
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xfffe ; |144| 
	.line	27
;----------------------------------------------------------------------
; 145 | EQep1Regs.QEPCTL.bit.QCLM               = 1;                // EQEP cap
;     | ture latch on Unit Time Out                                            
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+21,#0xfffb ; |145| 
        ORB       AL,#0x04              ; |145| 
        MOV       @_EQep1Regs+21,AL     ; |145| 
	.line	28
;----------------------------------------------------------------------
; 146 | EQep1Regs.QEPCTL.bit.QPEN               = 1;                // Enable E
;     | QEP position counter                                                   
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+21,#0xfff7 ; |146| 
        ORB       AL,#0x08              ; |146| 
        MOV       @_EQep1Regs+21,AL     ; |146| 
	.line	29
;----------------------------------------------------------------------
; 147 | EQep1Regs.QEPCTL.bit.PCRM               = 3;                // Position
;     |  Counter Reset on Unit Time Event                                      
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+21,#0xcfff ; |147| 
        OR        AL,#0x3000            ; |147| 
        MOV       @_EQep1Regs+21,AL     ; |147| 
	.line	30
;----------------------------------------------------------------------
; 148 | EQep1Regs.QEPCTL.bit.SEI                = 0;                // Strobe E
;     | vent actions disable                                                   
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xf3ff ; |148| 
	.line	31
;----------------------------------------------------------------------
; 149 | EQep1Regs.QEPCTL.bit.IEI                = 0;                // Index Ev
;     | ent actions disable                                                    
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xfcff ; |149| 
	.line	32
;----------------------------------------------------------------------
; 150 | EQep1Regs.QEPCTL.bit.SWI                = 0;                // Software
;     |  Initialization action enable                                          
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xff7f ; |150| 
	.line	33
;----------------------------------------------------------------------
; 151 | EQep1Regs.QEPCTL.bit.IEL                = 0;                // Index Ev
;     | ent Latch Reserved                                                     
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xffcf ; |151| 
	.line	34
;----------------------------------------------------------------------
; 152 | EQep1Regs.QEPCTL.bit.SWI                = 0;                // Enable S
;     | oftware initialization                                                 
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xff7f ; |152| 
	.line	35
;----------------------------------------------------------------------
; 153 | EQep1Regs.QEPCTL.bit.FREE_SOFT          = 0x10;             // Position
;     |  Counter is Unaffected by emulation suspend                            
; 154 | // End PCCU Module Configuration                                       
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0x3fff ; |153| 
	.line	37
;----------------------------------------------------------------------
; 155 | EQep1Regs.QPOSINIT                       = 0;                // EQEP Co
;     | unter Initial Position                                                 
;----------------------------------------------------------------------
        MOVB      ACC,#0
        MOVL      @_EQep1Regs+2,ACC     ; |155| 
	.line	38
;----------------------------------------------------------------------
; 156 | EQep1Regs.QPOSMAX                        = QEPCNTMAX;        // EQEP Co
;     | unter Max Position                                                     
;----------------------------------------------------------------------
        MOVL      XAR4,#10000           ; |156| 
        MOVL      @_EQep1Regs+4,XAR4    ; |156| 
	.line	39
;----------------------------------------------------------------------
; 157 | EQep1Regs.QPOSCMP                        = QEPCNTMAX;        // EQEP Po
;     | sition Compare                                                         
;----------------------------------------------------------------------
        MOVL      @_EQep1Regs+6,XAR4    ; |157| 
	.line	40
;----------------------------------------------------------------------
; 158 | EQep1Regs.QCTMR                          = 0;                // EQEP Po
;     | sition Compare                                                         
; 159 | // Position-Compare Configuration                                      
;----------------------------------------------------------------------
        MOV       @_EQep1Regs+29,#0     ; |158| 
	.line	42
;----------------------------------------------------------------------
; 160 | EQep1Regs.QPOSCTL.bit.PCSHDW            = 0;                // EQEP Pos
;     | ition-Compare Load Inmediate                                           
;----------------------------------------------------------------------
        AND       @_EQep1Regs+23,#0x7fff ; |160| 
	.line	43
;----------------------------------------------------------------------
; 161 | EQep1Regs.QPOSCTL.bit.PCLOAD            = 0;                // Position
;     |  Compare Loads in QPOSCNT=0                                            
;----------------------------------------------------------------------
        AND       @_EQep1Regs+23,#0xbfff ; |161| 
	.line	44
;----------------------------------------------------------------------
; 162 | EQep1Regs.QPOSCTL.bit.PCPOL             = 0;                // Polarity
;     |  of sync output Active High pulse output                               
;----------------------------------------------------------------------
        AND       @_EQep1Regs+23,#0xdfff ; |162| 
	.line	45
;----------------------------------------------------------------------
; 163 | EQep1Regs.QPOSCTL.bit.PCE               = 0;                // Position
;     |  Compare Disable                                                       
; 164 | // End Position-Compare Configuration                                  
; 165 | // Edge Capture Unit Configuration w_measure = velocfactor * X / dT    
;----------------------------------------------------------------------
        AND       @_EQep1Regs+23,#0xefff ; |163| 
	.line	48
;----------------------------------------------------------------------
; 166 | EQep1Regs.QCAPCTL.bit.UPPS                = UPEVENTDIV1;       // EQEP
;     | Unit Event /1 (X=1)                                                    
; 167 | //EQep1Regs.QCAPCTL.bit.UPPS                = UPEVENTDIV2;       // EQE
;     | P Unit Event /2 (X=2)                                                  
; 168 | //EQep1Regs.QCAPCTL.bit.UPPS                = UPEVENTDIV4;       // EQE
;     | P Unit Event /4 (X=4)                                                  
; 169 | //EQep1Regs.QCAPCTL.bit.UPPS                = UPEVENTDIV8;       // EQE
;     | P Unit Event /8 (X=8)                                                  
; 170 | //EQep1Regs.QCAPCTL.bit.CCPS                = SYSCLKDIV1;        // EQE
;     | P Capture Timer Prescaler /1                                           
; 171 | //EQep1Regs.QCAPCTL.bit.CCPS                = SYSCLKDIV2;        // EQE
;     | P Capture Timer Prescaler /2                                           
; 172 | //EQep1Regs.QCAPCTL.bit.CCPS                = SYSCLKDIV4;        // EQE
;     | P Capture Timer Prescaler /4                                           
; 173 | //EQep1Regs.QCAPCTL.bit.CCPS                = SYSCLKDIV8;        // EQE
;     | P Capture Timer Prescaler /8                                           
; 174 | //EQep1Regs.QCAPCTL.bit.CCPS                = SYSCLKDIV16;       // EQE
;     | P Capture Timer Prescaler /16                                          
;----------------------------------------------------------------------
        AND       @_EQep1Regs+22,#0xfff0 ; |166| 
	.line	57
;----------------------------------------------------------------------
; 175 | EQep1Regs.QCAPCTL.bit.CCPS                = SYSCLKDIV32;       // EQEP
;     | Capture Timer Prescaler /32                                            
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+22,#0xff8f ; |175| 
        ORB       AL,#0x50              ; |175| 
        MOV       @_EQep1Regs+22,AL     ; |175| 
	.line	58
;----------------------------------------------------------------------
; 176 | EQep1Regs.QCAPCTL.bit.CEN                 = 1;                 // EQEP
;     | Capture is Enable                                                      
; 177 | // End Edge Capture Unit Configuration                                 
; 178 | //UTIME Configuration                                                  
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+22,#0x7fff ; |176| 
        OR        AL,#0x8000            ; |176| 
        MOV       @_EQep1Regs+22,AL     ; |176| 
	.line	61
;----------------------------------------------------------------------
; 179 | EQep1Regs.QUPRD                            = INITIAL_TIMEOUT;        //
;     |  Unit Time Out Period                                                  
;----------------------------------------------------------------------
        MOVL      XAR4,#20000           ; |179| 
        MOVL      @_EQep1Regs+16,XAR4   ; |179| 
	.line	62
;----------------------------------------------------------------------
; 180 | EQep1Regs.QEPCTL.bit.UTE                = 1;                   // Enabl
;     | e the EQEP Unit Timer                                                  
; 181 | //End UTIME Configuration                                              
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+21,#0xfffd ; |180| 
        ORB       AL,#0x02              ; |180| 
        MOV       @_EQep1Regs+21,AL     ; |180| 
	.line	64
;----------------------------------------------------------------------
; 182 | EALLOW;                                                        // This
;     | is needed to write to EALLOW protected registers                       
;----------------------------------------------------------------------
 EALLOW
	.line	65
;----------------------------------------------------------------------
; 183 | PieVectTable.EQEP1_INT                    = &PTC5Feqep_isr;    // EQEP
;     | Interrupt Address                                                      
;----------------------------------------------------------------------
        MOVL      XAR4,#_PTC5Feqep_isr  ; |183| 
        MOVW      DP,#_PieVectTable+128
        MOVL      @_PieVectTable+128,XAR4 ; |183| 
	.line	66
;----------------------------------------------------------------------
; 184 | EDIS;                                                          // Disab
;     | le writing to EALLOW protected registers                               
; 186 | // Unit Time Out Interrupt                                             
;----------------------------------------------------------------------
 EDIS
	.line	69
;----------------------------------------------------------------------
; 187 | IER |= M_INT5;                                                 // Enabl
;     | e EQEP1 CPU-PIEIER5 for INT5 (Group 5)                                 
;----------------------------------------------------------------------
        OR        IER,#0x0010           ; |187| 
	.line	70
;----------------------------------------------------------------------
; 188 | PieCtrlRegs.PIEIER5.bit.INTx1              = 1;                // Enabl
;     | e the EQEP1_INT PIEIER5.1 to interrupt resquest sent to CPU Level      
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+10
        AND       AL,@_PieCtrlRegs+10,#0xfffe ; |188| 
        ORB       AL,#0x01              ; |188| 
        MOV       @_PieCtrlRegs+10,AL   ; |188| 
	.line	71
;----------------------------------------------------------------------
; 189 | PieCtrlRegs.PIEACK.bit.ACK5                = 1;                // Clear
;     |  the PIEACK of Group 5 for enables Interrupt Resquest at CPU Level     
;----------------------------------------------------------------------
        AND       AL,@_PieCtrlRegs+1,#0xffef ; |189| 
        ORB       AL,#0x10              ; |189| 
        MOV       @_PieCtrlRegs+1,AL    ; |189| 
	.line	72
;----------------------------------------------------------------------
; 190 | EQep1Regs.QEINT.bit.UTO                    = 1;                // Unit
;     | Time Out Interrupt Enable                                              
; 191 | // End Unit Time Out Interrupt                                         
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+24
        AND       AL,@_EQep1Regs+24,#0xf7ff ; |190| 
        OR        AL,#0x0800            ; |190| 
        MOV       @_EQep1Regs+24,AL     ; |190| 
	.line	75
        SPM       #0
        LRETR
        ; return occurs
	.endfunc	193,000000000h,0
	.sect	".text"
	.global	_PTC5Fepwm_isr
	.file	"C:\CCStudio_v3.3\MyProjects\DAVID_ADAPT_VEL\PTC5F_epwm.c"
	.sym	_PTC5Fepwm_isr,_PTC5Fepwm_isr, 32, 2, 0
	.func	134

;***************************************************************
;* FNAME: _PTC5Fepwm_isr                FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  4 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fepwm_isr:
	.line	1
;----------------------------------------------------------------------
; 134 | interrupt void PTC5Fepwm_isr(){                    // ePWM Interrupt Se
;     | rvice Rutine                                                           
;----------------------------------------------------------------------
        ASP
        PUSH      RB
        MOV32     *SP++,STF
        SETFLG    RNDF32=1, RNDF64=1
        CLRC      PAGE0,OVM
        CLRC      AMODE
	.line	3
;----------------------------------------------------------------------
; 136 | timeout                               = 0;       // Clear PWM Time Out
;     | Flag                                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_timeout
        MOV       @_timeout,#0          ; |136| 
	.line	5
;----------------------------------------------------------------------
; 138 | EPwm1Regs.ETCLR.bit.INT               = 1;       // Clear EPWM1_INT Fla
;     | g                                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+28
        AND       AL,@_EPwm1Regs+28,#0xfffe ; |138| 
        ORB       AL,#0x01              ; |138| 
        MOV       @_EPwm1Regs+28,AL     ; |138| 
	.line	6
;----------------------------------------------------------------------
; 139 | PieCtrlRegs.PIEACK.bit.ACK3           = 1;       // Clear the PIEACK of
;     |  Group 3 for enables Interrupt Resquest at CPU Level                   
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1
        AND       AL,@_PieCtrlRegs+1,#0xfffb ; |139| 
        ORB       AL,#0x04              ; |139| 
        MOV       @_PieCtrlRegs+1,AL    ; |139| 
	.line	8
;----------------------------------------------------------------------
; 141 | }// interrupt void IRFOC5Fepwm_isr()                                   
;----------------------------------------------------------------------
        MOV32     STF,*--SP
        POP       RB
        NASP
        IRET
        ; return occurs
	.endfunc	141,000000000h,4
	.sect	".text"
	.global	_PTC5Fepwm5F_start
	.sym	_PTC5Fepwm5F_start,_PTC5Fepwm5F_start, 32, 2, 0
	.func	147

;***************************************************************
;* FNAME: _PTC5Fepwm5F_start            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fepwm5F_start:
	.line	1
;----------------------------------------------------------------------
; 147 | void PTC5Fepwm5F_start(){                                              
;----------------------------------------------------------------------
	.line	3
;----------------------------------------------------------------------
; 149 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	4
;----------------------------------------------------------------------
; 150 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC     = 0;       // Stop all the TB clo
;     | cks                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       @_SysCtrlRegs+12,#0xfffb ; |150| 
	.line	5
;----------------------------------------------------------------------
; 151 | EDIS;                                            // Disable writing to
;     | EALLOW protected registers                                             
; 153 | //  Enable the  ePWM-CLK 1-6                                           
;----------------------------------------------------------------------
 EDIS
	.line	8
;----------------------------------------------------------------------
; 154 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	9
;----------------------------------------------------------------------
; 155 | SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK    = 1;       // Enable ePWM1 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffe ; |155| 
        ORB       AL,#0x01              ; |155| 
        MOV       @_SysCtrlRegs+13,AL   ; |155| 
	.line	10
;----------------------------------------------------------------------
; 156 | SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK    = 1;       // Enable ePWM2 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffd ; |156| 
        ORB       AL,#0x02              ; |156| 
        MOV       @_SysCtrlRegs+13,AL   ; |156| 
	.line	11
;----------------------------------------------------------------------
; 157 | SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK    = 1;       // Enable ePWM3 clock*
;     | /                                                                      
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffb ; |157| 
        ORB       AL,#0x04              ; |157| 
        MOV       @_SysCtrlRegs+13,AL   ; |157| 
	.line	12
;----------------------------------------------------------------------
; 158 | SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK    = 1;       // Enable ePWM4 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfff7 ; |158| 
        ORB       AL,#0x08              ; |158| 
        MOV       @_SysCtrlRegs+13,AL   ; |158| 
	.line	13
;----------------------------------------------------------------------
; 159 | SysCtrlRegs.PCLKCR1.bit.EPWM5ENCLK    = 1;       // Enable ePWM5 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xffef ; |159| 
        ORB       AL,#0x10              ; |159| 
        MOV       @_SysCtrlRegs+13,AL   ; |159| 
	.line	14
;----------------------------------------------------------------------
; 160 | EDIS;                                                                  
; 161 | //  End Enable the      ePWM-CLK 1-6                                   
; 163 | //  Configure the ePWM1-6 shared pins                                  
;----------------------------------------------------------------------
 EDIS
	.line	18
;----------------------------------------------------------------------
; 164 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	19
;----------------------------------------------------------------------
; 165 | GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 1;       // Configure GPIO0 as
;     | EPWM1A                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       AL,@_GpioCtrlRegs+6,#0xfffc ; |165| 
        ORB       AL,#0x01              ; |165| 
        MOV       @_GpioCtrlRegs+6,AL   ; |165| 
	.line	20
;----------------------------------------------------------------------
; 166 | GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 1;       // Configure GPIO1 as
;     | EPWM1B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xfff3 ; |166| 
        ORB       AL,#0x04              ; |166| 
        MOV       @_GpioCtrlRegs+6,AL   ; |166| 
	.line	21
;----------------------------------------------------------------------
; 167 | GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 1;       // Configure GPIO2 as
;     | EPWM2A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xffcf ; |167| 
        ORB       AL,#0x10              ; |167| 
        MOV       @_GpioCtrlRegs+6,AL   ; |167| 
	.line	22
;----------------------------------------------------------------------
; 168 | GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 1;       // Configure GPIO3 as
;     | EPWM2B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xff3f ; |168| 
        ORB       AL,#0x40              ; |168| 
        MOV       @_GpioCtrlRegs+6,AL   ; |168| 
	.line	23
;----------------------------------------------------------------------
; 169 | GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 1;       // Configure GPIO4 as
;     | EPWM3A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xfcff ; |169| 
        OR        AL,#0x0100            ; |169| 
        MOV       @_GpioCtrlRegs+6,AL   ; |169| 
	.line	24
;----------------------------------------------------------------------
; 170 | GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 1;       // Configure GPIO5 as
;     | EPWM3B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xf3ff ; |170| 
        OR        AL,#0x0400            ; |170| 
        MOV       @_GpioCtrlRegs+6,AL   ; |170| 
	.line	25
;----------------------------------------------------------------------
; 171 | GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 1;       // Configure GPIO6 as
;     | EPWM4A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xcfff ; |171| 
        OR        AL,#0x1000            ; |171| 
        MOV       @_GpioCtrlRegs+6,AL   ; |171| 
	.line	26
;----------------------------------------------------------------------
; 172 | GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 1;       // Configure GPIO7 as
;     | EPWM4B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0x3fff ; |172| 
        OR        AL,#0x4000            ; |172| 
        MOV       @_GpioCtrlRegs+6,AL   ; |172| 
	.line	27
;----------------------------------------------------------------------
; 173 | GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 1;       // Configure GPIO8 as
;     | EPWM5A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xfffc ; |173| 
        ORB       AL,#0x01              ; |173| 
        MOV       @_GpioCtrlRegs+7,AL   ; |173| 
	.line	28
;----------------------------------------------------------------------
; 174 | GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 1;       // Configure GPIO9 as
;     | EPWM5B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xfff3 ; |174| 
        ORB       AL,#0x04              ; |174| 
        MOV       @_GpioCtrlRegs+7,AL   ; |174| 
	.line	29
;----------------------------------------------------------------------
; 175 | EDIS;                                            // Disable writing to
;     | EALLOW protected registers*/                                           
; 176 | //  End Configure the ePWM1-5 shared pins                              
; 178 | // TB of ePWM 1-5 Configure                                            
;----------------------------------------------------------------------
 EDIS
	.line	33
;----------------------------------------------------------------------
; 179 | epwmmodule_config(&EPwm1Regs);                                         
;----------------------------------------------------------------------
        SPM       #0
        MOVL      XAR4,#_EPwm1Regs      ; |179| 
        LCR       #_epwmmodule_config   ; |179| 
        ; call occurs [#_epwmmodule_config] ; |179| 
	.line	34
;----------------------------------------------------------------------
; 180 | epwmmodule_config(&EPwm2Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm2Regs      ; |180| 
        LCR       #_epwmmodule_config   ; |180| 
        ; call occurs [#_epwmmodule_config] ; |180| 
	.line	35
;----------------------------------------------------------------------
; 181 | epwmmodule_config(&EPwm3Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm3Regs      ; |181| 
        LCR       #_epwmmodule_config   ; |181| 
        ; call occurs [#_epwmmodule_config] ; |181| 
	.line	36
;----------------------------------------------------------------------
; 182 | epwmmodule_config(&EPwm4Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm4Regs      ; |182| 
        LCR       #_epwmmodule_config   ; |182| 
        ; call occurs [#_epwmmodule_config] ; |182| 
	.line	37
;----------------------------------------------------------------------
; 183 | epwmmodule_config(&EPwm5Regs);                                         
; 184 | //  End TB Configure                                                   
; 187 | //  EPWM1 Module as Master Synchronization                             
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm5Regs      ; |183| 
        LCR       #_epwmmodule_config   ; |183| 
        ; call occurs [#_epwmmodule_config] ; |183| 
	.line	42
;----------------------------------------------------------------------
; 188 | EPwm1Regs.TBCTL.bit.PHSEN     = TB_DISABLE;      // Disables Phase Load
;     | ing (Master Mode ePWM1)                                                
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs
        AND       @_EPwm1Regs,#0xfffb   ; |188| 
	.line	43
;----------------------------------------------------------------------
; 189 | EPwm1Regs.TBCTL.bit.SYNCOSEL  = TB_CTR_ZERO;     // Generates EPWMxSYNC
;     |  output when CTR = Zero                                                
; 190 | //  End EPWM1 Module as Master Synchronization                         
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs,#0xffcf ; |189| 
        ORB       AL,#0x10              ; |189| 
        MOV       @_EPwm1Regs,AL        ; |189| 
	.line	48
;----------------------------------------------------------------------
; 194 | EALLOW;                                          // Allows CPU to write
;     |  to EALLOW protected registers                                         
;----------------------------------------------------------------------
 EALLOW
	.line	49
;----------------------------------------------------------------------
; 195 | PieVectTable.EPWM1_INT = &PTC5Fepwm_isr;         // ePWM1 Interrupt Rut
;     | ine Address                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_PieVectTable+96
        MOVL      XAR4,#_PTC5Fepwm_isr  ; |195| 
        MOVL      @_PieVectTable+96,XAR4 ; |195| 
	.line	50
;----------------------------------------------------------------------
; 196 | EDIS;
;     |                                                   // Disable CPU to wri
;     | te to EALLOW protected registers                                       
;----------------------------------------------------------------------
 EDIS
	.line	53
;----------------------------------------------------------------------
; 199 | EPwm1Regs.ETSEL.bit.INTSEL    = ET_CTR_PRD;      // Enable ePWM1 Module
;     |  Event Genaration when CTR=PRD                                         
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xfff8 ; |199| 
        ORB       AL,#0x02              ; |199| 
        MOV       @_EPwm1Regs+25,AL     ; |199| 
	.line	54
;----------------------------------------------------------------------
; 200 | EPwm1Regs.ETPS.bit.INTPRD     = ET_1ST;          // ePWM1 Module Event
;     | Prescaler DIV1                                                         
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs+26,#0xfffc ; |200| 
        ORB       AL,#0x01              ; |200| 
        MOV       @_EPwm1Regs+26,AL     ; |200| 
	.line	56
;----------------------------------------------------------------------
; 202 | IER |= M_INT3;                                   // Enable EPWM1 CPU-PI
;     | EIER3 for INT5 (Group 3)                                               
;----------------------------------------------------------------------
        OR        IER,#0x0004           ; |202| 
	.line	57
;----------------------------------------------------------------------
; 203 | PieCtrlRegs.PIEIER3.bit.INTx1 = 1;               // Enable the EPWM1_IN
;     | T PIEIER3.1 to interrupt resquest sent to CPU Level                    
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+6
        AND       AL,@_PieCtrlRegs+6,#0xfffe ; |203| 
        ORB       AL,#0x01              ; |203| 
        MOV       @_PieCtrlRegs+6,AL    ; |203| 
	.line	58
;----------------------------------------------------------------------
; 204 | PieCtrlRegs.PIEACK.bit.ACK3   = 0;               // Clear the PIEACK of
;     |  Group 3 for enables Interrupt Resquest at CPU Level                   
;----------------------------------------------------------------------
        AND       @_PieCtrlRegs+1,#0xfffb ; |204| 
	.line	59
;----------------------------------------------------------------------
; 205 | EPwm1Regs.ETSEL.bit.INTEN     = ET_ETSEL_ENABLE; // Enables EPWM1_INT G
;     | eneration                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xfff7 ; |205| 
        ORB       AL,#0x08              ; |205| 
        MOV       @_EPwm1Regs+25,AL     ; |205| 
	.line	62
;----------------------------------------------------------------------
; 208 | }//void PTC5Fepwm5F_start()                                            
;----------------------------------------------------------------------
        SPM       #0
        LRETR
        ; return occurs
	.endfunc	208,000000000h,0
	.sect	".text"
	.global	_PTC5Fepwm6F_start
	.sym	_PTC5Fepwm6F_start,_PTC5Fepwm6F_start, 32, 2, 0
	.func	220

;***************************************************************
;* FNAME: _PTC5Fepwm6F_start            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fepwm6F_start:
	.line	1
;----------------------------------------------------------------------
; 220 | void PTC5Fepwm6F_start(){                                              
;----------------------------------------------------------------------
	.line	3
;----------------------------------------------------------------------
; 222 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	4
;----------------------------------------------------------------------
; 223 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC     = 0;       // Stop all the TB clo
;     | cks                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       @_SysCtrlRegs+12,#0xfffb ; |223| 
	.line	5
;----------------------------------------------------------------------
; 224 | EDIS;                                            // Disable writing to
;     | EALLOW protected registers                                             
; 226 | //  Enable the  ePWM-CLK 1-6                                           
;----------------------------------------------------------------------
 EDIS
	.line	8
;----------------------------------------------------------------------
; 227 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	9
;----------------------------------------------------------------------
; 228 | SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK    = 1;       // Enable ePWM1 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffe ; |228| 
        ORB       AL,#0x01              ; |228| 
        MOV       @_SysCtrlRegs+13,AL   ; |228| 
	.line	10
;----------------------------------------------------------------------
; 229 | SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK    = 1;       // Enable ePWM2 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffd ; |229| 
        ORB       AL,#0x02              ; |229| 
        MOV       @_SysCtrlRegs+13,AL   ; |229| 
	.line	11
;----------------------------------------------------------------------
; 230 | SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK    = 1;       // Enable ePWM3 clock*
;     | /                                                                      
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffb ; |230| 
        ORB       AL,#0x04              ; |230| 
        MOV       @_SysCtrlRegs+13,AL   ; |230| 
	.line	12
;----------------------------------------------------------------------
; 231 | SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK    = 1;       // Enable ePWM4 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfff7 ; |231| 
        ORB       AL,#0x08              ; |231| 
        MOV       @_SysCtrlRegs+13,AL   ; |231| 
	.line	13
;----------------------------------------------------------------------
; 232 | SysCtrlRegs.PCLKCR1.bit.EPWM5ENCLK    = 1;       // Enable ePWM5 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xffef ; |232| 
        ORB       AL,#0x10              ; |232| 
        MOV       @_SysCtrlRegs+13,AL   ; |232| 
	.line	14
;----------------------------------------------------------------------
; 233 | SysCtrlRegs.PCLKCR1.bit.EPWM6ENCLK    = 1;       // Enable ePWM6 clock*
;     | /                                                                      
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xffdf ; |233| 
        ORB       AL,#0x20              ; |233| 
        MOV       @_SysCtrlRegs+13,AL   ; |233| 
	.line	15
;----------------------------------------------------------------------
; 234 | EDIS;                                                                  
; 235 | //  End Enable the      ePWM-CLK 1-6                                   
; 237 | //  Configure the ePWM1-6 shared pins                                  
;----------------------------------------------------------------------
 EDIS
	.line	19
;----------------------------------------------------------------------
; 238 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	20
;----------------------------------------------------------------------
; 239 | GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 1;       // Configure GPIO0 as
;     | EPWM1A                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       AL,@_GpioCtrlRegs+6,#0xfffc ; |239| 
        ORB       AL,#0x01              ; |239| 
        MOV       @_GpioCtrlRegs+6,AL   ; |239| 
	.line	21
;----------------------------------------------------------------------
; 240 | GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 1;       // Configure GPIO1 as
;     | EPWM1B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xfff3 ; |240| 
        ORB       AL,#0x04              ; |240| 
        MOV       @_GpioCtrlRegs+6,AL   ; |240| 
	.line	22
;----------------------------------------------------------------------
; 241 | GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 1;       // Configure GPIO2 as
;     | EPWM2A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xffcf ; |241| 
        ORB       AL,#0x10              ; |241| 
        MOV       @_GpioCtrlRegs+6,AL   ; |241| 
	.line	23
;----------------------------------------------------------------------
; 242 | GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 1;       // Configure GPIO3 as
;     | EPWM2B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xff3f ; |242| 
        ORB       AL,#0x40              ; |242| 
        MOV       @_GpioCtrlRegs+6,AL   ; |242| 
	.line	24
;----------------------------------------------------------------------
; 243 | GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 1;       // Configure GPIO4 as
;     | EPWM3A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xfcff ; |243| 
        OR        AL,#0x0100            ; |243| 
        MOV       @_GpioCtrlRegs+6,AL   ; |243| 
	.line	25
;----------------------------------------------------------------------
; 244 | GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 1;       // Configure GPIO5 as
;     | EPWM3B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xf3ff ; |244| 
        OR        AL,#0x0400            ; |244| 
        MOV       @_GpioCtrlRegs+6,AL   ; |244| 
	.line	26
;----------------------------------------------------------------------
; 245 | GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 1;       // Configure GPIO6 as
;     | EPWM4A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xcfff ; |245| 
        OR        AL,#0x1000            ; |245| 
        MOV       @_GpioCtrlRegs+6,AL   ; |245| 
	.line	27
;----------------------------------------------------------------------
; 246 | GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 1;       // Configure GPIO7 as
;     | EPWM4B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0x3fff ; |246| 
        OR        AL,#0x4000            ; |246| 
        MOV       @_GpioCtrlRegs+6,AL   ; |246| 
	.line	28
;----------------------------------------------------------------------
; 247 | GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 1;       // Configure GPIO8 as
;     | EPWM5A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xfffc ; |247| 
        ORB       AL,#0x01              ; |247| 
        MOV       @_GpioCtrlRegs+7,AL   ; |247| 
	.line	29
;----------------------------------------------------------------------
; 248 | GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 1;       // Configure GPIO9 as
;     | EPWM5B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xfff3 ; |248| 
        ORB       AL,#0x04              ; |248| 
        MOV       @_GpioCtrlRegs+7,AL   ; |248| 
	.line	30
;----------------------------------------------------------------------
; 249 | GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 1;       // Configure GPIO8 as
;     | EPWM6A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xffcf ; |249| 
        ORB       AL,#0x10              ; |249| 
        MOV       @_GpioCtrlRegs+7,AL   ; |249| 
	.line	31
;----------------------------------------------------------------------
; 250 | GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 1;       // Configure GPIO9 as
;     | EPWM6B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xff3f ; |250| 
        ORB       AL,#0x40              ; |250| 
        MOV       @_GpioCtrlRegs+7,AL   ; |250| 
	.line	32
;----------------------------------------------------------------------
; 251 | EDIS;                                            // Disable writing to
;     | EALLOW protected registers*/                                           
; 252 | //  End Configure the ePWM1-6 shared pins                              
; 254 | // TB of ePWM 1-6 Configure                                            
;----------------------------------------------------------------------
 EDIS
	.line	36
;----------------------------------------------------------------------
; 255 | epwmmodule_config(&EPwm1Regs);                                         
;----------------------------------------------------------------------
        SPM       #0
        MOVL      XAR4,#_EPwm1Regs      ; |255| 
        LCR       #_epwmmodule_config   ; |255| 
        ; call occurs [#_epwmmodule_config] ; |255| 
	.line	37
;----------------------------------------------------------------------
; 256 | epwmmodule_config(&EPwm2Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm2Regs      ; |256| 
        LCR       #_epwmmodule_config   ; |256| 
        ; call occurs [#_epwmmodule_config] ; |256| 
	.line	38
;----------------------------------------------------------------------
; 257 | epwmmodule_config(&EPwm3Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm3Regs      ; |257| 
        LCR       #_epwmmodule_config   ; |257| 
        ; call occurs [#_epwmmodule_config] ; |257| 
	.line	39
;----------------------------------------------------------------------
; 258 | epwmmodule_config(&EPwm4Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm4Regs      ; |258| 
        LCR       #_epwmmodule_config   ; |258| 
        ; call occurs [#_epwmmodule_config] ; |258| 
	.line	40
;----------------------------------------------------------------------
; 259 | epwmmodule_config(&EPwm5Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm5Regs      ; |259| 
        LCR       #_epwmmodule_config   ; |259| 
        ; call occurs [#_epwmmodule_config] ; |259| 
	.line	41
;----------------------------------------------------------------------
; 260 | epwmmodule_config(&EPwm6Regs);                                         
; 261 | //  End TB Configure                                                   
; 264 | //  EPWM1 Module as Master Synchronization                             
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm6Regs      ; |260| 
        LCR       #_epwmmodule_config   ; |260| 
        ; call occurs [#_epwmmodule_config] ; |260| 
	.line	46
;----------------------------------------------------------------------
; 265 | EPwm1Regs.TBCTL.bit.PHSEN     = TB_DISABLE;      // Disables Phase Load
;     | ing (Master Mode ePWM1)                                                
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs
        AND       @_EPwm1Regs,#0xfffb   ; |265| 
	.line	47
;----------------------------------------------------------------------
; 266 | EPwm1Regs.TBCTL.bit.SYNCOSEL  = TB_CTR_ZERO;     // Generates EPWMxSYNC
;     |  output when CTR = Zero                                                
; 267 | //  End EPWM1 Module as Master Synchronization                         
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs,#0xffcf ; |266| 
        ORB       AL,#0x10              ; |266| 
        MOV       @_EPwm1Regs,AL        ; |266| 
	.line	52
;----------------------------------------------------------------------
; 271 | EALLOW;                                          // Allows CPU to write
;     |  to EALLOW protected registers                                         
;----------------------------------------------------------------------
 EALLOW
	.line	53
;----------------------------------------------------------------------
; 272 | PieVectTable.EPWM1_INT = &PTC5Fepwm_isr;       // ePWM1 Interrupt Rutin
;     | e Address                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_PieVectTable+96
        MOVL      XAR4,#_PTC5Fepwm_isr  ; |272| 
        MOVL      @_PieVectTable+96,XAR4 ; |272| 
	.line	54
;----------------------------------------------------------------------
; 273 | EDIS;
;     |                                                   // Disable CPU to wri
;     | te to EALLOW protected registers                                       
;----------------------------------------------------------------------
 EDIS
	.line	57
;----------------------------------------------------------------------
; 276 | EPwm1Regs.ETSEL.bit.INTSEL    = ET_CTR_PRD;      // Enable ePWM1 Module
;     |  Event Genaration when CTR=PRD                                         
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xfff8 ; |276| 
        ORB       AL,#0x02              ; |276| 
        MOV       @_EPwm1Regs+25,AL     ; |276| 
	.line	58
;----------------------------------------------------------------------
; 277 | EPwm1Regs.ETPS.bit.INTPRD     = ET_1ST;          // ePWM1 Module Event
;     | Prescaler DIV1                                                         
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs+26,#0xfffc ; |277| 
        ORB       AL,#0x01              ; |277| 
        MOV       @_EPwm1Regs+26,AL     ; |277| 
	.line	60
;----------------------------------------------------------------------
; 279 | IER |= M_INT3;                                   // Enable EPWM1 CPU-PI
;     | EIER3 for INT5 (Group 3)                                               
;----------------------------------------------------------------------
        OR        IER,#0x0004           ; |279| 
	.line	61
;----------------------------------------------------------------------
; 280 | PieCtrlRegs.PIEIER3.bit.INTx1 = 1;               // Enable the EPWM1_IN
;     | T PIEIER3.1 to interrupt resquest sent to CPU Level                    
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+6
        AND       AL,@_PieCtrlRegs+6,#0xfffe ; |280| 
        ORB       AL,#0x01              ; |280| 
        MOV       @_PieCtrlRegs+6,AL    ; |280| 
	.line	62
;----------------------------------------------------------------------
; 281 | PieCtrlRegs.PIEACK.bit.ACK3   = 0;               // Clear the PIEACK of
;     |  Group 3 for enables Interrupt Resquest at CPU Level                   
;----------------------------------------------------------------------
        AND       @_PieCtrlRegs+1,#0xfffb ; |281| 
	.line	63
;----------------------------------------------------------------------
; 282 | EPwm1Regs.ETSEL.bit.INTEN     = ET_ETSEL_ENABLE; // Enables EPWM1_INT G
;     | eneration                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xfff7 ; |282| 
        ORB       AL,#0x08              ; |282| 
        MOV       @_EPwm1Regs+25,AL     ; |282| 
	.line	65
;----------------------------------------------------------------------
; 284 | }//void PTC5Fepwm6F_start()                                            
;----------------------------------------------------------------------
        SPM       #0
        LRETR
        ; return occurs
	.endfunc	284,000000000h,0
	.sect	".text"
	.global	_epwmmodule_config
	.sym	_epwmmodule_config,_epwmmodule_config, 32, 2, 0
	.func	287

;***************************************************************
;* FNAME: _epwmmodule_config            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_epwmmodule_config:
	.line	1
;----------------------------------------------------------------------
; 287 | void epwmmodule_config(volatile struct EPWM_REGS *ePWM_Regs){          
;----------------------------------------------------------------------
;* AR4   assigned to _ePWM_Regs
	.sym	_ePWM_Regs,12, 24, 17, 22, _EPWM_REGS
;* AR4   assigned to _ePWM_Regs
	.sym	_ePWM_Regs,12, 24, 4, 22, _EPWM_REGS
	.line	3
;----------------------------------------------------------------------
; 289 | ePWM_Regs->TBPRD = pwm_period;                    // Set timer period o
;     | f EPWMx                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_pwm_period
        MOV       AL,@_pwm_period       ; |289| 
        MOV       *+XAR4[5],AL          ; |289| 
	.line	5
;----------------------------------------------------------------------
; 291 | ePWM_Regs->TBPHS.half.TBPHS     = 0;                      // Set Phase
;     | register to zero                                                       
;----------------------------------------------------------------------
        MOV       *+XAR4[3],#0          ; |291| 
	.line	7
;----------------------------------------------------------------------
; 293 | ePWM_Regs->TBCTL.bit.CTRMODE   = TB_COUNT_UPDOWN; // Up-Down-Count Mode
;----------------------------------------------------------------------
        AND       AL,*+XAR4[0],#0xfffc  ; |293| 
        ORB       AL,#0x02              ; |293| 
        MOV       *+XAR4[0],AL          ; |293| 
	.line	8
;----------------------------------------------------------------------
; 294 | ePWM_Regs->TBCTL.bit.PHSEN     = TB_ENABLE;      // Phase loading Enabl
;     | ed (Slave Mode)                                                        
;----------------------------------------------------------------------
        AND       AL,*+XAR4[0],#0xfffb  ; |294| 
        ORB       AL,#0x04              ; |294| 
        MOV       *+XAR4[0],AL          ; |294| 
	.line	9
;----------------------------------------------------------------------
; 295 | ePWM_Regs->TBCTL.bit.PRDLD     = TB_SHADOW;       // Period load as sha
;     | dow mode                                                               
;----------------------------------------------------------------------
        AND       *+XAR4[0],#0xfff7     ; |295| 
	.line	10
;----------------------------------------------------------------------
; 296 | ePWM_Regs->TBCTL.bit.SYNCOSEL  = TB_SYNC_IN;      // Selects EPWMxSYNC
;     | as sincronization output                                               
;----------------------------------------------------------------------
        AND       *+XAR4[0],#0xffcf     ; |296| 
	.line	11
;----------------------------------------------------------------------
; 297 | ePWM_Regs->TBCTL.bit.CLKDIV    = TB_DIV4;         // division by 4     
;----------------------------------------------------------------------
        AND       AL,*+XAR4[0],#0xe3ff  ; |297| 
        OR        AL,#0x0800            ; |297| 
        MOV       *+XAR4[0],AL          ; |297| 
	.line	12
;----------------------------------------------------------------------
; 298 | ePWM_Regs->TBCTL.bit.HSPCLKDIV = TB_DIV1;              // TBCLK = SYSCL
;     | KOUT / (HSPCLKDIV x CLKDIV) = 37.5MHz                                  
;----------------------------------------------------------------------
        AND       *+XAR4[0],#0xfc7f     ; |298| 
	.line	13
;----------------------------------------------------------------------
; 299 | ePWM_Regs->TBCTL.bit.PHSDIR    = TB_UP;                        // Phase
;     |  Direction Count-Up after the synchronization                          
;----------------------------------------------------------------------
        AND       AL,*+XAR4[0],#0xdfff  ; |299| 
        OR        AL,#0x2000            ; |299| 
        MOV       *+XAR4[0],AL          ; |299| 
	.line	16
;----------------------------------------------------------------------
; 302 | ePWM_Regs->CMPCTL.bit.SHDWAMODE = CC_SHADOW;       // Enable the CMPA t
;     | o operate in Shadow Mode                                               
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |302| 
        ADDB      XAR5,#7               ; |302| 
        AND       *+XAR5[0],#0xffef     ; |302| 
	.line	17
;----------------------------------------------------------------------
; 303 | ePWM_Regs->CMPCTL.bit.SHDWBMODE = CC_SHADOW;       // Enable the CMPB t
;     | o operate in Shadow Mode                                               
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |303| 
        ADDB      XAR5,#7               ; |303| 
        AND       *+XAR5[0],#0xffbf     ; |303| 
	.line	18
;----------------------------------------------------------------------
; 304 | ePWM_Regs->CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;     // Load the CMPA whe
;     | n TBCTR=0                                                              
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |304| 
        ADDB      XAR5,#7               ; |304| 
        AND       *+XAR5[0],#0xfffc     ; |304| 
	.line	19
;----------------------------------------------------------------------
; 305 | ePWM_Regs->CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;     // Load the CMPB whe
;     | n TBCTR=0                                                              
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |305| 
        ADDB      XAR5,#7               ; |305| 
        AND       *+XAR5[0],#0xfff3     ; |305| 
	.line	22
;----------------------------------------------------------------------
; 308 | ePWM_Regs->AQCTLA.bit.CAU      = AQ_SET;          // Set the EPWMxA whe
;     | n TBCTR=CMPA in Up Mode                                                
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |308| 
        ADDB      XAR5,#11              ; |308| 
        AND       AL,*+XAR5[0],#0xffcf  ; |308| 
        ORB       AL,#0x20              ; |308| 
        MOV       *+XAR5[0],AL          ; |308| 
	.line	23
;----------------------------------------------------------------------
; 309 | ePWM_Regs->AQCTLA.bit.CAD      = AQ_CLEAR;        // Clear the EPWMxA w
;     | hen TBCTR=CMPA in Down Mode                                            
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |309| 
        ADDB      XAR5,#11              ; |309| 
        AND       AL,*+XAR5[0],#0xff3f  ; |309| 
        ORB       AL,#0x40              ; |309| 
        MOV       *+XAR5[0],AL          ; |309| 
	.line	24
;----------------------------------------------------------------------
; 310 | ePWM_Regs->AQCTLA.bit.ZRO      = AQ_CLEAR;             // Action Counte
;     | r = Zero                                                               
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |310| 
        ADDB      XAR5,#11              ; |310| 
        AND       AL,*+XAR5[0],#0xfffc  ; |310| 
        ORB       AL,#0x01              ; |310| 
        MOV       *+XAR5[0],AL          ; |310| 
	.line	27
;----------------------------------------------------------------------
; 313 | ePWM_Regs->AQCTLA.bit.PRD      = AQ_NO_ACTION;    // Action Counter = P
;     | eriod                                                                  
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |313| 
        ADDB      XAR5,#11              ; |313| 
        AND       *+XAR5[0],#0xfff3     ; |313| 
	.line	30
;----------------------------------------------------------------------
; 316 | ePWM_Regs->DBCTL.bit.POLSEL    = DB_ACTV_HIC;     // Active High Comple
;     | mertary: EPWMxB=~EPWMxA                                                
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |316| 
        ADDB      XAR5,#15              ; |316| 
        AND       AL,*+XAR5[0],#0xfff3  ; |316| 
        ORB       AL,#0x08              ; |316| 
        MOV       *+XAR5[0],AL          ; |316| 
	.line	31
;----------------------------------------------------------------------
; 317 | ePWM_Regs->DBCTL.bit.OUT_MODE  = DB_FULL_ENABLE;  // Active Dead-Band i
;     | n the EPWMx Output                                                     
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |317| 
        ADDB      XAR5,#15              ; |317| 
        AND       AL,*+XAR5[0],#0xfffc  ; |317| 
        ORB       AL,#0x03              ; |317| 
        MOV       *+XAR5[0],AL          ; |317| 
	.line	32
;----------------------------------------------------------------------
; 318 | ePWM_Regs->DBRED               = DEADBAND_VALUE;  // Rising-edge delay
;     | time = 1 uS (assuming TBCLK = 37.5MHz)                                 
;----------------------------------------------------------------------
        MOVB      XAR0,#16              ; |318| 
        MOV       *+XAR4[AR0],#0        ; |318| 
	.line	33
;----------------------------------------------------------------------
; 319 | ePWM_Regs->DBFED               = DEADBAND_VALUE;  // Falling-edge delay
;     |  time = 1 uS (assuming TBCLK = 37.5MHz)                                
;----------------------------------------------------------------------
        MOVB      XAR0,#17              ; |319| 
        MOV       *+XAR4[AR0],#0        ; |319| 
	.line	36
;----------------------------------------------------------------------
; 322 | ePWM_Regs->PCCTL.bit.CHPEN     = 0;                               // Di
;     | sable Chopper control                                                  
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |322| 
        ADDB      XAR5,#30              ; |322| 
        AND       *+XAR5[0],#0xfffe     ; |322| 
	.line	39
;----------------------------------------------------------------------
; 325 | ePWM_Regs->TZCTL.bit.TZA       = TZ_DISABLE;      // Trip-Zone A disabl
;     | e                                                                      
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |325| 
        ADDB      XAR5,#20              ; |325| 
        AND       AL,*+XAR5[0],#0xfffc  ; |325| 
        ORB       AL,#0x03              ; |325| 
        MOV       *+XAR5[0],AL          ; |325| 
	.line	40
;----------------------------------------------------------------------
; 326 | ePWM_Regs->TZCTL.bit.TZB       = TZ_DISABLE;      // Trip-Zone B disabl
;     | e                                                                      
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |326| 
        ADDB      XAR5,#20              ; |326| 
        AND       AL,*+XAR5[0],#0xfff3  ; |326| 
        ORB       AL,#0x0c              ; |326| 
        MOV       *+XAR5[0],AL          ; |326| 
	.line	42
;----------------------------------------------------------------------
; 328 | ePWM_Regs->TZEINT.bit.OST      = 0;                               // Tr
;     | ip-Zone A disable ON SHOT                                              
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |328| 
        ADDB      XAR5,#21              ; |328| 
        AND       *+XAR5[0],#0xfffb     ; |328| 
	.line	43
;----------------------------------------------------------------------
; 329 | ePWM_Regs->TZEINT.bit.CBC      = TZ_DISABLE;      // Trip-Zone B disabl
;     | e CYCLE BY CYCLE                                                       
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |329| 
        ADDB      XAR5,#21              ; |329| 
        AND       AL,*+XAR5[0],#0xfffd  ; |329| 
        ORB       AL,#0x02              ; |329| 
        MOV       *+XAR5[0],AL          ; |329| 
	.line	46
;----------------------------------------------------------------------
; 332 | ePWM_Regs->CMPA.half.CMPA      = pwm_period;      // Load de CMPA value
;     |  at PRD                                                                
;----------------------------------------------------------------------
        MOVB      XAR0,#9               ; |332| 
        MOV       AL,@_pwm_period       ; |332| 
        MOV       *+XAR4[AR0],AL        ; |332| 
	.line	50
;----------------------------------------------------------------------
; 336 | }//void epwmmodule_config(volatile struct EPWM_REGS *ePWM_Regs)        
;----------------------------------------------------------------------
        LRETR
        ; return occurs
	.endfunc	336,000000000h,0
	.sect	".text"
	.global	_PTC5Fadc_isr
	.file	"C:\CCStudio_v3.3\MyProjects\DAVID_ADAPT_VEL\PTC5F_adc.c"
	.sym	_PTC5Fadc_isr,_PTC5Fadc_isr, 32, 2, 0
	.func	5

;***************************************************************
;* FNAME: _PTC5Fadc_isr                 FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto, 20 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fadc_isr:
	.line	1
;----------------------------------------------------------------------
;   5 | interrupt void PTC5Fadc_isr(){                                         
;----------------------------------------------------------------------
        ASP
        PUSH      RB
        MOVL      *SP++,XAR5
        MOVL      *SP++,XAR6
        MOVL      *SP++,XAR7
        MOVL      *SP++,XT
        MOV32     *SP++,STF
        MOV32     *SP++,R0H
        MOV32     *SP++,R1H
        MOV32     *SP++,R2H
        MOV32     *SP++,R3H
        SETFLG    RNDF32=1, RNDF64=1
        CLRC      PAGE0,OVM
        CLRC      AMODE
	.line	3
;----------------------------------------------------------------------
;   7 | Ia_medido = (float) AdcMirror.ADCRESULT0;                              
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror
        UI16TOF32 R0H,@_AdcMirror       ; |7| 
        MOVW      DP,#_Ia_medido
        MOV32     @_Ia_medido,R0H
	.line	4
;----------------------------------------------------------------------
;   8 | Ib_medido = (float) AdcMirror.ADCRESULT1;                              
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+1
        UI16TOF32 R0H,@_AdcMirror+1     ; |8| 
        MOVW      DP,#_Ib_medido
        MOV32     @_Ib_medido,R0H
	.line	5
;----------------------------------------------------------------------
;   9 | Id_medido = (float) AdcMirror.ADCRESULT2;                              
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+2
        UI16TOF32 R0H,@_AdcMirror+2     ; |9| 
        MOVW      DP,#_Id_medido
        MOV32     @_Id_medido,R0H
	.line	6
;----------------------------------------------------------------------
;  10 | Ie_medido = (float) AdcMirror.ADCRESULT3;                              
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+3
        UI16TOF32 R0H,@_AdcMirror+3     ; |10| 
        MOVW      DP,#_Ie_medido
        MOV32     @_Ie_medido,R0H
	.line	9
;----------------------------------------------------------------------
;  13 | Ia_medido = (Ia_medido-CONST_OFFSET_IA)*CONST_GAINFACT_IA;             
;----------------------------------------------------------------------
        MOVW      DP,#_Ia_medido
        MOVIZ     R1H,#15446            ; |13| 
        MOVIZ     R0H,#17667            ; |13| 
        MOVXI     R0H,#40168            ; |13| 
        MOV32     R2H,@_Ia_medido
        SUBF32    R0H,R2H,R0H           ; |13| 
        MOVXI     R1H,#47756            ; |13| 
        MPYF32    R0H,R1H,R0H           ; |13| 
        NOP
        MOV32     @_Ia_medido,R0H
	.line	10
;----------------------------------------------------------------------
;  14 | Ib_medido = (Ib_medido-CONST_OFFSET_IB)*CONST_GAINFACT_IB;             
;----------------------------------------------------------------------
        MOVW      DP,#_Ib_medido
        MOVIZ     R1H,#15449            ; |14| 
        MOVIZ     R0H,#17666            ; |14| 
        MOVXI     R0H,#15671            ; |14| 
        MOV32     R2H,@_Ib_medido
        SUBF32    R0H,R2H,R0H           ; |14| 
        MOVXI     R1H,#37979            ; |14| 
        MPYF32    R0H,R1H,R0H           ; |14| 
        NOP
        MOV32     @_Ib_medido,R0H
	.line	11
;----------------------------------------------------------------------
;  15 | Id_medido = (Id_medido-CONST_OFFSET_ID)*CONST_GAINFACT_ID;             
;----------------------------------------------------------------------
        MOVW      DP,#_Id_medido
        MOVIZ     R1H,#15447            ; |15| 
        MOVIZ     R0H,#17667            ; |15| 
        MOVXI     R0H,#13831            ; |15| 
        MOV32     R2H,@_Id_medido
        SUBF32    R0H,R2H,R0H           ; |15| 
        MOVXI     R1H,#2621             ; |15| 
        MPYF32    R0H,R1H,R0H           ; |15| 
        NOP
        MOV32     @_Id_medido,R0H
	.line	12
;----------------------------------------------------------------------
;  16 | Ie_medido = (Ie_medido-CONST_OFFSET_IE)*CONST_GAINFACT_IE;             
;  18 | // Reinitialize for next ADC sequence                                  
;----------------------------------------------------------------------
        MOVW      DP,#_Ie_medido
        MOVIZ     R1H,#15450            ; |16| 
        MOVIZ     R0H,#17666            ; |16| 
        MOVXI     R0H,#19297            ; |16| 
        MOV32     R2H,@_Ie_medido
        SUBF32    R0H,R2H,R0H           ; |16| 
        MOVXI     R1H,#1434             ; |16| 
        MPYF32    R0H,R1H,R0H           ; |16| 
        NOP
        MOV32     @_Ie_medido,R0H
	.line	16
;----------------------------------------------------------------------
;  20 | AdcRegs.ADCTRL2.bit.RST_SEQ1   = 1;            // Reset SEQ1
;     |                                                                        
;----------------------------------------------------------------------
        MOVW      DP,#_AdcRegs+1
        AND       AL,@_AdcRegs+1,#0xbfff ; |20| 
        OR        AL,#0x4000            ; |20| 
        MOV       @_AdcRegs+1,AL        ; |20| 
	.line	17
;----------------------------------------------------------------------
;  21 | AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;            // Clear INT SEQ1 bit   
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+25,#0xffef ; |21| 
        ORB       AL,#0x10              ; |21| 
        MOV       @_AdcRegs+25,AL       ; |21| 
	.line	18
;----------------------------------------------------------------------
;  22 | PieCtrlRegs.PIEACK.bit.ACK1    = 0;            // Clear the PIEACK of G
;     | roup 1 for enables Interrupt Resquest at CPU Level                     
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1
        AND       @_PieCtrlRegs+1,#0xfffe ; |22| 
	.line	20
;----------------------------------------------------------------------
;  24 | } // interrupt void PTC5Fadc_isr()                                     
;----------------------------------------------------------------------
        MOV32     R3H,*--SP
        MOV32     R2H,*--SP
        MOV32     R1H,*--SP
        MOV32     R0H,*--SP
        MOV32     STF,*--SP
        MOVL      XT,*--SP
        MOVL      XAR7,*--SP
        MOVL      XAR6,*--SP
        MOVL      XAR5,*--SP
        POP       RB
        NASP
        IRET
        ; return occurs
	.endfunc	24,000000001h,20
	.sect	".text"
	.global	_PTC5Fadc_start
	.sym	_PTC5Fadc_start,_PTC5Fadc_start, 32, 2, 0
	.func	27

;***************************************************************
;* FNAME: _PTC5Fadc_start               FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fadc_start:
	.line	1
;----------------------------------------------------------------------
;  27 | void PTC5Fadc_start(){                                    // ADC Module
;     |  Configuration                                                         
;  29 | volatile Uint32 adcnt;                                                 
;----------------------------------------------------------------------
	.sym	_adcnt,-2, 15, 1, 32
        ADDB      SP,#2
	.line	5
;----------------------------------------------------------------------
;  31 | EALLOW;                                                // Enable CPU wr
;     | iting to EALLOW protected registers                                    
;----------------------------------------------------------------------
 EALLOW
	.line	6
;----------------------------------------------------------------------
;  32 | SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;                  // Enable ADC cl
;     | ock                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       AL,@_SysCtrlRegs+12,#0xfff7 ; |32| 
        ORB       AL,#0x08              ; |32| 
        MOV       @_SysCtrlRegs+12,AL   ; |32| 
	.line	7
;----------------------------------------------------------------------
;  33 | EDIS;                                                  // Disable CPU w
;     | riting to EALLOW protected registers                                   
;----------------------------------------------------------------------
 EDIS
	.line	9
;----------------------------------------------------------------------
;  35 | AdcRegs.ADCTRL3.bit.ADCBGRFDN = 3;                     // Power up band
;     | gap/reference                                                          
;----------------------------------------------------------------------
        MOVW      DP,#_AdcRegs+24
        AND       AL,@_AdcRegs+24,#0xff3f ; |35| 
        ORB       AL,#0xc0              ; |35| 
        MOV       @_AdcRegs+24,AL       ; |35| 
	.line	10
;----------------------------------------------------------------------
;  36 | AdcRegs.ADCTRL3.bit.ADCPWDN   = 1;
;     |                 //ADC power up                                         
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+24,#0xffdf ; |36| 
        ORB       AL,#0x20              ; |36| 
        MOV       @_AdcRegs+24,AL       ; |36| 
	.line	12
;----------------------------------------------------------------------
;  38 | AdcRegs.ADCTRL1.bit.ACQ_PS = 0x1;                      // S/H width in
;     | ADC module periods = 16 ADC clocks                                     
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs,#0xf0ff  ; |38| 
        OR        AL,#0x0100            ; |38| 
        MOV       @_AdcRegs,AL          ; |38| 
	.line	13
;----------------------------------------------------------------------
;  39 | AdcRegs.ADCTRL1.bit.CONT_RUN = 0;                      // Start-Stop Mo
;     | de                                                                     
;----------------------------------------------------------------------
        AND       @_AdcRegs,#0xffbf     ; |39| 
	.line	14
;----------------------------------------------------------------------
;  40 | AdcRegs.ADCTRL1.bit.SEQ_CASC = 1;                      // Cascaded Mode
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs,#0xffef  ; |40| 
        ORB       AL,#0x10              ; |40| 
        MOV       @_AdcRegs,AL          ; |40| 
	.line	16
;----------------------------------------------------------------------
;  42 | AdcRegs.ADCTRL3.bit.SMODE_SEL = 0;
;     |                // Sampling mode --> SEQUENTIAL                         
;----------------------------------------------------------------------
        AND       @_AdcRegs+24,#0xfffe  ; |42| 
	.line	17
;----------------------------------------------------------------------
;  43 | AdcRegs.ADCTRL1.bit.CPS       = 0;                     // CPS=1; ADCLK=
;     | HSPCLK/(ADCLKPS*CPS)                                                   
;----------------------------------------------------------------------
        AND       @_AdcRegs,#0xff7f     ; |43| 
	.line	18
;----------------------------------------------------------------------
;  44 | AdcRegs.ADCTRL3.bit.ADCCLKPS  = 3;                     // ADC module cl
;     | ock = HSPCLK/(1*8)   = 150MHz/(1*8) = 18.75MHz                         
;  45 | 
;     |                                         // aprox 0.853 us por conversio
;     | n                                                                      
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+24,#0xffe1 ; |44| 
        ORB       AL,#0x06              ; |44| 
        MOV       @_AdcRegs+24,AL       ; |44| 
	.line	20
;----------------------------------------------------------------------
;  46 | AdcRegs.ADCMAXCONV.all = 4;                            // Setup the num
;     | ber of conv's on SEQ1                                                  
;  48 | //  Initialize ADC Input Channel Select Sequencing Control Register:   
;  49 |      //  Placa gris                                                    
;----------------------------------------------------------------------
        MOVB      @_AdcRegs+2,#4,UNC    ; |46| 
	.line	24
;----------------------------------------------------------------------
;  50 | AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0;                   // Setup the 1st
;     |  SEQ1 conv.                                                            
;----------------------------------------------------------------------
        AND       @_AdcRegs+3,#0xfff0   ; |50| 
	.line	25
;----------------------------------------------------------------------
;  51 | AdcRegs.ADCCHSELSEQ1.bit.CONV01 = 2;                   // Setup the 2st
;     |  SEQ1 conv.                                                            
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+3,#0xff0f ; |51| 
        ORB       AL,#0x20              ; |51| 
        MOV       @_AdcRegs+3,AL        ; |51| 
	.line	26
;----------------------------------------------------------------------
;  52 | AdcRegs.ADCCHSELSEQ1.bit.CONV02 = 4;                   // Setup the 3st
;     |  SEQ1 conv.                                                            
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+3,#0xf0ff ; |52| 
        OR        AL,#0x0400            ; |52| 
        MOV       @_AdcRegs+3,AL        ; |52| 
	.line	27
;----------------------------------------------------------------------
;  53 | AdcRegs.ADCCHSELSEQ1.bit.CONV03 = 6;                   // Setup the 4st
;     |  SEQ1 conv.                                                            
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+3,#0x0fff ; |53| 
        OR        AL,#0x6000            ; |53| 
        MOV       @_AdcRegs+3,AL        ; |53| 
	.line	28
;----------------------------------------------------------------------
;  54 | AdcRegs.ADCCHSELSEQ2.bit.CONV04 = 1;                   // Setup the 5st
;     |  SEQ2 conv. (DC-LINK)                                                  
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+4,#0xfff0 ; |54| 
        ORB       AL,#0x01              ; |54| 
        MOV       @_AdcRegs+4,AL        ; |54| 
	.line	30
;----------------------------------------------------------------------
;  56 | AdcRegs.ADCTRL2.bit.EPWM_SOCA_SEQ1 = 1;                // Enable SOCA f
;     | rom ePWM to start SEQ1                                                 
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+1,#0xfeff ; |56| 
        OR        AL,#0x0100            ; |56| 
        MOV       @_AdcRegs+1,AL        ; |56| 
	.line	31
;----------------------------------------------------------------------
;  57 | AdcRegs.ADCTRL2.bit.INT_MOD_SEQ1   = 0;                // SEQ1 Interrup
;     | t mode at every end of SEQ1                                            
;----------------------------------------------------------------------
        AND       @_AdcRegs+1,#0xfbff   ; |57| 
	.line	32
;----------------------------------------------------------------------
;  58 | AdcRegs.ADCTRL2.bit.RST_SEQ1              = 1;                // Reset
;     | SEQ1                                                                   
;  61 | //  Start ADC with ePWM1 timer 1 event:                                
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+1,#0xbfff ; |58| 
        OR        AL,#0x4000            ; |58| 
        MOV       @_AdcRegs+1,AL        ; |58| 
	.line	36
;----------------------------------------------------------------------
;  62 | EPwm1Regs.ETSEL.bit.SOCAEN  = ET_ETSEL_ENABLE;         // Enable SOC on
;     |  A group                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xf7ff ; |62| 
        OR        AL,#0x0800            ; |62| 
        MOV       @_EPwm1Regs+25,AL     ; |62| 
	.line	37
;----------------------------------------------------------------------
;  63 | EPwm1Regs.ETSEL.bit.SOCASEL = ET_CTR_PRD;              // Generate SOC
;     | when time-base counter equal to period.                                
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs+25,#0xf8ff ; |63| 
        OR        AL,#0x0200            ; |63| 
        MOV       @_EPwm1Regs+25,AL     ; |63| 
	.line	38
;----------------------------------------------------------------------
;  64 | EPwm1Regs.ETPS.bit.SOCAPRD  = ET_1ST;                  // Generate puls
;     | e on 1st event                                                         
;  66 | //  start PWM generation                                               
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs+26,#0xfcff ; |64| 
        OR        AL,#0x0100            ; |64| 
        MOV       @_EPwm1Regs+26,AL     ; |64| 
	.line	41
;----------------------------------------------------------------------
;  67 | EALLOW;                                                // Enable writin
;     | g to EALLOW protected registers                                        
;----------------------------------------------------------------------
 EALLOW
	.line	42
;----------------------------------------------------------------------
;  68 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;                 // Start all the
;     |  timers synced                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       AL,@_SysCtrlRegs+12,#0xfffb ; |68| 
        ORB       AL,#0x04              ; |68| 
        MOV       @_SysCtrlRegs+12,AL   ; |68| 
	.line	43
;----------------------------------------------------------------------
;  69 | EDIS;                                                  // Disable writi
;     | ng to EALLOW protected areas                                           
;  72 | //  Delay routine 5ms                                                  
;----------------------------------------------------------------------
 EDIS
	.line	47
;----------------------------------------------------------------------
;  73 | for(adcnt=0;adcnt<76000;adcnt++);                                      
;----------------------------------------------------------------------
        MOVB      XAR6,#0
        MOVL      XAR4,#76000           ; |73| 
        MOVL      *-SP[2],XAR6          ; |73| 
        MOVL      ACC,XAR4              ; |73| 
        CMPL      ACC,*-SP[2]           ; |73| 
        B         $C$L12,LOS            ; |73| 
        ; branchcc occurs ; |73| 
$C$L11:    
        MOVB      ACC,#1
        ADDL      ACC,*-SP[2]           ; |73| 
        MOVL      *-SP[2],ACC           ; |73| 
        MOVL      ACC,XAR4              ; |73| 
        CMPL      ACC,*-SP[2]           ; |73| 
        B         $C$L11,HI             ; |73| 
        ; branchcc occurs ; |73| 
$C$L12:    
	.line	49
;----------------------------------------------------------------------
;  75 | AdcRegs.ADCTRL2.bit.RST_SEQ1   = 1;            // Reset SEQ1           
;----------------------------------------------------------------------
        MOVW      DP,#_AdcRegs+1
        AND       AL,@_AdcRegs+1,#0xbfff ; |75| 
        OR        AL,#0x4000            ; |75| 
        MOV       @_AdcRegs+1,AL        ; |75| 
	.line	51
;----------------------------------------------------------------------
;  77 | AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1    = 0;                // Disable SEQ1
;     |  interrupt (every EOS)                                                 
;----------------------------------------------------------------------
        AND       @_AdcRegs+1,#0xf7ff   ; |77| 
	.line	53
;----------------------------------------------------------------------
;  79 | }//void PTC5Fadc_start()                                               
;----------------------------------------------------------------------
        SPM       #0
        SUBB      SP,#2
        LRETR
        ; return occurs
	.endfunc	79,000000000h,2
	.sect	".text"
	.global	_PTC5Fcputmr0_isr
	.file	"C:\CCStudio_v3.3\MyProjects\DAVID_ADAPT_VEL\PTC5F_tmr0.c"
	.sym	_PTC5Fcputmr0_isr,_PTC5Fcputmr0_isr, 32, 2, 0
	.func	6

;***************************************************************
;* FNAME: _PTC5Fcputmr0_isr             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  4 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fcputmr0_isr:
	.line	1
;----------------------------------------------------------------------
;   6 | interrupt void PTC5Fcputmr0_isr(){                                     
;----------------------------------------------------------------------
        ASP
        PUSH      RB
        MOV32     *SP++,STF
        SETFLG    RNDF32=1, RNDF64=1
        CLRC      PAGE0,OVM
        CLRC      AMODE
	.line	3
;----------------------------------------------------------------------
;   8 | CpuTimer0Regs.TCR.bit.TIF   = 1;
;     | // Reset the Cpu-Timer0 Interrupt Flag                                 
;----------------------------------------------------------------------
        MOVW      DP,#_CpuTimer0Regs+4
        AND       AL,@_CpuTimer0Regs+4,#0x7fff ; |8| 
        OR        AL,#0x8000            ; |8| 
        MOV       @_CpuTimer0Regs+4,AL  ; |8| 
	.line	4
;----------------------------------------------------------------------
;   9 | PieCtrlRegs.PIEACK.bit.ACK1 = 1;                                   // C
;     | lear the PIEACK of Group 1 for new Interrupt Resquest                  
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1
        AND       AL,@_PieCtrlRegs+1,#0xfffe ; |9| 
        ORB       AL,#0x01              ; |9| 
        MOV       @_PieCtrlRegs+1,AL    ; |9| 
	.line	6
;----------------------------------------------------------------------
;  11 | }// void PTC5Fcputmr0_isr()                                            
;----------------------------------------------------------------------
        MOV32     STF,*--SP
        POP       RB
        NASP
        IRET
        ; return occurs
	.endfunc	11,000000000h,4
	.sect	".text"
	.global	_PTC5Fcputmr0_start
	.sym	_PTC5Fcputmr0_start,_PTC5Fcputmr0_start, 32, 2, 0
	.func	15

;***************************************************************
;* FNAME: _PTC5Fcputmr0_start           FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fcputmr0_start:
	.line	1
;----------------------------------------------------------------------
;  15 | void PTC5Fcputmr0_start(){                                             
;----------------------------------------------------------------------
	.line	3
;----------------------------------------------------------------------
;  17 | CpuTimer0Regs.PRD.all = FRECUENCIA_SYSCLKOUT/FCPUTIMER0_HZ;
;     |   // Timer Period                                                      
;----------------------------------------------------------------------
        MOV       PH,#228
        MOV       PL,#57792
        MOVW      DP,#_CpuTimer0Regs+2
        MOVL      @_CpuTimer0Regs+2,P   ; |17| 
	.line	5
;----------------------------------------------------------------------
;  19 | CpuTimer0Regs.TCR.bit.FREE=1;
;     |      // CPU-TIMER0 free run                                            
;----------------------------------------------------------------------
        AND       AL,@_CpuTimer0Regs+4,#0xf7ff ; |19| 
        OR        AL,#0x0800            ; |19| 
        MOV       @_CpuTimer0Regs+4,AL  ; |19| 
	.line	7
;----------------------------------------------------------------------
;  21 | EALLOW;
;     |                                              // Enable CPU writing to p
;     | rotected registers                                                     
;----------------------------------------------------------------------
 EALLOW
	.line	8
;----------------------------------------------------------------------
;  22 | PieVectTable.TINT0=&PTC5Fcputmr0_isr;                       // Define t
;     | he CPU-TIMER0 ISR address                                              
;----------------------------------------------------------------------
        MOVL      XAR4,#_PTC5Fcputmr0_isr ; |22| 
        MOVW      DP,#_PieVectTable+76
        MOVL      @_PieVectTable+76,XAR4 ; |22| 
	.line	9
;----------------------------------------------------------------------
;  23 | EDIS;
;     |                                              // Disable CPU writing to 
;     | protected registers                                                    
;----------------------------------------------------------------------
 EDIS
	.line	11
;----------------------------------------------------------------------
;  25 | EALLOW;                                        // Enable CPU writing to
;     |  protected registers                                                   
;----------------------------------------------------------------------
 EALLOW
	.line	12
;----------------------------------------------------------------------
;  26 | SysCtrlRegs.PCLKCR3.bit.CPUTIMER0ENCLK = 1;    // Enable the SYSCLKOUT
;     | to the CPU-TIMER0                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+16
        AND       AL,@_SysCtrlRegs+16,#0xfeff ; |26| 
        OR        AL,#0x0100            ; |26| 
        MOV       @_SysCtrlRegs+16,AL   ; |26| 
	.line	13
;----------------------------------------------------------------------
;  27 | EDIS;                                          // Disable CPU writing t
;     | o protected registers                                                  
;----------------------------------------------------------------------
 EDIS
	.line	15
;----------------------------------------------------------------------
;  29 | IER |=M_INT1;
;     |                                      // Enable de CPU-TIMER0 CPU-PIEIER
;     | 1 for INT1 (Group 1)                                                   
;----------------------------------------------------------------------
        OR        IER,#0x0001           ; |29| 
	.line	16
;----------------------------------------------------------------------
;  30 | PieCtrlRegs.PIEIER1.bit.INTx7 = 1;                                  //
;     | Enable the CPU-TIMER0 PIEIER1.7 to interrupt resquest sent to CPU Level
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+2
        AND       AL,@_PieCtrlRegs+2,#0xffbf ; |30| 
        ORB       AL,#0x40              ; |30| 
        MOV       @_PieCtrlRegs+2,AL    ; |30| 
	.line	17
;----------------------------------------------------------------------
;  31 | PieCtrlRegs.PIEACK.bit.ACK1  = 0;                                   //
;     | Clear the PIEACK of Group 1 for enables Interrupt Resquest at CPU Level
;----------------------------------------------------------------------
        AND       @_PieCtrlRegs+1,#0xfffe ; |31| 
	.line	18
;----------------------------------------------------------------------
;  32 | CpuTimer0Regs.TCR.bit.TIE   = 1;                                    //
;     | CPU-TIMER0 Interruption Enable                                         
;----------------------------------------------------------------------
        MOVW      DP,#_CpuTimer0Regs+4
        AND       AL,@_CpuTimer0Regs+4,#0xbfff ; |32| 
        OR        AL,#0x4000            ; |32| 
        MOV       @_CpuTimer0Regs+4,AL  ; |32| 
	.line	20
;----------------------------------------------------------------------
;  34 | }// PTC5Fcputmr0_start()                                               
;----------------------------------------------------------------------
        SPM       #0
        LRETR
        ; return occurs
	.endfunc	34,000000000h,0
	.sect	".text"
	.global	_main
	.file	"C:\CCStudio_v3.3\MyProjects\DAVID_ADAPT_VEL\PTC5F_main.c"
	.sym	_main,_main, 32, 2, 0
	.func	41

;***************************************************************
;* FNAME: _main                         FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto, 12 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_main:
	.line	1
;----------------------------------------------------------------------
;  41 | void main (){                                                          
;  43 | int sv;                                                                
;----------------------------------------------------------------------
;* AR6   assigned to _sv
	.sym	_sv,16, 4, 4, 16
;* AR2   assigned to _fds
	.sym	_fds,8, 4, 4, 16
;* AR1   assigned to _sopt
	.sym	_sopt,6, 4, 4, 16
        MOVL      *SP++,XAR1
        MOVL      *SP++,XAR2
        MOV32     *SP++,R4H
        MOV32     *SP++,R5H
        MOV32     *SP++,R6H
        MOV32     *SP++,R7H
	.line	4
;----------------------------------------------------------------------
;  44 | int fds = 1;                                                           
;  45 | int sopt, sapr;                                                        
;  46 | int cuadr, fila;                                                       
;----------------------------------------------------------------------
        MOVB      XAR2,#1               ; |44| 
	.line	8
;----------------------------------------------------------------------
;  48 | EALLOW;                                      // Enable writing to EALLO
;     | W protected registers                                                  
;----------------------------------------------------------------------
 EALLOW
	.line	10
;----------------------------------------------------------------------
;  50 | SysCtrlRegs.HISPCP.bit.HSPCLK       = 0;     // HSPCLK = SYSCLKOUT / 1
;     | = 150MHz                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+10
        AND       @_SysCtrlRegs+10,#0xfff8 ; |50| 
	.line	11
;----------------------------------------------------------------------
;  51 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 0;     // Stop all the TB clocks 
;----------------------------------------------------------------------
        AND       @_SysCtrlRegs+12,#0xfffb ; |51| 
	.line	12
;----------------------------------------------------------------------
;  52 | SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK = 1;     // Enable the SYSCLKOUT to
;     |  the GPIO                                                              
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+16,#0xdfff ; |52| 
        OR        AL,#0x2000            ; |52| 
        MOV       @_SysCtrlRegs+16,AL   ; |52| 
	.line	14
;----------------------------------------------------------------------
;  54 | GpioCtrlRegs.GPBMUX2.bit.GPIO54            = 0;          // Configure G
;     | PIO54 (JP3-PIN 19) as digital I/O                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       @_GpioCtrlRegs+24,#0xcfff ; |54| 
	.line	15
;----------------------------------------------------------------------
;  55 | GpioCtrlRegs.GPBDIR.bit.GPIO54            = 1;          // Configure GP
;     | IO54 as digital Output (JP3-PIN 19)                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xffbf ; |55| 
        ORB       AL,#0x40              ; |55| 
        MOV       @_GpioCtrlRegs+27,AL  ; |55| 
	.line	16
;----------------------------------------------------------------------
;  56 | GpioDataRegs.GPBCLEAR.bit.GPIO54          = 1;          // CLEAR the TR
;     | IGGER SIGNAL (JP3-PIN 19)                                              
;  59 | // LED_1 Configuration                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xffbf ; |56| 
        ORB       AL,#0x40              ; |56| 
        MOV       @_GpioDataRegs+13,AL  ; |56| 
	.line	20
;----------------------------------------------------------------------
;  60 | GpioCtrlRegs.GPAMUX2.bit.GPIO16   = 0;       // Configure JP4#16(GPIO 1
;     | 6) as digital I/O                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+8
        AND       @_GpioCtrlRegs+8,#0xfffc ; |60| 
	.line	21
;----------------------------------------------------------------------
;  61 | GpioCtrlRegs.GPADIR.bit.GPIO16    = 1;       //Configure JP4#16(GPIO 16
;     | ) as digital Output                                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+11,#0xfffe ; |61| 
        ORB       AL,#0x01              ; |61| 
        MOV       @_GpioCtrlRegs+11,AL  ; |61| 
	.line	22
;----------------------------------------------------------------------
;  62 | GpioDataRegs.GPASET.bit.GPIO16      = 1;       // Turn-off LED_1       
;  63 | // End LED_1 Configuration                                             
;  65 | //salidas que controla rele (KSM)                                      
;  66 | // Configuracion de J3-9                                               
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+3
        AND       AL,@_GpioDataRegs+3,#0xfffe ; |62| 
        ORB       AL,#0x01              ; |62| 
        MOV       @_GpioDataRegs+3,AL   ; |62| 
	.line	27
;----------------------------------------------------------------------
;  67 | GpioCtrlRegs.GPBMUX2.bit.GPIO48     = 0;   // Configure GPIO48 as digit
;     | al I/O                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       @_GpioCtrlRegs+24,#0xfffc ; |67| 
	.line	28
;----------------------------------------------------------------------
;  68 | GpioCtrlRegs.GPBDIR.bit.GPIO48     = 1;          //Configure shared pin
;     | s as digital Output (GPIO 48);                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xfffe ; |68| 
        ORB       AL,#0x01              ; |68| 
        MOV       @_GpioCtrlRegs+27,AL  ; |68| 
	.line	29
;----------------------------------------------------------------------
;  69 | GpioDataRegs.GPBCLEAR.bit.GPIO48     = 1;          //Enciende los venti
;     | ladores                                                                
;  71 | //salidas que controla rele (KP)                                       
;  72 | // Configuracion de J3-10                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xfffe ; |69| 
        ORB       AL,#0x01              ; |69| 
        MOV       @_GpioDataRegs+13,AL  ; |69| 
	.line	33
;----------------------------------------------------------------------
;  73 | GpioCtrlRegs.GPBMUX2.bit.GPIO49     = 0;   // Configure GPIO49 as digit
;     | al I/O                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       @_GpioCtrlRegs+24,#0xfff3 ; |73| 
	.line	34
;----------------------------------------------------------------------
;  74 | GpioCtrlRegs.GPBDIR.bit.GPIO49     = 1;          //Configure shared pin
;     | s as digital Output (GPIO 49);                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xfffd ; |74| 
        ORB       AL,#0x02              ; |74| 
        MOV       @_GpioCtrlRegs+27,AL  ; |74| 
	.line	35
;----------------------------------------------------------------------
;  75 | GpioDataRegs.GPBCLEAR.bit.GPIO49     = 1;          //habilita KP       
;  77 | // LED_2 Configuration                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xfffd ; |75| 
        ORB       AL,#0x02              ; |75| 
        MOV       @_GpioDataRegs+13,AL  ; |75| 
	.line	38
;----------------------------------------------------------------------
;  78 | GpioCtrlRegs.GPAMUX2.bit.GPIO17   = 0;       // Configure JP4#18(GPIO 1
;     | 7) as digital I/O                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+8
        AND       @_GpioCtrlRegs+8,#0xfff3 ; |78| 
	.line	39
;----------------------------------------------------------------------
;  79 | GpioCtrlRegs.GPADIR.bit.GPIO17    = 1;       //Configure JP4#18(GPIO 17
;     | ) as digital Output                                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+11,#0xfffd ; |79| 
        ORB       AL,#0x02              ; |79| 
        MOV       @_GpioCtrlRegs+11,AL  ; |79| 
	.line	40
;----------------------------------------------------------------------
;  80 | GpioDataRegs.GPASET.bit.GPIO17      = 1;       // Turn-off LED_2       
;  81 | // End LED_1 Configuration                                             
;  83 | //############################### SA_UP & SA_DOWN #####################
;     | #####################                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+3
        AND       AL,@_GpioDataRegs+3,#0xfffd ; |80| 
        ORB       AL,#0x02              ; |80| 
        MOV       @_GpioDataRegs+3,AL   ; |80| 
	.line	45
;----------------------------------------------------------------------
;  85 | GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 0;       // Configure GPIO0 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfffc ; |85| 
	.line	46
;----------------------------------------------------------------------
;  86 | GpioCtrlRegs.GPADIR.bit.GPIO0         = 1;       // Configure GPIO0 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffe ; |86| 
        ORB       AL,#0x01              ; |86| 
        MOV       @_GpioCtrlRegs+10,AL  ; |86| 
	.line	47
;----------------------------------------------------------------------
;  87 | GpioDataRegs.GPACLEAR.bit.GPIO0          = 1;       // Clear GPIO0     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffe ; |87| 
        ORB       AL,#0x01              ; |87| 
        MOV       @_GpioDataRegs+4,AL   ; |87| 
	.line	49
;----------------------------------------------------------------------
;  89 | GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 0;       // Configure GPIO1 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfff3 ; |89| 
	.line	50
;----------------------------------------------------------------------
;  90 | GpioCtrlRegs.GPADIR.bit.GPIO1         = 1;       // Configure GPIO1 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffd ; |90| 
        ORB       AL,#0x02              ; |90| 
        MOV       @_GpioCtrlRegs+10,AL  ; |90| 
	.line	51
;----------------------------------------------------------------------
;  91 | GpioDataRegs.GPACLEAR.bit.GPIO1          = 1;       // Clear GPIO1     
;  93 | //####################################### SB_UP & SB_DOWN #############
;     | #######################                                                
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffd ; |91| 
        ORB       AL,#0x02              ; |91| 
        MOV       @_GpioDataRegs+4,AL   ; |91| 
	.line	55
;----------------------------------------------------------------------
;  95 | GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 0;       // Configure GPIO2 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xffcf ; |95| 
	.line	56
;----------------------------------------------------------------------
;  96 | GpioCtrlRegs.GPADIR.bit.GPIO2         = 1;       // Configure GPIO2 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffb ; |96| 
        ORB       AL,#0x04              ; |96| 
        MOV       @_GpioCtrlRegs+10,AL  ; |96| 
	.line	57
;----------------------------------------------------------------------
;  97 | GpioDataRegs.GPACLEAR.bit.GPIO2          = 1;       // Clear GPIO2     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffb ; |97| 
        ORB       AL,#0x04              ; |97| 
        MOV       @_GpioDataRegs+4,AL   ; |97| 
	.line	59
;----------------------------------------------------------------------
;  99 | GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 0;       // Configure GPIO3 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xff3f ; |99| 
	.line	60
;----------------------------------------------------------------------
; 100 | GpioCtrlRegs.GPADIR.bit.GPIO3         = 1;       // Configure GPIO3 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfff7 ; |100| 
        ORB       AL,#0x08              ; |100| 
        MOV       @_GpioCtrlRegs+10,AL  ; |100| 
	.line	61
;----------------------------------------------------------------------
; 101 | GpioDataRegs.GPACLEAR.bit.GPIO3          = 1;       // Clear GPIO3     
; 103 | //##################################### SC_UP & SC_DOWN ###############
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfff7 ; |101| 
        ORB       AL,#0x08              ; |101| 
        MOV       @_GpioDataRegs+4,AL   ; |101| 
	.line	65
;----------------------------------------------------------------------
; 105 | GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 0;       // Configure GPIO4 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfcff ; |105| 
	.line	66
;----------------------------------------------------------------------
; 106 | GpioCtrlRegs.GPADIR.bit.GPIO4         = 1;       // Configure GPIO4 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffef ; |106| 
        ORB       AL,#0x10              ; |106| 
        MOV       @_GpioCtrlRegs+10,AL  ; |106| 
	.line	67
;----------------------------------------------------------------------
; 107 | GpioDataRegs.GPACLEAR.bit.GPIO4          = 1;       // Clear GPIO4     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffef ; |107| 
        ORB       AL,#0x10              ; |107| 
        MOV       @_GpioDataRegs+4,AL   ; |107| 
	.line	69
;----------------------------------------------------------------------
; 109 | GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 0;       // Configure GPIO5 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xf3ff ; |109| 
	.line	70
;----------------------------------------------------------------------
; 110 | GpioCtrlRegs.GPADIR.bit.GPIO5         = 1;       // Configure GPIO5 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffdf ; |110| 
        ORB       AL,#0x20              ; |110| 
        MOV       @_GpioCtrlRegs+10,AL  ; |110| 
	.line	71
;----------------------------------------------------------------------
; 111 | GpioDataRegs.GPACLEAR.bit.GPIO5          = 1;       // Clear GPIO5     
; 113 | //################################### SD_UP & SD_DOWN #################
;     | #####################                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffdf ; |111| 
        ORB       AL,#0x20              ; |111| 
        MOV       @_GpioDataRegs+4,AL   ; |111| 
	.line	75
;----------------------------------------------------------------------
; 115 | GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 0;       // Configure GPIO6 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xcfff ; |115| 
	.line	76
;----------------------------------------------------------------------
; 116 | GpioCtrlRegs.GPADIR.bit.GPIO6         = 1;       // Configure GPIO6 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffbf ; |116| 
        ORB       AL,#0x40              ; |116| 
        MOV       @_GpioCtrlRegs+10,AL  ; |116| 
	.line	77
;----------------------------------------------------------------------
; 117 | GpioDataRegs.GPACLEAR.bit.GPIO6          = 1;       // Clear GPIO6     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffbf ; |117| 
        ORB       AL,#0x40              ; |117| 
        MOV       @_GpioDataRegs+4,AL   ; |117| 
	.line	79
;----------------------------------------------------------------------
; 119 | GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 0;       // Configure GPIO7 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0x3fff ; |119| 
	.line	80
;----------------------------------------------------------------------
; 120 | GpioCtrlRegs.GPADIR.bit.GPIO7         = 1;       // Configure GPIO7 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xff7f ; |120| 
        ORB       AL,#0x80              ; |120| 
        MOV       @_GpioCtrlRegs+10,AL  ; |120| 
	.line	81
;----------------------------------------------------------------------
; 121 | GpioDataRegs.GPACLEAR.bit.GPIO7          = 1;       // Clear GPIO7     
; 123 | //#################################### SE_UP & SE_DOWN ################
;     | #########################                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xff7f ; |121| 
        ORB       AL,#0x80              ; |121| 
        MOV       @_GpioDataRegs+4,AL   ; |121| 
	.line	85
;----------------------------------------------------------------------
; 125 | GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 0;       // Configure GPIO8 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xfffc ; |125| 
	.line	86
;----------------------------------------------------------------------
; 126 | GpioCtrlRegs.GPADIR.bit.GPIO8         = 1;       // Configure GPIO8 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfeff ; |126| 
        OR        AL,#0x0100            ; |126| 
        MOV       @_GpioCtrlRegs+10,AL  ; |126| 
	.line	87
;----------------------------------------------------------------------
; 127 | GpioDataRegs.GPACLEAR.bit.GPIO8          = 1;       // Clear GPIO8     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfeff ; |127| 
        OR        AL,#0x0100            ; |127| 
        MOV       @_GpioDataRegs+4,AL   ; |127| 
	.line	89
;----------------------------------------------------------------------
; 129 | GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 0;       // Configure GPIO9 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xfff3 ; |129| 
	.line	90
;----------------------------------------------------------------------
; 130 | GpioCtrlRegs.GPADIR.bit.GPIO9         = 1;       // Configure GPIO9 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfdff ; |130| 
        OR        AL,#0x0200            ; |130| 
        MOV       @_GpioCtrlRegs+10,AL  ; |130| 
	.line	91
;----------------------------------------------------------------------
; 131 | GpioDataRegs.GPACLEAR.bit.GPIO9          = 1;       // Clear GPIO9     
; 133 | //#################################### SF_UP & SF_DOWN ################
;     | #########################                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfdff ; |131| 
        OR        AL,#0x0200            ; |131| 
        MOV       @_GpioDataRegs+4,AL   ; |131| 
	.line	95
;----------------------------------------------------------------------
; 135 | GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 0;       // Configure GPIO10 as
;     |  Digital I/O                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xffcf ; |135| 
	.line	96
;----------------------------------------------------------------------
; 136 | GpioCtrlRegs.GPADIR.bit.GPIO10        = 1;       // Configure GPIO10 as
;     |  digital Output                                                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfbff ; |136| 
        OR        AL,#0x0400            ; |136| 
        MOV       @_GpioCtrlRegs+10,AL  ; |136| 
	.line	97
;----------------------------------------------------------------------
; 137 | GpioDataRegs.GPACLEAR.bit.GPIO10            = 1;       // Clear GPIO10 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfbff ; |137| 
        OR        AL,#0x0400            ; |137| 
        MOV       @_GpioDataRegs+4,AL   ; |137| 
	.line	99
;----------------------------------------------------------------------
; 139 | GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 0;       // Configure GPIO11 as
;     |  Digital I/O                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xff3f ; |139| 
	.line	100
;----------------------------------------------------------------------
; 140 | GpioCtrlRegs.GPADIR.bit.GPIO11        = 1;       // Configure GPIO11 as
;     |  digital Output                                                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xf7ff ; |140| 
        OR        AL,#0x0800            ; |140| 
        MOV       @_GpioCtrlRegs+10,AL  ; |140| 
	.line	101
;----------------------------------------------------------------------
; 141 | GpioDataRegs.GPACLEAR.bit.GPIO11            = 1;       // Clear GPIO11 
; 142 | //#####################################################################
;     | ######################                                                 
; 144 | //#################################### TRIGGER ########################
;     | #################                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xf7ff ; |141| 
        OR        AL,#0x0800            ; |141| 
        MOV       @_GpioDataRegs+4,AL   ; |141| 
	.line	105
;----------------------------------------------------------------------
; 145 | GpioCtrlRegs.GPBMUX2.bit.GPIO55            = 0;          // Configure G
;     | PIO54 (JP3-PIN 20) as digital I/O                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       @_GpioCtrlRegs+24,#0x3fff ; |145| 
	.line	106
;----------------------------------------------------------------------
; 146 | GpioCtrlRegs.GPBDIR.bit.GPIO55            = 1;          // Configure GP
;     | IO54 as digital Output (JP3-PIN 20)                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xff7f ; |146| 
        ORB       AL,#0x80              ; |146| 
        MOV       @_GpioCtrlRegs+27,AL  ; |146| 
	.line	107
;----------------------------------------------------------------------
; 147 | GpioDataRegs.GPBCLEAR.bit.GPIO55          = 1;          // CLEAR the TR
;     | IGGER SIGNAL (JP3-PIN 20)                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xff7f ; |147| 
        ORB       AL,#0x80              ; |147| 
        MOV       @_GpioDataRegs+13,AL  ; |147| 
	.line	109
;----------------------------------------------------------------------
; 149 | GpioCtrlRegs.GPBMUX2.bit.GPIO56            = 0;          // Configure G
;     | PIO54 (JP3-PIN 21) as digital I/O                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+25
        AND       @_GpioCtrlRegs+25,#0xfffc ; |149| 
	.line	110
;----------------------------------------------------------------------
; 150 | GpioCtrlRegs.GPBDIR.bit.GPIO56       = 1;          // Configure GPIO54
;     | as digital Output (JP3-PIN 21)                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xfeff ; |150| 
        OR        AL,#0x0100            ; |150| 
        MOV       @_GpioCtrlRegs+27,AL  ; |150| 
	.line	111
;----------------------------------------------------------------------
; 151 | GpioDataRegs.GPBCLEAR.bit.GPIO56          = 1;          // CLEAR the TR
;     | IGGER SIGNAL (JP3-PIN 21)                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xfeff ; |151| 
        OR        AL,#0x0100            ; |151| 
        MOV       @_GpioDataRegs+13,AL  ; |151| 
	.line	113
;----------------------------------------------------------------------
; 153 | GpioCtrlRegs.GPBMUX2.bit.GPIO57            = 0;          // Configure G
;     | PIO54 (JP3-PIN 22) as digital I/O                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+25
        AND       @_GpioCtrlRegs+25,#0xfff3 ; |153| 
	.line	114
;----------------------------------------------------------------------
; 154 | GpioCtrlRegs.GPBDIR.bit.GPIO57            = 1;          // Configure GP
;     | IO54 as digital Output (JP3-PIN 22)                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xfdff ; |154| 
        OR        AL,#0x0200            ; |154| 
        MOV       @_GpioCtrlRegs+27,AL  ; |154| 
	.line	115
;----------------------------------------------------------------------
; 155 | GpioDataRegs.GPBCLEAR.bit.GPIO57          = 1;          // CLEAR the TR
;     | IGGER SIGNAL (JP3-PIN 22)                                              
; 158 | //#####################################################################
;     | #########################                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xfdff ; |155| 
        OR        AL,#0x0200            ; |155| 
        MOV       @_GpioDataRegs+13,AL  ; |155| 
	.line	119
;----------------------------------------------------------------------
; 159 | EDIS;                                        // Disable writing to EALL
;     | OW protected registers                                                 
;----------------------------------------------------------------------
 EDIS
	.line	121
;----------------------------------------------------------------------
; 161 | Fm          = FRECUENCIA_MUESTREO_HZ;        // Sampling Frequency (Hz)
;----------------------------------------------------------------------
        MOVW      DP,#_Fm
        MOV       @_Fm,#20000           ; |161| 
	.line	122
;----------------------------------------------------------------------
; 162 | Tm          = 1/(float)Fm;                   // Sampling Period (s)    
;----------------------------------------------------------------------
        SPM       #0
        MOVIZ     R0H,#16256            ; |162| 
        UI16TOF32 R1H,@_Fm              ; |162| 
        LCR       #FS$$DIV              ; |162| 
        ; call occurs [#FS$$DIV] ; |162| 
        MOVW      DP,#_Tm
        MOV32     @_Tm,R0H
	.line	123
;----------------------------------------------------------------------
; 163 | pwm_period  = 37500000 / (2*Fm);             // timer de micro a 37.5 M
;     | Hz y up/down counter                                                   
; 166 |  // ###################################################################
;     | ##############                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_Fm
        MOV       ACC,@_Fm << #1        ; |163| 
        MOVZ      AR6,AL
        MOV       AL,#13408
        MOV       AH,#572
        MOVL      P,ACC                 ; |163| 
        MOVB      ACC,#0
        RPT       #31
||     SUBCUL    ACC,XAR6              ; |163| 
        MOV       @_pwm_period,P        ; |163| 
	.line	128
;----------------------------------------------------------------------
; 168 | Ia_mir = 0;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_Ia_mir
        ZERO      R0H                   ; |168| 
        MOV32     @_Ia_mir,R0H
	.line	129
;----------------------------------------------------------------------
; 169 | Ib_mir = 0;                                                            
;----------------------------------------------------------------------
        MOV32     @_Ib_mir,R0H
	.line	130
;----------------------------------------------------------------------
; 170 | Id_mir = 0;                                                            
;----------------------------------------------------------------------
        MOV32     @_Id_mir,R0H
	.line	131
;----------------------------------------------------------------------
; 171 | Ie_mir = 0;                                                            
;----------------------------------------------------------------------
        MOV32     @_Ie_mir,R0H
	.line	132
;----------------------------------------------------------------------
; 172 | Vdc_mir = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_Vdc_mir
        MOV32     @_Vdc_mir,R0H
	.line	133
;----------------------------------------------------------------------
; 173 | i_alpha = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_i_alpha
        MOV32     @_i_alpha,R0H
	.line	134
;----------------------------------------------------------------------
; 174 | i_beta = 0;                                                            
;----------------------------------------------------------------------
        MOV32     @_i_beta,R0H
	.line	135
;----------------------------------------------------------------------
; 175 | i_x = 0;                                                               
;----------------------------------------------------------------------
        MOV32     @_i_x,R0H
	.line	136
;----------------------------------------------------------------------
; 176 | i_y = 0;                                                               
;----------------------------------------------------------------------
        MOV32     @_i_y,R0H
	.line	137
;----------------------------------------------------------------------
; 177 | i_alpha_p1 = 0;                                                        
;----------------------------------------------------------------------
        MOVW      DP,#_i_alpha_p1
        MOV32     @_i_alpha_p1,R0H
	.line	138
;----------------------------------------------------------------------
; 178 | i_alpha_p2 = 0;                                                        
;----------------------------------------------------------------------
        MOV32     @_i_alpha_p2,R0H
	.line	139
;----------------------------------------------------------------------
; 179 | i_beta_p = 0;                                                          
;----------------------------------------------------------------------
        MOV32     @_i_beta_p,R0H
	.line	140
;----------------------------------------------------------------------
; 180 | i_x_p = 0;                                                             
;----------------------------------------------------------------------
        MOV32     @_i_x_p,R0H
	.line	141
;----------------------------------------------------------------------
; 181 | i_y_p = 0;                                                             
;----------------------------------------------------------------------
        MOV32     @_i_y_p,R0H
	.line	142
;----------------------------------------------------------------------
; 182 | i_alpha_med = 0;                                                       
;----------------------------------------------------------------------
        MOV32     @_i_alpha_med,R0H
	.line	143
;----------------------------------------------------------------------
; 183 | i_beta_med = 0;                                                        
;----------------------------------------------------------------------
        MOV32     @_i_beta_med,R0H
	.line	144
;----------------------------------------------------------------------
; 184 | i_x_med = 0;                                                           
;----------------------------------------------------------------------
        MOV32     @_i_x_med,R0H
	.line	145
;----------------------------------------------------------------------
; 185 | i_y_med = 0;                                                           
;----------------------------------------------------------------------
        MOV32     @_i_y_med,R0H
	.line	146
;----------------------------------------------------------------------
; 186 | Werror = 0;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_Werror
        MOV32     @_Werror,R0H
	.line	147
;----------------------------------------------------------------------
; 187 | e_km1 = 0;                                                             
;----------------------------------------------------------------------
        MOV32     @_e_km1,R0H
	.line	149
;----------------------------------------------------------------------
; 189 | Kinte     = (float).5*Tm;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_Tm
        MOV32     R0H,@_Tm
        MPYF32    R0H,R0H,#16128        ; |189| 
        MOVW      DP,#_Kinte
        MOV32     @_Kinte,R0H
	.line	151
;----------------------------------------------------------------------
; 191 | wm_k     = 0;                                                          
;----------------------------------------------------------------------
        ZERO      R0H                   ; |191| 
        MOV32     @_wm_k,R0H
	.line	152
;----------------------------------------------------------------------
; 192 | wm_km1 = 0;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_wm_km1
        MOV32     @_wm_km1,R0H
	.line	153
;----------------------------------------------------------------------
; 193 | wr = 0;                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_wr
        MOV32     @_wr,R0H
	.line	154
;----------------------------------------------------------------------
; 194 | inte_k = 0;                                                            
;----------------------------------------------------------------------
        MOV32     @_inte_k,R0H
	.line	155
;----------------------------------------------------------------------
; 195 | inte_km1 = 0;                                                          
;----------------------------------------------------------------------
        MOV32     @_inte_km1,R0H
	.line	157
;----------------------------------------------------------------------
; 197 | Isd = Isd_ref;                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_Isd
        MOVIZ     R0H,#16145            ; |197| 
        MOVXI     R0H,#60293            ; |197| 
        MOV32     @_Isd,R0H
	.line	158
;----------------------------------------------------------------------
; 198 | Isq = 0;                                                               
;----------------------------------------------------------------------
        ZERO      R0H                   ; |198| 
        MOV32     @_Isq,R0H
	.line	159
;----------------------------------------------------------------------
; 199 | Iy_ref = 0;                                                            
;----------------------------------------------------------------------
        MOV32     @_Iy_ref,R0H
	.line	160
;----------------------------------------------------------------------
; 200 | Ix_ref = 0;                                                            
;----------------------------------------------------------------------
        MOV32     @_Ix_ref,R0H
	.line	161
;----------------------------------------------------------------------
; 201 | Ibeta_ref = 0;                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_Ibeta_ref
        MOV32     @_Ibeta_ref,R0H
	.line	162
;----------------------------------------------------------------------
; 202 | Ialfa_ref = 0;                                                         
;----------------------------------------------------------------------
        MOV32     @_Ialfa_ref,R0H
	.line	164
;----------------------------------------------------------------------
; 204 | e_alpha = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_e_alpha
        MOV32     @_e_alpha,R0H
	.line	165
;----------------------------------------------------------------------
; 205 | e_beta = 0;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_e_beta
        MOV32     @_e_beta,R0H
	.line	166
;----------------------------------------------------------------------
; 206 | e_x = 0;                                                               
;----------------------------------------------------------------------
        MOV32     @_e_x,R0H
	.line	167
;----------------------------------------------------------------------
; 207 | e_y = 0;                                                               
;----------------------------------------------------------------------
        MOV32     @_e_y,R0H
	.line	169
;----------------------------------------------------------------------
; 209 | wsl= 0;                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_wsl
        MOV32     @_wsl,R0H
	.line	170
;----------------------------------------------------------------------
; 210 | we= 0;                                                                 
;----------------------------------------------------------------------
        MOV32     @_we,R0H
	.line	171
;----------------------------------------------------------------------
; 211 | thetae= 0;                                                             
;----------------------------------------------------------------------
        MOV32     @_thetae,R0H
	.line	172
;----------------------------------------------------------------------
; 212 | thetaenm1= 0;                                                          
;----------------------------------------------------------------------
        MOV32     @_thetaenm1,R0H
	.line	173
;----------------------------------------------------------------------
; 213 | sinthetae= 0;                                                          
;----------------------------------------------------------------------
        MOVW      DP,#_sinthetae
        MOV32     @_sinthetae,R0H
	.line	174
;----------------------------------------------------------------------
; 214 | costhetae= 1;                                                          
;----------------------------------------------------------------------
        MOVIZ     R0H,#16256            ; |214| 
        MOV32     @_costhetae,R0H
	.line	176
;----------------------------------------------------------------------
; 216 | Imax = Irated;                                                         
; 218 |  // -------------Se a�ade para mi funci�n J de coste---------      
;----------------------------------------------------------------------
        MOVIZ     R0H,#16411            ; |216| 
        MOVXI     R0H,#34079            ; |216| 
        MOV32     @_Imax,R0H
	.line	179
;----------------------------------------------------------------------
; 219 | sinthetae_p= 0;                                                        
;----------------------------------------------------------------------
        MOVW      DP,#_sinthetae_p
        ZERO      R0H                   ; |219| 
        MOV32     @_sinthetae_p,R0H
	.line	180
;----------------------------------------------------------------------
; 220 | costhetae_p= 1;                                                        
;----------------------------------------------------------------------
        MOVIZ     R0H,#16256            ; |220| 
        MOV32     @_costhetae_p,R0H
	.line	181
;----------------------------------------------------------------------
; 221 | Ibeta_ref_p = 0;                                                       
;----------------------------------------------------------------------
        ZERO      R0H                   ; |221| 
        MOV32     @_Ibeta_ref_p,R0H
	.line	182
;----------------------------------------------------------------------
; 222 | Ialfa_ref_p = 0;                                                       
;----------------------------------------------------------------------
        MOV32     @_Ialfa_ref_p,R0H
	.line	183
;----------------------------------------------------------------------
; 223 | thetae_p= 0;                                                           
; 224 | // ---------------------------------------------------------           
; 235 | // ###################################### INITALITIATION ##############
;     | #############################                                          
;----------------------------------------------------------------------
        MOV32     @_thetae_p,R0H
	.line	198
;----------------------------------------------------------------------
; 238 | Vdc          = (float)300;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_Vdc
        MOVIZ     R0H,#17302            ; |238| 
        MOV32     @_Vdc,R0H
	.line	200
;----------------------------------------------------------------------
; 240 | timeout = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_timeout
        MOV       @_timeout,#0          ; |240| 
	.line	201
;----------------------------------------------------------------------
; 241 | mstart  = 0;                                                           
; 244 | //  Start Peripherics                                                  
;----------------------------------------------------------------------
        ZERO      R0H                   ; |241| 
        MOV32     @_mstart,R0H
	.line	206
;----------------------------------------------------------------------
; 246 | PTC5Fepwm5F_start();                                                   
;----------------------------------------------------------------------
        LCR       #_PTC5Fepwm5F_start   ; |246| 
        ; call occurs [#_PTC5Fepwm5F_start] ; |246| 
	.line	207
;----------------------------------------------------------------------
; 247 | PTC5Feqep_start();                                                     
;----------------------------------------------------------------------
        LCR       #_PTC5Feqep_start     ; |247| 
        ; call occurs [#_PTC5Feqep_start] ; |247| 
	.line	208
;----------------------------------------------------------------------
; 248 | PTC5Fadc_start();                                                      
;----------------------------------------------------------------------
        LCR       #_PTC5Fadc_start      ; |248| 
        ; call occurs [#_PTC5Fadc_start] ; |248| 
	.line	209
;----------------------------------------------------------------------
; 249 | PTC5Fcputmr0_start();                                                  
; 251 | //      End Start Peripherics                                          
; 253 | // Hist�ricos                                                        
;----------------------------------------------------------------------
        LCR       #_PTC5Fcputmr0_start  ; |249| 
        ; call occurs [#_PTC5Fcputmr0_start] ; |249| 
	.line	214
;----------------------------------------------------------------------
; 254 | for (sv = 0; sv<CANTIDAD_LOG; sv++ )                                   
; 256 |      //ias[sv] = 0;                                                    
; 257 |      //ibs[sv] = 0;                                                    
; 258 |      //ids[sv] = 0;                                                    
; 259 |      //ies[sv] = 0;                                                    
; 260 |      //ids_m[sv] = 0;                                                  
; 261 |      //iqs_m[sv] = 0;                                                  
; 262 |      //wmech1[sv] = 0;                                                 
; 263 |      //we_m[sv] = 0;                                                   
; 264 | //          svopt[sv] = 0;  //A�ADIDO POR MI                         
; 265 | //          svapr[sv] = 0;  //A�ADIDO POR MI                         
; 266 |      //ias_p1[sv] = 0;  //A�ADIDO POR MI                             
; 267 |      //ias_p2[sv] = 0;  //A�ADIDO POR MI                             
;----------------------------------------------------------------------
        MOVB      XAR6,#0
        CMP       AR6,#15000            ; |254| 
        B         $C$L14,GEQ            ; |254| 
        ; branchcc occurs ; |254| 
$C$L13:    
	.line	228
;----------------------------------------------------------------------
; 268 | wm_log[sv] = 0;                                                        
;----------------------------------------------------------------------
        SETC      SXM
        MOV       AL,AR6
        MOVL      XAR4,#_wm_log         ; |268| 
        MOV       ACC,AL                ; |268| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |268| 
	.line	229
;----------------------------------------------------------------------
; 269 | eqep_1[sv] = 0;                                                        
;----------------------------------------------------------------------
        MOV       AL,AR6                ; |269| 
        MOVL      XAR4,#_eqep_1         ; |269| 
        MOV       ACC,AL                ; |269| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |269| 
	.line	230
;----------------------------------------------------------------------
; 270 | eqep_2[sv] = 0;                                                        
;----------------------------------------------------------------------
        MOV       AL,AR6                ; |270| 
        MOVL      XAR4,#_eqep_2         ; |270| 
        MOV       ACC,AL                ; |270| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |270| 
	.line	231
;----------------------------------------------------------------------
; 271 | corr[sv] = 0;                                                          
; 272 | //          vfila[sv] = 0;                                             
; 276 | // Parte del modelo predictivo correspondiente a las tensiones. Se ejec
;     | uta solo una vez al iniciar                                            
; 277 | //for(sv = 0; sv<31; sv++)                                             
;----------------------------------------------------------------------
        MOV       AL,AR6                ; |271| 
        MOVL      XAR4,#_corr           ; |271| 
        MOV       ACC,AL                ; |271| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |271| 
	.line	214
        MOV       AL,AR6
        ADDB      AL,#1                 ; |254| 
        MOVZ      AR6,AL                ; |254| 
        CMP       AR6,#15000            ; |254| 
        B         $C$L13,LT             ; |254| 
        ; branchcc occurs ; |254| 
$C$L14:    
	.line	238
;----------------------------------------------------------------------
; 278 | for(sv = 0; sv<11; sv++) // retocado para 10vv L + 2vv Z (o 10vv M + 2v
;     | v Z)                                                                   
;----------------------------------------------------------------------
        MOVB      XAR6,#0
        MOV       AL,AR6                ; |278| 
        CMPB      AL,#11                ; |278| 
        B         $C$L16,GEQ            ; |278| 
        ; branchcc occurs ; |278| 
$C$L15:    
	.line	240
;----------------------------------------------------------------------
; 280 | Usa[sv] = Tm*B11*Vdc*vsa[sv];                                          
;----------------------------------------------------------------------
        MOVW      DP,#_Tm
        SETC      SXM
        MOVIZ     R0H,#16617            ; |280| 
        MOVL      XAR4,#_vsa            ; |280| 
        MOV32     R1H,@_Tm
        MOV       ACC,AR6 << 1          ; |280| 
        MOVXI     R0H,#20326            ; |280| 
        MOVL      XAR5,#_Usa            ; |280| 
        MOVW      DP,#_Vdc
        ADDL      XAR4,ACC
        MPYF32    R0H,R0H,R1H           ; |280| 
        MOV       ACC,AR6 << 1          ; |280| 
        MOV32     R1H,@_Vdc
        MPYF32    R0H,R1H,R0H           ; |280| 
        MOV32     R1H,*+XAR4[0]
        MPYF32    R0H,R1H,R0H           ; |280| 
        ADDL      XAR5,ACC
        MOV32     *+XAR5[0],R0H
	.line	241
;----------------------------------------------------------------------
; 281 | Usb[sv] = Tm*B22*Vdc*vsb[sv];                                          
;----------------------------------------------------------------------
        MOVW      DP,#_Tm
        MOVIZ     R0H,#16617            ; |281| 
        MOV       ACC,AR6 << 1          ; |281| 
        MOV32     R1H,@_Tm
        MOVL      XAR4,#_vsb            ; |281| 
        MOVXI     R0H,#20326            ; |281| 
        MOVW      DP,#_Vdc
        MOVL      XAR5,#_Usb            ; |281| 
        ADDL      XAR4,ACC
        MPYF32    R0H,R0H,R1H           ; |281| 
        MOV       ACC,AR6 << 1          ; |281| 
        MOV32     R1H,@_Vdc
        MPYF32    R0H,R1H,R0H           ; |281| 
        MOV32     R1H,*+XAR4[0]
        MPYF32    R0H,R1H,R0H           ; |281| 
        ADDL      XAR5,ACC
        MOV32     *+XAR5[0],R0H
	.line	242
;----------------------------------------------------------------------
; 282 | Usx[sv] = Tm*B33*Vdc*vsx[sv];                                          
;----------------------------------------------------------------------
        MOVW      DP,#_Tm
        MOVIZ     R0H,#16670            ; |282| 
        MOV       ACC,AR6 << 1          ; |282| 
        MOVL      XAR4,#_vsx            ; |282| 
        MOV32     R1H,@_Tm
        MOVXI     R0H,#58182            ; |282| 
        MOVL      XAR5,#_Usx            ; |282| 
        MOVW      DP,#_Vdc
        ADDL      XAR4,ACC
        MPYF32    R0H,R0H,R1H           ; |282| 
        MOV       ACC,AR6 << 1          ; |282| 
        MOV32     R1H,@_Vdc
        MPYF32    R0H,R1H,R0H           ; |282| 
        MOV32     R1H,*+XAR4[0]
        MPYF32    R0H,R1H,R0H           ; |282| 
        ADDL      XAR5,ACC
        MOV32     *+XAR5[0],R0H
	.line	243
;----------------------------------------------------------------------
; 283 | Usy[sv] = Tm*B44*Vdc*vsy[sv];                                          
; 286 | // Multiplicaci�n de matriz A por Tm                                 
;----------------------------------------------------------------------
        MOVW      DP,#_Tm
        MOVIZ     R0H,#16670            ; |283| 
        MOV       ACC,AR6 << 1          ; |283| 
        MOVL      XAR4,#_vsy            ; |283| 
        MOVXI     R0H,#58182            ; |283| 
        MOV32     R1H,@_Tm
        MOVL      XAR5,#_Usy            ; |283| 
        ADDL      XAR4,ACC
        MOVW      DP,#_Vdc
        MPYF32    R0H,R0H,R1H           ; |283| 
        MOV       ACC,AR6 << 1          ; |283| 
        MOV32     R1H,@_Vdc
        MPYF32    R0H,R1H,R0H           ; |283| 
        MOV32     R1H,*+XAR4[0]
        MPYF32    R0H,R1H,R0H           ; |283| 
        ADDL      XAR5,ACC
        MOV32     *+XAR5[0],R0H
	.line	238
        MOV       AL,AR6
        ADDB      AL,#1                 ; |278| 
        CMPB      AL,#11                ; |278| 
        MOVZ      AR6,AL                ; |278| 
        B         $C$L15,LT             ; |278| 
        ; branchcc occurs ; |278| 
$C$L16:    
	.line	247
;----------------------------------------------------------------------
; 287 | TmA11 = Tm*A11;                                                        
;----------------------------------------------------------------------
        MOVW      DP,#_Tm
        MOVIZ     R0H,#49933            ; |287| 
        MOV32     R1H,@_Tm
        MOVXI     R0H,#53007            ; |287| 
        MPYF32    R0H,R0H,R1H           ; |287| 
        MOVW      DP,#_TmA11
        MOV32     @_TmA11,R0H
	.line	248
;----------------------------------------------------------------------
; 288 | TmA12 = Tm*A12;                                                        
;----------------------------------------------------------------------
        MOVIZ     R0H,#16528            ; |288| 
        MOVXI     R0H,#43414            ; |288| 
        MPYF32    R0H,R0H,R1H           ; |288| 
        NOP
        MOV32     @_TmA12,R0H
	.line	249
;----------------------------------------------------------------------
; 289 | TmA21 = Tm*A21;                                                        
;----------------------------------------------------------------------
        MOVIZ     R0H,#49296            ; |289| 
        MOVXI     R0H,#43414            ; |289| 
        MPYF32    R0H,R0H,R1H           ; |289| 
        NOP
        MOV32     @_TmA21,R0H
	.line	250
;----------------------------------------------------------------------
; 290 | TmA22 = Tm*A22;                                                        
;----------------------------------------------------------------------
        MOVIZ     R0H,#49933            ; |290| 
        MOVXI     R0H,#53007            ; |290| 
        MPYF32    R0H,R0H,R1H           ; |290| 
        NOP
        MOV32     @_TmA22,R0H
	.line	251
;----------------------------------------------------------------------
; 291 | TmA33 = Tm*A33;                                                        
;----------------------------------------------------------------------
        MOVIZ     R0H,#49985            ; |291| 
        MOVXI     R0H,#9697             ; |291| 
        MPYF32    R0H,R0H,R1H           ; |291| 
        NOP
        MOV32     @_TmA33,R0H
	.line	252
;----------------------------------------------------------------------
; 292 | TmA44 = Tm*A44;                                                        
; 293 | // nuevas variables                                                    
;----------------------------------------------------------------------
        MOVIZ     R0H,#49985            ; |292| 
        MOVXI     R0H,#9697             ; |292| 
        MPYF32    R0H,R0H,R1H           ; |292| 
        NOP
        MOV32     @_TmA44,R0H
	.line	254
;----------------------------------------------------------------------
; 294 | UmTmA11 = 1 + TmA11;                                                   
;----------------------------------------------------------------------
        MOV32     R0H,@_TmA11
        ADDF32    R0H,R0H,#16256        ; |294| 
        MOVW      DP,#_UmTmA11
        MOV32     @_UmTmA11,R0H
	.line	255
;----------------------------------------------------------------------
; 295 | UmTmA22 = 1 + TmA22;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_TmA22
        MOV32     R0H,@_TmA22
        ADDF32    R0H,R0H,#16256        ; |295| 
        MOVW      DP,#_UmTmA22
        MOV32     @_UmTmA22,R0H
	.line	256
;----------------------------------------------------------------------
; 296 | H_alpha = 0;                                                           
;----------------------------------------------------------------------
        ZERO      R0H                   ; |296| 
        MOV32     @_H_alpha,R0H
	.line	257
;----------------------------------------------------------------------
; 297 | H_beta  = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_H_beta
        MOV32     @_H_beta,R0H
	.line	258
;----------------------------------------------------------------------
; 298 | g_alfa  = 0;                                                           
;----------------------------------------------------------------------
        MOV32     @_g_alfa,R0H
	.line	259
;----------------------------------------------------------------------
; 299 | g_beta  = 0;                                                           
;----------------------------------------------------------------------
        MOV32     @_g_beta,R0H
	.line	260
;----------------------------------------------------------------------
; 300 | signoa  = 0;                                                           
;----------------------------------------------------------------------
        MOV32     @_signoa,R0H
	.line	261
;----------------------------------------------------------------------
; 301 | signob  = 0;                                                           
;----------------------------------------------------------------------
        MOV32     @_signob,R0H
	.line	263
;----------------------------------------------------------------------
; 303 | U_r2   = 0.005;//0.01;//0.05;  // valor a retocar en el futuro         
;----------------------------------------------------------------------
        MOVIZ     R0H,#15267            ; |303| 
        MOVXI     R0H,#55050            ; |303| 
        MOV32     @_U_r2,R0H
	.line	264
;----------------------------------------------------------------------
; 304 | U_alfa = 0.0075;                                                       
;----------------------------------------------------------------------
        MOVIZ     R0H,#15349            ; |304| 
        MOVXI     R0H,#49807            ; |304| 
        MOV32     @_U_alfa,R0H
	.line	265
;----------------------------------------------------------------------
; 305 | U_tau1 = 0.3249;                                                       
;----------------------------------------------------------------------
        MOVIZ     R0H,#16038            ; |305| 
        MOVXI     R0H,#22859            ; |305| 
        MOV32     @_U_tau1,R0H
	.line	266
;----------------------------------------------------------------------
; 306 | U_tau2 = 1.3764;                                                       
;----------------------------------------------------------------------
        MOVIZ     R0H,#16304            ; |306| 
        MOVXI     R0H,#11744            ; |306| 
        MOVW      DP,#_U_tau2
        MOV32     @_U_tau2,R0H
	.line	271
;----------------------------------------------------------------------
; 311 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.line	272
;----------------------------------------------------------------------
; 312 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 0;                              /
;     | / Stop all the TB clocks                                               
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       @_SysCtrlRegs+12,#0xfffb ; |312| 
	.line	273
;----------------------------------------------------------------------
; 313 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 1;                              /
;     | / Start all the TB clocks                                              
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+12,#0xfffb ; |313| 
        ORB       AL,#0x04              ; |313| 
        MOV       @_SysCtrlRegs+12,AL   ; |313| 
	.line	274
;----------------------------------------------------------------------
; 314 | EDIS;                                                                  
;----------------------------------------------------------------------
 EDIS
	.line	276
;----------------------------------------------------------------------
; 316 | EINT;
;     |                         // Enable interrupts to CPU-Level              
;----------------------------------------------------------------------
 clrc INTM
	.line	278
;----------------------------------------------------------------------
; 318 | CpuTimer0Regs.TCR.bit.TRB = 1;
;     |     // Reload CPU-TIMER0 Period                                        
; 321 | //Pesos ctes funcion de coste:                                         
;----------------------------------------------------------------------
        MOVW      DP,#_CpuTimer0Regs+4
        AND       AL,@_CpuTimer0Regs+4,#0xffdf ; |318| 
        ORB       AL,#0x20              ; |318| 
        MOV       @_CpuTimer0Regs+4,AL  ; |318| 
	.line	282
;----------------------------------------------------------------------
; 322 | K_xy=0;                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_K_xy
        ZERO      R0H                   ; |322| 
        MOV32     @_K_xy,R0H
	.line	283
;----------------------------------------------------------------------
; 323 | K_sc=0;                                                                
;----------------------------------------------------------------------
        MOV32     @_K_sc,R0H
	.line	284
;----------------------------------------------------------------------
; 324 | SC_suma=0;                                                             
; 326 | //Hay que habilitar y desahbilitar el Kp segun la prueba de reversal qu
;     | e se quiera hacer. Se quita del bucle if fallo de mas adelante.        
;----------------------------------------------------------------------
        MOV32     @_SC_suma,R0H
	.line	287
;----------------------------------------------------------------------
; 327 | GpioDataRegs.GPBSET.bit.GPIO49     = 1;                                
; 329 | //wm_ref = 0.75*wnom;                                                  
; 330 | // wm_ref = 0.50*wnom;                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+11
        AND       AL,@_GpioDataRegs+11,#0xfffd ; |327| 
        ORB       AL,#0x02              ; |327| 
        MOV       @_GpioDataRegs+11,AL  ; |327| 
	.line	291
;----------------------------------------------------------------------
; 331 | wm_ref = 0.25*wnom;                                                    
; 333 | // C�lculo de coeficientes que dependen de wr                        
;----------------------------------------------------------------------
        MOVW      DP,#_wm_ref
        MOVIZ     R0H,#16849            ; |331| 
        MOVXI     R0H,#28797            ; |331| 
        MOV32     @_wm_ref,R0H
	.line	294
;----------------------------------------------------------------------
; 334 | wr = P*wm_ref;                                                         
;----------------------------------------------------------------------
        MPYF32    R0H,R0H,#16448        ; |334| 
        NOP
        MOV32     @_wr,R0H
	.line	296
;----------------------------------------------------------------------
; 336 | TmA12 = wr*TmA12;                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_TmA12
        MOV32     R0H,@_TmA12
        MOVW      DP,#_wr
        MOV32     R1H,@_wr
        MPYF32    R0H,R1H,R0H           ; |336| 
        MOVW      DP,#_TmA12
        MOV32     @_TmA12,R0H
	.line	297
;----------------------------------------------------------------------
; 337 | TmA21 = wr*TmA21;                                                      
;----------------------------------------------------------------------
        MOV32     R0H,@_TmA21
        MPYF32    R0H,R1H,R0H           ; |337| 
        NOP
        MOV32     @_TmA21,R0H
	.line	299
;----------------------------------------------------------------------
; 339 | while(!stop)                                                           
;----------------------------------------------------------------------
        MOV       AL,@_stop             ; |339| 
        BF        $C$L36,NEQ            ; |339| 
        ; branchcc occurs ; |339| 
$C$L17:    
	.line	301
;----------------------------------------------------------------------
; 341 | TRIGGER_TOGGLE_54;                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xffbf ; |341| 
        ORB       AL,#0x40              ; |341| 
        MOV       @_GpioDataRegs+15,AL  ; |341| 
	.line	303
;----------------------------------------------------------------------
; 343 | ret_val_mon = (*callmon28335)(); // Arrancar 28335. MSK Execute Control
;----------------------------------------------------------------------
        SPM       #0
        MOVL      XAR7,#3342336         ; |343| 
        LCR       *XAR7                 ; |343| 
        ; call occurs [XAR7] ; |343| 
        MOVW      DP,#_ret_val_mon
        MOV       @_ret_val_mon,AL      ; |343| 
	.line	304
;----------------------------------------------------------------------
; 344 | GpioDataRegs.GPBSET.bit.GPIO48     = 1; //ventiladores encendidos      
; 346 | //TRIGGER_TOGGLE_55;                                                   
; 347 | while(timeout) // Stand by the timeout Flag cleared in the EPWM1 interr
;     | upt                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+11
        AND       AL,@_GpioDataRegs+11,#0xfffe ; |344| 
        ORB       AL,#0x01              ; |344| 
        MOV       @_GpioDataRegs+11,AL  ; |344| 
$C$L18:    
	.line	309
;----------------------------------------------------------------------
; 350 | //TRIGGER_TOGGLE_55;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_timeout
        MOV       AL,@_timeout          ; |349| 
        BF        $C$L18,NEQ            ; |349| 
        ; branchcc occurs ; |349| 
	.line	312
;----------------------------------------------------------------------
; 352 | timeout = 1;
;     |            // Set PWM Time Out Flag                                    
;----------------------------------------------------------------------
        MOVB      @_timeout,#1,UNC      ; |352| 
	.line	314
;----------------------------------------------------------------------
; 354 | TRIGGER_TOGGLE_57;                                                     
; 355 | // Deshabilitaci�n Kp de fase, en_fallo, se�al ctrl m�quina DC fa
;     | llo                                                                    
; 356 | //=====================================================================
;     | ======                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xfdff ; |354| 
        OR        AL,#0x0200            ; |354| 
        MOV       @_GpioDataRegs+15,AL  ; |354| 
	.line	317
;----------------------------------------------------------------------
; 357 | if (mstart2 != 0)                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_mstart2
        MOV32     R0H,@_mstart2
        CMPF32    R0H,#0                ; |357| 
        MOVST0    ZF, NF                ; |357| 
        BF        $C$L19,EQ             ; |357| 
        ; branchcc occurs ; |357| 
	.line	319
;----------------------------------------------------------------------
; 359 | contfallo++;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_contfallo
        MOV32     R0H,@_contfallo
        ADDF32    R0H,R0H,#16256        ; |359| 
        NOP
        MOV32     @_contfallo,R0H
	.line	320
;----------------------------------------------------------------------
; 360 | if (contfallo == 2.0*FRECUENCIA_MUESTREO_HZ)                           
;----------------------------------------------------------------------
        MOV32     R1H,@_contfallo
        MOVIZ     R0H,#18204            ; |360| 
        MOVXI     R0H,#16384            ; |360| 
        CMPF32    R1H,R0H               ; |360| 
        MOVST0    ZF, NF                ; |360| 
        BF        $C$L19,NEQ            ; |360| 
        ; branchcc occurs ; |360| 
	.line	322
;----------------------------------------------------------------------
; 362 | contfallo = 0;                                                         
;----------------------------------------------------------------------
        ZERO      R0H                   ; |362| 
        MOV32     @_contfallo,R0H
	.line	323
;----------------------------------------------------------------------
; 363 | mstart=1;                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_mstart
        MOVIZ     R0H,#16256            ; |363| 
        MOV32     @_mstart,R0H
	.line	324
;----------------------------------------------------------------------
; 364 | mstart2=0;                                                             
;----------------------------------------------------------------------
        ZERO      R0H                   ; |364| 
        MOV32     @_mstart2,R0H
	.line	325
;----------------------------------------------------------------------
; 365 | logger = 0;                                                            
; 370 | //------------------------- PI Isq -------------------------           
; 372 | // Control de Iq para el caso en el que la velocidad se quiere mantener
;     |  constante                                                             
; 386 | // Cálculo de la velocidad mecánica (rad/s) a partir de medida direct
;     | a de QPOSCNT                                                           
; 387 | // mi_DeltaCNT =                                                       
; 388 | // wm_k =                                                              
; 390 | //TRIGGER_TOGGLE_55;                                                   
;----------------------------------------------------------------------
        MOV       @_logger,#0           ; |365| 
$C$L19:    
	.line	351
;----------------------------------------------------------------------
; 391 | wm_d3 = wm_k;                                                          
; 392 | // Bucle externo ctrl velocidad:                                       
;----------------------------------------------------------------------
        MOVW      DP,#_wm_k
        MOVL      ACC,@_wm_k            ; |391| 
        MOVL      @_wm_d3,ACC           ; |391| 
	.line	353
;----------------------------------------------------------------------
; 393 | Werror = wm_ref - wm_d3;                                               
;----------------------------------------------------------------------
        MOV32     R1H,@_wm_ref
        MOV32     R0H,@_wm_d3
        SUBF32    R0H,R1H,R0H           ; |393| 
        NOP
        MOV32     @_Werror,R0H
	.line	354
;----------------------------------------------------------------------
; 394 | inte_k = Kinte*(Werror + e_km1) + inte_km1;                            
;----------------------------------------------------------------------
        MOV32     R1H,@_e_km1
        ADDF32    R0H,R1H,R0H           ; |394| 
        MOV32     R1H,@_Kinte
        MPYF32    R0H,R1H,R0H           ; |394| 
        MOV32     R1H,@_inte_km1
        ADDF32    R0H,R1H,R0H           ; |394| 
        NOP
        MOV32     @_inte_k,R0H
	.line	356
;----------------------------------------------------------------------
; 396 | Isq = KPW*(Werror + KPIW*inte_k);                                      
; 398 | //Saturaci�n del Par o Isq* en mi caso                               
;----------------------------------------------------------------------
        MOVW      DP,#_KPIW
        MOV32     R0H,@_KPIW
        MOVW      DP,#_inte_k
        MOV32     R1H,@_inte_k

        MOV32     R0H,@_Werror
||      MPYF32    R1H,R1H,R0H           ; |396| 

        MOVW      DP,#_KPW
        ADDF32    R0H,R1H,R0H           ; |396| 
        MOV32     R1H,@_KPW
        MPYF32    R0H,R1H,R0H           ; |396| 
        MOVW      DP,#_Isq
        MOV32     @_Isq,R0H
	.line	360
;----------------------------------------------------------------------
; 400 | Isq_par = Isq;                                                         
;----------------------------------------------------------------------
        MOVL      ACC,@_Isq             ; |400| 
        MOVW      DP,#_Isq_par
        MOVL      @_Isq_par,ACC         ; |400| 
	.line	362
;----------------------------------------------------------------------
; 402 | if (Isq >= Imax)                                                       
; 404 |      Isq = Imax;                                                       
;----------------------------------------------------------------------
        MOVW      DP,#_Imax
        MOV32     R0H,@_Imax
        MOVW      DP,#_Isq
        MOV32     R1H,@_Isq
        CMPF32    R1H,R0H               ; |402| 
        MOVST0    ZF, NF                ; |402| 
        B         $C$L21,GEQ            ; |402| 
        ; branchcc occurs ; |402| 
	.line	366
;----------------------------------------------------------------------
; 406 | else if (Isq <= -Imax)                                                 
; 408 |      Isq = -Imax;                                                      
; 410 | else //Unicamente actualiza las var's si el ctrl no esta saturado, de l
;     | o contrario quedan al valor en t-1                                     
;----------------------------------------------------------------------
        NEGF32    R0H,R0H               ; |406| 
        CMPF32    R1H,R0H               ; |406| 
        MOVST0    ZF, NF                ; |406| 
        B         $C$L20,LEQ            ; |406| 
        ; branchcc occurs ; |406| 
	.line	372
;----------------------------------------------------------------------
; 412 | e_km1 = Werror;                                                        
;----------------------------------------------------------------------
        MOVW      DP,#_Werror
        MOVL      ACC,@_Werror          ; |412| 
        MOVL      @_e_km1,ACC           ; |412| 
	.line	373
;----------------------------------------------------------------------
; 413 | inte_km1 = inte_k;                                                     
; 416 | //}                                                                    
; 417 | //------------------------- End Isq -------------------------*/        
; 422 | // medida velocidad fuera de la ISR EQEP                               
; 423 | //mi_qcprdlat = EQep1Regs.QCPRDLAT;                                    
; 424 | //mi_qposlat = EQep1Regs.QPOSLAT;                                      
; 430 | //------------------------- dq - alfa-beta ----------------------*/    
; 432 | // Transf. de las corrientes de referencia Isd-Isq:                    
;----------------------------------------------------------------------
        MOVL      ACC,@_inte_k          ; |413| 
        MOVL      @_inte_km1,ACC        ; |413| 
        B         $C$L22,UNC            ; |413| 
        ; branch occurs ; |413| 
$C$L20:    
	.line	368
        MOVW      DP,#_Imax
        MOV32     R0H,@_Imax
        MOVW      DP,#_Isq
        NEGF32    R0H,R0H               ; |408| 
        MOV32     @_Isq,R0H
	.line	369
        B         $C$L22,UNC            ; |409| 
        ; branch occurs ; |409| 
$C$L21:    
	.line	364
        MOVW      DP,#_Imax
        MOVL      ACC,@_Imax            ; |404| 
        MOVW      DP,#_Isq
        MOVL      @_Isq,ACC             ; |404| 
$C$L22:    
	.line	393
;----------------------------------------------------------------------
; 433 | Ialfa_ref = Isd*costhetae - Isq*sinthetae;                             
;----------------------------------------------------------------------
        MOVW      DP,#_sinthetae
        MOV32     R1H,@_sinthetae
        MOVW      DP,#_Isq
        MOV32     R0H,@_Isq
        MOVW      DP,#_costhetae
        MOV32     R2H,@_costhetae
        MOVW      DP,#_Isd

        MOV32     R1H,@_Isd
||      MPYF32    R0H,R1H,R0H           ; |433| 

        MPYF32    R1H,R2H,R1H           ; |433| 
        NOP
        SUBF32    R0H,R1H,R0H           ; |433| 
        MOVW      DP,#_Ialfa_ref
        MOV32     @_Ialfa_ref,R0H
	.line	394
;----------------------------------------------------------------------
; 434 | Ibeta_ref = Isd*sinthetae + Isq*costhetae;                             
; 436 | // ------------------Se a�ade para mi funci�n J de coste-----------
;     | ---                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_sinthetae
        MOV32     R1H,@_sinthetae
        MOVW      DP,#_Isd
        MOV32     R0H,@_Isd

        MOV32     R1H,@_Isq
||      MPYF32    R0H,R1H,R0H           ; |434| 

        MPYF32    R1H,R2H,R1H           ; |434| 
        NOP
        ADDF32    R0H,R1H,R0H           ; |434| 
        MOVW      DP,#_Ibeta_ref
        MOV32     @_Ibeta_ref,R0H
	.line	397
;----------------------------------------------------------------------
; 437 | Ialfa_ref_p = Isd*costhetae_p - Isq*sinthetae_p;                       
;----------------------------------------------------------------------
        MOV32     R1H,@_sinthetae_p
        MOVW      DP,#_Isq
        MOV32     R0H,@_Isq
        MOVW      DP,#_costhetae_p
        MOV32     R2H,@_costhetae_p
        MOVW      DP,#_Isd

        MOV32     R1H,@_Isd
||      MPYF32    R0H,R1H,R0H           ; |437| 

        MPYF32    R1H,R2H,R1H           ; |437| 
        NOP
        SUBF32    R0H,R1H,R0H           ; |437| 
        MOVW      DP,#_Ialfa_ref_p
        MOV32     @_Ialfa_ref_p,R0H
	.line	398
;----------------------------------------------------------------------
; 438 | Ibeta_ref_p = Isd*sinthetae_p + Isq*costhetae_p;                       
; 439 | // ------------------------------------------------------------------- 
; 441 | //------------------------- End dq - alfa-beta ------------------------
;     | --*/                                                                   
; 443 | //------------- Control KP, Estimador, dq-alfa-beta, Referencias  -----
;     | --------*/                                                             
; 445 | //---------------------------- Ref's Corriente ------------------------
;     | -*/                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_Isd
        MOV32     R0H,@_Isd
        MOVW      DP,#_sinthetae_p
        MOV32     R1H,@_sinthetae_p
        MOVW      DP,#_Isq
        MOV32     R2H,@_Isq
        MOVW      DP,#_costhetae_p

        MOV32     R1H,@_costhetae_p
||      MPYF32    R0H,R1H,R0H           ; |438| 

        MPYF32    R1H,R1H,R2H           ; |438| 
        NOP
        ADDF32    R0H,R1H,R0H           ; |438| 
        NOP
        MOV32     @_Ibeta_ref_p,R0H
	.line	406
;----------------------------------------------------------------------
; 446 | Ix_ref = 0;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_Ix_ref
        ZERO      R0H                   ; |446| 
        MOV32     @_Ix_ref,R0H
	.line	407
;----------------------------------------------------------------------
; 447 | Iy_ref = 0;                                                            
; 448 | //-------------------------End Ref's Corriente ------------------------
;     | -*/                                                                    
; 451 | //----------------- Med. y  acondicionamiento corrientes de fase - Tran
;     | s. alfa-beta-x-y ------------------*/                                  
; 452 | //TRIGGER_TOGGLE_56;                                                   
; 453 | // ADC converter SEQ 1 is Busy                                         
;----------------------------------------------------------------------
        MOV32     @_Iy_ref,R0H
	.line	414
;----------------------------------------------------------------------
; 454 | while(AdcRegs.ADCST.bit.SEQ1_BSY)                                      
;----------------------------------------------------------------------
        B         $C$L24,UNC            ; |454| 
        ; branch occurs ; |454| 
$C$L23:    
	.line	416
;----------------------------------------------------------------------
; 456 | LED2_TOGGLE;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+7
        AND       AL,@_GpioDataRegs+7,#0xfffd ; |456| 
        ORB       AL,#0x02              ; |456| 
        MOV       @_GpioDataRegs+7,AL   ; |456| 
$C$L24:    
	.line	417
;----------------------------------------------------------------------
; 477 | //TRIGGER_TOGGLE_56;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_AdcRegs+25
        TBIT      @_AdcRegs+25,#2       ; |457| 
        BF        $C$L23,TC             ; |457| 
        ; branchcc occurs ; |457| 
	.line	438
;----------------------------------------------------------------------
; 478 | TRIGGER_TOGGLE_55;                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xff7f ; |478| 
        ORB       AL,#0x80              ; |478| 
        MOV       @_GpioDataRegs+15,AL  ; |478| 
	.line	440
;----------------------------------------------------------------------
; 480 | Ia_medido = ((float) AdcMirror.ADCRESULT0 - CONST_OFFSET_IA)*CONST_GAIN
;     | FACT_IA;                                                               
;----------------------------------------------------------------------
        MOVIZ     R0H,#17667            ; |480| 
        MOVIZ     R1H,#15446            ; |480| 
        MOVW      DP,#_AdcMirror
        UI16TOF32 R2H,@_AdcMirror       ; |480| 
        MOVXI     R0H,#40168            ; |480| 
        SUBF32    R0H,R2H,R0H           ; |480| 
        MOVXI     R1H,#47756            ; |480| 
        MPYF32    R0H,R1H,R0H           ; |480| 
        MOVW      DP,#_Ia_medido
        MOV32     @_Ia_medido,R0H
	.line	441
;----------------------------------------------------------------------
; 481 | Ib_medido = ((float) AdcMirror.ADCRESULT1 - CONST_OFFSET_IB)*CONST_GAIN
;     | FACT_IB;                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+1
        MOVIZ     R1H,#15449            ; |481| 
        MOVIZ     R0H,#17666            ; |481| 
        UI16TOF32 R2H,@_AdcMirror+1     ; |481| 
        MOVXI     R0H,#15671            ; |481| 
        SUBF32    R0H,R2H,R0H           ; |481| 
        MOVXI     R1H,#37979            ; |481| 
        MPYF32    R0H,R1H,R0H           ; |481| 
        MOVW      DP,#_Ib_medido
        MOV32     @_Ib_medido,R0H
	.line	442
;----------------------------------------------------------------------
; 482 | Id_medido = ((float) AdcMirror.ADCRESULT2 - CONST_OFFSET_ID)*CONST_GAIN
;     | FACT_ID;                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+2
        MOVIZ     R1H,#15447            ; |482| 
        MOVIZ     R0H,#17667            ; |482| 
        UI16TOF32 R2H,@_AdcMirror+2     ; |482| 
        MOVXI     R0H,#13831            ; |482| 
        SUBF32    R0H,R2H,R0H           ; |482| 
        MOVXI     R1H,#2621             ; |482| 
        MPYF32    R0H,R1H,R0H           ; |482| 
        MOVW      DP,#_Id_medido
        MOV32     @_Id_medido,R0H
	.line	443
;----------------------------------------------------------------------
; 483 | Ie_medido = ((float) AdcMirror.ADCRESULT3 - CONST_OFFSET_IE)*CONST_GAIN
;     | FACT_IE;                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+3
        MOVIZ     R1H,#15450            ; |483| 
        MOVIZ     R0H,#17666            ; |483| 
        UI16TOF32 R2H,@_AdcMirror+3     ; |483| 
        MOVXI     R0H,#19297            ; |483| 
        SUBF32    R0H,R2H,R0H           ; |483| 
        MOVXI     R1H,#1434             ; |483| 
        MPYF32    R0H,R1H,R0H           ; |483| 
        MOVW      DP,#_Ie_medido
        MOV32     @_Ie_medido,R0H
	.line	444
;----------------------------------------------------------------------
; 484 | Ic_medido = -Ia_medido -Ib_medido -Id_medido -Ie_medido;               
; 487 | //------------- End Med. y  acondicionamiento corrientes de fase - Tran
;     | s. alfa-beta-x-y ------------------*/                                  
; 489 | // -------------------------- abcde to alpha-beta-x-y -----------------
;     | ---------------------------                                            
;----------------------------------------------------------------------
        MOVW      DP,#_Ia_medido
        MOV32     R0H,@_Ia_medido
        MOVW      DP,#_Ib_medido
        NEGF32    R0H,R0H               ; |484| 
        MOV32     R1H,@_Ib_medido
        MOVW      DP,#_Id_medido

        MOV32     R0H,@_Id_medido
||      SUBF32    R1H,R0H,R1H           ; |484| 

        MOVW      DP,#_Ie_medido
        SUBF32    R1H,R1H,R0H           ; |484| 
        MOV32     R0H,@_Ie_medido
        SUBF32    R0H,R1H,R0H           ; |484| 
        MOVW      DP,#_Ic_medido
        MOV32     @_Ic_medido,R0H
	.line	451
;----------------------------------------------------------------------
; 491 | i_alpha_med = T11*Ia_medido + T12*Ib_medido + T13*Ic_medido + T14*Id_me
;     | dido + T15*Ie_medido;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_Ib_medido
        MOVIZ     R3H,#48805            ; |491| 
        MOVIZ     R4H,#15869            ; |491| 
        MOV32     R6H,@_Ib_medido
        MOVIZ     R1H,#15869            ; |491| 
        MOVIZ     R2H,#48805            ; |491| 
        MOVIZ     R0H,#16076            ; |491| 
        MOVXI     R3H,#45110            ; |491| 
        MOVW      DP,#_Ia_medido
        MOVXI     R4H,#10045            ; |491| 
        MOVXI     R1H,#10045            ; |491| 
        MOVXI     R0H,#52429            ; |491| 
        MOVXI     R2H,#45110            ; |491| 

        MPYF32    R1H,R1H,R6H           ; |491| 
||      MOV32     R5H,@_Ia_medido

        MPYF32    R0H,R0H,R5H           ; |491| 
||      MOV32     R7H,@_Ic_medido

        MOV32     R6H,@_Id_medido

        MPYF32    R1H,R2H,R7H           ; |491| 
||      ADDF32    R0H,R1H,R0H           ; |491| 

        MOVW      DP,#_Ie_medido

        MPYF32    R1H,R3H,R6H           ; |491| 
||      ADDF32    R0H,R1H,R0H           ; |491| 

        MOV32     R5H,@_Ie_medido

        MPYF32    R1H,R4H,R5H           ; |491| 
||      ADDF32    R0H,R1H,R0H           ; |491| 

        NOP
        ADDF32    R0H,R1H,R0H           ; |491| 
        MOVW      DP,#_i_alpha_med
        MOV32     @_i_alpha_med,R0H
	.line	452
;----------------------------------------------------------------------
; 492 | i_beta_med  =                      T22*Ib_medido + T23*Ic_medido + T24*
;     | Id_medido + T25*Ie_medido;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_Ic_medido
        MOVIZ     R2H,#48752            ; |492| 
        MOVIZ     R1H,#15984            ; |492| 
        MOVIZ     R3H,#48834            ; |492| 
        MOVIZ     R0H,#16066            ; |492| 
        MOV32     R5H,@_Ic_medido
        MOVXI     R1H,#49325            ; |492| 
        MOVXI     R2H,#49325            ; |492| 
        MOVW      DP,#_Ib_medido
        MOVXI     R0H,#50793            ; |492| 
        MOVXI     R3H,#50793            ; |492| 

        MOV32     R4H,@_Ib_medido
||      MPYF32    R1H,R1H,R5H           ; |492| 

        MOVW      DP,#_Id_medido

        MOV32     R5H,@_Id_medido
||      MPYF32    R0H,R0H,R4H           ; |492| 

        MOVW      DP,#_Ie_medido

        MPYF32    R1H,R2H,R5H           ; |492| 
||      ADDF32    R0H,R1H,R0H           ; |492| 

        MOV32     R4H,@_Ie_medido

        MPYF32    R1H,R3H,R4H           ; |492| 
||      ADDF32    R0H,R1H,R0H           ; |492| 

        NOP
        ADDF32    R0H,R1H,R0H           ; |492| 
        MOVW      DP,#_i_beta_med
        MOV32     @_i_beta_med,R0H
	.line	453
;----------------------------------------------------------------------
; 493 | i_x_med         = T31*Ia_medido + T32*Ib_medido + T33*Ic_medido + T34*I
;     | d_medido + T35*Ie_medido;                                              
;----------------------------------------------------------------------
        MOVIZ     R2H,#15869            ; |493| 
        MOVIZ     R1H,#48805            ; |493| 
        MOVIZ     R3H,#15869            ; |493| 
        MOVIZ     R4H,#48805            ; |493| 
        MOV32     R6H,@_Ib_medido
        MOVIZ     R0H,#16076            ; |493| 
        MOVXI     R1H,#45110            ; |493| 
        MOVXI     R2H,#10045            ; |493| 
        MOVW      DP,#_Ia_medido
        MOVXI     R0H,#52429            ; |493| 
        MOVXI     R3H,#10045            ; |493| 
        MOVXI     R4H,#45110            ; |493| 

        MOV32     R5H,@_Ia_medido
||      MPYF32    R1H,R1H,R6H           ; |493| 

        MPYF32    R0H,R0H,R5H           ; |493| 
        MOV32     R6H,@_Id_medido

        MPYF32    R1H,R2H,R7H           ; |493| 
||      ADDF32    R0H,R1H,R0H           ; |493| 

        MOVW      DP,#_Ie_medido

        MPYF32    R1H,R3H,R6H           ; |493| 
||      ADDF32    R0H,R1H,R0H           ; |493| 

        MOV32     R5H,@_Ie_medido

        MPYF32    R1H,R4H,R5H           ; |493| 
||      ADDF32    R0H,R1H,R0H           ; |493| 

        NOP
        ADDF32    R0H,R1H,R0H           ; |493| 
        MOVW      DP,#_i_x_med
        MOV32     @_i_x_med,R0H
	.line	454
;----------------------------------------------------------------------
; 494 | i_y_med         =                      T42*Ib_medido + T43*Ic_medido +
;     | T44*Id_medido + T45*Ie_medido;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_Ic_medido
        MOVIZ     R2H,#16066            ; |494| 
        MOVIZ     R1H,#48834            ; |494| 
        MOVIZ     R3H,#48752            ; |494| 
        MOVIZ     R0H,#15984            ; |494| 
        MOV32     R5H,@_Ic_medido
        MOVXI     R1H,#50793            ; |494| 
        MOVXI     R2H,#50793            ; |494| 
        MOVW      DP,#_Ib_medido
        MOVXI     R0H,#49325            ; |494| 
        MOVXI     R3H,#49325            ; |494| 

        MOV32     R4H,@_Ib_medido
||      MPYF32    R1H,R1H,R5H           ; |494| 

        MOVW      DP,#_Id_medido

        MOV32     R5H,@_Id_medido
||      MPYF32    R0H,R0H,R4H           ; |494| 

        MOVW      DP,#_Ie_medido

        MPYF32    R1H,R2H,R5H           ; |494| 
||      ADDF32    R0H,R1H,R0H           ; |494| 

        MOV32     R4H,@_Ie_medido

        MPYF32    R1H,R3H,R4H           ; |494| 
||      ADDF32    R0H,R1H,R0H           ; |494| 

        NOP
        ADDF32    R0H,R1H,R0H           ; |494| 
        MOVW      DP,#_i_y_med
        MOV32     @_i_y_med,R0H
	.line	456
;----------------------------------------------------------------------
; 496 | TRIGGER_TOGGLE_55;                                                     
; 507 | // ------------------------ End abcde to alpha-beta-x-y ---------------
;     | -------------*/                                                        
; 510 | //-------- End Med. y  acondicionamiento corrientes de fase - Trans. al
;     | fa-beta-x-y ------------------                                         
; 512 | // --------------------------------------- PREDICTIVE MODEL -----------
;     | ------------------------------------------                             
; 513 | // C�lculo de la funci�n G (ya va multiplicada por Tm)             
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xff7f ; |496| 
        ORB       AL,#0x80              ; |496| 
        MOV       @_GpioDataRegs+15,AL  ; |496| 
	.line	474
;----------------------------------------------------------------------
; 514 | G1 = i_alpha_med - H_alpha;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_H_alpha
        MOV32     R0H,@_H_alpha
        MOV32     R1H,@_i_alpha_med
        SUBF32    R0H,R1H,R0H           ; |514| 
        MOVW      DP,#_G1
        MOV32     @_G1,R0H
	.line	475
;----------------------------------------------------------------------
; 515 | G2 = i_beta_med  - H_beta;                                             
; 516 | // Predicci�n para k+1                                               
;----------------------------------------------------------------------
        MOVW      DP,#_H_beta
        MOV32     R0H,@_H_beta
        MOVW      DP,#_i_beta_med
        MOV32     R1H,@_i_beta_med
        SUBF32    R0H,R1H,R0H           ; |515| 
        MOVW      DP,#_G2
        MOV32     @_G2,R0H
	.line	477
;----------------------------------------------------------------------
; 517 | H_alpha = UmTmA11*i_alpha_med + TmA12*i_beta_med   + Usa[sopt];        
;----------------------------------------------------------------------
        SETC      SXM
        MOVL      XAR4,#_Usa            ; |517| 
        MOV32     R1H,@_TmA12
        MOV       ACC,AR1 << 1          ; |517| 
        MOVW      DP,#_i_beta_med
        MOV32     R2H,@_i_beta_med
        MOV32     R0H,@_UmTmA11

        MOV32     R1H,@_i_alpha_med
||      MPYF32    R2H,R2H,R1H           ; |517| 

        MPYF32    R0H,R1H,R0H           ; |517| 
        ADDL      XAR4,ACC
        ADDF32    R0H,R2H,R0H           ; |517| 
        MOV32     R1H,*+XAR4[0]
        ADDF32    R0H,R1H,R0H           ; |517| 
        NOP
        MOV32     @_H_alpha,R0H
	.line	478
;----------------------------------------------------------------------
; 518 | H_beta  = TmA21*i_alpha_med   + UmTmA22*i_beta_med + Usb[sopt];        
;----------------------------------------------------------------------
        MOV32     R1H,@_UmTmA22
        MOV       ACC,AR1 << 1          ; |518| 
        MOV32     R2H,@_i_beta_med
        MOVL      XAR4,#_Usb            ; |518| 
        MOVW      DP,#_TmA21
        MOV32     R0H,@_TmA21
        MOVW      DP,#_i_alpha_med

        MOV32     R1H,@_i_alpha_med
||      MPYF32    R2H,R2H,R1H           ; |518| 

        MPYF32    R0H,R1H,R0H           ; |518| 
        ADDL      XAR4,ACC
        ADDF32    R0H,R2H,R0H           ; |518| 
        MOV32     R1H,*+XAR4[0]
        ADDF32    R0H,R1H,R0H           ; |518| 
        MOVW      DP,#_H_beta
        MOV32     @_H_beta,R0H
	.line	479
;----------------------------------------------------------------------
; 519 | i_alpha_p1 = H_alpha  + G1;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_H_alpha
        MOV32     R0H,@_H_alpha
        MOVW      DP,#_G1
        MOV32     R1H,@_G1
        ADDF32    R0H,R1H,R0H           ; |519| 
        MOVW      DP,#_i_alpha_p1
        MOV32     @_i_alpha_p1,R0H
	.line	480
;----------------------------------------------------------------------
; 520 | i_beta_p   = H_beta   + G2;                                            
; 523 | // ----------------------------------------- OPTIMIZATION -------------
;     | ------------------------------------------                             
; 524 | // T�rminos indep del sv:                                            
;----------------------------------------------------------------------
        MOVW      DP,#_H_beta
        MOV32     R0H,@_H_beta
        MOVW      DP,#_G2
        MOV32     R1H,@_G2
        ADDF32    R0H,R1H,R0H           ; |520| 
        MOVW      DP,#_i_beta_p
        MOV32     @_i_beta_p,R0H
	.line	485
;----------------------------------------------------------------------
; 525 | g_alfa = Ialfa_ref_p - (UmTmA11*i_alpha_p1 + TmA12*i_beta_p);          
;----------------------------------------------------------------------
        MOVW      DP,#_TmA12
        MOV32     R1H,@_TmA12
        MOVW      DP,#_UmTmA11
        MOV32     R0H,@_UmTmA11
        MOV32     R2H,@_i_beta_p

        MOV32     R1H,@_i_alpha_p1
||      MPYF32    R2H,R2H,R1H           ; |525| 

        MPYF32    R0H,R1H,R0H           ; |525| 
        MOVW      DP,#_Ialfa_ref_p
        ADDF32    R0H,R2H,R0H           ; |525| 
        MOV32     R1H,@_Ialfa_ref_p
        SUBF32    R0H,R1H,R0H           ; |525| 
        MOVW      DP,#_g_alfa
        MOV32     @_g_alfa,R0H
	.line	486
;----------------------------------------------------------------------
; 526 | g_beta = Ibeta_ref_p - (TmA21*i_alpha_p1   + UmTmA22*i_beta_p);        
;----------------------------------------------------------------------
        MOVW      DP,#_UmTmA22
        MOV32     R1H,@_UmTmA22
        MOVW      DP,#_TmA21
        MOV32     R0H,@_TmA21
        MOVW      DP,#_i_beta_p
        MOV32     R2H,@_i_beta_p

        MOV32     R1H,@_i_alpha_p1
||      MPYF32    R2H,R2H,R1H           ; |526| 

        MPYF32    R0H,R1H,R0H           ; |526| 
        MOVW      DP,#_Ibeta_ref_p
        ADDF32    R0H,R2H,R0H           ; |526| 
        MOV32     R1H,@_Ibeta_ref_p
        SUBF32    R0H,R1H,R0H           ; |526| 
        MOVW      DP,#_g_beta
        MOV32     @_g_beta,R0H
	.line	488
;----------------------------------------------------------------------
; 528 | sopt = 0;                                                              
;----------------------------------------------------------------------
        MOVB      XAR1,#0
	.line	489
;----------------------------------------------------------------------
; 529 | Jopt = 500000;                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_Jopt
        MOVIZ     R0H,#18676            ; |529| 
        MOVXI     R0H,#9216             ; |529| 
        MOV32     @_Jopt,R0H
	.line	491
;----------------------------------------------------------------------
; 531 | TRIGGER_TOGGLE_56;                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xfeff ; |531| 
        OR        AL,#0x0100            ; |531| 
        MOV       @_GpioDataRegs+15,AL  ; |531| 
	.line	492
;----------------------------------------------------------------------
; 532 | for (sv=0; sv<11 ; sv++)  // retocado para 10vv L + 2vv Z              
;----------------------------------------------------------------------
        MOVB      XAR6,#0
        MOV       AL,AR6                ; |532| 
        CMPB      AL,#11                ; |532| 
        B         $C$L27,GEQ            ; |532| 
        ; branchcc occurs ; |532| 
$C$L25:    
	.line	494
;----------------------------------------------------------------------
; 534 | e_alpha = g_alfa - Usa[sv];                                            
;----------------------------------------------------------------------
        SETC      SXM
        MOVL      XAR4,#_Usa            ; |534| 
        MOV       ACC,AR6 << 1          ; |534| 
        ADDL      XAR4,ACC
        MOVW      DP,#_g_alfa
        MOV32     R1H,@_g_alfa
        MOV32     R0H,*+XAR4[0]
        SUBF32    R0H,R1H,R0H           ; |534| 
        MOVW      DP,#_e_alpha
        MOV32     @_e_alpha,R0H
	.line	495
;----------------------------------------------------------------------
; 535 | e_beta  = g_beta - Usb[sv];                                            
;----------------------------------------------------------------------
        MOV       ACC,AR6 << 1          ; |535| 
        MOVL      XAR4,#_Usb            ; |535| 
        ADDL      XAR4,ACC
        MOVW      DP,#_g_beta
        MOV32     R1H,@_g_beta
        MOV32     R0H,*+XAR4[0]
        SUBF32    R0H,R1H,R0H           ; |535| 
        MOVW      DP,#_e_beta
        MOV32     @_e_beta,R0H
	.line	496
;----------------------------------------------------------------------
; 536 | Jsv = e_alpha*e_alpha + e_beta*e_beta;                                 
;----------------------------------------------------------------------
        MOVW      DP,#_e_alpha
        MOV32     R1H,@_e_alpha
        MOV32     R0H,@_e_alpha
        MOVW      DP,#_e_beta
        MOV32     R2H,@_e_beta

        MOV32     R1H,@_e_beta
||      MPYF32    R0H,R1H,R0H           ; |536| 

        MPYF32    R1H,R2H,R1H           ; |536| 
        NOP
        ADDF32    R0H,R1H,R0H           ; |536| 
        NOP
        MOV32     @_Jsv,R0H
	.line	497
;----------------------------------------------------------------------
; 537 | if(Jopt>Jsv)                                                           
;----------------------------------------------------------------------
        MOV32     R1H,@_Jopt
        CMPF32    R1H,R0H               ; |537| 
        MOVST0    ZF, NF                ; |537| 
        B         $C$L26,LEQ            ; |537| 
        ; branchcc occurs ; |537| 
	.line	499
;----------------------------------------------------------------------
; 539 | sopt = sv;                                                             
;----------------------------------------------------------------------
        MOVZ      AR1,AR6
	.line	500
;----------------------------------------------------------------------
; 540 | Jopt = Jsv;                                                            
;----------------------------------------------------------------------
        MOVL      ACC,@_Jsv             ; |540| 
        MOVL      @_Jopt,ACC            ; |540| 
$C$L26:    
	.line	492
        MOV       AL,AR6
        ADDB      AL,#1                 ; |532| 
        CMPB      AL,#11                ; |532| 
        MOVZ      AR6,AL                ; |532| 
        B         $C$L25,LT             ; |532| 
        ; branchcc occurs ; |532| 
$C$L27:    
	.line	503
;----------------------------------------------------------------------
; 543 | TRIGGER_TOGGLE_56;                                                     
; 547 | // Transf. de las corrientes de medida a Isd-Isq:                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xfeff ; |543| 
        OR        AL,#0x0100            ; |543| 
        MOV       @_GpioDataRegs+15,AL  ; |543| 
	.line	508
;----------------------------------------------------------------------
; 548 | Id_med =  i_alpha_med*costhetae + i_beta_med*sinthetae;                
;----------------------------------------------------------------------
        MOVW      DP,#_costhetae
        MOV32     R1H,@_costhetae
        MOVW      DP,#_i_alpha_med
        MOV32     R0H,@_i_alpha_med
        MOVW      DP,#_sinthetae
        MOV32     R2H,@_sinthetae
        MOVW      DP,#_i_beta_med

        MOV32     R1H,@_i_beta_med
||      MPYF32    R0H,R1H,R0H           ; |548| 

        MPYF32    R1H,R2H,R1H           ; |548| 
        NOP
        ADDF32    R0H,R1H,R0H           ; |548| 
        MOVW      DP,#_Id_med
        MOV32     @_Id_med,R0H
	.line	509
;----------------------------------------------------------------------
; 549 | Iq_med = -i_alpha_med*sinthetae + i_beta_med*costhetae;                
; 551 | //----------------------- Estimador de Posici�n ---------------------
;     | ------                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_i_alpha_med
        MOV32     R0H,@_i_alpha_med
        MOVW      DP,#_sinthetae
        NEGF32    R0H,R0H               ; |549| 
        MOV32     R1H,@_sinthetae
        MOVW      DP,#_i_beta_med
        MOV32     R2H,@_i_beta_med
        MOVW      DP,#_costhetae

        MOV32     R1H,@_costhetae
||      MPYF32    R0H,R1H,R0H           ; |549| 

        MPYF32    R1H,R1H,R2H           ; |549| 
        NOP
        ADDF32    R0H,R1H,R0H           ; |549| 
        MOVW      DP,#_Iq_med
        MOV32     @_Iq_med,R0H
	.line	512
;----------------------------------------------------------------------
; 552 | wsl = invTau_r*Isq/Isd;                                                
;----------------------------------------------------------------------
        MOVIZ     R0H,#16667            ; |552| 
        MOVXI     R0H,#54631            ; |552| 
        MOV32     R1H,@_Isq

        MOV32     R1H,@_Isd
||      MPYF32    R0H,R0H,R1H           ; |552| 

        LCR       #FS$$DIV              ; |552| 
        ; call occurs [#FS$$DIV] ; |552| 
        MOVW      DP,#_wsl
        MOV32     @_wsl,R0H
	.line	513
;----------------------------------------------------------------------
; 553 | we  = P*wm_d3 + wsl;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_wm_d3
        MOV32     R0H,@_wm_d3
        MOVW      DP,#_wsl
        MPYF32    R0H,R0H,#16448        ; |553| 
        MOV32     R1H,@_wsl
        ADDF32    R0H,R1H,R0H           ; |553| 
        NOP
        MOV32     @_we,R0H
	.line	515
;----------------------------------------------------------------------
; 555 | thetae = thetaenm1 + we*Tm;                                            
; 557 | // ---------A�adido para mi funci�n J de coste--------------       
;----------------------------------------------------------------------
        MOV32     R1H,@_Tm
        MPYF32    R1H,R1H,R0H           ; |555| 
        MOV32     R0H,@_thetaenm1
        ADDF32    R0H,R1H,R0H           ; |555| 
        NOP
        MOV32     @_thetae,R0H
	.line	518
;----------------------------------------------------------------------
; 558 | thetae_p = thetaenm1 + we*Tm*3;                                        
; 559 | // ---------------------------------------------------------           
;----------------------------------------------------------------------
        MOV32     R1H,@_Tm
        MOV32     R0H,@_we
        MPYF32    R1H,R1H,R0H           ; |558| 
        NOP
        MPYF32    R1H,R1H,#16448        ; |558| 
        MOV32     R0H,@_thetaenm1
        ADDF32    R0H,R1H,R0H           ; |558| 
        NOP
        MOV32     @_thetae_p,R0H
	.line	521
;----------------------------------------------------------------------
; 561 | if (thetae > PI2)                                                      
; 563 |      thetae -= PI2;                                                    
;----------------------------------------------------------------------
        MOVIZ     R0H,#16585            ; |561| 
        MOVXI     R0H,#4048             ; |561| 
        MOV32     R1H,@_thetae
        CMPF32    R1H,R0H               ; |561| 
        MOVST0    ZF, NF                ; |561| 
        B         $C$L28,GT             ; |561| 
        ; branchcc occurs ; |561| 
	.line	525
;----------------------------------------------------------------------
; 565 | else if (thetae<0)                                                     
;----------------------------------------------------------------------
        MOV32     R0H,@_thetae
        CMPF32    R0H,#0                ; |565| 
        MOVST0    ZF, NF                ; |565| 
        B         $C$L29,GEQ            ; |565| 
        ; branchcc occurs ; |565| 
	.line	527
;----------------------------------------------------------------------
; 567 | thetae += PI2;                                                         
; 570 | // ---------A�adido para mi funci�n J de coste--------------       
;----------------------------------------------------------------------
        MOVIZ     R0H,#16585            ; |567| 
        MOVXI     R0H,#4048             ; |567| 
        ADDF32    R0H,R0H,R1H           ; |567| 
        NOP
        MOV32     @_thetae,R0H
        B         $C$L29,UNC            ; |567| 
        ; branch occurs ; |567| 
$C$L28:    
	.line	523
        MOVIZ     R0H,#16585            ; |563| 
        MOVXI     R0H,#4048             ; |563| 
        SUBF32    R0H,R1H,R0H           ; |563| 
        NOP
        MOV32     @_thetae,R0H
$C$L29:    
	.line	531
;----------------------------------------------------------------------
; 571 | if (thetae_p > PI2)                                                    
; 573 |      thetae_p -= PI2;                                                  
;----------------------------------------------------------------------
        MOVIZ     R0H,#16585            ; |571| 
        MOV32     R1H,@_thetae_p
        MOVXI     R0H,#4048             ; |571| 
        CMPF32    R1H,R0H               ; |571| 
        MOVST0    ZF, NF                ; |571| 
        B         $C$L30,GT             ; |571| 
        ; branchcc occurs ; |571| 
	.line	535
;----------------------------------------------------------------------
; 575 | else if (thetae_p<0)                                                   
;----------------------------------------------------------------------
        MOV32     R0H,@_thetae_p
        CMPF32    R0H,#0                ; |575| 
        MOVST0    ZF, NF                ; |575| 
        B         $C$L31,GEQ            ; |575| 
        ; branchcc occurs ; |575| 
	.line	537
;----------------------------------------------------------------------
; 577 | thetae_p += PI2;                                                       
; 579 | // ---------------------------------------------------------           
;----------------------------------------------------------------------
        MOVIZ     R0H,#16585            ; |577| 
        MOVXI     R0H,#4048             ; |577| 
        ADDF32    R0H,R0H,R1H           ; |577| 
        NOP
        MOV32     @_thetae_p,R0H
        B         $C$L31,UNC            ; |577| 
        ; branch occurs ; |577| 
$C$L30:    
	.line	533
        MOVIZ     R0H,#16585            ; |573| 
        MOVXI     R0H,#4048             ; |573| 
        SUBF32    R0H,R1H,R0H           ; |573| 
        NOP
        MOV32     @_thetae_p,R0H
$C$L31:    
	.line	540
;----------------------------------------------------------------------
; 580 | sinthetae = sin(thetae);                                               
;----------------------------------------------------------------------
        MOV32     R0H,@_thetae
        LCR       #_sin                 ; |580| 
        ; call occurs [#_sin] ; |580| 
        MOVW      DP,#_sinthetae
        MOV32     @_sinthetae,R0H
	.line	541
;----------------------------------------------------------------------
; 581 | costhetae = cos(thetae);                                               
; 582 | // ---------A�adido para mi funci�n J de coste--------------       
;----------------------------------------------------------------------
        MOVW      DP,#_thetae
        MOV32     R0H,@_thetae
        LCR       #_cos                 ; |581| 
        ; call occurs [#_cos] ; |581| 
        MOVW      DP,#_costhetae
        MOV32     @_costhetae,R0H
	.line	543
;----------------------------------------------------------------------
; 583 | sinthetae_p = sin(thetae_p);                                           
;----------------------------------------------------------------------
        MOVW      DP,#_thetae_p
        MOV32     R0H,@_thetae_p
        LCR       #_sin                 ; |583| 
        ; call occurs [#_sin] ; |583| 
        MOVW      DP,#_sinthetae_p
        MOV32     @_sinthetae_p,R0H
	.line	544
;----------------------------------------------------------------------
; 584 | costhetae_p = cos(thetae_p);                                           
; 585 | // ---------------------------------------------------------           
;----------------------------------------------------------------------
        MOV32     R0H,@_thetae_p
        LCR       #_cos                 ; |584| 
        ; call occurs [#_cos] ; |584| 
        MOVW      DP,#_costhetae_p
        MOV32     @_costhetae_p,R0H
	.line	546
;----------------------------------------------------------------------
; 586 | thetaenm1 = thetae;                                                    
; 589 | //TRIGGER_TOGGLE_55;                                                   
; 590 | //----------------------- End Estimador de Posici�n -----------------
;     | ----------                                                             
;----------------------------------------------------------------------
        MOVL      ACC,@_thetae          ; |586| 
        MOVL      @_thetaenm1,ACC       ; |586| 
	.line	554
;----------------------------------------------------------------------
; 594 | if ((dsampled >=DOWNSAMP)&&(mstart))                                   
; 596 | fds = 1;                                                               
; 597 | dsampled=0;                                                            
; 599 | else                                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_mstart
        MOV32     R0H,@_mstart
        CMPF32    R0H,#0                ; |594| 
        MOVST0    ZF, NF                ; |594| 
        BF        $C$L32,NEQ            ; |594| 
        ; branchcc occurs ; |594| 
	.line	561
;----------------------------------------------------------------------
; 601 | dsampled++;                                                            
; 603 | //Medida decimada:                                                     
;----------------------------------------------------------------------
        INC       @_dsampled            ; |601| 
        B         $C$L33,UNC            ; |601| 
        ; branch occurs ; |601| 
$C$L32:    
	.line	556
        MOVB      XAR2,#1               ; |596| 
	.line	557
        MOV       @_dsampled,#0         ; |597| 
$C$L33:    
	.line	565
;----------------------------------------------------------------------
; 605 | if ((logger < CANTIDAD_LOG)&&(fds)&&(mstart)) // (mstart2==1)          
; 608 | // Para pruebas OMEGAFINA MRA FEB 2023                                 
; 609 |      //svopt[logger] = (Uint16) (65.534 *(wm_k + 500));                
; 610 |      //svapr[logger] = global_cont_eqep_interr;                        
;----------------------------------------------------------------------
        CMP       @_logger,#15000       ; |605| 
        B         $C$L35,HIS            ; |605| 
        ; branchcc occurs ; |605| 
        MOV       AL,AR2
        BF        $C$L35,EQ             ; |605| 
        ; branchcc occurs ; |605| 
        CMPF32    R0H,#0                ; |605| 
        MOVST0    ZF, NF                ; |605| 
        BF        $C$L35,EQ             ; |605| 
        ; branchcc occurs ; |605| 
	.line	571
;----------------------------------------------------------------------
; 611 | wm_log[logger] = (Uint16) (65.534 *(wm_k + 500));                      
;----------------------------------------------------------------------
        MOVW      DP,#_wm_k
        MOVIZ     R0H,#17027            ; |611| 
        MOV32     R1H,@_wm_k
        ADDF32    R1H,R1H,#17402        ; |611| 
        MOVXI     R0H,#4456             ; |611| 
        MPYF32    R0H,R0H,R1H           ; |611| 
        MOVW      DP,#_logger
        F32TOUI16 R0H,R0H               ; |611| 
        MOVZ      AR0,@_logger          ; |611| 
        MOVL      XAR4,#_wm_log         ; |611| 
        MOV32     ACC,R0H
        MOV       *+XAR4[AR0],AL        ; |611| 
	.line	572
;----------------------------------------------------------------------
; 612 | eqep_1[logger] =  (Uint16) (eqep_timeout_period>>16);                  
;----------------------------------------------------------------------
        MOVZ      AR0,@_logger          ; |612| 
        MOVL      XAR4,#_eqep_1         ; |612| 
        MOVW      DP,#_eqep_timeout_period
        MOVL      ACC,@_eqep_timeout_period ; |612| 
        MOVH      *+XAR4[AR0],ACC << 0  ; |612| 
	.line	573
;----------------------------------------------------------------------
; 613 | eqep_2[logger] =  (Uint16) (eqep_timeout_period & 0x0000FFFF);         
;----------------------------------------------------------------------
        MOVW      DP,#_logger
        MOVZ      AR0,@_logger          ; |613| 
        MOVL      XAR4,#_eqep_2         ; |613| 
        MOVW      DP,#_eqep_timeout_period
        MOV       AL,@_eqep_timeout_period ; |613| 
        MOV       *+XAR4[AR0],AL        ; |613| 
	.line	574
;----------------------------------------------------------------------
; 614 | corr[logger] = (Uint16) corrections;                                   
;----------------------------------------------------------------------
        MOVW      DP,#_logger
        MOVZ      AR0,@_logger          ; |614| 
        MOVL      XAR4,#_corr           ; |614| 
        MOV       AL,@_corrections      ; |614| 
        MOV       *+XAR4[AR0],AL        ; |614| 
	.line	575
;----------------------------------------------------------------------
; 615 | if (logger==1000){                                                     
;----------------------------------------------------------------------
        CMP       @_logger,#1000        ; |615| 
        BF        $C$L34,NEQ            ; |615| 
        ; branchcc occurs ; |615| 
	.line	576
;----------------------------------------------------------------------
; 616 | wm_ref = 0.75*wnom;                                                    
; 618 | //vfila[logger] =  (Uint16) (1638.35 * (veloc_radsec + 20));           
;----------------------------------------------------------------------
        MOVIZ     R0H,#17053            ; |616| 
        MOVW      DP,#_wm_ref
        MOVXI     R0H,#5214             ; |616| 
        MOV32     @_wm_ref,R0H
$C$L34:    
	.line	580
;----------------------------------------------------------------------
; 620 | logger++;                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_logger
        INC       @_logger              ; |620| 
	.line	581
;----------------------------------------------------------------------
; 621 | fds=0;                                                                 
; 625 | // la actuaci�n debe ir al final del todo                            
; 626 | //------------------------------- Generaci�n de disparos ------------
;     | ----------------------*/                                               
;----------------------------------------------------------------------
        MOVB      XAR2,#0
$C$L35:    
	.line	587
;----------------------------------------------------------------------
; 627 | EPwm1Regs.CMPA.half.CMPA = (1-SCMP[sopt][0])*pwm_period;    // Load EPW
;     | M1 CMPA                                                                
;----------------------------------------------------------------------
        MOV       T,AR1                 ; |627| 
        MOVL      XAR4,#_SCMP           ; |627| 
        MPYB      ACC,T,#10             ; |627| 
        ADDL      XAR4,ACC
        UI16TOF32 R1H,@_pwm_period      ; |627| 
        MOV32     R0H,*+XAR4[0]
        SUBF32    R0H,#16256,R0H        ; |627| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |627| 
        NOP
        F32TOUI16 R0H,R0H               ; |627| 
        NOP
        MOVW      DP,#_EPwm1Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm1Regs+9,AL      ; |627| 
	.line	588
;----------------------------------------------------------------------
; 628 | EPwm2Regs.CMPA.half.CMPA = (1-SCMP[sopt][1])*pwm_period;    // Load EPW
;     | M2 CMPA                                                                
;----------------------------------------------------------------------
        MPYB      ACC,T,#10             ; |628| 
        MOVL      XAR4,#_SCMP+2         ; |628| 
        ADDL      XAR4,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |628| 
        MOV32     R0H,*+XAR4[0]
        SUBF32    R0H,#16256,R0H        ; |628| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |628| 
        NOP
        F32TOUI16 R0H,R0H               ; |628| 
        NOP
        MOVW      DP,#_EPwm2Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm2Regs+9,AL      ; |628| 
	.line	589
;----------------------------------------------------------------------
; 629 | EPwm3Regs.CMPA.half.CMPA = (1-SCMP[sopt][2])*pwm_period;    // Load EPW
;     | M3 CMPA                                                                
;----------------------------------------------------------------------
        MPYB      ACC,T,#10             ; |629| 
        MOVL      XAR4,#_SCMP+4         ; |629| 
        ADDL      XAR4,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |629| 
        MOV32     R0H,*+XAR4[0]
        SUBF32    R0H,#16256,R0H        ; |629| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |629| 
        NOP
        F32TOUI16 R0H,R0H               ; |629| 
        NOP
        MOVW      DP,#_EPwm3Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm3Regs+9,AL      ; |629| 
	.line	590
;----------------------------------------------------------------------
; 630 | EPwm4Regs.CMPA.half.CMPA = (1-SCMP[sopt][3])*pwm_period;    // Load EPW
;     | M4 CMPA                                                                
;----------------------------------------------------------------------
        MPYB      ACC,T,#10             ; |630| 
        MOVL      XAR4,#_SCMP+6         ; |630| 
        ADDL      XAR4,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |630| 
        MOV32     R0H,*+XAR4[0]
        SUBF32    R0H,#16256,R0H        ; |630| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |630| 
        NOP
        F32TOUI16 R0H,R0H               ; |630| 
        NOP
        MOVW      DP,#_EPwm4Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm4Regs+9,AL      ; |630| 
	.line	591
;----------------------------------------------------------------------
; 631 | EPwm5Regs.CMPA.half.CMPA = (1-SCMP[sopt][4])*pwm_period;    // Load EPW
;     | M5 CMPA                                                                
; 633 | //TRIGGER_TOGGLE_54;                                                   
;----------------------------------------------------------------------
        MPYB      ACC,T,#10             ; |631| 
        MOVL      XAR4,#_SCMP+8         ; |631| 
        ADDL      XAR4,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |631| 
        MOV32     R0H,*+XAR4[0]
        SUBF32    R0H,#16256,R0H        ; |631| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |631| 
        NOP
        F32TOUI16 R0H,R0H               ; |631| 
        NOP
        MOVW      DP,#_EPwm5Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm5Regs+9,AL      ; |631| 
	.line	594
;----------------------------------------------------------------------
; 634 | TRIGGER_TOGGLE_57;                                                     
; 637 | }// while(!stop)                                                       
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xfdff ; |634| 
        OR        AL,#0x0200            ; |634| 
        MOV       @_GpioDataRegs+15,AL  ; |634| 
	.line	299
        MOVW      DP,#_stop
        MOV       AL,@_stop             ; |339| 
        BF        $C$L17,EQ             ; |339| 
        ; branchcc occurs ; |339| 
$C$L36:    
	.line	599
;----------------------------------------------------------------------
; 639 | DINT;                                                                  
;----------------------------------------------------------------------
 setc INTM
	.line	601
;----------------------------------------------------------------------
; 641 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	603
;----------------------------------------------------------------------
; 643 | GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 0;       // Configure GPIO0 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfffc ; |643| 
	.line	604
;----------------------------------------------------------------------
; 644 | GpioCtrlRegs.GPADIR.bit.GPIO0         = 1;       // Configure GPIO0 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffe ; |644| 
        ORB       AL,#0x01              ; |644| 
        MOV       @_GpioCtrlRegs+10,AL  ; |644| 
	.line	605
;----------------------------------------------------------------------
; 645 | GpioDataRegs.GPACLEAR.bit.GPIO0          = 1;       // Clear GPIO0     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffe ; |645| 
        ORB       AL,#0x01              ; |645| 
        MOV       @_GpioDataRegs+4,AL   ; |645| 
	.line	607
;----------------------------------------------------------------------
; 647 | GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 0;       // Configure GPIO1 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfff3 ; |647| 
	.line	608
;----------------------------------------------------------------------
; 648 | GpioCtrlRegs.GPADIR.bit.GPIO1         = 1;       // Configure GPIO1 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffd ; |648| 
        ORB       AL,#0x02              ; |648| 
        MOV       @_GpioCtrlRegs+10,AL  ; |648| 
	.line	609
;----------------------------------------------------------------------
; 649 | GpioDataRegs.GPACLEAR.bit.GPIO1          = 1;       // Clear GPIO1     
; 650 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffd ; |649| 
        ORB       AL,#0x02              ; |649| 
        MOV       @_GpioDataRegs+4,AL   ; |649| 
	.line	612
;----------------------------------------------------------------------
; 652 | GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 0;       // Configure GPIO2 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xffcf ; |652| 
	.line	613
;----------------------------------------------------------------------
; 653 | GpioCtrlRegs.GPADIR.bit.GPIO2         = 1;       // Configure GPIO2 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffb ; |653| 
        ORB       AL,#0x04              ; |653| 
        MOV       @_GpioCtrlRegs+10,AL  ; |653| 
	.line	614
;----------------------------------------------------------------------
; 654 | GpioDataRegs.GPACLEAR.bit.GPIO2          = 1;       // Clear GPIO2     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffb ; |654| 
        ORB       AL,#0x04              ; |654| 
        MOV       @_GpioDataRegs+4,AL   ; |654| 
	.line	616
;----------------------------------------------------------------------
; 656 | GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 0;       // Configure GPIO3 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xff3f ; |656| 
	.line	617
;----------------------------------------------------------------------
; 657 | GpioCtrlRegs.GPADIR.bit.GPIO3         = 1;       // Configure GPIO3 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfff7 ; |657| 
        ORB       AL,#0x08              ; |657| 
        MOV       @_GpioCtrlRegs+10,AL  ; |657| 
	.line	618
;----------------------------------------------------------------------
; 658 | GpioDataRegs.GPACLEAR.bit.GPIO3          = 1;       // Clear GPIO3     
; 659 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfff7 ; |658| 
        ORB       AL,#0x08              ; |658| 
        MOV       @_GpioDataRegs+4,AL   ; |658| 
	.line	621
;----------------------------------------------------------------------
; 661 | GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 0;       // Configure GPIO4 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfcff ; |661| 
	.line	622
;----------------------------------------------------------------------
; 662 | GpioCtrlRegs.GPADIR.bit.GPIO4         = 1;       // Configure GPIO4 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffef ; |662| 
        ORB       AL,#0x10              ; |662| 
        MOV       @_GpioCtrlRegs+10,AL  ; |662| 
	.line	623
;----------------------------------------------------------------------
; 663 | GpioDataRegs.GPACLEAR.bit.GPIO4          = 1;       // Clear GPIO4     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffef ; |663| 
        ORB       AL,#0x10              ; |663| 
        MOV       @_GpioDataRegs+4,AL   ; |663| 
	.line	625
;----------------------------------------------------------------------
; 665 | GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 0;       // Configure GPIO5 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xf3ff ; |665| 
	.line	626
;----------------------------------------------------------------------
; 666 | GpioCtrlRegs.GPADIR.bit.GPIO5         = 1;       // Configure GPIO5 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffdf ; |666| 
        ORB       AL,#0x20              ; |666| 
        MOV       @_GpioCtrlRegs+10,AL  ; |666| 
	.line	627
;----------------------------------------------------------------------
; 667 | GpioDataRegs.GPACLEAR.bit.GPIO5          = 1;       // Clear GPIO5     
; 668 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffdf ; |667| 
        ORB       AL,#0x20              ; |667| 
        MOV       @_GpioDataRegs+4,AL   ; |667| 
	.line	630
;----------------------------------------------------------------------
; 670 | GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 0;       // Configure GPIO6 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xcfff ; |670| 
	.line	631
;----------------------------------------------------------------------
; 671 | GpioCtrlRegs.GPADIR.bit.GPIO6         = 1;       // Configure GPIO6 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffbf ; |671| 
        ORB       AL,#0x40              ; |671| 
        MOV       @_GpioCtrlRegs+10,AL  ; |671| 
	.line	632
;----------------------------------------------------------------------
; 672 | GpioDataRegs.GPACLEAR.bit.GPIO6          = 1;       // Clear GPIO6     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffbf ; |672| 
        ORB       AL,#0x40              ; |672| 
        MOV       @_GpioDataRegs+4,AL   ; |672| 
	.line	634
;----------------------------------------------------------------------
; 674 | GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 0;       // Configure GPIO7 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0x3fff ; |674| 
	.line	635
;----------------------------------------------------------------------
; 675 | GpioCtrlRegs.GPADIR.bit.GPIO7         = 1;       // Configure GPIO7 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xff7f ; |675| 
        ORB       AL,#0x80              ; |675| 
        MOV       @_GpioCtrlRegs+10,AL  ; |675| 
	.line	636
;----------------------------------------------------------------------
; 676 | GpioDataRegs.GPACLEAR.bit.GPIO7          = 1;       // Clear GPIO7     
; 677 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xff7f ; |676| 
        ORB       AL,#0x80              ; |676| 
        MOV       @_GpioDataRegs+4,AL   ; |676| 
	.line	639
;----------------------------------------------------------------------
; 679 | GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 0;       // Configure GPIO8 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xfffc ; |679| 
	.line	640
;----------------------------------------------------------------------
; 680 | GpioCtrlRegs.GPADIR.bit.GPIO8         = 1;       // Configure GPIO8 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfeff ; |680| 
        OR        AL,#0x0100            ; |680| 
        MOV       @_GpioCtrlRegs+10,AL  ; |680| 
	.line	641
;----------------------------------------------------------------------
; 681 | GpioDataRegs.GPACLEAR.bit.GPIO8          = 1;       // Clear GPIO8     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfeff ; |681| 
        OR        AL,#0x0100            ; |681| 
        MOV       @_GpioDataRegs+4,AL   ; |681| 
	.line	643
;----------------------------------------------------------------------
; 683 | GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 0;       // Configure GPIO9 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xfff3 ; |683| 
	.line	644
;----------------------------------------------------------------------
; 684 | GpioCtrlRegs.GPADIR.bit.GPIO9         = 1;       // Configure GPIO9 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfdff ; |684| 
        OR        AL,#0x0200            ; |684| 
        MOV       @_GpioCtrlRegs+10,AL  ; |684| 
	.line	645
;----------------------------------------------------------------------
; 685 | GpioDataRegs.GPACLEAR.bit.GPIO9          = 1;       // Clear GPIO9     
; 686 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfdff ; |685| 
        OR        AL,#0x0200            ; |685| 
        MOV       @_GpioDataRegs+4,AL   ; |685| 
	.line	648
;----------------------------------------------------------------------
; 688 | GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 0;       // Configure GPIO10 as
;     |  Digital I/O                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xffcf ; |688| 
	.line	649
;----------------------------------------------------------------------
; 689 | GpioCtrlRegs.GPADIR.bit.GPIO10        = 1;       // Configure GPIO10 as
;     |  digital Output                                                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfbff ; |689| 
        OR        AL,#0x0400            ; |689| 
        MOV       @_GpioCtrlRegs+10,AL  ; |689| 
	.line	650
;----------------------------------------------------------------------
; 690 | GpioDataRegs.GPACLEAR.bit.GPIO10            = 1;       // Clear GPIO10 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfbff ; |690| 
        OR        AL,#0x0400            ; |690| 
        MOV       @_GpioDataRegs+4,AL   ; |690| 
	.line	652
;----------------------------------------------------------------------
; 692 | GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 0;       // Configure GPIO11 as
;     |  Digital I/O                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xff3f ; |692| 
	.line	653
;----------------------------------------------------------------------
; 693 | GpioCtrlRegs.GPADIR.bit.GPIO11        = 1;       // Configure GPIO11 as
;     |  digital Output                                                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xf7ff ; |693| 
        OR        AL,#0x0800            ; |693| 
        MOV       @_GpioCtrlRegs+10,AL  ; |693| 
	.line	654
;----------------------------------------------------------------------
; 694 | GpioDataRegs.GPACLEAR.bit.GPIO11          = 1;       // Clear GPIO11   
; 695 | //#####################################################################
;     | ######################                                                 
; 696 |                               // Enable writing to EALLOW protected reg
;     | isters                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xf7ff ; |694| 
        OR        AL,#0x0800            ; |694| 
        MOV       @_GpioDataRegs+4,AL   ; |694| 
	.line	657
;----------------------------------------------------------------------
; 697 | SysCtrlRegs.WDCR = 0x000F;            // Enable Watchdog and write inco
;     | rrect WD Check Bits                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+25
        MOVB      @_SysCtrlRegs+25,#15,UNC ; |697| 
	.line	658
;----------------------------------------------------------------------
; 698 | asm(" NOP");                                                           
;----------------------------------------------------------------------
 NOP
	.line	659
;----------------------------------------------------------------------
; 699 | asm(" NOP");                                                           
;----------------------------------------------------------------------
 NOP
	.line	660
;----------------------------------------------------------------------
; 700 | asm(" NOP");                                                           
;----------------------------------------------------------------------
 NOP
	.line	661
;----------------------------------------------------------------------
; 701 | }// void main ()                                                       
;----------------------------------------------------------------------
        SPM       #0
        MOV32     R7H,*--SP
        MOV32     R6H,*--SP
        MOV32     R5H,*--SP
        MOV32     R4H,*--SP
        MOVL      XAR2,*--SP
        MOVL      XAR1,*--SP
        LRETR
        ; return occurs
	.endfunc	701,00cccc078h,12
;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_sin
	.global	_cos
	.global	FS$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************
	.sym	_PINT, 0, 144, 13, 22
	.sym	_int16, 0, 4, 13, 16
	.sym	_Uint16, 0, 14, 13, 16
	.sym	_Uint16, 0, 14, 13, 16
	.sym	_int32, 0, 5, 13, 32
	.sym	_Uint32, 0, 15, 13, 32
	.sym	_Uint32, 0, 15, 13, 32
	.sym	_float32, 0, 6, 13, 32
	.sym	_float64, 0, 11, 13, 64
	.stag	_TBCTL_BITS, 16
	.member	_CTRMODE, 0, 14, 18, 2
	.member	_PHSEN, 2, 14, 18, 1
	.member	_PRDLD, 3, 14, 18, 1
	.member	_SYNCOSEL, 4, 14, 18, 2
	.member	_SWFSYNC, 6, 14, 18, 1
	.member	_HSPCLKDIV, 7, 14, 18, 3
	.member	_CLKDIV, 10, 14, 18, 3
	.member	_PHSDIR, 13, 14, 18, 1
	.member	_FREE_SOFT, 14, 14, 18, 2
	.eos
	.utag	_TBCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TBCTL_BITS
	.eos
	.stag	_TBSTS_BITS, 16
	.member	_CTRDIR, 0, 14, 18, 1
	.member	_SYNCI, 1, 14, 18, 1
	.member	_CTRMAX, 2, 14, 18, 1
	.member	_rsvd1, 3, 14, 18, 13
	.eos
	.utag	_TBSTS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TBSTS_BITS
	.eos
	.stag	_TBPHS_HRPWM_REG, 32
	.member	_TBPHSHR, 0, 14, 8, 16
	.member	_TBPHS, 16, 14, 8, 16
	.eos
	.utag	_TBPHS_HRPWM_GROUP, 32
	.member	_all, 0, 15, 11, 32
	.member	_half, 0, 8, 11, 32, _TBPHS_HRPWM_REG
	.eos
	.stag	_CMPCTL_BITS, 16
	.member	_LOADAMODE, 0, 14, 18, 2
	.member	_LOADBMODE, 2, 14, 18, 2
	.member	_SHDWAMODE, 4, 14, 18, 1
	.member	_rsvd1, 5, 14, 18, 1
	.member	_SHDWBMODE, 6, 14, 18, 1
	.member	_rsvd2, 7, 14, 18, 1
	.member	_SHDWAFULL, 8, 14, 18, 1
	.member	_SHDWBFULL, 9, 14, 18, 1
	.member	_rsvd3, 10, 14, 18, 6
	.eos
	.utag	_CMPCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _CMPCTL_BITS
	.eos
	.stag	_CMPA_HRPWM_REG, 32
	.member	_CMPAHR, 0, 14, 8, 16
	.member	_CMPA, 16, 14, 8, 16
	.eos
	.utag	_CMPA_HRPWM_GROUP, 32
	.member	_all, 0, 15, 11, 32
	.member	_half, 0, 8, 11, 32, _CMPA_HRPWM_REG
	.eos
	.stag	_AQCTL_BITS, 16
	.member	_ZRO, 0, 14, 18, 2
	.member	_PRD, 2, 14, 18, 2
	.member	_CAU, 4, 14, 18, 2
	.member	_CAD, 6, 14, 18, 2
	.member	_CBU, 8, 14, 18, 2
	.member	_CBD, 10, 14, 18, 2
	.member	_rsvd, 12, 14, 18, 4
	.eos
	.utag	_AQCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _AQCTL_BITS
	.eos
	.stag	_AQSFRC_BITS, 16
	.member	_ACTSFA, 0, 14, 18, 2
	.member	_OTSFA, 2, 14, 18, 1
	.member	_ACTSFB, 3, 14, 18, 2
	.member	_OTSFB, 5, 14, 18, 1
	.member	_RLDCSF, 6, 14, 18, 2
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_AQSFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _AQSFRC_BITS
	.eos
	.stag	_AQCSFRC_BITS, 16
	.member	_CSFA, 0, 14, 18, 2
	.member	_CSFB, 2, 14, 18, 2
	.member	_rsvd1, 4, 14, 18, 12
	.eos
	.utag	_AQCSFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _AQCSFRC_BITS
	.eos
	.stag	_DBCTL_BITS, 16
	.member	_OUT_MODE, 0, 14, 18, 2
	.member	_POLSEL, 2, 14, 18, 2
	.member	_IN_MODE, 4, 14, 18, 2
	.member	_rsvd1, 6, 14, 18, 10
	.eos
	.utag	_DBCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DBCTL_BITS
	.eos
	.stag	_TZSEL_BITS, 16
	.member	_CBC1, 0, 14, 18, 1
	.member	_CBC2, 1, 14, 18, 1
	.member	_CBC3, 2, 14, 18, 1
	.member	_CBC4, 3, 14, 18, 1
	.member	_CBC5, 4, 14, 18, 1
	.member	_CBC6, 5, 14, 18, 1
	.member	_rsvd1, 6, 14, 18, 2
	.member	_OSHT1, 8, 14, 18, 1
	.member	_OSHT2, 9, 14, 18, 1
	.member	_OSHT3, 10, 14, 18, 1
	.member	_OSHT4, 11, 14, 18, 1
	.member	_OSHT5, 12, 14, 18, 1
	.member	_OSHT6, 13, 14, 18, 1
	.member	_rsvd2, 14, 14, 18, 2
	.eos
	.utag	_TZSEL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZSEL_BITS
	.eos
	.stag	_TZCTL_BITS, 16
	.member	_TZA, 0, 14, 18, 2
	.member	_TZB, 2, 14, 18, 2
	.member	_rsvd, 4, 14, 18, 12
	.eos
	.utag	_TZCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZCTL_BITS
	.eos
	.stag	_TZEINT_BITS, 16
	.member	_rsvd1, 0, 14, 18, 1
	.member	_CBC, 1, 14, 18, 1
	.member	_OST, 2, 14, 18, 1
	.member	_rsvd2, 3, 14, 18, 13
	.eos
	.utag	_TZEINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZEINT_BITS
	.eos
	.stag	_TZFLG_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_CBC, 1, 14, 18, 1
	.member	_OST, 2, 14, 18, 1
	.member	_rsvd2, 3, 14, 18, 13
	.eos
	.utag	_TZFLG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZFLG_BITS
	.eos
	.stag	_TZCLR_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_CBC, 1, 14, 18, 1
	.member	_OST, 2, 14, 18, 1
	.member	_rsvd2, 3, 14, 18, 13
	.eos
	.utag	_TZCLR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZCLR_BITS
	.eos
	.stag	_TZFRC_BITS, 16
	.member	_rsvd1, 0, 14, 18, 1
	.member	_CBC, 1, 14, 18, 1
	.member	_OST, 2, 14, 18, 1
	.member	_rsvd2, 3, 14, 18, 13
	.eos
	.utag	_TZFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZFRC_BITS
	.eos
	.stag	_ETSEL_BITS, 16
	.member	_INTSEL, 0, 14, 18, 3
	.member	_INTEN, 3, 14, 18, 1
	.member	_rsvd1, 4, 14, 18, 4
	.member	_SOCASEL, 8, 14, 18, 3
	.member	_SOCAEN, 11, 14, 18, 1
	.member	_SOCBSEL, 12, 14, 18, 3
	.member	_SOCBEN, 15, 14, 18, 1
	.eos
	.utag	_ETSEL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETSEL_BITS
	.eos
	.stag	_ETPS_BITS, 16
	.member	_INTPRD, 0, 14, 18, 2
	.member	_INTCNT, 2, 14, 18, 2
	.member	_rsvd1, 4, 14, 18, 4
	.member	_SOCAPRD, 8, 14, 18, 2
	.member	_SOCACNT, 10, 14, 18, 2
	.member	_SOCBPRD, 12, 14, 18, 2
	.member	_SOCBCNT, 14, 14, 18, 2
	.eos
	.utag	_ETPS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETPS_BITS
	.eos
	.stag	_ETFLG_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_SOCA, 2, 14, 18, 1
	.member	_SOCB, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_ETFLG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETFLG_BITS
	.eos
	.stag	_ETCLR_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_SOCA, 2, 14, 18, 1
	.member	_SOCB, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_ETCLR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETCLR_BITS
	.eos
	.stag	_ETFRC_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_SOCA, 2, 14, 18, 1
	.member	_SOCB, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_ETFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETFRC_BITS
	.eos
	.stag	_PCCTL_BITS, 16
	.member	_CHPEN, 0, 14, 18, 1
	.member	_OSHTWTH, 1, 14, 18, 4
	.member	_CHPFREQ, 5, 14, 18, 3
	.member	_CHPDUTY, 8, 14, 18, 3
	.member	_rsvd1, 11, 14, 18, 5
	.eos
	.utag	_PCCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCCTL_BITS
	.eos
	.stag	_HRCNFG_BITS, 16
	.member	_EDGMODE, 0, 14, 18, 2
	.member	_CTLMODE, 2, 14, 18, 1
	.member	_HRLOAD, 3, 14, 18, 1
	.member	_rsvd1, 4, 14, 18, 12
	.eos
	.utag	_HRCNFG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _HRCNFG_BITS
	.eos
	.stag	_EPWM_REGS, 544
	.member	_TBCTL, 0, 9, 8, 16, _TBCTL_REG
	.member	_TBSTS, 16, 9, 8, 16, _TBSTS_REG
	.member	_TBPHS, 32, 9, 8, 32, _TBPHS_HRPWM_GROUP
	.member	_TBCTR, 64, 14, 8, 16
	.member	_TBPRD, 80, 14, 8, 16
	.member	_rsvd1, 96, 14, 8, 16
	.member	_CMPCTL, 112, 9, 8, 16, _CMPCTL_REG
	.member	_CMPA, 128, 9, 8, 32, _CMPA_HRPWM_GROUP
	.member	_CMPB, 160, 14, 8, 16
	.member	_AQCTLA, 176, 9, 8, 16, _AQCTL_REG
	.member	_AQCTLB, 192, 9, 8, 16, _AQCTL_REG
	.member	_AQSFRC, 208, 9, 8, 16, _AQSFRC_REG
	.member	_AQCSFRC, 224, 9, 8, 16, _AQCSFRC_REG
	.member	_DBCTL, 240, 9, 8, 16, _DBCTL_REG
	.member	_DBRED, 256, 14, 8, 16
	.member	_DBFED, 272, 14, 8, 16
	.member	_TZSEL, 288, 9, 8, 16, _TZSEL_REG
	.member	_rsvd2, 304, 14, 8, 16
	.member	_TZCTL, 320, 9, 8, 16, _TZCTL_REG
	.member	_TZEINT, 336, 9, 8, 16, _TZEINT_REG
	.member	_TZFLG, 352, 9, 8, 16, _TZFLG_REG
	.member	_TZCLR, 368, 9, 8, 16, _TZCLR_REG
	.member	_TZFRC, 384, 9, 8, 16, _TZFRC_REG
	.member	_ETSEL, 400, 9, 8, 16, _ETSEL_REG
	.member	_ETPS, 416, 9, 8, 16, _ETPS_REG
	.member	_ETFLG, 432, 9, 8, 16, _ETFLG_REG
	.member	_ETCLR, 448, 9, 8, 16, _ETCLR_REG
	.member	_ETFRC, 464, 9, 8, 16, _ETFRC_REG
	.member	_PCCTL, 480, 9, 8, 16, _PCCTL_REG
	.member	_rsvd3, 496, 14, 8, 16
	.member	_HRCNFG, 512, 9, 8, 16, _HRCNFG_REG
	.eos
	.stag	_ADCTRL1_BITS, 16
	.member	_rsvd1, 0, 14, 18, 4
	.member	_SEQ_CASC, 4, 14, 18, 1
	.member	_SEQ_OVRD, 5, 14, 18, 1
	.member	_CONT_RUN, 6, 14, 18, 1
	.member	_CPS, 7, 14, 18, 1
	.member	_ACQ_PS, 8, 14, 18, 4
	.member	_SUSMOD, 12, 14, 18, 2
	.member	_RESET, 14, 14, 18, 1
	.member	_rsvd2, 15, 14, 18, 1
	.eos
	.utag	_ADCTRL1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCTRL1_BITS
	.eos
	.stag	_ADCTRL2_BITS, 16
	.member	_EPWM_SOCB_SEQ2, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_INT_MOD_SEQ2, 2, 14, 18, 1
	.member	_INT_ENA_SEQ2, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 1
	.member	_SOC_SEQ2, 5, 14, 18, 1
	.member	_RST_SEQ2, 6, 14, 18, 1
	.member	_EXT_SOC_SEQ1, 7, 14, 18, 1
	.member	_EPWM_SOCA_SEQ1, 8, 14, 18, 1
	.member	_rsvd3, 9, 14, 18, 1
	.member	_INT_MOD_SEQ1, 10, 14, 18, 1
	.member	_INT_ENA_SEQ1, 11, 14, 18, 1
	.member	_rsvd4, 12, 14, 18, 1
	.member	_SOC_SEQ1, 13, 14, 18, 1
	.member	_RST_SEQ1, 14, 14, 18, 1
	.member	_EPWM_SOCB_SEQ, 15, 14, 18, 1
	.eos
	.utag	_ADCTRL2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCTRL2_BITS
	.eos
	.stag	_ADCMAXCONV_BITS, 16
	.member	_MAX_CONV1, 0, 14, 18, 4
	.member	_MAX_CONV2, 4, 14, 18, 3
	.member	_rsvd1, 7, 14, 18, 9
	.eos
	.utag	_ADCMAXCONV_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCMAXCONV_BITS
	.eos
	.stag	_ADCCHSELSEQ1_BITS, 16
	.member	_CONV00, 0, 14, 18, 4
	.member	_CONV01, 4, 14, 18, 4
	.member	_CONV02, 8, 14, 18, 4
	.member	_CONV03, 12, 14, 18, 4
	.eos
	.utag	_ADCCHSELSEQ1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCCHSELSEQ1_BITS
	.eos
	.stag	_ADCCHSELSEQ2_BITS, 16
	.member	_CONV04, 0, 14, 18, 4
	.member	_CONV05, 4, 14, 18, 4
	.member	_CONV06, 8, 14, 18, 4
	.member	_CONV07, 12, 14, 18, 4
	.eos
	.utag	_ADCCHSELSEQ2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCCHSELSEQ2_BITS
	.eos
	.stag	_ADCCHSELSEQ3_BITS, 16
	.member	_CONV08, 0, 14, 18, 4
	.member	_CONV09, 4, 14, 18, 4
	.member	_CONV10, 8, 14, 18, 4
	.member	_CONV11, 12, 14, 18, 4
	.eos
	.utag	_ADCCHSELSEQ3_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCCHSELSEQ3_BITS
	.eos
	.stag	_ADCCHSELSEQ4_BITS, 16
	.member	_CONV12, 0, 14, 18, 4
	.member	_CONV13, 4, 14, 18, 4
	.member	_CONV14, 8, 14, 18, 4
	.member	_CONV15, 12, 14, 18, 4
	.eos
	.utag	_ADCCHSELSEQ4_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCCHSELSEQ4_BITS
	.eos
	.stag	_ADCASEQSR_BITS, 16
	.member	_SEQ1_STATE, 0, 14, 18, 4
	.member	_SEQ2_STATE, 4, 14, 18, 3
	.member	_rsvd1, 7, 14, 18, 1
	.member	_SEQ_CNTR, 8, 14, 18, 4
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_ADCASEQSR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCASEQSR_BITS
	.eos
	.stag	_ADCTRL3_BITS, 16
	.member	_SMODE_SEL, 0, 14, 18, 1
	.member	_ADCCLKPS, 1, 14, 18, 4
	.member	_ADCPWDN, 5, 14, 18, 1
	.member	_ADCBGRFDN, 6, 14, 18, 2
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_ADCTRL3_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCTRL3_BITS
	.eos
	.stag	_ADCST_BITS, 16
	.member	_INT_SEQ1, 0, 14, 18, 1
	.member	_INT_SEQ2, 1, 14, 18, 1
	.member	_SEQ1_BSY, 2, 14, 18, 1
	.member	_SEQ2_BSY, 3, 14, 18, 1
	.member	_INT_SEQ1_CLR, 4, 14, 18, 1
	.member	_INT_SEQ2_CLR, 5, 14, 18, 1
	.member	_EOS_BUF1, 6, 14, 18, 1
	.member	_EOS_BUF2, 7, 14, 18, 1
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_ADCST_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCST_BITS
	.eos
	.stag	_ADCREFSEL_BITS, 16
	.member	_rsvd1, 0, 14, 18, 14
	.member	_REF_SEL, 14, 14, 18, 2
	.eos
	.utag	_ADCREFSEL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCREFSEL_BITS
	.eos
	.stag	_ADCOFFTRIM_BITS, 16
	.member	_OFFSET_TRIM, 0, 4, 18, 9
	.member	_rsvd1, 9, 14, 18, 7
	.eos
	.utag	_ADCOFFTRIM_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCOFFTRIM_BITS
	.eos
	.stag	_ADC_REGS, 480
	.member	_ADCTRL1, 0, 9, 8, 16, _ADCTRL1_REG
	.member	_ADCTRL2, 16, 9, 8, 16, _ADCTRL2_REG
	.member	_ADCMAXCONV, 32, 9, 8, 16, _ADCMAXCONV_REG
	.member	_ADCCHSELSEQ1, 48, 9, 8, 16, _ADCCHSELSEQ1_REG
	.member	_ADCCHSELSEQ2, 64, 9, 8, 16, _ADCCHSELSEQ2_REG
	.member	_ADCCHSELSEQ3, 80, 9, 8, 16, _ADCCHSELSEQ3_REG
	.member	_ADCCHSELSEQ4, 96, 9, 8, 16, _ADCCHSELSEQ4_REG
	.member	_ADCASEQSR, 112, 9, 8, 16, _ADCASEQSR_REG
	.member	_ADCRESULT0, 128, 14, 8, 16
	.member	_ADCRESULT1, 144, 14, 8, 16
	.member	_ADCRESULT2, 160, 14, 8, 16
	.member	_ADCRESULT3, 176, 14, 8, 16
	.member	_ADCRESULT4, 192, 14, 8, 16
	.member	_ADCRESULT5, 208, 14, 8, 16
	.member	_ADCRESULT6, 224, 14, 8, 16
	.member	_ADCRESULT7, 240, 14, 8, 16
	.member	_ADCRESULT8, 256, 14, 8, 16
	.member	_ADCRESULT9, 272, 14, 8, 16
	.member	_ADCRESULT10, 288, 14, 8, 16
	.member	_ADCRESULT11, 304, 14, 8, 16
	.member	_ADCRESULT12, 320, 14, 8, 16
	.member	_ADCRESULT13, 336, 14, 8, 16
	.member	_ADCRESULT14, 352, 14, 8, 16
	.member	_ADCRESULT15, 368, 14, 8, 16
	.member	_ADCTRL3, 384, 9, 8, 16, _ADCTRL3_REG
	.member	_ADCST, 400, 9, 8, 16, _ADCST_REG
	.member	_rsvd1, 416, 14, 8, 16
	.member	_rsvd2, 432, 14, 8, 16
	.member	_ADCREFSEL, 448, 9, 8, 16, _ADCREFSEL_REG
	.member	_ADCOFFTRIM, 464, 9, 8, 16, _ADCOFFTRIM_REG
	.eos
	.stag	_ADC_RESULT_MIRROR_REGS, 256
	.member	_ADCRESULT0, 0, 14, 8, 16
	.member	_ADCRESULT1, 16, 14, 8, 16
	.member	_ADCRESULT2, 32, 14, 8, 16
	.member	_ADCRESULT3, 48, 14, 8, 16
	.member	_ADCRESULT4, 64, 14, 8, 16
	.member	_ADCRESULT5, 80, 14, 8, 16
	.member	_ADCRESULT6, 96, 14, 8, 16
	.member	_ADCRESULT7, 112, 14, 8, 16
	.member	_ADCRESULT8, 128, 14, 8, 16
	.member	_ADCRESULT9, 144, 14, 8, 16
	.member	_ADCRESULT10, 160, 14, 8, 16
	.member	_ADCRESULT11, 176, 14, 8, 16
	.member	_ADCRESULT12, 192, 14, 8, 16
	.member	_ADCRESULT13, 208, 14, 8, 16
	.member	_ADCRESULT14, 224, 14, 8, 16
	.member	_ADCRESULT15, 240, 14, 8, 16
	.eos
	.stag	_TIM_REG, 32
	.member	_LSW, 0, 14, 8, 16
	.member	_MSW, 16, 14, 8, 16
	.eos
	.utag	_TIM_GROUP, 32
	.member	_all, 0, 15, 11, 32
	.member	_half, 0, 8, 11, 32, _TIM_REG
	.eos
	.stag	_PRD_REG, 32
	.member	_LSW, 0, 14, 8, 16
	.member	_MSW, 16, 14, 8, 16
	.eos
	.utag	_PRD_GROUP, 32
	.member	_all, 0, 15, 11, 32
	.member	_half, 0, 8, 11, 32, _PRD_REG
	.eos
	.stag	_TCR_BITS, 16
	.member	_rsvd1, 0, 14, 18, 4
	.member	_TSS, 4, 14, 18, 1
	.member	_TRB, 5, 14, 18, 1
	.member	_rsvd2, 6, 14, 18, 4
	.member	_SOFT, 10, 14, 18, 1
	.member	_FREE, 11, 14, 18, 1
	.member	_rsvd3, 12, 14, 18, 2
	.member	_TIE, 14, 14, 18, 1
	.member	_TIF, 15, 14, 18, 1
	.eos
	.utag	_TCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TCR_BITS
	.eos
	.stag	_TPR_BITS, 16
	.member	_TDDR, 0, 14, 18, 8
	.member	_PSC, 8, 14, 18, 8
	.eos
	.utag	_TPR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TPR_BITS
	.eos
	.stag	_TPRH_BITS, 16
	.member	_TDDRH, 0, 14, 18, 8
	.member	_PSCH, 8, 14, 18, 8
	.eos
	.utag	_TPRH_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TPRH_BITS
	.eos
	.stag	_CPUTIMER_REGS, 128
	.member	_TIM, 0, 9, 8, 32, _TIM_GROUP
	.member	_PRD, 32, 9, 8, 32, _PRD_GROUP
	.member	_TCR, 64, 9, 8, 16, _TCR_REG
	.member	_rsvd1, 80, 14, 8, 16
	.member	_TPR, 96, 9, 8, 16, _TPR_REG
	.member	_TPRH, 112, 9, 8, 16, _TPRH_REG
	.eos
	.stag	_CSM_PWL, 128
	.member	_PSWD0, 0, 14, 8, 16
	.member	_PSWD1, 16, 14, 8, 16
	.member	_PSWD2, 32, 14, 8, 16
	.member	_PSWD3, 48, 14, 8, 16
	.member	_PSWD4, 64, 14, 8, 16
	.member	_PSWD5, 80, 14, 8, 16
	.member	_PSWD6, 96, 14, 8, 16
	.member	_PSWD7, 112, 14, 8, 16
	.eos
	.stag	_CSMSCR_BITS, 16
	.member	_SECURE, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 14
	.member	_FORCESEC, 15, 14, 18, 1
	.eos
	.utag	_CSMSCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _CSMSCR_BITS
	.eos
	.stag	_CSM_REGS, 256
	.member	_KEY0, 0, 14, 8, 16
	.member	_KEY1, 16, 14, 8, 16
	.member	_KEY2, 32, 14, 8, 16
	.member	_KEY3, 48, 14, 8, 16
	.member	_KEY4, 64, 14, 8, 16
	.member	_KEY5, 80, 14, 8, 16
	.member	_KEY6, 96, 14, 8, 16
	.member	_KEY7, 112, 14, 8, 16
	.member	_rsvd1, 128, 14, 8, 16
	.member	_rsvd2, 144, 14, 8, 16
	.member	_rsvd3, 160, 14, 8, 16
	.member	_rsvd4, 176, 14, 8, 16
	.member	_rsvd5, 192, 14, 8, 16
	.member	_rsvd6, 208, 14, 8, 16
	.member	_rsvd7, 224, 14, 8, 16
	.member	_CSMSCR, 240, 9, 8, 16, _CSMSCR_REG
	.eos
	.stag	_DEVICECNF_BITS, 32
	.member	_rsvd1, 0, 14, 18, 3
	.member	_VMAPS, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 1
	.member	_XRSn, 5, 14, 18, 1
	.member	_rsvd3, 6, 14, 18, 10
	.member	_rsvd4, 16, 14, 18, 3
	.member	_ENPROT, 19, 14, 18, 1
	.member	_MONPRIV, 20, 14, 18, 1
	.member	_rsvd5, 21, 14, 18, 1
	.member	_EMU0SEL, 22, 14, 18, 2
	.member	_EMU1SEL, 24, 14, 18, 2
	.member	_MCBSPCON, 26, 14, 18, 1
	.member	_rsvd6, 27, 14, 18, 5
	.eos
	.utag	_DEVICECNF_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _DEVICECNF_BITS
	.eos
	.stag	_PARTID_BITS, 16
	.member	_PARTNO, 0, 14, 18, 8
	.member	_PARTTYPE, 8, 14, 18, 8
	.eos
	.utag	_PARTID_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PARTID_BITS
	.eos
	.stag	_DEV_EMU_REGS, 3328
	.member	_DEVICECNF, 0, 9, 8, 32, _DEVICECNF_REG
	.member	_PARTID, 32, 9, 8, 16, _PARTID_REG
	.member	_REVID, 48, 14, 8, 16
	.member	_PROTSTART, 64, 14, 8, 16
	.member	_PROTRANGE, 80, 14, 8, 16
	.member	_rsvd2, 96, 62, 8, 3232, , 202
	.eos
	.stag	_DMACTRL_BITS, 16
	.member	_HARDRESET, 0, 14, 18, 1
	.member	_PRIORITYRESET, 1, 14, 18, 1
	.member	_rsvd1, 2, 14, 18, 14
	.eos
	.utag	_DMACTRL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DMACTRL_BITS
	.eos
	.stag	_DEBUGCTRL_BITS, 16
	.member	_rsvd1, 0, 14, 18, 15
	.member	_FREE, 15, 14, 18, 1
	.eos
	.utag	_DEBUGCTRL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DEBUGCTRL_BITS
	.eos
	.stag	_PRIORITYCTRL1_BITS, 16
	.member	_CH1PRIORITY, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 15
	.eos
	.utag	_PRIORITYCTRL1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PRIORITYCTRL1_BITS
	.eos
	.stag	_PRIORITYSTAT_BITS, 16
	.member	_ACTIVESTS, 0, 14, 18, 3
	.member	_rsvd1, 3, 14, 18, 1
	.member	_ACTIVESTS_SHADOW, 4, 14, 18, 3
	.member	_rsvd2, 7, 14, 18, 9
	.eos
	.utag	_PRIORITYSTAT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PRIORITYSTAT_BITS
	.eos
	.stag	_MODE_BITS, 16
	.member	_PERINTSEL, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 2
	.member	_OVRINTE, 7, 14, 18, 1
	.member	_PERINTE, 8, 14, 18, 1
	.member	_CHINTMODE, 9, 14, 18, 1
	.member	_ONESHOT, 10, 14, 18, 1
	.member	_CONTINUOUS, 11, 14, 18, 1
	.member	_SYNCE, 12, 14, 18, 1
	.member	_SYNCSEL, 13, 14, 18, 1
	.member	_DATASIZE, 14, 14, 18, 1
	.member	_CHINTE, 15, 14, 18, 1
	.eos
	.utag	_MODE_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MODE_BITS
	.eos
	.stag	_CONTROL_BITS, 16
	.member	_RUN, 0, 14, 18, 1
	.member	_HALT, 1, 14, 18, 1
	.member	_SOFTRESET, 2, 14, 18, 1
	.member	_PERINTFRC, 3, 14, 18, 1
	.member	_PERINTCLR, 4, 14, 18, 1
	.member	_SYNCFRC, 5, 14, 18, 1
	.member	_SYNCCLR, 6, 14, 18, 1
	.member	_ERRCLR, 7, 14, 18, 1
	.member	_PERINTFLG, 8, 14, 18, 1
	.member	_SYNCFLG, 9, 14, 18, 1
	.member	_SYNCERR, 10, 14, 18, 1
	.member	_TRANSFERSTS, 11, 14, 18, 1
	.member	_BURSTSTS, 12, 14, 18, 1
	.member	_RUNSTS, 13, 14, 18, 1
	.member	_OVRFLG, 14, 14, 18, 1
	.member	_rsvd1, 15, 14, 18, 1
	.eos
	.utag	_CONTROL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _CONTROL_BITS
	.eos
	.stag	_BURST_SIZE_BITS, 16
	.member	_BURSTSIZE, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 11
	.eos
	.utag	_BURST_SIZE_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _BURST_SIZE_BITS
	.eos
	.stag	_BURST_COUNT_BITS, 16
	.member	_BURSTCOUNT, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 11
	.eos
	.utag	_BURST_COUNT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _BURST_COUNT_BITS
	.eos
	.stag	_CH_REGS, 512
	.member	_MODE, 0, 9, 8, 16, _MODE_REG
	.member	_CONTROL, 16, 9, 8, 16, _CONTROL_REG
	.member	_BURST_SIZE, 32, 9, 8, 16, _BURST_SIZE_REG
	.member	_BURST_COUNT, 48, 9, 8, 16, _BURST_COUNT_REG
	.member	_SRC_BURST_STEP, 64, 4, 8, 16
	.member	_DST_BURST_STEP, 80, 4, 8, 16
	.member	_TRANSFER_SIZE, 96, 14, 8, 16
	.member	_TRANSFER_COUNT, 112, 14, 8, 16
	.member	_SRC_TRANSFER_STEP, 128, 4, 8, 16
	.member	_DST_TRANSFER_STEP, 144, 4, 8, 16
	.member	_SRC_WRAP_SIZE, 160, 14, 8, 16
	.member	_SRC_WRAP_COUNT, 176, 14, 8, 16
	.member	_SRC_WRAP_STEP, 192, 4, 8, 16
	.member	_DST_WRAP_SIZE, 208, 14, 8, 16
	.member	_DST_WRAP_COUNT, 224, 14, 8, 16
	.member	_DST_WRAP_STEP, 240, 4, 8, 16
	.member	_SRC_BEG_ADDR_SHADOW, 256, 15, 8, 32
	.member	_SRC_ADDR_SHADOW, 288, 15, 8, 32
	.member	_SRC_BEG_ADDR_ACTIVE, 320, 15, 8, 32
	.member	_SRC_ADDR_ACTIVE, 352, 15, 8, 32
	.member	_DST_BEG_ADDR_SHADOW, 384, 15, 8, 32
	.member	_DST_ADDR_SHADOW, 416, 15, 8, 32
	.member	_DST_BEG_ADDR_ACTIVE, 448, 15, 8, 32
	.member	_DST_ADDR_ACTIVE, 480, 15, 8, 32
	.eos
	.stag	_DMA_REGS, 3584
	.member	_DMACTRL, 0, 9, 8, 16, _DMACTRL_REG
	.member	_DEBUGCTRL, 16, 9, 8, 16, _DEBUGCTRL_REG
	.member	_rsvd0, 32, 14, 8, 16
	.member	_rsvd1, 48, 14, 8, 16
	.member	_PRIORITYCTRL1, 64, 9, 8, 16, _PRIORITYCTRL1_REG
	.member	_rsvd2, 80, 14, 8, 16
	.member	_PRIORITYSTAT, 96, 9, 8, 16, _PRIORITYSTAT_REG
	.member	_rsvd3, 112, 62, 8, 400, , 25
	.member	_CH1, 512, 8, 8, 512, _CH_REGS
	.member	_CH2, 1024, 8, 8, 512, _CH_REGS
	.member	_CH3, 1536, 8, 8, 512, _CH_REGS
	.member	_CH4, 2048, 8, 8, 512, _CH_REGS
	.member	_CH5, 2560, 8, 8, 512, _CH_REGS
	.member	_CH6, 3072, 8, 8, 512, _CH_REGS
	.eos
	.stag	_CANME_BITS, 32
	.member	_ME0, 0, 14, 18, 1
	.member	_ME1, 1, 14, 18, 1
	.member	_ME2, 2, 14, 18, 1
	.member	_ME3, 3, 14, 18, 1
	.member	_ME4, 4, 14, 18, 1
	.member	_ME5, 5, 14, 18, 1
	.member	_ME6, 6, 14, 18, 1
	.member	_ME7, 7, 14, 18, 1
	.member	_ME8, 8, 14, 18, 1
	.member	_ME9, 9, 14, 18, 1
	.member	_ME10, 10, 14, 18, 1
	.member	_ME11, 11, 14, 18, 1
	.member	_ME12, 12, 14, 18, 1
	.member	_ME13, 13, 14, 18, 1
	.member	_ME14, 14, 14, 18, 1
	.member	_ME15, 15, 14, 18, 1
	.member	_ME16, 16, 14, 18, 1
	.member	_ME17, 17, 14, 18, 1
	.member	_ME18, 18, 14, 18, 1
	.member	_ME19, 19, 14, 18, 1
	.member	_ME20, 20, 14, 18, 1
	.member	_ME21, 21, 14, 18, 1
	.member	_ME22, 22, 14, 18, 1
	.member	_ME23, 23, 14, 18, 1
	.member	_ME24, 24, 14, 18, 1
	.member	_ME25, 25, 14, 18, 1
	.member	_ME26, 26, 14, 18, 1
	.member	_ME27, 27, 14, 18, 1
	.member	_ME28, 28, 14, 18, 1
	.member	_ME29, 29, 14, 18, 1
	.member	_ME30, 30, 14, 18, 1
	.member	_ME31, 31, 14, 18, 1
	.eos
	.utag	_CANME_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANME_BITS
	.eos
	.stag	_CANMD_BITS, 32
	.member	_MD0, 0, 14, 18, 1
	.member	_MD1, 1, 14, 18, 1
	.member	_MD2, 2, 14, 18, 1
	.member	_MD3, 3, 14, 18, 1
	.member	_MD4, 4, 14, 18, 1
	.member	_MD5, 5, 14, 18, 1
	.member	_MD6, 6, 14, 18, 1
	.member	_MD7, 7, 14, 18, 1
	.member	_MD8, 8, 14, 18, 1
	.member	_MD9, 9, 14, 18, 1
	.member	_MD10, 10, 14, 18, 1
	.member	_MD11, 11, 14, 18, 1
	.member	_MD12, 12, 14, 18, 1
	.member	_MD13, 13, 14, 18, 1
	.member	_MD14, 14, 14, 18, 1
	.member	_MD15, 15, 14, 18, 1
	.member	_MD16, 16, 14, 18, 1
	.member	_MD17, 17, 14, 18, 1
	.member	_MD18, 18, 14, 18, 1
	.member	_MD19, 19, 14, 18, 1
	.member	_MD20, 20, 14, 18, 1
	.member	_MD21, 21, 14, 18, 1
	.member	_MD22, 22, 14, 18, 1
	.member	_MD23, 23, 14, 18, 1
	.member	_MD24, 24, 14, 18, 1
	.member	_MD25, 25, 14, 18, 1
	.member	_MD26, 26, 14, 18, 1
	.member	_MD27, 27, 14, 18, 1
	.member	_MD28, 28, 14, 18, 1
	.member	_MD29, 29, 14, 18, 1
	.member	_MD30, 30, 14, 18, 1
	.member	_MD31, 31, 14, 18, 1
	.eos
	.utag	_CANMD_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMD_BITS
	.eos
	.stag	_CANTRS_BITS, 32
	.member	_TRS0, 0, 14, 18, 1
	.member	_TRS1, 1, 14, 18, 1
	.member	_TRS2, 2, 14, 18, 1
	.member	_TRS3, 3, 14, 18, 1
	.member	_TRS4, 4, 14, 18, 1
	.member	_TRS5, 5, 14, 18, 1
	.member	_TRS6, 6, 14, 18, 1
	.member	_TRS7, 7, 14, 18, 1
	.member	_TRS8, 8, 14, 18, 1
	.member	_TRS9, 9, 14, 18, 1
	.member	_TRS10, 10, 14, 18, 1
	.member	_TRS11, 11, 14, 18, 1
	.member	_TRS12, 12, 14, 18, 1
	.member	_TRS13, 13, 14, 18, 1
	.member	_TRS14, 14, 14, 18, 1
	.member	_TRS15, 15, 14, 18, 1
	.member	_TRS16, 16, 14, 18, 1
	.member	_TRS17, 17, 14, 18, 1
	.member	_TRS18, 18, 14, 18, 1
	.member	_TRS19, 19, 14, 18, 1
	.member	_TRS20, 20, 14, 18, 1
	.member	_TRS21, 21, 14, 18, 1
	.member	_TRS22, 22, 14, 18, 1
	.member	_TRS23, 23, 14, 18, 1
	.member	_TRS24, 24, 14, 18, 1
	.member	_TRS25, 25, 14, 18, 1
	.member	_TRS26, 26, 14, 18, 1
	.member	_TRS27, 27, 14, 18, 1
	.member	_TRS28, 28, 14, 18, 1
	.member	_TRS29, 29, 14, 18, 1
	.member	_TRS30, 30, 14, 18, 1
	.member	_TRS31, 31, 14, 18, 1
	.eos
	.utag	_CANTRS_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTRS_BITS
	.eos
	.stag	_CANTRR_BITS, 32
	.member	_TRR0, 0, 14, 18, 1
	.member	_TRR1, 1, 14, 18, 1
	.member	_TRR2, 2, 14, 18, 1
	.member	_TRR3, 3, 14, 18, 1
	.member	_TRR4, 4, 14, 18, 1
	.member	_TRR5, 5, 14, 18, 1
	.member	_TRR6, 6, 14, 18, 1
	.member	_TRR7, 7, 14, 18, 1
	.member	_TRR8, 8, 14, 18, 1
	.member	_TRR9, 9, 14, 18, 1
	.member	_TRR10, 10, 14, 18, 1
	.member	_TRR11, 11, 14, 18, 1
	.member	_TRR12, 12, 14, 18, 1
	.member	_TRR13, 13, 14, 18, 1
	.member	_TRR14, 14, 14, 18, 1
	.member	_TRR15, 15, 14, 18, 1
	.member	_TRR16, 16, 14, 18, 1
	.member	_TRR17, 17, 14, 18, 1
	.member	_TRR18, 18, 14, 18, 1
	.member	_TRR19, 19, 14, 18, 1
	.member	_TRR20, 20, 14, 18, 1
	.member	_TRR21, 21, 14, 18, 1
	.member	_TRR22, 22, 14, 18, 1
	.member	_TRR23, 23, 14, 18, 1
	.member	_TRR24, 24, 14, 18, 1
	.member	_TRR25, 25, 14, 18, 1
	.member	_TRR26, 26, 14, 18, 1
	.member	_TRR27, 27, 14, 18, 1
	.member	_TRR28, 28, 14, 18, 1
	.member	_TRR29, 29, 14, 18, 1
	.member	_TRR30, 30, 14, 18, 1
	.member	_TRR31, 31, 14, 18, 1
	.eos
	.utag	_CANTRR_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTRR_BITS
	.eos
	.stag	_CANTA_BITS, 32
	.member	_TA0, 0, 14, 18, 1
	.member	_TA1, 1, 14, 18, 1
	.member	_TA2, 2, 14, 18, 1
	.member	_TA3, 3, 14, 18, 1
	.member	_TA4, 4, 14, 18, 1
	.member	_TA5, 5, 14, 18, 1
	.member	_TA6, 6, 14, 18, 1
	.member	_TA7, 7, 14, 18, 1
	.member	_TA8, 8, 14, 18, 1
	.member	_TA9, 9, 14, 18, 1
	.member	_TA10, 10, 14, 18, 1
	.member	_TA11, 11, 14, 18, 1
	.member	_TA12, 12, 14, 18, 1
	.member	_TA13, 13, 14, 18, 1
	.member	_TA14, 14, 14, 18, 1
	.member	_TA15, 15, 14, 18, 1
	.member	_TA16, 16, 14, 18, 1
	.member	_TA17, 17, 14, 18, 1
	.member	_TA18, 18, 14, 18, 1
	.member	_TA19, 19, 14, 18, 1
	.member	_TA20, 20, 14, 18, 1
	.member	_TA21, 21, 14, 18, 1
	.member	_TA22, 22, 14, 18, 1
	.member	_TA23, 23, 14, 18, 1
	.member	_TA24, 24, 14, 18, 1
	.member	_TA25, 25, 14, 18, 1
	.member	_TA26, 26, 14, 18, 1
	.member	_TA27, 27, 14, 18, 1
	.member	_TA28, 28, 14, 18, 1
	.member	_TA29, 29, 14, 18, 1
	.member	_TA30, 30, 14, 18, 1
	.member	_TA31, 31, 14, 18, 1
	.eos
	.utag	_CANTA_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTA_BITS
	.eos
	.stag	_CANAA_BITS, 32
	.member	_AA0, 0, 14, 18, 1
	.member	_AA1, 1, 14, 18, 1
	.member	_AA2, 2, 14, 18, 1
	.member	_AA3, 3, 14, 18, 1
	.member	_AA4, 4, 14, 18, 1
	.member	_AA5, 5, 14, 18, 1
	.member	_AA6, 6, 14, 18, 1
	.member	_AA7, 7, 14, 18, 1
	.member	_AA8, 8, 14, 18, 1
	.member	_AA9, 9, 14, 18, 1
	.member	_AA10, 10, 14, 18, 1
	.member	_AA11, 11, 14, 18, 1
	.member	_AA12, 12, 14, 18, 1
	.member	_AA13, 13, 14, 18, 1
	.member	_AA14, 14, 14, 18, 1
	.member	_AA15, 15, 14, 18, 1
	.member	_AA16, 16, 14, 18, 1
	.member	_AA17, 17, 14, 18, 1
	.member	_AA18, 18, 14, 18, 1
	.member	_AA19, 19, 14, 18, 1
	.member	_AA20, 20, 14, 18, 1
	.member	_AA21, 21, 14, 18, 1
	.member	_AA22, 22, 14, 18, 1
	.member	_AA23, 23, 14, 18, 1
	.member	_AA24, 24, 14, 18, 1
	.member	_AA25, 25, 14, 18, 1
	.member	_AA26, 26, 14, 18, 1
	.member	_AA27, 27, 14, 18, 1
	.member	_AA28, 28, 14, 18, 1
	.member	_AA29, 29, 14, 18, 1
	.member	_AA30, 30, 14, 18, 1
	.member	_AA31, 31, 14, 18, 1
	.eos
	.utag	_CANAA_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANAA_BITS
	.eos
	.stag	_CANRMP_BITS, 32
	.member	_RMP0, 0, 14, 18, 1
	.member	_RMP1, 1, 14, 18, 1
	.member	_RMP2, 2, 14, 18, 1
	.member	_RMP3, 3, 14, 18, 1
	.member	_RMP4, 4, 14, 18, 1
	.member	_RMP5, 5, 14, 18, 1
	.member	_RMP6, 6, 14, 18, 1
	.member	_RMP7, 7, 14, 18, 1
	.member	_RMP8, 8, 14, 18, 1
	.member	_RMP9, 9, 14, 18, 1
	.member	_RMP10, 10, 14, 18, 1
	.member	_RMP11, 11, 14, 18, 1
	.member	_RMP12, 12, 14, 18, 1
	.member	_RMP13, 13, 14, 18, 1
	.member	_RMP14, 14, 14, 18, 1
	.member	_RMP15, 15, 14, 18, 1
	.member	_RMP16, 16, 14, 18, 1
	.member	_RMP17, 17, 14, 18, 1
	.member	_RMP18, 18, 14, 18, 1
	.member	_RMP19, 19, 14, 18, 1
	.member	_RMP20, 20, 14, 18, 1
	.member	_RMP21, 21, 14, 18, 1
	.member	_RMP22, 22, 14, 18, 1
	.member	_RMP23, 23, 14, 18, 1
	.member	_RMP24, 24, 14, 18, 1
	.member	_RMP25, 25, 14, 18, 1
	.member	_RMP26, 26, 14, 18, 1
	.member	_RMP27, 27, 14, 18, 1
	.member	_RMP28, 28, 14, 18, 1
	.member	_RMP29, 29, 14, 18, 1
	.member	_RMP30, 30, 14, 18, 1
	.member	_RMP31, 31, 14, 18, 1
	.eos
	.utag	_CANRMP_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANRMP_BITS
	.eos
	.stag	_CANRML_BITS, 32
	.member	_RML0, 0, 14, 18, 1
	.member	_RML1, 1, 14, 18, 1
	.member	_RML2, 2, 14, 18, 1
	.member	_RML3, 3, 14, 18, 1
	.member	_RML4, 4, 14, 18, 1
	.member	_RML5, 5, 14, 18, 1
	.member	_RML6, 6, 14, 18, 1
	.member	_RML7, 7, 14, 18, 1
	.member	_RML8, 8, 14, 18, 1
	.member	_RML9, 9, 14, 18, 1
	.member	_RML10, 10, 14, 18, 1
	.member	_RML11, 11, 14, 18, 1
	.member	_RML12, 12, 14, 18, 1
	.member	_RML13, 13, 14, 18, 1
	.member	_RML14, 14, 14, 18, 1
	.member	_RML15, 15, 14, 18, 1
	.member	_RML16, 16, 14, 18, 1
	.member	_RML17, 17, 14, 18, 1
	.member	_RML18, 18, 14, 18, 1
	.member	_RML19, 19, 14, 18, 1
	.member	_RML20, 20, 14, 18, 1
	.member	_RML21, 21, 14, 18, 1
	.member	_RML22, 22, 14, 18, 1
	.member	_RML23, 23, 14, 18, 1
	.member	_RML24, 24, 14, 18, 1
	.member	_RML25, 25, 14, 18, 1
	.member	_RML26, 26, 14, 18, 1
	.member	_RML27, 27, 14, 18, 1
	.member	_RML28, 28, 14, 18, 1
	.member	_RML29, 29, 14, 18, 1
	.member	_RML30, 30, 14, 18, 1
	.member	_RML31, 31, 14, 18, 1
	.eos
	.utag	_CANRML_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANRML_BITS
	.eos
	.stag	_CANRFP_BITS, 32
	.member	_RFP0, 0, 14, 18, 1
	.member	_RFP1, 1, 14, 18, 1
	.member	_RFP2, 2, 14, 18, 1
	.member	_RFP3, 3, 14, 18, 1
	.member	_RFP4, 4, 14, 18, 1
	.member	_RFP5, 5, 14, 18, 1
	.member	_RFP6, 6, 14, 18, 1
	.member	_RFP7, 7, 14, 18, 1
	.member	_RFP8, 8, 14, 18, 1
	.member	_RFP9, 9, 14, 18, 1
	.member	_RFP10, 10, 14, 18, 1
	.member	_RFP11, 11, 14, 18, 1
	.member	_RFP12, 12, 14, 18, 1
	.member	_RFP13, 13, 14, 18, 1
	.member	_RFP14, 14, 14, 18, 1
	.member	_RFP15, 15, 14, 18, 1
	.member	_RFP16, 16, 14, 18, 1
	.member	_RFP17, 17, 14, 18, 1
	.member	_RFP18, 18, 14, 18, 1
	.member	_RFP19, 19, 14, 18, 1
	.member	_RFP20, 20, 14, 18, 1
	.member	_RFP21, 21, 14, 18, 1
	.member	_RFP22, 22, 14, 18, 1
	.member	_RFP23, 23, 14, 18, 1
	.member	_RFP24, 24, 14, 18, 1
	.member	_RFP25, 25, 14, 18, 1
	.member	_RFP26, 26, 14, 18, 1
	.member	_RFP27, 27, 14, 18, 1
	.member	_RFP28, 28, 14, 18, 1
	.member	_RFP29, 29, 14, 18, 1
	.member	_RFP30, 30, 14, 18, 1
	.member	_RFP31, 31, 14, 18, 1
	.eos
	.utag	_CANRFP_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANRFP_BITS
	.eos
	.stag	_CANGAM_BITS, 32
	.member	_GAM150, 0, 14, 18, 16
	.member	_GAM2816, 16, 14, 18, 13
	.member	_rsvd, 29, 14, 18, 2
	.member	_AMI, 31, 14, 18, 1
	.eos
	.utag	_CANGAM_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANGAM_BITS
	.eos
	.stag	_CANMC_BITS, 32
	.member	_MBNR, 0, 14, 18, 5
	.member	_SRES, 5, 14, 18, 1
	.member	_STM, 6, 14, 18, 1
	.member	_ABO, 7, 14, 18, 1
	.member	_CDR, 8, 14, 18, 1
	.member	_WUBA, 9, 14, 18, 1
	.member	_DBO, 10, 14, 18, 1
	.member	_PDR, 11, 14, 18, 1
	.member	_CCR, 12, 14, 18, 1
	.member	_SCB, 13, 14, 18, 1
	.member	_TCC, 14, 14, 18, 1
	.member	_MBCC, 15, 14, 18, 1
	.member	_SUSP, 16, 14, 18, 1
	.member	_rsvd, 17, 14, 18, 15
	.eos
	.utag	_CANMC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMC_BITS
	.eos
	.stag	_CANBTC_BITS, 32
	.member	_TSEG2REG, 0, 14, 18, 3
	.member	_TSEG1REG, 3, 14, 18, 4
	.member	_SAM, 7, 14, 18, 1
	.member	_SJWREG, 8, 14, 18, 2
	.member	_rsvd1, 10, 14, 18, 6
	.member	_BRPREG, 16, 14, 18, 8
	.member	_rsvd2, 24, 14, 18, 8
	.eos
	.utag	_CANBTC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANBTC_BITS
	.eos
	.stag	_CANES_BITS, 32
	.member	_TM, 0, 14, 18, 1
	.member	_RM, 1, 14, 18, 1
	.member	_rsvd1, 2, 14, 18, 1
	.member	_PDA, 3, 14, 18, 1
	.member	_CCE, 4, 14, 18, 1
	.member	_SMA, 5, 14, 18, 1
	.member	_rsvd2, 6, 14, 18, 10
	.member	_EW, 16, 14, 18, 1
	.member	_EP, 17, 14, 18, 1
	.member	_BO, 18, 14, 18, 1
	.member	_ACKE, 19, 14, 18, 1
	.member	_SE, 20, 14, 18, 1
	.member	_CRCE, 21, 14, 18, 1
	.member	_SA1, 22, 14, 18, 1
	.member	_BE, 23, 14, 18, 1
	.member	_FE, 24, 14, 18, 1
	.member	_rsvd3, 25, 14, 18, 7
	.eos
	.utag	_CANES_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANES_BITS
	.eos
	.stag	_CANTEC_BITS, 32
	.member	_TEC, 0, 14, 18, 8
	.member	_rsvd1, 8, 14, 18, 8
	.member	_rsvd2, 16, 14, 18, 16
	.eos
	.utag	_CANTEC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTEC_BITS
	.eos
	.stag	_CANREC_BITS, 32
	.member	_REC, 0, 14, 18, 8
	.member	_rsvd1, 8, 14, 18, 8
	.member	_rsvd2, 16, 14, 18, 16
	.eos
	.utag	_CANREC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANREC_BITS
	.eos
	.stag	_CANGIF0_BITS, 32
	.member	_MIV0, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 3
	.member	_WLIF0, 8, 14, 18, 1
	.member	_EPIF0, 9, 14, 18, 1
	.member	_BOIF0, 10, 14, 18, 1
	.member	_RMLIF0, 11, 14, 18, 1
	.member	_WUIF0, 12, 14, 18, 1
	.member	_WDIF0, 13, 14, 18, 1
	.member	_AAIF0, 14, 14, 18, 1
	.member	_GMIF0, 15, 14, 18, 1
	.member	_TCOF0, 16, 14, 18, 1
	.member	_MTOF0, 17, 14, 18, 1
	.member	_rsvd2, 18, 14, 18, 14
	.eos
	.utag	_CANGIF0_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANGIF0_BITS
	.eos
	.stag	_CANGIM_BITS, 32
	.member	_I0EN, 0, 14, 18, 1
	.member	_I1EN, 1, 14, 18, 1
	.member	_GIL, 2, 14, 18, 1
	.member	_rsvd1, 3, 14, 18, 5
	.member	_WLIM, 8, 14, 18, 1
	.member	_EPIM, 9, 14, 18, 1
	.member	_BOIM, 10, 14, 18, 1
	.member	_RMLIM, 11, 14, 18, 1
	.member	_WUIM, 12, 14, 18, 1
	.member	_WDIM, 13, 14, 18, 1
	.member	_AAIM, 14, 14, 18, 1
	.member	_rsvd2, 15, 14, 18, 1
	.member	_TCOM, 16, 14, 18, 1
	.member	_MTOM, 17, 14, 18, 1
	.member	_rsvd3, 18, 14, 18, 14
	.eos
	.utag	_CANGIM_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANGIM_BITS
	.eos
	.stag	_CANGIF1_BITS, 32
	.member	_MIV1, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 3
	.member	_WLIF1, 8, 14, 18, 1
	.member	_EPIF1, 9, 14, 18, 1
	.member	_BOIF1, 10, 14, 18, 1
	.member	_RMLIF1, 11, 14, 18, 1
	.member	_WUIF1, 12, 14, 18, 1
	.member	_WDIF1, 13, 14, 18, 1
	.member	_AAIF1, 14, 14, 18, 1
	.member	_GMIF1, 15, 14, 18, 1
	.member	_TCOF1, 16, 14, 18, 1
	.member	_MTOF1, 17, 14, 18, 1
	.member	_rsvd2, 18, 14, 18, 14
	.eos
	.utag	_CANGIF1_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANGIF1_BITS
	.eos
	.stag	_CANMIM_BITS, 32
	.member	_MIM0, 0, 14, 18, 1
	.member	_MIM1, 1, 14, 18, 1
	.member	_MIM2, 2, 14, 18, 1
	.member	_MIM3, 3, 14, 18, 1
	.member	_MIM4, 4, 14, 18, 1
	.member	_MIM5, 5, 14, 18, 1
	.member	_MIM6, 6, 14, 18, 1
	.member	_MIM7, 7, 14, 18, 1
	.member	_MIM8, 8, 14, 18, 1
	.member	_MIM9, 9, 14, 18, 1
	.member	_MIM10, 10, 14, 18, 1
	.member	_MIM11, 11, 14, 18, 1
	.member	_MIM12, 12, 14, 18, 1
	.member	_MIM13, 13, 14, 18, 1
	.member	_MIM14, 14, 14, 18, 1
	.member	_MIM15, 15, 14, 18, 1
	.member	_MIM16, 16, 14, 18, 1
	.member	_MIM17, 17, 14, 18, 1
	.member	_MIM18, 18, 14, 18, 1
	.member	_MIM19, 19, 14, 18, 1
	.member	_MIM20, 20, 14, 18, 1
	.member	_MIM21, 21, 14, 18, 1
	.member	_MIM22, 22, 14, 18, 1
	.member	_MIM23, 23, 14, 18, 1
	.member	_MIM24, 24, 14, 18, 1
	.member	_MIM25, 25, 14, 18, 1
	.member	_MIM26, 26, 14, 18, 1
	.member	_MIM27, 27, 14, 18, 1
	.member	_MIM28, 28, 14, 18, 1
	.member	_MIM29, 29, 14, 18, 1
	.member	_MIM30, 30, 14, 18, 1
	.member	_MIM31, 31, 14, 18, 1
	.eos
	.utag	_CANMIM_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMIM_BITS
	.eos
	.stag	_CANMIL_BITS, 32
	.member	_MIL0, 0, 14, 18, 1
	.member	_MIL1, 1, 14, 18, 1
	.member	_MIL2, 2, 14, 18, 1
	.member	_MIL3, 3, 14, 18, 1
	.member	_MIL4, 4, 14, 18, 1
	.member	_MIL5, 5, 14, 18, 1
	.member	_MIL6, 6, 14, 18, 1
	.member	_MIL7, 7, 14, 18, 1
	.member	_MIL8, 8, 14, 18, 1
	.member	_MIL9, 9, 14, 18, 1
	.member	_MIL10, 10, 14, 18, 1
	.member	_MIL11, 11, 14, 18, 1
	.member	_MIL12, 12, 14, 18, 1
	.member	_MIL13, 13, 14, 18, 1
	.member	_MIL14, 14, 14, 18, 1
	.member	_MIL15, 15, 14, 18, 1
	.member	_MIL16, 16, 14, 18, 1
	.member	_MIL17, 17, 14, 18, 1
	.member	_MIL18, 18, 14, 18, 1
	.member	_MIL19, 19, 14, 18, 1
	.member	_MIL20, 20, 14, 18, 1
	.member	_MIL21, 21, 14, 18, 1
	.member	_MIL22, 22, 14, 18, 1
	.member	_MIL23, 23, 14, 18, 1
	.member	_MIL24, 24, 14, 18, 1
	.member	_MIL25, 25, 14, 18, 1
	.member	_MIL26, 26, 14, 18, 1
	.member	_MIL27, 27, 14, 18, 1
	.member	_MIL28, 28, 14, 18, 1
	.member	_MIL29, 29, 14, 18, 1
	.member	_MIL30, 30, 14, 18, 1
	.member	_MIL31, 31, 14, 18, 1
	.eos
	.utag	_CANMIL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMIL_BITS
	.eos
	.stag	_CANOPC_BITS, 32
	.member	_OPC0, 0, 14, 18, 1
	.member	_OPC1, 1, 14, 18, 1
	.member	_OPC2, 2, 14, 18, 1
	.member	_OPC3, 3, 14, 18, 1
	.member	_OPC4, 4, 14, 18, 1
	.member	_OPC5, 5, 14, 18, 1
	.member	_OPC6, 6, 14, 18, 1
	.member	_OPC7, 7, 14, 18, 1
	.member	_OPC8, 8, 14, 18, 1
	.member	_OPC9, 9, 14, 18, 1
	.member	_OPC10, 10, 14, 18, 1
	.member	_OPC11, 11, 14, 18, 1
	.member	_OPC12, 12, 14, 18, 1
	.member	_OPC13, 13, 14, 18, 1
	.member	_OPC14, 14, 14, 18, 1
	.member	_OPC15, 15, 14, 18, 1
	.member	_OPC16, 16, 14, 18, 1
	.member	_OPC17, 17, 14, 18, 1
	.member	_OPC18, 18, 14, 18, 1
	.member	_OPC19, 19, 14, 18, 1
	.member	_OPC20, 20, 14, 18, 1
	.member	_OPC21, 21, 14, 18, 1
	.member	_OPC22, 22, 14, 18, 1
	.member	_OPC23, 23, 14, 18, 1
	.member	_OPC24, 24, 14, 18, 1
	.member	_OPC25, 25, 14, 18, 1
	.member	_OPC26, 26, 14, 18, 1
	.member	_OPC27, 27, 14, 18, 1
	.member	_OPC28, 28, 14, 18, 1
	.member	_OPC29, 29, 14, 18, 1
	.member	_OPC30, 30, 14, 18, 1
	.member	_OPC31, 31, 14, 18, 1
	.eos
	.utag	_CANOPC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANOPC_BITS
	.eos
	.stag	_CANTIOC_BITS, 32
	.member	_rsvd1, 0, 14, 18, 3
	.member	_TXFUNC, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.member	_rsvd3, 16, 14, 18, 16
	.eos
	.utag	_CANTIOC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTIOC_BITS
	.eos
	.stag	_CANRIOC_BITS, 32
	.member	_rsvd1, 0, 14, 18, 3
	.member	_RXFUNC, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.member	_rsvd3, 16, 14, 18, 16
	.eos
	.utag	_CANRIOC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANRIOC_BITS
	.eos
	.stag	_CANTOC_BITS, 32
	.member	_TOC0, 0, 14, 18, 1
	.member	_TOC1, 1, 14, 18, 1
	.member	_TOC2, 2, 14, 18, 1
	.member	_TOC3, 3, 14, 18, 1
	.member	_TOC4, 4, 14, 18, 1
	.member	_TOC5, 5, 14, 18, 1
	.member	_TOC6, 6, 14, 18, 1
	.member	_TOC7, 7, 14, 18, 1
	.member	_TOC8, 8, 14, 18, 1
	.member	_TOC9, 9, 14, 18, 1
	.member	_TOC10, 10, 14, 18, 1
	.member	_TOC11, 11, 14, 18, 1
	.member	_TOC12, 12, 14, 18, 1
	.member	_TOC13, 13, 14, 18, 1
	.member	_TOC14, 14, 14, 18, 1
	.member	_TOC15, 15, 14, 18, 1
	.member	_TOC16, 16, 14, 18, 1
	.member	_TOC17, 17, 14, 18, 1
	.member	_TOC18, 18, 14, 18, 1
	.member	_TOC19, 19, 14, 18, 1
	.member	_TOC20, 20, 14, 18, 1
	.member	_TOC21, 21, 14, 18, 1
	.member	_TOC22, 22, 14, 18, 1
	.member	_TOC23, 23, 14, 18, 1
	.member	_TOC24, 24, 14, 18, 1
	.member	_TOC25, 25, 14, 18, 1
	.member	_TOC26, 26, 14, 18, 1
	.member	_TOC27, 27, 14, 18, 1
	.member	_TOC28, 28, 14, 18, 1
	.member	_TOC29, 29, 14, 18, 1
	.member	_TOC30, 30, 14, 18, 1
	.member	_TOC31, 31, 14, 18, 1
	.eos
	.utag	_CANTOC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTOC_BITS
	.eos
	.stag	_CANTOS_BITS, 32
	.member	_TOS0, 0, 14, 18, 1
	.member	_TOS1, 1, 14, 18, 1
	.member	_TOS2, 2, 14, 18, 1
	.member	_TOS3, 3, 14, 18, 1
	.member	_TOS4, 4, 14, 18, 1
	.member	_TOS5, 5, 14, 18, 1
	.member	_TOS6, 6, 14, 18, 1
	.member	_TOS7, 7, 14, 18, 1
	.member	_TOS8, 8, 14, 18, 1
	.member	_TOS9, 9, 14, 18, 1
	.member	_TOS10, 10, 14, 18, 1
	.member	_TOS11, 11, 14, 18, 1
	.member	_TOS12, 12, 14, 18, 1
	.member	_TOS13, 13, 14, 18, 1
	.member	_TOS14, 14, 14, 18, 1
	.member	_TOS15, 15, 14, 18, 1
	.member	_TOS16, 16, 14, 18, 1
	.member	_TOS17, 17, 14, 18, 1
	.member	_TOS18, 18, 14, 18, 1
	.member	_TOS19, 19, 14, 18, 1
	.member	_TOS20, 20, 14, 18, 1
	.member	_TOS21, 21, 14, 18, 1
	.member	_TOS22, 22, 14, 18, 1
	.member	_TOS23, 23, 14, 18, 1
	.member	_TOS24, 24, 14, 18, 1
	.member	_TOS25, 25, 14, 18, 1
	.member	_TOS26, 26, 14, 18, 1
	.member	_TOS27, 27, 14, 18, 1
	.member	_TOS28, 28, 14, 18, 1
	.member	_TOS29, 29, 14, 18, 1
	.member	_TOS30, 30, 14, 18, 1
	.member	_TOS31, 31, 14, 18, 1
	.eos
	.utag	_CANTOS_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTOS_BITS
	.eos
	.stag	_ECAN_REGS, 832
	.member	_CANME, 0, 9, 8, 32, _CANME_REG
	.member	_CANMD, 32, 9, 8, 32, _CANMD_REG
	.member	_CANTRS, 64, 9, 8, 32, _CANTRS_REG
	.member	_CANTRR, 96, 9, 8, 32, _CANTRR_REG
	.member	_CANTA, 128, 9, 8, 32, _CANTA_REG
	.member	_CANAA, 160, 9, 8, 32, _CANAA_REG
	.member	_CANRMP, 192, 9, 8, 32, _CANRMP_REG
	.member	_CANRML, 224, 9, 8, 32, _CANRML_REG
	.member	_CANRFP, 256, 9, 8, 32, _CANRFP_REG
	.member	_CANGAM, 288, 9, 8, 32, _CANGAM_REG
	.member	_CANMC, 320, 9, 8, 32, _CANMC_REG
	.member	_CANBTC, 352, 9, 8, 32, _CANBTC_REG
	.member	_CANES, 384, 9, 8, 32, _CANES_REG
	.member	_CANTEC, 416, 9, 8, 32, _CANTEC_REG
	.member	_CANREC, 448, 9, 8, 32, _CANREC_REG
	.member	_CANGIF0, 480, 9, 8, 32, _CANGIF0_REG
	.member	_CANGIM, 512, 9, 8, 32, _CANGIM_REG
	.member	_CANGIF1, 544, 9, 8, 32, _CANGIF1_REG
	.member	_CANMIM, 576, 9, 8, 32, _CANMIM_REG
	.member	_CANMIL, 608, 9, 8, 32, _CANMIL_REG
	.member	_CANOPC, 640, 9, 8, 32, _CANOPC_REG
	.member	_CANTIOC, 672, 9, 8, 32, _CANTIOC_REG
	.member	_CANRIOC, 704, 9, 8, 32, _CANRIOC_REG
	.member	_CANTSC, 736, 15, 8, 32
	.member	_CANTOC, 768, 9, 8, 32, _CANTOC_REG
	.member	_CANTOS, 800, 9, 8, 32, _CANTOS_REG
	.eos
	.stag	_CANMSGID_BITS, 32
	.member	_EXTMSGID_L, 0, 14, 18, 16
	.member	_EXTMSGID_H, 16, 14, 18, 2
	.member	_STDMSGID, 18, 14, 18, 11
	.member	_AAM, 29, 14, 18, 1
	.member	_AME, 30, 14, 18, 1
	.member	_IDE, 31, 14, 18, 1
	.eos
	.utag	_CANMSGID_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMSGID_BITS
	.eos
	.stag	_CANMSGCTRL_BITS, 32
	.member	_DLC, 0, 14, 18, 4
	.member	_RTR, 4, 14, 18, 1
	.member	_rsvd1, 5, 14, 18, 3
	.member	_TPL, 8, 14, 18, 5
	.member	_rsvd2, 13, 14, 18, 3
	.member	_rsvd3, 16, 14, 18, 16
	.eos
	.utag	_CANMSGCTRL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMSGCTRL_BITS
	.eos
	.stag	_CANMDL_WORDS, 32
	.member	_LOW_WORD, 0, 14, 18, 16
	.member	_HI_WORD, 16, 14, 18, 16
	.eos
	.stag	_CANMDL_BYTES, 32
	.member	_BYTE3, 0, 14, 18, 8
	.member	_BYTE2, 8, 14, 18, 8
	.member	_BYTE1, 16, 14, 18, 8
	.member	_BYTE0, 24, 14, 18, 8
	.eos
	.utag	_CANMDL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_word, 0, 8, 11, 32, _CANMDL_WORDS
	.member	_byte, 0, 8, 11, 32, _CANMDL_BYTES
	.eos
	.stag	_CANMDH_WORDS, 32
	.member	_LOW_WORD, 0, 14, 18, 16
	.member	_HI_WORD, 16, 14, 18, 16
	.eos
	.stag	_CANMDH_BYTES, 32
	.member	_BYTE7, 0, 14, 18, 8
	.member	_BYTE6, 8, 14, 18, 8
	.member	_BYTE5, 16, 14, 18, 8
	.member	_BYTE4, 24, 14, 18, 8
	.eos
	.utag	_CANMDH_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_word, 0, 8, 11, 32, _CANMDH_WORDS
	.member	_byte, 0, 8, 11, 32, _CANMDH_BYTES
	.eos
	.stag	_MBOX, 128
	.member	_MSGID, 0, 9, 8, 32, _CANMSGID_REG
	.member	_MSGCTRL, 32, 9, 8, 32, _CANMSGCTRL_REG
	.member	_MDL, 64, 9, 8, 32, _CANMDL_REG
	.member	_MDH, 96, 9, 8, 32, _CANMDH_REG
	.eos
	.stag	_ECAN_MBOXES, 4096
	.member	_MBOX0, 0, 8, 8, 128, _MBOX
	.member	_MBOX1, 128, 8, 8, 128, _MBOX
	.member	_MBOX2, 256, 8, 8, 128, _MBOX
	.member	_MBOX3, 384, 8, 8, 128, _MBOX
	.member	_MBOX4, 512, 8, 8, 128, _MBOX
	.member	_MBOX5, 640, 8, 8, 128, _MBOX
	.member	_MBOX6, 768, 8, 8, 128, _MBOX
	.member	_MBOX7, 896, 8, 8, 128, _MBOX
	.member	_MBOX8, 1024, 8, 8, 128, _MBOX
	.member	_MBOX9, 1152, 8, 8, 128, _MBOX
	.member	_MBOX10, 1280, 8, 8, 128, _MBOX
	.member	_MBOX11, 1408, 8, 8, 128, _MBOX
	.member	_MBOX12, 1536, 8, 8, 128, _MBOX
	.member	_MBOX13, 1664, 8, 8, 128, _MBOX
	.member	_MBOX14, 1792, 8, 8, 128, _MBOX
	.member	_MBOX15, 1920, 8, 8, 128, _MBOX
	.member	_MBOX16, 2048, 8, 8, 128, _MBOX
	.member	_MBOX17, 2176, 8, 8, 128, _MBOX
	.member	_MBOX18, 2304, 8, 8, 128, _MBOX
	.member	_MBOX19, 2432, 8, 8, 128, _MBOX
	.member	_MBOX20, 2560, 8, 8, 128, _MBOX
	.member	_MBOX21, 2688, 8, 8, 128, _MBOX
	.member	_MBOX22, 2816, 8, 8, 128, _MBOX
	.member	_MBOX23, 2944, 8, 8, 128, _MBOX
	.member	_MBOX24, 3072, 8, 8, 128, _MBOX
	.member	_MBOX25, 3200, 8, 8, 128, _MBOX
	.member	_MBOX26, 3328, 8, 8, 128, _MBOX
	.member	_MBOX27, 3456, 8, 8, 128, _MBOX
	.member	_MBOX28, 3584, 8, 8, 128, _MBOX
	.member	_MBOX29, 3712, 8, 8, 128, _MBOX
	.member	_MBOX30, 3840, 8, 8, 128, _MBOX
	.member	_MBOX31, 3968, 8, 8, 128, _MBOX
	.eos
	.stag	_CANLAM_BITS, 32
	.member	_LAM_L, 0, 14, 18, 16
	.member	_LAM_H, 16, 14, 18, 13
	.member	_rsvd1, 29, 14, 18, 2
	.member	_LAMI, 31, 14, 18, 1
	.eos
	.utag	_CANLAM_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANLAM_BITS
	.eos
	.stag	_LAM_REGS, 1024
	.member	_LAM0, 0, 9, 8, 32, _CANLAM_REG
	.member	_LAM1, 32, 9, 8, 32, _CANLAM_REG
	.member	_LAM2, 64, 9, 8, 32, _CANLAM_REG
	.member	_LAM3, 96, 9, 8, 32, _CANLAM_REG
	.member	_LAM4, 128, 9, 8, 32, _CANLAM_REG
	.member	_LAM5, 160, 9, 8, 32, _CANLAM_REG
	.member	_LAM6, 192, 9, 8, 32, _CANLAM_REG
	.member	_LAM7, 224, 9, 8, 32, _CANLAM_REG
	.member	_LAM8, 256, 9, 8, 32, _CANLAM_REG
	.member	_LAM9, 288, 9, 8, 32, _CANLAM_REG
	.member	_LAM10, 320, 9, 8, 32, _CANLAM_REG
	.member	_LAM11, 352, 9, 8, 32, _CANLAM_REG
	.member	_LAM12, 384, 9, 8, 32, _CANLAM_REG
	.member	_LAM13, 416, 9, 8, 32, _CANLAM_REG
	.member	_LAM14, 448, 9, 8, 32, _CANLAM_REG
	.member	_LAM15, 480, 9, 8, 32, _CANLAM_REG
	.member	_LAM16, 512, 9, 8, 32, _CANLAM_REG
	.member	_LAM17, 544, 9, 8, 32, _CANLAM_REG
	.member	_LAM18, 576, 9, 8, 32, _CANLAM_REG
	.member	_LAM19, 608, 9, 8, 32, _CANLAM_REG
	.member	_LAM20, 640, 9, 8, 32, _CANLAM_REG
	.member	_LAM21, 672, 9, 8, 32, _CANLAM_REG
	.member	_LAM22, 704, 9, 8, 32, _CANLAM_REG
	.member	_LAM23, 736, 9, 8, 32, _CANLAM_REG
	.member	_LAM24, 768, 9, 8, 32, _CANLAM_REG
	.member	_LAM25, 800, 9, 8, 32, _CANLAM_REG
	.member	_LAM26, 832, 9, 8, 32, _CANLAM_REG
	.member	_LAM27, 864, 9, 8, 32, _CANLAM_REG
	.member	_LAM28, 896, 9, 8, 32, _CANLAM_REG
	.member	_LAM29, 928, 9, 8, 32, _CANLAM_REG
	.member	_LAM30, 960, 9, 8, 32, _CANLAM_REG
	.member	_LAM31, 992, 9, 8, 32, _CANLAM_REG
	.eos
	.stag	_MOTS_REGS, 1024
	.member	_MOTS0, 0, 15, 8, 32
	.member	_MOTS1, 32, 15, 8, 32
	.member	_MOTS2, 64, 15, 8, 32
	.member	_MOTS3, 96, 15, 8, 32
	.member	_MOTS4, 128, 15, 8, 32
	.member	_MOTS5, 160, 15, 8, 32
	.member	_MOTS6, 192, 15, 8, 32
	.member	_MOTS7, 224, 15, 8, 32
	.member	_MOTS8, 256, 15, 8, 32
	.member	_MOTS9, 288, 15, 8, 32
	.member	_MOTS10, 320, 15, 8, 32
	.member	_MOTS11, 352, 15, 8, 32
	.member	_MOTS12, 384, 15, 8, 32
	.member	_MOTS13, 416, 15, 8, 32
	.member	_MOTS14, 448, 15, 8, 32
	.member	_MOTS15, 480, 15, 8, 32
	.member	_MOTS16, 512, 15, 8, 32
	.member	_MOTS17, 544, 15, 8, 32
	.member	_MOTS18, 576, 15, 8, 32
	.member	_MOTS19, 608, 15, 8, 32
	.member	_MOTS20, 640, 15, 8, 32
	.member	_MOTS21, 672, 15, 8, 32
	.member	_MOTS22, 704, 15, 8, 32
	.member	_MOTS23, 736, 15, 8, 32
	.member	_MOTS24, 768, 15, 8, 32
	.member	_MOTS25, 800, 15, 8, 32
	.member	_MOTS26, 832, 15, 8, 32
	.member	_MOTS27, 864, 15, 8, 32
	.member	_MOTS28, 896, 15, 8, 32
	.member	_MOTS29, 928, 15, 8, 32
	.member	_MOTS30, 960, 15, 8, 32
	.member	_MOTS31, 992, 15, 8, 32
	.eos
	.stag	_MOTO_REGS, 1024
	.member	_MOTO0, 0, 15, 8, 32
	.member	_MOTO1, 32, 15, 8, 32
	.member	_MOTO2, 64, 15, 8, 32
	.member	_MOTO3, 96, 15, 8, 32
	.member	_MOTO4, 128, 15, 8, 32
	.member	_MOTO5, 160, 15, 8, 32
	.member	_MOTO6, 192, 15, 8, 32
	.member	_MOTO7, 224, 15, 8, 32
	.member	_MOTO8, 256, 15, 8, 32
	.member	_MOTO9, 288, 15, 8, 32
	.member	_MOTO10, 320, 15, 8, 32
	.member	_MOTO11, 352, 15, 8, 32
	.member	_MOTO12, 384, 15, 8, 32
	.member	_MOTO13, 416, 15, 8, 32
	.member	_MOTO14, 448, 15, 8, 32
	.member	_MOTO15, 480, 15, 8, 32
	.member	_MOTO16, 512, 15, 8, 32
	.member	_MOTO17, 544, 15, 8, 32
	.member	_MOTO18, 576, 15, 8, 32
	.member	_MOTO19, 608, 15, 8, 32
	.member	_MOTO20, 640, 15, 8, 32
	.member	_MOTO21, 672, 15, 8, 32
	.member	_MOTO22, 704, 15, 8, 32
	.member	_MOTO23, 736, 15, 8, 32
	.member	_MOTO24, 768, 15, 8, 32
	.member	_MOTO25, 800, 15, 8, 32
	.member	_MOTO26, 832, 15, 8, 32
	.member	_MOTO27, 864, 15, 8, 32
	.member	_MOTO28, 896, 15, 8, 32
	.member	_MOTO29, 928, 15, 8, 32
	.member	_MOTO30, 960, 15, 8, 32
	.member	_MOTO31, 992, 15, 8, 32
	.eos
	.stag	_ECCTL1_BITS, 16
	.member	_CAP1POL, 0, 14, 18, 1
	.member	_CTRRST1, 1, 14, 18, 1
	.member	_CAP2POL, 2, 14, 18, 1
	.member	_CTRRST2, 3, 14, 18, 1
	.member	_CAP3POL, 4, 14, 18, 1
	.member	_CTRRST3, 5, 14, 18, 1
	.member	_CAP4POL, 6, 14, 18, 1
	.member	_CTRRST4, 7, 14, 18, 1
	.member	_CAPLDEN, 8, 14, 18, 1
	.member	_PRESCALE, 9, 14, 18, 5
	.member	_FREE_SOFT, 14, 14, 18, 2
	.eos
	.utag	_ECCTL1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ECCTL1_BITS
	.eos
	.stag	_ECCTL2_BITS, 16
	.member	_CONT_ONESHT, 0, 14, 18, 1
	.member	_STOP_WRAP, 1, 14, 18, 2
	.member	_REARM, 3, 14, 18, 1
	.member	_TSCTRSTOP, 4, 14, 18, 1
	.member	_SYNCI_EN, 5, 14, 18, 1
	.member	_SYNCO_SEL, 6, 14, 18, 2
	.member	_SWSYNC, 8, 14, 18, 1
	.member	_CAP_APWM, 9, 14, 18, 1
	.member	_APWMPOL, 10, 14, 18, 1
	.member	_rsvd1, 11, 14, 18, 5
	.eos
	.utag	_ECCTL2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ECCTL2_BITS
	.eos
	.stag	_ECEINT_BITS, 16
	.member	_rsvd1, 0, 14, 18, 1
	.member	_CEVT1, 1, 14, 18, 1
	.member	_CEVT2, 2, 14, 18, 1
	.member	_CEVT3, 3, 14, 18, 1
	.member	_CEVT4, 4, 14, 18, 1
	.member	_CTROVF, 5, 14, 18, 1
	.member	_CTR_EQ_PRD, 6, 14, 18, 1
	.member	_CTR_EQ_CMP, 7, 14, 18, 1
	.member	_rsvd2, 8, 14, 18, 8
	.eos
	.utag	_ECEINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ECEINT_BITS
	.eos
	.stag	_ECFLG_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_CEVT1, 1, 14, 18, 1
	.member	_CEVT2, 2, 14, 18, 1
	.member	_CEVT3, 3, 14, 18, 1
	.member	_CEVT4, 4, 14, 18, 1
	.member	_CTROVF, 5, 14, 18, 1
	.member	_CTR_EQ_PRD, 6, 14, 18, 1
	.member	_CTR_EQ_CMP, 7, 14, 18, 1
	.member	_rsvd2, 8, 14, 18, 8
	.eos
	.utag	_ECFLG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ECFLG_BITS
	.eos
	.stag	_ECAP_REGS, 512
	.member	_TSCTR, 0, 15, 8, 32
	.member	_CTRPHS, 32, 15, 8, 32
	.member	_CAP1, 64, 15, 8, 32
	.member	_CAP2, 96, 15, 8, 32
	.member	_CAP3, 128, 15, 8, 32
	.member	_CAP4, 160, 15, 8, 32
	.member	_rsvd1, 192, 62, 8, 128, , 8
	.member	_ECCTL1, 320, 9, 8, 16, _ECCTL1_REG
	.member	_ECCTL2, 336, 9, 8, 16, _ECCTL2_REG
	.member	_ECEINT, 352, 9, 8, 16, _ECEINT_REG
	.member	_ECFLG, 368, 9, 8, 16, _ECFLG_REG
	.member	_ECCLR, 384, 9, 8, 16, _ECFLG_REG
	.member	_ECFRC, 400, 9, 8, 16, _ECEINT_REG
	.member	_rsvd2, 416, 62, 8, 96, , 6
	.eos
	.stag	_QDECCTL_BITS, 16
	.member	_rsvd1, 0, 14, 18, 5
	.member	_QSP, 5, 14, 18, 1
	.member	_QIP, 6, 14, 18, 1
	.member	_QBP, 7, 14, 18, 1
	.member	_QAP, 8, 14, 18, 1
	.member	_IGATE, 9, 14, 18, 1
	.member	_SWAP, 10, 14, 18, 1
	.member	_XCR, 11, 14, 18, 1
	.member	_SPSEL, 12, 14, 18, 1
	.member	_SOEN, 13, 14, 18, 1
	.member	_QSRC, 14, 14, 18, 2
	.eos
	.utag	_QDECCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QDECCTL_BITS
	.eos
	.stag	_QEPCTL_BITS, 16
	.member	_WDE, 0, 14, 18, 1
	.member	_UTE, 1, 14, 18, 1
	.member	_QCLM, 2, 14, 18, 1
	.member	_QPEN, 3, 14, 18, 1
	.member	_IEL, 4, 14, 18, 2
	.member	_SEL, 6, 14, 18, 1
	.member	_SWI, 7, 14, 18, 1
	.member	_IEI, 8, 14, 18, 2
	.member	_SEI, 10, 14, 18, 2
	.member	_PCRM, 12, 14, 18, 2
	.member	_FREE_SOFT, 14, 14, 18, 2
	.eos
	.utag	_QEPCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QEPCTL_BITS
	.eos
	.stag	_QCAPCTL_BITS, 16
	.member	_UPPS, 0, 14, 18, 4
	.member	_CCPS, 4, 14, 18, 3
	.member	_rsvd1, 7, 14, 18, 8
	.member	_CEN, 15, 14, 18, 1
	.eos
	.utag	_QCAPCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QCAPCTL_BITS
	.eos
	.stag	_QPOSCTL_BITS, 16
	.member	_PCSPW, 0, 14, 18, 12
	.member	_PCE, 12, 14, 18, 1
	.member	_PCPOL, 13, 14, 18, 1
	.member	_PCLOAD, 14, 14, 18, 1
	.member	_PCSHDW, 15, 14, 18, 1
	.eos
	.utag	_QPOSCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QPOSCTL_BITS
	.eos
	.stag	_QEINT_BITS, 16
	.member	_rsvd1, 0, 14, 18, 1
	.member	_PCE, 1, 14, 18, 1
	.member	_QPE, 2, 14, 18, 1
	.member	_QDC, 3, 14, 18, 1
	.member	_WTO, 4, 14, 18, 1
	.member	_PCU, 5, 14, 18, 1
	.member	_PCO, 6, 14, 18, 1
	.member	_PCR, 7, 14, 18, 1
	.member	_PCM, 8, 14, 18, 1
	.member	_SEL, 9, 14, 18, 1
	.member	_IEL, 10, 14, 18, 1
	.member	_UTO, 11, 14, 18, 1
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_QEINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QEINT_BITS
	.eos
	.stag	_QFLG_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_PCE, 1, 14, 18, 1
	.member	_PHE, 2, 14, 18, 1
	.member	_QDC, 3, 14, 18, 1
	.member	_WTO, 4, 14, 18, 1
	.member	_PCU, 5, 14, 18, 1
	.member	_PCO, 6, 14, 18, 1
	.member	_PCR, 7, 14, 18, 1
	.member	_PCM, 8, 14, 18, 1
	.member	_SEL, 9, 14, 18, 1
	.member	_IEL, 10, 14, 18, 1
	.member	_UTO, 11, 14, 18, 1
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_QFLG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QFLG_BITS
	.eos
	.stag	_QFRC_BITS, 16
	.member	_reserved, 0, 14, 18, 1
	.member	_PCE, 1, 14, 18, 1
	.member	_PHE, 2, 14, 18, 1
	.member	_QDC, 3, 14, 18, 1
	.member	_WTO, 4, 14, 18, 1
	.member	_PCU, 5, 14, 18, 1
	.member	_PCO, 6, 14, 18, 1
	.member	_PCR, 7, 14, 18, 1
	.member	_PCM, 8, 14, 18, 1
	.member	_SEL, 9, 14, 18, 1
	.member	_IEL, 10, 14, 18, 1
	.member	_UTO, 11, 14, 18, 1
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_QFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QFRC_BITS
	.eos
	.stag	_QEPSTS_BITS, 16
	.member	_PCEF, 0, 14, 18, 1
	.member	_FIMF, 1, 14, 18, 1
	.member	_CDEF, 2, 14, 18, 1
	.member	_COEF, 3, 14, 18, 1
	.member	_QDLF, 4, 14, 18, 1
	.member	_QDF, 5, 14, 18, 1
	.member	_FIDF, 6, 14, 18, 1
	.member	_UPEVNT, 7, 14, 18, 1
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_QEPSTS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QEPSTS_BITS
	.eos
	.stag	_EQEP_REGS, 1024
	.member	_QPOSCNT, 0, 15, 8, 32
	.member	_QPOSINIT, 32, 15, 8, 32
	.member	_QPOSMAX, 64, 15, 8, 32
	.member	_QPOSCMP, 96, 15, 8, 32
	.member	_QPOSILAT, 128, 15, 8, 32
	.member	_QPOSSLAT, 160, 15, 8, 32
	.member	_QPOSLAT, 192, 15, 8, 32
	.member	_QUTMR, 224, 15, 8, 32
	.member	_QUPRD, 256, 15, 8, 32
	.member	_QWDTMR, 288, 14, 8, 16
	.member	_QWDPRD, 304, 14, 8, 16
	.member	_QDECCTL, 320, 9, 8, 16, _QDECCTL_REG
	.member	_QEPCTL, 336, 9, 8, 16, _QEPCTL_REG
	.member	_QCAPCTL, 352, 9, 8, 16, _QCAPCTL_REG
	.member	_QPOSCTL, 368, 9, 8, 16, _QPOSCTL_REG
	.member	_QEINT, 384, 9, 8, 16, _QEINT_REG
	.member	_QFLG, 400, 9, 8, 16, _QFLG_REG
	.member	_QCLR, 416, 9, 8, 16, _QFLG_REG
	.member	_QFRC, 432, 9, 8, 16, _QFRC_REG
	.member	_QEPSTS, 448, 9, 8, 16, _QEPSTS_REG
	.member	_QCTMR, 464, 14, 8, 16
	.member	_QCPRD, 480, 14, 8, 16
	.member	_QCTMRLAT, 496, 14, 8, 16
	.member	_QCPRDLAT, 512, 14, 8, 16
	.member	_rsvd1, 528, 62, 8, 480, , 30
	.eos
	.stag	_GPACTRL_BITS, 32
	.member	_QUALPRD0, 0, 14, 18, 8
	.member	_QUALPRD1, 8, 14, 18, 8
	.member	_QUALPRD2, 16, 14, 18, 8
	.member	_QUALPRD3, 24, 14, 18, 8
	.eos
	.utag	_GPACTRL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPACTRL_BITS
	.eos
	.stag	_GPA1_BITS, 32
	.member	_GPIO0, 0, 14, 18, 2
	.member	_GPIO1, 2, 14, 18, 2
	.member	_GPIO2, 4, 14, 18, 2
	.member	_GPIO3, 6, 14, 18, 2
	.member	_GPIO4, 8, 14, 18, 2
	.member	_GPIO5, 10, 14, 18, 2
	.member	_GPIO6, 12, 14, 18, 2
	.member	_GPIO7, 14, 14, 18, 2
	.member	_GPIO8, 16, 14, 18, 2
	.member	_GPIO9, 18, 14, 18, 2
	.member	_GPIO10, 20, 14, 18, 2
	.member	_GPIO11, 22, 14, 18, 2
	.member	_GPIO12, 24, 14, 18, 2
	.member	_GPIO13, 26, 14, 18, 2
	.member	_GPIO14, 28, 14, 18, 2
	.member	_GPIO15, 30, 14, 18, 2
	.eos
	.utag	_GPA1_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPA1_BITS
	.eos
	.stag	_GPA2_BITS, 32
	.member	_GPIO16, 0, 14, 18, 2
	.member	_GPIO17, 2, 14, 18, 2
	.member	_GPIO18, 4, 14, 18, 2
	.member	_GPIO19, 6, 14, 18, 2
	.member	_GPIO20, 8, 14, 18, 2
	.member	_GPIO21, 10, 14, 18, 2
	.member	_GPIO22, 12, 14, 18, 2
	.member	_GPIO23, 14, 14, 18, 2
	.member	_GPIO24, 16, 14, 18, 2
	.member	_GPIO25, 18, 14, 18, 2
	.member	_GPIO26, 20, 14, 18, 2
	.member	_GPIO27, 22, 14, 18, 2
	.member	_GPIO28, 24, 14, 18, 2
	.member	_GPIO29, 26, 14, 18, 2
	.member	_GPIO30, 28, 14, 18, 2
	.member	_GPIO31, 30, 14, 18, 2
	.eos
	.utag	_GPA2_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPA2_BITS
	.eos
	.stag	_GPADAT_BITS, 32
	.member	_GPIO0, 0, 14, 18, 1
	.member	_GPIO1, 1, 14, 18, 1
	.member	_GPIO2, 2, 14, 18, 1
	.member	_GPIO3, 3, 14, 18, 1
	.member	_GPIO4, 4, 14, 18, 1
	.member	_GPIO5, 5, 14, 18, 1
	.member	_GPIO6, 6, 14, 18, 1
	.member	_GPIO7, 7, 14, 18, 1
	.member	_GPIO8, 8, 14, 18, 1
	.member	_GPIO9, 9, 14, 18, 1
	.member	_GPIO10, 10, 14, 18, 1
	.member	_GPIO11, 11, 14, 18, 1
	.member	_GPIO12, 12, 14, 18, 1
	.member	_GPIO13, 13, 14, 18, 1
	.member	_GPIO14, 14, 14, 18, 1
	.member	_GPIO15, 15, 14, 18, 1
	.member	_GPIO16, 16, 14, 18, 1
	.member	_GPIO17, 17, 14, 18, 1
	.member	_GPIO18, 18, 14, 18, 1
	.member	_GPIO19, 19, 14, 18, 1
	.member	_GPIO20, 20, 14, 18, 1
	.member	_GPIO21, 21, 14, 18, 1
	.member	_GPIO22, 22, 14, 18, 1
	.member	_GPIO23, 23, 14, 18, 1
	.member	_GPIO24, 24, 14, 18, 1
	.member	_GPIO25, 25, 14, 18, 1
	.member	_GPIO26, 26, 14, 18, 1
	.member	_GPIO27, 27, 14, 18, 1
	.member	_GPIO28, 28, 14, 18, 1
	.member	_GPIO29, 29, 14, 18, 1
	.member	_GPIO30, 30, 14, 18, 1
	.member	_GPIO31, 31, 14, 18, 1
	.eos
	.utag	_GPADAT_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPADAT_BITS
	.eos
	.stag	_GPBCTRL_BITS, 32
	.member	_QUALPRD0, 0, 14, 18, 8
	.member	_QUALPRD1, 8, 14, 18, 8
	.member	_QUALPRD2, 16, 14, 18, 8
	.member	_QUALPRD3, 24, 14, 18, 8
	.eos
	.utag	_GPBCTRL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPBCTRL_BITS
	.eos
	.stag	_GPB1_BITS, 32
	.member	_GPIO32, 0, 14, 18, 2
	.member	_GPIO33, 2, 14, 18, 2
	.member	_GPIO34, 4, 14, 18, 2
	.member	_GPIO35, 6, 14, 18, 2
	.member	_GPIO36, 8, 14, 18, 2
	.member	_GPIO37, 10, 14, 18, 2
	.member	_GPIO38, 12, 14, 18, 2
	.member	_GPIO39, 14, 14, 18, 2
	.member	_GPIO40, 16, 14, 18, 2
	.member	_GPIO41, 18, 14, 18, 2
	.member	_GPIO42, 20, 14, 18, 2
	.member	_GPIO43, 22, 14, 18, 2
	.member	_GPIO44, 24, 14, 18, 2
	.member	_GPIO45, 26, 14, 18, 2
	.member	_GPIO46, 28, 14, 18, 2
	.member	_GPIO47, 30, 14, 18, 2
	.eos
	.utag	_GPB1_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPB1_BITS
	.eos
	.stag	_GPB2_BITS, 32
	.member	_GPIO48, 0, 14, 18, 2
	.member	_GPIO49, 2, 14, 18, 2
	.member	_GPIO50, 4, 14, 18, 2
	.member	_GPIO51, 6, 14, 18, 2
	.member	_GPIO52, 8, 14, 18, 2
	.member	_GPIO53, 10, 14, 18, 2
	.member	_GPIO54, 12, 14, 18, 2
	.member	_GPIO55, 14, 14, 18, 2
	.member	_GPIO56, 16, 14, 18, 2
	.member	_GPIO57, 18, 14, 18, 2
	.member	_GPIO58, 20, 14, 18, 2
	.member	_GPIO59, 22, 14, 18, 2
	.member	_GPIO60, 24, 14, 18, 2
	.member	_GPIO61, 26, 14, 18, 2
	.member	_GPIO62, 28, 14, 18, 2
	.member	_GPIO63, 30, 14, 18, 2
	.eos
	.utag	_GPB2_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPB2_BITS
	.eos
	.stag	_GPBDAT_BITS, 32
	.member	_GPIO32, 0, 14, 18, 1
	.member	_GPIO33, 1, 14, 18, 1
	.member	_GPIO34, 2, 14, 18, 1
	.member	_GPIO35, 3, 14, 18, 1
	.member	_GPIO36, 4, 14, 18, 1
	.member	_GPIO37, 5, 14, 18, 1
	.member	_GPIO38, 6, 14, 18, 1
	.member	_GPIO39, 7, 14, 18, 1
	.member	_GPIO40, 8, 14, 18, 1
	.member	_GPIO41, 9, 14, 18, 1
	.member	_GPIO42, 10, 14, 18, 1
	.member	_GPIO43, 11, 14, 18, 1
	.member	_GPIO44, 12, 14, 18, 1
	.member	_GPIO45, 13, 14, 18, 1
	.member	_GPIO46, 14, 14, 18, 1
	.member	_GPIO47, 15, 14, 18, 1
	.member	_GPIO48, 16, 14, 18, 1
	.member	_GPIO49, 17, 14, 18, 1
	.member	_GPIO50, 18, 14, 18, 1
	.member	_GPIO51, 19, 14, 18, 1
	.member	_GPIO52, 20, 14, 18, 1
	.member	_GPIO53, 21, 14, 18, 1
	.member	_GPIO54, 22, 14, 18, 1
	.member	_GPIO55, 23, 14, 18, 1
	.member	_GPIO56, 24, 14, 18, 1
	.member	_GPIO57, 25, 14, 18, 1
	.member	_GPIO58, 26, 14, 18, 1
	.member	_GPIO59, 27, 14, 18, 1
	.member	_GPIO60, 28, 14, 18, 1
	.member	_GPIO61, 29, 14, 18, 1
	.member	_GPIO62, 30, 14, 18, 1
	.member	_GPIO63, 31, 14, 18, 1
	.eos
	.utag	_GPBDAT_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPBDAT_BITS
	.eos
	.stag	_GPC1_BITS, 32
	.member	_GPIO64, 0, 14, 18, 2
	.member	_GPIO65, 2, 14, 18, 2
	.member	_GPIO66, 4, 14, 18, 2
	.member	_GPIO67, 6, 14, 18, 2
	.member	_GPIO68, 8, 14, 18, 2
	.member	_GPIO69, 10, 14, 18, 2
	.member	_GPIO70, 12, 14, 18, 2
	.member	_GPIO71, 14, 14, 18, 2
	.member	_GPIO72, 16, 14, 18, 2
	.member	_GPIO73, 18, 14, 18, 2
	.member	_GPIO74, 20, 14, 18, 2
	.member	_GPIO75, 22, 14, 18, 2
	.member	_GPIO76, 24, 14, 18, 2
	.member	_GPIO77, 26, 14, 18, 2
	.member	_GPIO78, 28, 14, 18, 2
	.member	_GPIO79, 30, 14, 18, 2
	.eos
	.utag	_GPC1_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPC1_BITS
	.eos
	.stag	_GPC2_BITS, 32
	.member	_GPIO80, 0, 14, 18, 2
	.member	_GPIO81, 2, 14, 18, 2
	.member	_GPIO82, 4, 14, 18, 2
	.member	_GPIO83, 6, 14, 18, 2
	.member	_GPIO84, 8, 14, 18, 2
	.member	_GPIO85, 10, 14, 18, 2
	.member	_GPIO86, 12, 14, 18, 2
	.member	_GPIO87, 14, 14, 18, 2
	.member	_rsvd, 16, 14, 18, 16
	.eos
	.utag	_GPC2_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPC2_BITS
	.eos
	.stag	_GPCDAT_BITS, 32
	.member	_GPIO64, 0, 14, 18, 1
	.member	_GPIO65, 1, 14, 18, 1
	.member	_GPIO66, 2, 14, 18, 1
	.member	_GPIO67, 3, 14, 18, 1
	.member	_GPIO68, 4, 14, 18, 1
	.member	_GPIO69, 5, 14, 18, 1
	.member	_GPIO70, 6, 14, 18, 1
	.member	_GPIO71, 7, 14, 18, 1
	.member	_GPIO72, 8, 14, 18, 1
	.member	_GPIO73, 9, 14, 18, 1
	.member	_GPIO74, 10, 14, 18, 1
	.member	_GPIO75, 11, 14, 18, 1
	.member	_GPIO76, 12, 14, 18, 1
	.member	_GPIO77, 13, 14, 18, 1
	.member	_GPIO78, 14, 14, 18, 1
	.member	_GPIO79, 15, 14, 18, 1
	.member	_GPIO80, 16, 14, 18, 1
	.member	_GPIO81, 17, 14, 18, 1
	.member	_GPIO82, 18, 14, 18, 1
	.member	_GPIO83, 19, 14, 18, 1
	.member	_GPIO84, 20, 14, 18, 1
	.member	_GPIO85, 21, 14, 18, 1
	.member	_GPIO86, 22, 14, 18, 1
	.member	_GPIO87, 23, 14, 18, 1
	.member	_rsvd1, 24, 14, 18, 8
	.eos
	.utag	_GPCDAT_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPCDAT_BITS
	.eos
	.stag	_GPIO_CTRL_REGS, 736
	.member	_GPACTRL, 0, 9, 8, 32, _GPACTRL_REG
	.member	_GPAQSEL1, 32, 9, 8, 32, _GPA1_REG
	.member	_GPAQSEL2, 64, 9, 8, 32, _GPA2_REG
	.member	_GPAMUX1, 96, 9, 8, 32, _GPA1_REG
	.member	_GPAMUX2, 128, 9, 8, 32, _GPA2_REG
	.member	_GPADIR, 160, 9, 8, 32, _GPADAT_REG
	.member	_GPAPUD, 192, 9, 8, 32, _GPADAT_REG
	.member	_rsvd1, 224, 15, 8, 32
	.member	_GPBCTRL, 256, 9, 8, 32, _GPBCTRL_REG
	.member	_GPBQSEL1, 288, 9, 8, 32, _GPB1_REG
	.member	_GPBQSEL2, 320, 9, 8, 32, _GPB2_REG
	.member	_GPBMUX1, 352, 9, 8, 32, _GPB1_REG
	.member	_GPBMUX2, 384, 9, 8, 32, _GPB2_REG
	.member	_GPBDIR, 416, 9, 8, 32, _GPBDAT_REG
	.member	_GPBPUD, 448, 9, 8, 32, _GPBDAT_REG
	.member	_rsvd2, 480, 62, 8, 128, , 8
	.member	_GPCMUX1, 608, 9, 8, 32, _GPC1_REG
	.member	_GPCMUX2, 640, 9, 8, 32, _GPC2_REG
	.member	_GPCDIR, 672, 9, 8, 32, _GPCDAT_REG
	.member	_GPCPUD, 704, 9, 8, 32, _GPCDAT_REG
	.eos
	.stag	_GPIO_DATA_REGS, 512
	.member	_GPADAT, 0, 9, 8, 32, _GPADAT_REG
	.member	_GPASET, 32, 9, 8, 32, _GPADAT_REG
	.member	_GPACLEAR, 64, 9, 8, 32, _GPADAT_REG
	.member	_GPATOGGLE, 96, 9, 8, 32, _GPADAT_REG
	.member	_GPBDAT, 128, 9, 8, 32, _GPBDAT_REG
	.member	_GPBSET, 160, 9, 8, 32, _GPBDAT_REG
	.member	_GPBCLEAR, 192, 9, 8, 32, _GPBDAT_REG
	.member	_GPBTOGGLE, 224, 9, 8, 32, _GPBDAT_REG
	.member	_GPCDAT, 256, 9, 8, 32, _GPCDAT_REG
	.member	_GPCSET, 288, 9, 8, 32, _GPCDAT_REG
	.member	_GPCCLEAR, 320, 9, 8, 32, _GPCDAT_REG
	.member	_GPCTOGGLE, 352, 9, 8, 32, _GPCDAT_REG
	.member	_rsvd1, 384, 62, 8, 128, , 8
	.eos
	.stag	_GPIOXINT_BITS, 16
	.member	_GPIOSEL, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 11
	.eos
	.utag	_GPIOXINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _GPIOXINT_BITS
	.eos
	.stag	_GPIO_INT_REGS, 160
	.member	_GPIOXINT1SEL, 0, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT2SEL, 16, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXNMISEL, 32, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT3SEL, 48, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT4SEL, 64, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT5SEL, 80, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT6SEL, 96, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT7SEL, 112, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOLPMSEL, 128, 9, 8, 32, _GPADAT_REG
	.eos
	.stag	_I2CIER_BITS, 16
	.member	_ARBL, 0, 14, 18, 1
	.member	_NACK, 1, 14, 18, 1
	.member	_ARDY, 2, 14, 18, 1
	.member	_RRDY, 3, 14, 18, 1
	.member	_XRDY, 4, 14, 18, 1
	.member	_SCD, 5, 14, 18, 1
	.member	_AAS, 6, 14, 18, 1
	.member	_rsvd, 7, 14, 18, 9
	.eos
	.utag	_I2CIER_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CIER_BITS
	.eos
	.stag	_I2CSTR_BITS, 16
	.member	_ARBL, 0, 14, 18, 1
	.member	_NACK, 1, 14, 18, 1
	.member	_ARDY, 2, 14, 18, 1
	.member	_RRDY, 3, 14, 18, 1
	.member	_XRDY, 4, 14, 18, 1
	.member	_SCD, 5, 14, 18, 1
	.member	_rsvd1, 6, 14, 18, 2
	.member	_AD0, 8, 14, 18, 1
	.member	_AAS, 9, 14, 18, 1
	.member	_XSMT, 10, 14, 18, 1
	.member	_RSFULL, 11, 14, 18, 1
	.member	_BB, 12, 14, 18, 1
	.member	_NACKSNT, 13, 14, 18, 1
	.member	_SDIR, 14, 14, 18, 1
	.member	_rsvd2, 15, 14, 18, 1
	.eos
	.utag	_I2CSTR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CSTR_BITS
	.eos
	.stag	_I2CMDR_BITS, 16
	.member	_BC, 0, 14, 18, 3
	.member	_FDF, 3, 14, 18, 1
	.member	_STB, 4, 14, 18, 1
	.member	_IRS, 5, 14, 18, 1
	.member	_DLB, 6, 14, 18, 1
	.member	_RM, 7, 14, 18, 1
	.member	_XA, 8, 14, 18, 1
	.member	_TRX, 9, 14, 18, 1
	.member	_MST, 10, 14, 18, 1
	.member	_STP, 11, 14, 18, 1
	.member	_rsvd1, 12, 14, 18, 1
	.member	_STT, 13, 14, 18, 1
	.member	_FREE, 14, 14, 18, 1
	.member	_NACKMOD, 15, 14, 18, 1
	.eos
	.utag	_I2CMDR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CMDR_BITS
	.eos
	.stag	_I2CISRC_BITS, 16
	.member	_INTCODE, 0, 14, 18, 3
	.member	_rsvd1, 3, 14, 18, 13
	.eos
	.utag	_I2CISRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CISRC_BITS
	.eos
	.stag	_I2CPSC_BITS, 16
	.member	_IPSC, 0, 14, 18, 8
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_I2CPSC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CPSC_BITS
	.eos
	.stag	_I2CFFTX_BITS, 16
	.member	_TXFFIL, 0, 14, 18, 5
	.member	_TXFFIENA, 5, 14, 18, 1
	.member	_TXFFINTCLR, 6, 14, 18, 1
	.member	_TXFFINT, 7, 14, 18, 1
	.member	_TXFFST, 8, 14, 18, 5
	.member	_TXFFRST, 13, 14, 18, 1
	.member	_I2CFFEN, 14, 14, 18, 1
	.member	_rsvd1, 15, 14, 18, 1
	.eos
	.utag	_I2CFFTX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CFFTX_BITS
	.eos
	.stag	_I2CFFRX_BITS, 16
	.member	_RXFFIL, 0, 14, 18, 5
	.member	_RXFFIENA, 5, 14, 18, 1
	.member	_RXFFINTCLR, 6, 14, 18, 1
	.member	_RXFFINT, 7, 14, 18, 1
	.member	_RXFFST, 8, 14, 18, 5
	.member	_RXFFRST, 13, 14, 18, 1
	.member	_rsvd1, 14, 14, 18, 2
	.eos
	.utag	_I2CFFRX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CFFRX_BITS
	.eos
	.stag	_I2C_REGS, 544
	.member	_I2COAR, 0, 14, 8, 16
	.member	_I2CIER, 16, 9, 8, 16, _I2CIER_REG
	.member	_I2CSTR, 32, 9, 8, 16, _I2CSTR_REG
	.member	_I2CCLKL, 48, 14, 8, 16
	.member	_I2CCLKH, 64, 14, 8, 16
	.member	_I2CCNT, 80, 14, 8, 16
	.member	_I2CDRR, 96, 14, 8, 16
	.member	_I2CSAR, 112, 14, 8, 16
	.member	_I2CDXR, 128, 14, 8, 16
	.member	_I2CMDR, 144, 9, 8, 16, _I2CMDR_REG
	.member	_I2CISRC, 160, 9, 8, 16, _I2CISRC_REG
	.member	_rsvd1, 176, 14, 8, 16
	.member	_I2CPSC, 192, 9, 8, 16, _I2CPSC_REG
	.member	_rsvd2, 208, 62, 8, 304, , 19
	.member	_I2CFFTX, 512, 9, 8, 16, _I2CFFTX_REG
	.member	_I2CFFRX, 528, 9, 8, 16, _I2CFFRX_REG
	.eos
	.stag	_DRR2_BITS, 16
	.member	_HWLB, 0, 14, 18, 8
	.member	_HWHB, 8, 14, 18, 8
	.eos
	.utag	_DRR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DRR2_BITS
	.eos
	.stag	_DRR1_BITS, 16
	.member	_LWLB, 0, 14, 18, 8
	.member	_LWHB, 8, 14, 18, 8
	.eos
	.utag	_DRR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DRR1_BITS
	.eos
	.stag	_DXR2_BITS, 16
	.member	_HWLB, 0, 14, 18, 8
	.member	_HWHB, 8, 14, 18, 8
	.eos
	.utag	_DXR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DXR2_BITS
	.eos
	.stag	_DXR1_BITS, 16
	.member	_LWLB, 0, 14, 18, 8
	.member	_LWHB, 8, 14, 18, 8
	.eos
	.utag	_DXR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DXR1_BITS
	.eos
	.stag	_SPCR2_BITS, 16
	.member	_XRST, 0, 14, 18, 1
	.member	_XRDY, 1, 14, 18, 1
	.member	_XEMPTY, 2, 14, 18, 1
	.member	_XSYNCERR, 3, 14, 18, 1
	.member	_XINTM, 4, 14, 18, 2
	.member	_GRST, 6, 14, 18, 1
	.member	_FRST, 7, 14, 18, 1
	.member	_SOFT, 8, 14, 18, 1
	.member	_FREE, 9, 14, 18, 1
	.member	_rsvd, 10, 14, 18, 6
	.eos
	.utag	_SPCR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPCR2_BITS
	.eos
	.stag	_SPCR1_BITS, 16
	.member	_RRST, 0, 14, 18, 1
	.member	_RRDY, 1, 14, 18, 1
	.member	_RFULL, 2, 14, 18, 1
	.member	_RSYNCERR, 3, 14, 18, 1
	.member	_RINTM, 4, 14, 18, 2
	.member	_ABIS, 6, 14, 18, 1
	.member	_DXENA, 7, 14, 18, 1
	.member	_rsvd, 8, 14, 18, 3
	.member	_CLKSTP, 11, 14, 18, 2
	.member	_RJUST, 13, 14, 18, 2
	.member	_DLB, 15, 14, 18, 1
	.eos
	.utag	_SPCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPCR1_BITS
	.eos
	.stag	_RCR2_BITS, 16
	.member	_RDATDLY, 0, 14, 18, 2
	.member	_RFIG, 2, 14, 18, 1
	.member	_RCOMPAND, 3, 14, 18, 2
	.member	_RWDLEN2, 5, 14, 18, 3
	.member	_RFRLEN2, 8, 14, 18, 7
	.member	_RPHASE, 15, 14, 18, 1
	.eos
	.utag	_RCR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCR2_BITS
	.eos
	.stag	_RCR1_BITS, 16
	.member	_rsvd1, 0, 14, 18, 5
	.member	_RWDLEN1, 5, 14, 18, 3
	.member	_RFRLEN1, 8, 14, 18, 7
	.member	_rsvd2, 15, 14, 18, 1
	.eos
	.utag	_RCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCR1_BITS
	.eos
	.stag	_XCR2_BITS, 16
	.member	_XDATDLY, 0, 14, 18, 2
	.member	_XFIG, 2, 14, 18, 1
	.member	_XCOMPAND, 3, 14, 18, 2
	.member	_XWDLEN2, 5, 14, 18, 3
	.member	_XFRLEN2, 8, 14, 18, 7
	.member	_XPHASE, 15, 14, 18, 1
	.eos
	.utag	_XCR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCR2_BITS
	.eos
	.stag	_XCR1_BITS, 16
	.member	_rsvd1, 0, 14, 18, 5
	.member	_XWDLEN1, 5, 14, 18, 3
	.member	_XFRLEN1, 8, 14, 18, 7
	.member	_rsvd2, 15, 14, 18, 1
	.eos
	.utag	_XCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCR1_BITS
	.eos
	.stag	_SRGR2_BITS, 16
	.member	_FPER, 0, 14, 18, 12
	.member	_FSGM, 12, 14, 18, 1
	.member	_CLKSM, 13, 14, 18, 1
	.member	_rsvd, 14, 14, 18, 1
	.member	_GSYNC, 15, 14, 18, 1
	.eos
	.utag	_SRGR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SRGR2_BITS
	.eos
	.stag	_SRGR1_BITS, 16
	.member	_CLKGDV, 0, 14, 18, 8
	.member	_FWID, 8, 14, 18, 8
	.eos
	.utag	_SRGR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SRGR1_BITS
	.eos
	.stag	_MCR2_BITS, 16
	.member	_XMCM, 0, 14, 18, 2
	.member	_XCBLK, 2, 14, 18, 3
	.member	_XPABLK, 5, 14, 18, 2
	.member	_XPBBLK, 7, 14, 18, 2
	.member	_XMCME, 9, 14, 18, 1
	.member	_rsvd, 10, 14, 18, 6
	.eos
	.utag	_MCR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MCR2_BITS
	.eos
	.stag	_MCR1_BITS, 16
	.member	_RMCM, 0, 14, 18, 1
	.member	_rsvd, 1, 14, 18, 1
	.member	_RCBLK, 2, 14, 18, 3
	.member	_RPABLK, 5, 14, 18, 2
	.member	_RPBBLK, 7, 14, 18, 2
	.member	_RMCME, 9, 14, 18, 1
	.member	_rsvd1, 10, 14, 18, 6
	.eos
	.utag	_MCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MCR1_BITS
	.eos
	.stag	_RCERA_BITS, 16
	.member	_RCEA0, 0, 14, 18, 1
	.member	_RCEA1, 1, 14, 18, 1
	.member	_RCEA2, 2, 14, 18, 1
	.member	_RCEA3, 3, 14, 18, 1
	.member	_RCEA4, 4, 14, 18, 1
	.member	_RCEA5, 5, 14, 18, 1
	.member	_RCEA6, 6, 14, 18, 1
	.member	_RCEA7, 7, 14, 18, 1
	.member	_RCEA8, 8, 14, 18, 1
	.member	_RCEA9, 9, 14, 18, 1
	.member	_RCEA10, 10, 14, 18, 1
	.member	_RCEA11, 11, 14, 18, 1
	.member	_RCEA12, 12, 14, 18, 1
	.member	_RCEA13, 13, 14, 18, 1
	.member	_RCEA14, 14, 14, 18, 1
	.member	_RCEA15, 15, 14, 18, 1
	.eos
	.utag	_RCERA_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERA_BITS
	.eos
	.stag	_RCERB_BITS, 16
	.member	_RCEB0, 0, 14, 18, 1
	.member	_RCEB1, 1, 14, 18, 1
	.member	_RCEB2, 2, 14, 18, 1
	.member	_RCEB3, 3, 14, 18, 1
	.member	_RCEB4, 4, 14, 18, 1
	.member	_RCEB5, 5, 14, 18, 1
	.member	_RCEB6, 6, 14, 18, 1
	.member	_RCEB7, 7, 14, 18, 1
	.member	_RCEB8, 8, 14, 18, 1
	.member	_RCEB9, 9, 14, 18, 1
	.member	_RCEB10, 10, 14, 18, 1
	.member	_RCEB11, 11, 14, 18, 1
	.member	_RCEB12, 12, 14, 18, 1
	.member	_RCEB13, 13, 14, 18, 1
	.member	_RCEB14, 14, 14, 18, 1
	.member	_RCEB15, 15, 14, 18, 1
	.eos
	.utag	_RCERB_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERB_BITS
	.eos
	.stag	_XCERA_BITS, 16
	.member	_XCERA0, 0, 14, 18, 1
	.member	_XCERA1, 1, 14, 18, 1
	.member	_XCERA2, 2, 14, 18, 1
	.member	_XCERA3, 3, 14, 18, 1
	.member	_XCERA4, 4, 14, 18, 1
	.member	_XCERA5, 5, 14, 18, 1
	.member	_XCERA6, 6, 14, 18, 1
	.member	_XCERA7, 7, 14, 18, 1
	.member	_XCERA8, 8, 14, 18, 1
	.member	_XCERA9, 9, 14, 18, 1
	.member	_XCERA10, 10, 14, 18, 1
	.member	_XCERA11, 11, 14, 18, 1
	.member	_XCERA12, 12, 14, 18, 1
	.member	_XCERA13, 13, 14, 18, 1
	.member	_XCERA14, 14, 14, 18, 1
	.member	_XCERA15, 15, 14, 18, 1
	.eos
	.utag	_XCERA_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERA_BITS
	.eos
	.stag	_XCERB_BITS, 16
	.member	_XCERB0, 0, 14, 18, 1
	.member	_XCERB1, 1, 14, 18, 1
	.member	_XCERB2, 2, 14, 18, 1
	.member	_XCERB3, 3, 14, 18, 1
	.member	_XCERB4, 4, 14, 18, 1
	.member	_XCERB5, 5, 14, 18, 1
	.member	_XCERB6, 6, 14, 18, 1
	.member	_XCERB7, 7, 14, 18, 1
	.member	_XCERB8, 8, 14, 18, 1
	.member	_XCERB9, 9, 14, 18, 1
	.member	_XCERB10, 10, 14, 18, 1
	.member	_XCERB11, 11, 14, 18, 1
	.member	_XCERB12, 12, 14, 18, 1
	.member	_XCERB13, 13, 14, 18, 1
	.member	_XCERB14, 14, 14, 18, 1
	.member	_XCERB15, 15, 14, 18, 1
	.eos
	.utag	_XCERB_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERB_BITS
	.eos
	.stag	_PCR_BITS, 16
	.member	_CLKRP, 0, 14, 18, 1
	.member	_CLKXP, 1, 14, 18, 1
	.member	_FSRP, 2, 14, 18, 1
	.member	_FSXP, 3, 14, 18, 1
	.member	_DR_STAT, 4, 14, 18, 1
	.member	_DX_STAT, 5, 14, 18, 1
	.member	_CLKS_STAT, 6, 14, 18, 1
	.member	_SCLKME, 7, 14, 18, 1
	.member	_CLKRM, 8, 14, 18, 1
	.member	_CLKXM, 9, 14, 18, 1
	.member	_FSRM, 10, 14, 18, 1
	.member	_FSXM, 11, 14, 18, 1
	.member	_RIOEN, 12, 14, 18, 1
	.member	_XIOEN, 13, 14, 18, 1
	.member	_IDEL_EN, 14, 14, 18, 1
	.member	_rsvd, 15, 14, 18, 1
	.eos
	.utag	_PCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCR_BITS
	.eos
	.stag	_RCERC_BITS, 16
	.member	_RCEC0, 0, 14, 18, 1
	.member	_RCEC1, 1, 14, 18, 1
	.member	_RCEC2, 2, 14, 18, 1
	.member	_RCEC3, 3, 14, 18, 1
	.member	_RCEC4, 4, 14, 18, 1
	.member	_RCEC5, 5, 14, 18, 1
	.member	_RCEC6, 6, 14, 18, 1
	.member	_RCEC7, 7, 14, 18, 1
	.member	_RCEC8, 8, 14, 18, 1
	.member	_RCEC9, 9, 14, 18, 1
	.member	_RCEC10, 10, 14, 18, 1
	.member	_RCEC11, 11, 14, 18, 1
	.member	_RCEC12, 12, 14, 18, 1
	.member	_RCEC13, 13, 14, 18, 1
	.member	_RCEC14, 14, 14, 18, 1
	.member	_RCEC15, 15, 14, 18, 1
	.eos
	.utag	_RCERC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERC_BITS
	.eos
	.stag	_RCERD_BITS, 16
	.member	_RCED0, 0, 14, 18, 1
	.member	_RCED1, 1, 14, 18, 1
	.member	_RCED2, 2, 14, 18, 1
	.member	_RCED3, 3, 14, 18, 1
	.member	_RCED4, 4, 14, 18, 1
	.member	_RCED5, 5, 14, 18, 1
	.member	_RCED6, 6, 14, 18, 1
	.member	_RCED7, 7, 14, 18, 1
	.member	_RCED8, 8, 14, 18, 1
	.member	_RCED9, 9, 14, 18, 1
	.member	_RCED10, 10, 14, 18, 1
	.member	_RCED11, 11, 14, 18, 1
	.member	_RCED12, 12, 14, 18, 1
	.member	_RCED13, 13, 14, 18, 1
	.member	_RCED14, 14, 14, 18, 1
	.member	_RCED15, 15, 14, 18, 1
	.eos
	.utag	_RCERD_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERD_BITS
	.eos
	.stag	_XCERC_BITS, 16
	.member	_XCERC0, 0, 14, 18, 1
	.member	_XCERC1, 1, 14, 18, 1
	.member	_XCERC2, 2, 14, 18, 1
	.member	_XCERC3, 3, 14, 18, 1
	.member	_XCERC4, 4, 14, 18, 1
	.member	_XCERC5, 5, 14, 18, 1
	.member	_XCERC6, 6, 14, 18, 1
	.member	_XCERC7, 7, 14, 18, 1
	.member	_XCERC8, 8, 14, 18, 1
	.member	_XCERC9, 9, 14, 18, 1
	.member	_XCERC10, 10, 14, 18, 1
	.member	_XCERC11, 11, 14, 18, 1
	.member	_XCERC12, 12, 14, 18, 1
	.member	_XCERC13, 13, 14, 18, 1
	.member	_XCERC14, 14, 14, 18, 1
	.member	_XCERC15, 15, 14, 18, 1
	.eos
	.utag	_XCERC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERC_BITS
	.eos
	.stag	_XCERD_BITS, 16
	.member	_XCERD0, 0, 14, 18, 1
	.member	_XCERD1, 1, 14, 18, 1
	.member	_XCERD2, 2, 14, 18, 1
	.member	_XCERD3, 3, 14, 18, 1
	.member	_XCERD4, 4, 14, 18, 1
	.member	_XCERD5, 5, 14, 18, 1
	.member	_XCERD6, 6, 14, 18, 1
	.member	_XCERD7, 7, 14, 18, 1
	.member	_XCERD8, 8, 14, 18, 1
	.member	_XCERD9, 9, 14, 18, 1
	.member	_XCERD10, 10, 14, 18, 1
	.member	_XCERD11, 11, 14, 18, 1
	.member	_XCERD12, 12, 14, 18, 1
	.member	_XCERD13, 13, 14, 18, 1
	.member	_XCERD14, 14, 14, 18, 1
	.member	_XCERD15, 15, 14, 18, 1
	.eos
	.utag	_XCERD_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERD_BITS
	.eos
	.stag	_RCERE_BITS, 16
	.member	_RCEE0, 0, 14, 18, 1
	.member	_RCEE1, 1, 14, 18, 1
	.member	_RCEE2, 2, 14, 18, 1
	.member	_RCEE3, 3, 14, 18, 1
	.member	_RCEE4, 4, 14, 18, 1
	.member	_RCEE5, 5, 14, 18, 1
	.member	_RCEE6, 6, 14, 18, 1
	.member	_RCEE7, 7, 14, 18, 1
	.member	_RCEE8, 8, 14, 18, 1
	.member	_RCEE9, 9, 14, 18, 1
	.member	_RCEE10, 10, 14, 18, 1
	.member	_RCEE11, 11, 14, 18, 1
	.member	_RCEE12, 12, 14, 18, 1
	.member	_RCEE13, 13, 14, 18, 1
	.member	_RCEE14, 14, 14, 18, 1
	.member	_RCEE15, 15, 14, 18, 1
	.eos
	.utag	_RCERE_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERE_BITS
	.eos
	.stag	_RCERF_BITS, 16
	.member	_RCEF0, 0, 14, 18, 1
	.member	_RCEF1, 1, 14, 18, 1
	.member	_RCEF2, 2, 14, 18, 1
	.member	_RCEF3, 3, 14, 18, 1
	.member	_RCEF4, 4, 14, 18, 1
	.member	_RCEF5, 5, 14, 18, 1
	.member	_RCEF6, 6, 14, 18, 1
	.member	_RCEF7, 7, 14, 18, 1
	.member	_RCEF8, 8, 14, 18, 1
	.member	_RCEF9, 9, 14, 18, 1
	.member	_RCEF10, 10, 14, 18, 1
	.member	_RCEF11, 11, 14, 18, 1
	.member	_RCEF12, 12, 14, 18, 1
	.member	_RCEF13, 13, 14, 18, 1
	.member	_RCEF14, 14, 14, 18, 1
	.member	_RCEF15, 15, 14, 18, 1
	.eos
	.utag	_RCERF_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERF_BITS
	.eos
	.stag	_XCERE_BITS, 16
	.member	_XCERE0, 0, 14, 18, 1
	.member	_XCERE1, 1, 14, 18, 1
	.member	_XCERE2, 2, 14, 18, 1
	.member	_XCERE3, 3, 14, 18, 1
	.member	_XCERE4, 4, 14, 18, 1
	.member	_XCERE5, 5, 14, 18, 1
	.member	_XCERE6, 6, 14, 18, 1
	.member	_XCERE7, 7, 14, 18, 1
	.member	_XCERE8, 8, 14, 18, 1
	.member	_XCERE9, 9, 14, 18, 1
	.member	_XCERE10, 10, 14, 18, 1
	.member	_XCERE11, 11, 14, 18, 1
	.member	_XCERE12, 12, 14, 18, 1
	.member	_XCERE13, 13, 14, 18, 1
	.member	_XCERE14, 14, 14, 18, 1
	.member	_XCERE15, 15, 14, 18, 1
	.eos
	.utag	_XCERE_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERE_BITS
	.eos
	.stag	_XCERF_BITS, 16
	.member	_XCERF0, 0, 14, 18, 1
	.member	_XCERF1, 1, 14, 18, 1
	.member	_XCERF2, 2, 14, 18, 1
	.member	_XCERF3, 3, 14, 18, 1
	.member	_XCERF4, 4, 14, 18, 1
	.member	_XCERF5, 5, 14, 18, 1
	.member	_XCERF6, 6, 14, 18, 1
	.member	_XCERF7, 7, 14, 18, 1
	.member	_XCERF8, 8, 14, 18, 1
	.member	_XCERF9, 9, 14, 18, 1
	.member	_XCERF10, 10, 14, 18, 1
	.member	_XCERF11, 11, 14, 18, 1
	.member	_XCERF12, 12, 14, 18, 1
	.member	_XCERF13, 13, 14, 18, 1
	.member	_XCERF14, 14, 14, 18, 1
	.member	_XCERF15, 15, 14, 18, 1
	.eos
	.utag	_XCERF_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERF_BITS
	.eos
	.stag	_RCERG_BITS, 16
	.member	_RCEG0, 0, 14, 18, 1
	.member	_RCEG1, 1, 14, 18, 1
	.member	_RCEG2, 2, 14, 18, 1
	.member	_RCEG3, 3, 14, 18, 1
	.member	_RCEG4, 4, 14, 18, 1
	.member	_RCEG5, 5, 14, 18, 1
	.member	_RCEG6, 6, 14, 18, 1
	.member	_RCEG7, 7, 14, 18, 1
	.member	_RCEG8, 8, 14, 18, 1
	.member	_RCEG9, 9, 14, 18, 1
	.member	_RCEG10, 10, 14, 18, 1
	.member	_RCEG11, 11, 14, 18, 1
	.member	_RCEG12, 12, 14, 18, 1
	.member	_RCEG13, 13, 14, 18, 1
	.member	_RCEG14, 14, 14, 18, 1
	.member	_RCEG15, 15, 14, 18, 1
	.eos
	.utag	_RCERG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERG_BITS
	.eos
	.stag	_RCERH_BITS, 16
	.member	_RCEH0, 0, 14, 18, 1
	.member	_RCEH1, 1, 14, 18, 1
	.member	_RCEH2, 2, 14, 18, 1
	.member	_RCEH3, 3, 14, 18, 1
	.member	_RCEH4, 4, 14, 18, 1
	.member	_RCEH5, 5, 14, 18, 1
	.member	_RCEH6, 6, 14, 18, 1
	.member	_RCEH7, 7, 14, 18, 1
	.member	_RCEH8, 8, 14, 18, 1
	.member	_RCEH9, 9, 14, 18, 1
	.member	_RCEH10, 10, 14, 18, 1
	.member	_RCEH11, 11, 14, 18, 1
	.member	_RCEH12, 12, 14, 18, 1
	.member	_RCEH13, 13, 14, 18, 1
	.member	_RCEH14, 14, 14, 18, 1
	.member	_RCEH15, 15, 14, 18, 1
	.eos
	.utag	_RCERH_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERH_BITS
	.eos
	.stag	_XCERG_BITS, 16
	.member	_XCERG0, 0, 14, 18, 1
	.member	_XCERG1, 1, 14, 18, 1
	.member	_XCERG2, 2, 14, 18, 1
	.member	_XCERG3, 3, 14, 18, 1
	.member	_XCERG4, 4, 14, 18, 1
	.member	_XCERG5, 5, 14, 18, 1
	.member	_XCERG6, 6, 14, 18, 1
	.member	_XCERG7, 7, 14, 18, 1
	.member	_XCERG8, 8, 14, 18, 1
	.member	_XCERG9, 9, 14, 18, 1
	.member	_XCERG10, 10, 14, 18, 1
	.member	_XCERG11, 11, 14, 18, 1
	.member	_XCERG12, 12, 14, 18, 1
	.member	_XCERG13, 13, 14, 18, 1
	.member	_XCERG14, 14, 14, 18, 1
	.member	_XCERG15, 15, 14, 18, 1
	.eos
	.utag	_XCERG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERG_BITS
	.eos
	.stag	_XCERH_BITS, 16
	.member	_XCEH0, 0, 14, 18, 1
	.member	_XCEH1, 1, 14, 18, 1
	.member	_XCEH2, 2, 14, 18, 1
	.member	_XCEH3, 3, 14, 18, 1
	.member	_XCEH4, 4, 14, 18, 1
	.member	_XCEH5, 5, 14, 18, 1
	.member	_XCEH6, 6, 14, 18, 1
	.member	_XCEH7, 7, 14, 18, 1
	.member	_XCEH8, 8, 14, 18, 1
	.member	_XCEH9, 9, 14, 18, 1
	.member	_XCEH10, 10, 14, 18, 1
	.member	_XCEH11, 11, 14, 18, 1
	.member	_XCEH12, 12, 14, 18, 1
	.member	_XCEH13, 13, 14, 18, 1
	.member	_XCEH14, 14, 14, 18, 1
	.member	_XCEH15, 15, 14, 18, 1
	.eos
	.utag	_XCERH_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERH_BITS
	.eos
	.stag	_MFFINT_BITS, 16
	.member	_XINT, 0, 14, 18, 1
	.member	_XEVTA, 1, 14, 18, 1
	.member	_RINT, 2, 14, 18, 1
	.member	_REVTA, 3, 14, 18, 1
	.member	_rsvd, 4, 14, 18, 12
	.eos
	.utag	_MFFINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MFFINT_BITS
	.eos
	.stag	_MFFST_BITS, 16
	.member	_EOBX, 0, 14, 18, 1
	.member	_FSX, 1, 14, 18, 1
	.member	_EOBR, 2, 14, 18, 1
	.member	_FSR, 3, 14, 18, 1
	.member	_rsvd, 4, 14, 18, 12
	.eos
	.utag	_MFFST_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MFFST_BITS
	.eos
	.stag	_MCBSP_REGS, 592
	.member	_DRR2, 0, 9, 8, 16, _DRR2_REG
	.member	_DRR1, 16, 9, 8, 16, _DRR1_REG
	.member	_DXR2, 32, 9, 8, 16, _DXR2_REG
	.member	_DXR1, 48, 9, 8, 16, _DXR1_REG
	.member	_SPCR2, 64, 9, 8, 16, _SPCR2_REG
	.member	_SPCR1, 80, 9, 8, 16, _SPCR1_REG
	.member	_RCR2, 96, 9, 8, 16, _RCR2_REG
	.member	_RCR1, 112, 9, 8, 16, _RCR1_REG
	.member	_XCR2, 128, 9, 8, 16, _XCR2_REG
	.member	_XCR1, 144, 9, 8, 16, _XCR1_REG
	.member	_SRGR2, 160, 9, 8, 16, _SRGR2_REG
	.member	_SRGR1, 176, 9, 8, 16, _SRGR1_REG
	.member	_MCR2, 192, 9, 8, 16, _MCR2_REG
	.member	_MCR1, 208, 9, 8, 16, _MCR1_REG
	.member	_RCERA, 224, 9, 8, 16, _RCERA_REG
	.member	_RCERB, 240, 9, 8, 16, _RCERB_REG
	.member	_XCERA, 256, 9, 8, 16, _XCERA_REG
	.member	_XCERB, 272, 9, 8, 16, _XCERB_REG
	.member	_PCR, 288, 9, 8, 16, _PCR_REG
	.member	_RCERC, 304, 9, 8, 16, _RCERC_REG
	.member	_RCERD, 320, 9, 8, 16, _RCERD_REG
	.member	_XCERC, 336, 9, 8, 16, _XCERC_REG
	.member	_XCERD, 352, 9, 8, 16, _XCERD_REG
	.member	_RCERE, 368, 9, 8, 16, _RCERE_REG
	.member	_RCERF, 384, 9, 8, 16, _RCERF_REG
	.member	_XCERE, 400, 9, 8, 16, _XCERE_REG
	.member	_XCERF, 416, 9, 8, 16, _XCERF_REG
	.member	_RCERG, 432, 9, 8, 16, _RCERG_REG
	.member	_RCERH, 448, 9, 8, 16, _RCERH_REG
	.member	_XCERG, 464, 9, 8, 16, _XCERG_REG
	.member	_XCERH, 480, 9, 8, 16, _XCERH_REG
	.member	_rsvd1, 496, 62, 8, 64, , 4
	.member	_MFFINT, 560, 9, 8, 16, _MFFINT_REG
	.member	_MFFST, 576, 9, 8, 16, _MFFST_REG
	.eos
	.stag	_PIECTRL_BITS, 16
	.member	_ENPIE, 0, 14, 18, 1
	.member	_PIEVECT, 1, 14, 18, 15
	.eos
	.utag	_PIECTRL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PIECTRL_BITS
	.eos
	.stag	_PIEACK_BITS, 16
	.member	_ACK1, 0, 14, 18, 1
	.member	_ACK2, 1, 14, 18, 1
	.member	_ACK3, 2, 14, 18, 1
	.member	_ACK4, 3, 14, 18, 1
	.member	_ACK5, 4, 14, 18, 1
	.member	_ACK6, 5, 14, 18, 1
	.member	_ACK7, 6, 14, 18, 1
	.member	_ACK8, 7, 14, 18, 1
	.member	_ACK9, 8, 14, 18, 1
	.member	_ACK10, 9, 14, 18, 1
	.member	_ACK11, 10, 14, 18, 1
	.member	_ACK12, 11, 14, 18, 1
	.member	_rsvd, 12, 14, 18, 4
	.eos
	.utag	_PIEACK_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PIEACK_BITS
	.eos
	.stag	_PIEIER_BITS, 16
	.member	_INTx1, 0, 14, 18, 1
	.member	_INTx2, 1, 14, 18, 1
	.member	_INTx3, 2, 14, 18, 1
	.member	_INTx4, 3, 14, 18, 1
	.member	_INTx5, 4, 14, 18, 1
	.member	_INTx6, 5, 14, 18, 1
	.member	_INTx7, 6, 14, 18, 1
	.member	_INTx8, 7, 14, 18, 1
	.member	_rsvd, 8, 14, 18, 8
	.eos
	.utag	_PIEIER_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PIEIER_BITS
	.eos
	.stag	_PIEIFR_BITS, 16
	.member	_INTx1, 0, 14, 18, 1
	.member	_INTx2, 1, 14, 18, 1
	.member	_INTx3, 2, 14, 18, 1
	.member	_INTx4, 3, 14, 18, 1
	.member	_INTx5, 4, 14, 18, 1
	.member	_INTx6, 5, 14, 18, 1
	.member	_INTx7, 6, 14, 18, 1
	.member	_INTx8, 7, 14, 18, 1
	.member	_rsvd, 8, 14, 18, 8
	.eos
	.utag	_PIEIFR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PIEIFR_BITS
	.eos
	.stag	_PIE_CTRL_REGS, 416
	.member	_PIECTRL, 0, 9, 8, 16, _PIECTRL_REG
	.member	_PIEACK, 16, 9, 8, 16, _PIEACK_REG
	.member	_PIEIER1, 32, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR1, 48, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER2, 64, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR2, 80, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER3, 96, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR3, 112, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER4, 128, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR4, 144, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER5, 160, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR5, 176, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER6, 192, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR6, 208, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER7, 224, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR7, 240, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER8, 256, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR8, 272, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER9, 288, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR9, 304, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER10, 320, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR10, 336, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER11, 352, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR11, 368, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER12, 384, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR12, 400, 9, 8, 16, _PIEIFR_REG
	.eos
	.stag	_PIE_VECT_TABLE, 4096
	.member	_PIE1_RESERVED, 0, 144, 8, 22
	.member	_PIE2_RESERVED, 32, 144, 8, 22
	.member	_PIE3_RESERVED, 64, 144, 8, 22
	.member	_PIE4_RESERVED, 96, 144, 8, 22
	.member	_PIE5_RESERVED, 128, 144, 8, 22
	.member	_PIE6_RESERVED, 160, 144, 8, 22
	.member	_PIE7_RESERVED, 192, 144, 8, 22
	.member	_PIE8_RESERVED, 224, 144, 8, 22
	.member	_PIE9_RESERVED, 256, 144, 8, 22
	.member	_PIE10_RESERVED, 288, 144, 8, 22
	.member	_PIE11_RESERVED, 320, 144, 8, 22
	.member	_PIE12_RESERVED, 352, 144, 8, 22
	.member	_PIE13_RESERVED, 384, 144, 8, 22
	.member	_XINT13, 416, 144, 8, 22
	.member	_TINT2, 448, 144, 8, 22
	.member	_DATALOG, 480, 144, 8, 22
	.member	_RTOSINT, 512, 144, 8, 22
	.member	_EMUINT, 544, 144, 8, 22
	.member	_XNMI, 576, 144, 8, 22
	.member	_ILLEGAL, 608, 144, 8, 22
	.member	_USER1, 640, 144, 8, 22
	.member	_USER2, 672, 144, 8, 22
	.member	_USER3, 704, 144, 8, 22
	.member	_USER4, 736, 144, 8, 22
	.member	_USER5, 768, 144, 8, 22
	.member	_USER6, 800, 144, 8, 22
	.member	_USER7, 832, 144, 8, 22
	.member	_USER8, 864, 144, 8, 22
	.member	_USER9, 896, 144, 8, 22
	.member	_USER10, 928, 144, 8, 22
	.member	_USER11, 960, 144, 8, 22
	.member	_USER12, 992, 144, 8, 22
	.member	_SEQ1INT, 1024, 144, 8, 22
	.member	_SEQ2INT, 1056, 144, 8, 22
	.member	_rsvd1_3, 1088, 144, 8, 22
	.member	_XINT1, 1120, 144, 8, 22
	.member	_XINT2, 1152, 144, 8, 22
	.member	_ADCINT, 1184, 144, 8, 22
	.member	_TINT0, 1216, 144, 8, 22
	.member	_WAKEINT, 1248, 144, 8, 22
	.member	_EPWM1_TZINT, 1280, 144, 8, 22
	.member	_EPWM2_TZINT, 1312, 144, 8, 22
	.member	_EPWM3_TZINT, 1344, 144, 8, 22
	.member	_EPWM4_TZINT, 1376, 144, 8, 22
	.member	_EPWM5_TZINT, 1408, 144, 8, 22
	.member	_EPWM6_TZINT, 1440, 144, 8, 22
	.member	_rsvd2_7, 1472, 144, 8, 22
	.member	_rsvd2_8, 1504, 144, 8, 22
	.member	_EPWM1_INT, 1536, 144, 8, 22
	.member	_EPWM2_INT, 1568, 144, 8, 22
	.member	_EPWM3_INT, 1600, 144, 8, 22
	.member	_EPWM4_INT, 1632, 144, 8, 22
	.member	_EPWM5_INT, 1664, 144, 8, 22
	.member	_EPWM6_INT, 1696, 144, 8, 22
	.member	_rsvd3_7, 1728, 144, 8, 22
	.member	_rsvd3_8, 1760, 144, 8, 22
	.member	_ECAP1_INT, 1792, 144, 8, 22
	.member	_ECAP2_INT, 1824, 144, 8, 22
	.member	_ECAP3_INT, 1856, 144, 8, 22
	.member	_ECAP4_INT, 1888, 144, 8, 22
	.member	_ECAP5_INT, 1920, 144, 8, 22
	.member	_ECAP6_INT, 1952, 144, 8, 22
	.member	_rsvd4_7, 1984, 144, 8, 22
	.member	_rsvd4_8, 2016, 144, 8, 22
	.member	_EQEP1_INT, 2048, 144, 8, 22
	.member	_EQEP2_INT, 2080, 144, 8, 22
	.member	_rsvd5_3, 2112, 144, 8, 22
	.member	_rsvd5_4, 2144, 144, 8, 22
	.member	_rsvd5_5, 2176, 144, 8, 22
	.member	_rsvd5_6, 2208, 144, 8, 22
	.member	_rsvd5_7, 2240, 144, 8, 22
	.member	_rsvd5_8, 2272, 144, 8, 22
	.member	_SPIRXINTA, 2304, 144, 8, 22
	.member	_SPITXINTA, 2336, 144, 8, 22
	.member	_MRINTB, 2368, 144, 8, 22
	.member	_MXINTB, 2400, 144, 8, 22
	.member	_MRINTA, 2432, 144, 8, 22
	.member	_MXINTA, 2464, 144, 8, 22
	.member	_rsvd6_7, 2496, 144, 8, 22
	.member	_rsvd6_8, 2528, 144, 8, 22
	.member	_DINTCH1, 2560, 144, 8, 22
	.member	_DINTCH2, 2592, 144, 8, 22
	.member	_DINTCH3, 2624, 144, 8, 22
	.member	_DINTCH4, 2656, 144, 8, 22
	.member	_DINTCH5, 2688, 144, 8, 22
	.member	_DINTCH6, 2720, 144, 8, 22
	.member	_rsvd7_7, 2752, 144, 8, 22
	.member	_rsvd7_8, 2784, 144, 8, 22
	.member	_I2CINT1A, 2816, 144, 8, 22
	.member	_I2CINT2A, 2848, 144, 8, 22
	.member	_rsvd8_3, 2880, 144, 8, 22
	.member	_rsvd8_4, 2912, 144, 8, 22
	.member	_SCIRXINTC, 2944, 144, 8, 22
	.member	_SCITXINTC, 2976, 144, 8, 22
	.member	_rsvd8_7, 3008, 144, 8, 22
	.member	_rsvd8_8, 3040, 144, 8, 22
	.member	_SCIRXINTA, 3072, 144, 8, 22
	.member	_SCITXINTA, 3104, 144, 8, 22
	.member	_SCIRXINTB, 3136, 144, 8, 22
	.member	_SCITXINTB, 3168, 144, 8, 22
	.member	_ECAN0INTA, 3200, 144, 8, 22
	.member	_ECAN1INTA, 3232, 144, 8, 22
	.member	_ECAN0INTB, 3264, 144, 8, 22
	.member	_ECAN1INTB, 3296, 144, 8, 22
	.member	_rsvd10_1, 3328, 144, 8, 22
	.member	_rsvd10_2, 3360, 144, 8, 22
	.member	_rsvd10_3, 3392, 144, 8, 22
	.member	_rsvd10_4, 3424, 144, 8, 22
	.member	_rsvd10_5, 3456, 144, 8, 22
	.member	_rsvd10_6, 3488, 144, 8, 22
	.member	_rsvd10_7, 3520, 144, 8, 22
	.member	_rsvd10_8, 3552, 144, 8, 22
	.member	_rsvd11_1, 3584, 144, 8, 22
	.member	_rsvd11_2, 3616, 144, 8, 22
	.member	_rsvd11_3, 3648, 144, 8, 22
	.member	_rsvd11_4, 3680, 144, 8, 22
	.member	_rsvd11_5, 3712, 144, 8, 22
	.member	_rsvd11_6, 3744, 144, 8, 22
	.member	_rsvd11_7, 3776, 144, 8, 22
	.member	_rsvd11_8, 3808, 144, 8, 22
	.member	_XINT3, 3840, 144, 8, 22
	.member	_XINT4, 3872, 144, 8, 22
	.member	_XINT5, 3904, 144, 8, 22
	.member	_XINT6, 3936, 144, 8, 22
	.member	_XINT7, 3968, 144, 8, 22
	.member	_rsvd12_6, 4000, 144, 8, 22
	.member	_LVF, 4032, 144, 8, 22
	.member	_LUF, 4064, 144, 8, 22
	.eos
	.stag	_SCICCR_BITS, 16
	.member	_SCICHAR, 0, 14, 18, 3
	.member	_ADDRIDLE_MODE, 3, 14, 18, 1
	.member	_LOOPBKENA, 4, 14, 18, 1
	.member	_PARITYENA, 5, 14, 18, 1
	.member	_PARITY, 6, 14, 18, 1
	.member	_STOPBITS, 7, 14, 18, 1
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_SCICCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCICCR_BITS
	.eos
	.stag	_SCICTL1_BITS, 16
	.member	_RXENA, 0, 14, 18, 1
	.member	_TXENA, 1, 14, 18, 1
	.member	_SLEEP, 2, 14, 18, 1
	.member	_TXWAKE, 3, 14, 18, 1
	.member	_rsvd, 4, 14, 18, 1
	.member	_SWRESET, 5, 14, 18, 1
	.member	_RXERRINTENA, 6, 14, 18, 1
	.member	_rsvd1, 7, 14, 18, 9
	.eos
	.utag	_SCICTL1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCICTL1_BITS
	.eos
	.stag	_SCICTL2_BITS, 16
	.member	_TXINTENA, 0, 14, 18, 1
	.member	_RXBKINTENA, 1, 14, 18, 1
	.member	_rsvd, 2, 14, 18, 4
	.member	_TXEMPTY, 6, 14, 18, 1
	.member	_TXRDY, 7, 14, 18, 1
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_SCICTL2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCICTL2_BITS
	.eos
	.stag	_SCIRXST_BITS, 16
	.member	_rsvd, 0, 14, 18, 1
	.member	_RXWAKE, 1, 14, 18, 1
	.member	_PE, 2, 14, 18, 1
	.member	_OE, 3, 14, 18, 1
	.member	_FE, 4, 14, 18, 1
	.member	_BRKDT, 5, 14, 18, 1
	.member	_RXRDY, 6, 14, 18, 1
	.member	_RXERROR, 7, 14, 18, 1
	.eos
	.utag	_SCIRXST_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIRXST_BITS
	.eos
	.stag	_SCIRXBUF_BITS, 16
	.member	_RXDT, 0, 14, 18, 8
	.member	_rsvd, 8, 14, 18, 6
	.member	_SCIFFPE, 14, 14, 18, 1
	.member	_SCIFFFE, 15, 14, 18, 1
	.eos
	.utag	_SCIRXBUF_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIRXBUF_BITS
	.eos
	.stag	_SCIFFTX_BITS, 16
	.member	_TXFFIL, 0, 14, 18, 5
	.member	_TXFFIENA, 5, 14, 18, 1
	.member	_TXFFINTCLR, 6, 14, 18, 1
	.member	_TXFFINT, 7, 14, 18, 1
	.member	_TXFFST, 8, 14, 18, 5
	.member	_TXFIFOXRESET, 13, 14, 18, 1
	.member	_SCIFFENA, 14, 14, 18, 1
	.member	_SCIRST, 15, 14, 18, 1
	.eos
	.utag	_SCIFFTX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIFFTX_BITS
	.eos
	.stag	_SCIFFRX_BITS, 16
	.member	_RXFFIL, 0, 14, 18, 5
	.member	_RXFFIENA, 5, 14, 18, 1
	.member	_RXFFINTCLR, 6, 14, 18, 1
	.member	_RXFFINT, 7, 14, 18, 1
	.member	_RXFFST, 8, 14, 18, 5
	.member	_RXFIFORESET, 13, 14, 18, 1
	.member	_RXFFOVRCLR, 14, 14, 18, 1
	.member	_RXFFOVF, 15, 14, 18, 1
	.eos
	.utag	_SCIFFRX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIFFRX_BITS
	.eos
	.stag	_SCIFFCT_BITS, 16
	.member	_FFTXDLY, 0, 14, 18, 8
	.member	_rsvd, 8, 14, 18, 5
	.member	_CDC, 13, 14, 18, 1
	.member	_ABDCLR, 14, 14, 18, 1
	.member	_ABD, 15, 14, 18, 1
	.eos
	.utag	_SCIFFCT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIFFCT_BITS
	.eos
	.stag	_SCIPRI_BITS, 16
	.member	_rsvd, 0, 14, 18, 3
	.member	_FREE, 3, 14, 18, 1
	.member	_SOFT, 4, 14, 18, 1
	.member	_rsvd1, 5, 14, 18, 3
	.eos
	.utag	_SCIPRI_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIPRI_BITS
	.eos
	.stag	_SCI_REGS, 256
	.member	_SCICCR, 0, 9, 8, 16, _SCICCR_REG
	.member	_SCICTL1, 16, 9, 8, 16, _SCICTL1_REG
	.member	_SCIHBAUD, 32, 14, 8, 16
	.member	_SCILBAUD, 48, 14, 8, 16
	.member	_SCICTL2, 64, 9, 8, 16, _SCICTL2_REG
	.member	_SCIRXST, 80, 9, 8, 16, _SCIRXST_REG
	.member	_SCIRXEMU, 96, 14, 8, 16
	.member	_SCIRXBUF, 112, 9, 8, 16, _SCIRXBUF_REG
	.member	_rsvd1, 128, 14, 8, 16
	.member	_SCITXBUF, 144, 14, 8, 16
	.member	_SCIFFTX, 160, 9, 8, 16, _SCIFFTX_REG
	.member	_SCIFFRX, 176, 9, 8, 16, _SCIFFRX_REG
	.member	_SCIFFCT, 192, 9, 8, 16, _SCIFFCT_REG
	.member	_rsvd2, 208, 14, 8, 16
	.member	_rsvd3, 224, 14, 8, 16
	.member	_SCIPRI, 240, 9, 8, 16, _SCIPRI_REG
	.eos
	.stag	_SPICCR_BITS, 16
	.member	_SPICHAR, 0, 14, 18, 4
	.member	_SPILBK, 4, 14, 18, 1
	.member	_rsvd1, 5, 14, 18, 1
	.member	_CLKPOLARITY, 6, 14, 18, 1
	.member	_SPISWRESET, 7, 14, 18, 1
	.member	_rsvd2, 8, 14, 18, 8
	.eos
	.utag	_SPICCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPICCR_BITS
	.eos
	.stag	_SPICTL_BITS, 16
	.member	_SPIINTENA, 0, 14, 18, 1
	.member	_TALK, 1, 14, 18, 1
	.member	_MASTER_SLAVE, 2, 14, 18, 1
	.member	_CLK_PHASE, 3, 14, 18, 1
	.member	_OVERRUNINTENA, 4, 14, 18, 1
	.member	_rsvd, 5, 14, 18, 11
	.eos
	.utag	_SPICTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPICTL_BITS
	.eos
	.stag	_SPISTS_BITS, 16
	.member	_rsvd1, 0, 14, 18, 5
	.member	_BUFFULL_FLAG, 5, 14, 18, 1
	.member	_INT_FLAG, 6, 14, 18, 1
	.member	_OVERRUN_FLAG, 7, 14, 18, 1
	.member	_rsvd2, 8, 14, 18, 8
	.eos
	.utag	_SPISTS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPISTS_BITS
	.eos
	.stag	_SPIFFTX_BITS, 16
	.member	_TXFFIL, 0, 14, 18, 5
	.member	_TXFFIENA, 5, 14, 18, 1
	.member	_TXFFINTCLR, 6, 14, 18, 1
	.member	_TXFFINT, 7, 14, 18, 1
	.member	_TXFFST, 8, 14, 18, 5
	.member	_TXFIFO, 13, 14, 18, 1
	.member	_SPIFFENA, 14, 14, 18, 1
	.member	_SPIRST, 15, 14, 18, 1
	.eos
	.utag	_SPIFFTX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPIFFTX_BITS
	.eos
	.stag	_SPIFFRX_BITS, 16
	.member	_RXFFIL, 0, 14, 18, 5
	.member	_RXFFIENA, 5, 14, 18, 1
	.member	_RXFFINTCLR, 6, 14, 18, 1
	.member	_RXFFINT, 7, 14, 18, 1
	.member	_RXFFST, 8, 14, 18, 5
	.member	_RXFIFORESET, 13, 14, 18, 1
	.member	_RXFFOVFCLR, 14, 14, 18, 1
	.member	_RXFFOVF, 15, 14, 18, 1
	.eos
	.utag	_SPIFFRX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPIFFRX_BITS
	.eos
	.stag	_SPIFFCT_BITS, 16
	.member	_TXDLY, 0, 14, 18, 8
	.member	_rsvd, 8, 14, 18, 8
	.eos
	.utag	_SPIFFCT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPIFFCT_BITS
	.eos
	.stag	_SPIPRI_BITS, 16
	.member	_rsvd1, 0, 14, 18, 4
	.member	_FREE, 4, 14, 18, 1
	.member	_SOFT, 5, 14, 18, 1
	.member	_PRIORITY, 6, 14, 18, 1
	.member	_rsvd2, 7, 14, 18, 9
	.eos
	.utag	_SPIPRI_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPIPRI_BITS
	.eos
	.stag	_SPI_REGS, 256
	.member	_SPICCR, 0, 9, 8, 16, _SPICCR_REG
	.member	_SPICTL, 16, 9, 8, 16, _SPICTL_REG
	.member	_SPISTS, 32, 9, 8, 16, _SPISTS_REG
	.member	_rsvd1, 48, 14, 8, 16
	.member	_SPIBRR, 64, 14, 8, 16
	.member	_rsvd2, 80, 14, 8, 16
	.member	_SPIRXEMU, 96, 14, 8, 16
	.member	_SPIRXBUF, 112, 14, 8, 16
	.member	_SPITXBUF, 128, 14, 8, 16
	.member	_SPIDAT, 144, 14, 8, 16
	.member	_SPIFFTX, 160, 9, 8, 16, _SPIFFTX_REG
	.member	_SPIFFRX, 176, 9, 8, 16, _SPIFFRX_REG
	.member	_SPIFFCT, 192, 9, 8, 16, _SPIFFCT_REG
	.member	_rsvd3, 208, 62, 8, 32, , 2
	.member	_SPIPRI, 240, 9, 8, 16, _SPIPRI_REG
	.eos
	.stag	_PLLSTS_BITS, 16
	.member	_PLLLOCKS, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_PLLOFF, 2, 14, 18, 1
	.member	_MCLKSTS, 3, 14, 18, 1
	.member	_MCLKCLR, 4, 14, 18, 1
	.member	_OSCOFF, 5, 14, 18, 1
	.member	_MCLKOFF, 6, 14, 18, 1
	.member	_DIVSEL, 7, 14, 18, 2
	.member	_rsvd2, 9, 14, 18, 7
	.eos
	.utag	_PLLSTS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PLLSTS_BITS
	.eos
	.stag	_HISPCP_BITS, 16
	.member	_HSPCLK, 0, 14, 18, 3
	.member	_rsvd1, 3, 14, 18, 13
	.eos
	.utag	_HISPCP_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _HISPCP_BITS
	.eos
	.stag	_LOSPCP_BITS, 16
	.member	_LSPCLK, 0, 14, 18, 3
	.member	_rsvd1, 3, 14, 18, 13
	.eos
	.utag	_LOSPCP_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _LOSPCP_BITS
	.eos
	.stag	_PCLKCR0_BITS, 16
	.member	_rsvd1, 0, 14, 18, 2
	.member	_TBCLKSYNC, 2, 14, 18, 1
	.member	_ADCENCLK, 3, 14, 18, 1
	.member	_I2CAENCLK, 4, 14, 18, 1
	.member	_SCICENCLK, 5, 14, 18, 1
	.member	_rsvd2, 6, 14, 18, 2
	.member	_SPIAENCLK, 8, 14, 18, 1
	.member	_rsvd3, 9, 14, 18, 1
	.member	_SCIAENCLK, 10, 14, 18, 1
	.member	_SCIBENCLK, 11, 14, 18, 1
	.member	_MCBSPAENCLK, 12, 14, 18, 1
	.member	_MCBSPBENCLK, 13, 14, 18, 1
	.member	_ECANAENCLK, 14, 14, 18, 1
	.member	_ECANBENCLK, 15, 14, 18, 1
	.eos
	.utag	_PCLKCR0_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCLKCR0_BITS
	.eos
	.stag	_PCLKCR1_BITS, 16
	.member	_EPWM1ENCLK, 0, 14, 18, 1
	.member	_EPWM2ENCLK, 1, 14, 18, 1
	.member	_EPWM3ENCLK, 2, 14, 18, 1
	.member	_EPWM4ENCLK, 3, 14, 18, 1
	.member	_EPWM5ENCLK, 4, 14, 18, 1
	.member	_EPWM6ENCLK, 5, 14, 18, 1
	.member	_rsvd1, 6, 14, 18, 2
	.member	_ECAP1ENCLK, 8, 14, 18, 1
	.member	_ECAP2ENCLK, 9, 14, 18, 1
	.member	_ECAP3ENCLK, 10, 14, 18, 1
	.member	_ECAP4ENCLK, 11, 14, 18, 1
	.member	_ECAP5ENCLK, 12, 14, 18, 1
	.member	_ECAP6ENCLK, 13, 14, 18, 1
	.member	_EQEP1ENCLK, 14, 14, 18, 1
	.member	_EQEP2ENCLK, 15, 14, 18, 1
	.eos
	.utag	_PCLKCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCLKCR1_BITS
	.eos
	.stag	_LPMCR0_BITS, 16
	.member	_LPM, 0, 14, 18, 2
	.member	_QUALSTDBY, 2, 14, 18, 6
	.member	_rsvd1, 8, 14, 18, 7
	.member	_WDINTE, 15, 14, 18, 1
	.eos
	.utag	_LPMCR0_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _LPMCR0_BITS
	.eos
	.stag	_PCLKCR3_BITS, 16
	.member	_rsvd1, 0, 14, 18, 8
	.member	_CPUTIMER0ENCLK, 8, 14, 18, 1
	.member	_CPUTIMER1ENCLK, 9, 14, 18, 1
	.member	_CPUTIMER2ENCLK, 10, 14, 18, 1
	.member	_DMAENCLK, 11, 14, 18, 1
	.member	_XINTFENCLK, 12, 14, 18, 1
	.member	_GPIOINENCLK, 13, 14, 18, 1
	.member	_rsvd2, 14, 14, 18, 2
	.eos
	.utag	_PCLKCR3_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCLKCR3_BITS
	.eos
	.stag	_PLLCR_BITS, 16
	.member	_DIV, 0, 14, 18, 4
	.member	_rsvd1, 4, 14, 18, 12
	.eos
	.utag	_PLLCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PLLCR_BITS
	.eos
	.stag	_SYS_CTRL_REGS, 512
	.member	_rsvd7, 0, 14, 8, 16
	.member	_PLLSTS, 16, 9, 8, 16, _PLLSTS_REG
	.member	_rsvd1, 32, 62, 8, 128, , 8
	.member	_HISPCP, 160, 9, 8, 16, _HISPCP_REG
	.member	_LOSPCP, 176, 9, 8, 16, _LOSPCP_REG
	.member	_PCLKCR0, 192, 9, 8, 16, _PCLKCR0_REG
	.member	_PCLKCR1, 208, 9, 8, 16, _PCLKCR1_REG
	.member	_LPMCR0, 224, 9, 8, 16, _LPMCR0_REG
	.member	_rsvd2, 240, 14, 8, 16
	.member	_PCLKCR3, 256, 9, 8, 16, _PCLKCR3_REG
	.member	_PLLCR, 272, 9, 8, 16, _PLLCR_REG
	.member	_SCSR, 288, 14, 8, 16
	.member	_WDCNTR, 304, 14, 8, 16
	.member	_rsvd4, 320, 14, 8, 16
	.member	_WDKEY, 336, 14, 8, 16
	.member	_rsvd5, 352, 62, 8, 48, , 3
	.member	_WDCR, 400, 14, 8, 16
	.member	_rsvd6, 416, 62, 8, 96, , 6
	.eos
	.stag	_FOPT_BITS, 16
	.member	_ENPIPE, 0, 14, 18, 1
	.member	_rsvd, 1, 14, 18, 15
	.eos
	.utag	_FOPT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FOPT_BITS
	.eos
	.stag	_FPWR_BITS, 16
	.member	_PWR, 0, 14, 18, 2
	.member	_rsvd, 2, 14, 18, 14
	.eos
	.utag	_FPWR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FPWR_BITS
	.eos
	.stag	_FSTATUS_BITS, 16
	.member	_PWRS, 0, 14, 18, 2
	.member	_STDBYWAITS, 2, 14, 18, 1
	.member	_ACTIVEWAITS, 3, 14, 18, 1
	.member	_rsvd1, 4, 14, 18, 4
	.member	_V3STAT, 8, 14, 18, 1
	.member	_rsvd2, 9, 14, 18, 7
	.eos
	.utag	_FSTATUS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FSTATUS_BITS
	.eos
	.stag	_FSTDBYWAIT_BITS, 16
	.member	_STDBYWAIT, 0, 14, 18, 9
	.member	_rsvd, 9, 14, 18, 7
	.eos
	.utag	_FSTDBYWAIT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FSTDBYWAIT_BITS
	.eos
	.stag	_FACTIVEWAIT_BITS, 16
	.member	_ACTIVEWAIT, 0, 14, 18, 9
	.member	_rsvd, 9, 14, 18, 7
	.eos
	.utag	_FACTIVEWAIT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FACTIVEWAIT_BITS
	.eos
	.stag	_FBANKWAIT_BITS, 16
	.member	_RANDWAIT, 0, 14, 18, 4
	.member	_rsvd1, 4, 14, 18, 4
	.member	_PAGEWAIT, 8, 14, 18, 4
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_FBANKWAIT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FBANKWAIT_BITS
	.eos
	.stag	_FOTPWAIT_BITS, 16
	.member	_OTPWAIT, 0, 14, 18, 5
	.member	_rsvd, 5, 14, 18, 11
	.eos
	.utag	_FOTPWAIT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FOTPWAIT_BITS
	.eos
	.stag	_FLASH_REGS, 128
	.member	_FOPT, 0, 9, 8, 16, _FOPT_REG
	.member	_rsvd1, 16, 14, 8, 16
	.member	_FPWR, 32, 9, 8, 16, _FPWR_REG
	.member	_FSTATUS, 48, 9, 8, 16, _FSTATUS_REG
	.member	_FSTDBYWAIT, 64, 9, 8, 16, _FSTDBYWAIT_REG
	.member	_FACTIVEWAIT, 80, 9, 8, 16, _FACTIVEWAIT_REG
	.member	_FBANKWAIT, 96, 9, 8, 16, _FBANKWAIT_REG
	.member	_FOTPWAIT, 112, 9, 8, 16, _FOTPWAIT_REG
	.eos
	.stag	_XINTCR_BITS, 16
	.member	_ENABLE, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_POLARITY, 2, 14, 18, 2
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_XINTCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XINTCR_BITS
	.eos
	.stag	_XNMICR_BITS, 16
	.member	_ENABLE, 0, 14, 18, 1
	.member	_SELECT, 1, 14, 18, 1
	.member	_POLARITY, 2, 14, 18, 2
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_XNMICR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XNMICR_BITS
	.eos
	.stag	_XINTRUPT_REGS, 256
	.member	_XINT1CR, 0, 9, 8, 16, _XINTCR_REG
	.member	_XINT2CR, 16, 9, 8, 16, _XINTCR_REG
	.member	_XINT3CR, 32, 9, 8, 16, _XINTCR_REG
	.member	_XINT4CR, 48, 9, 8, 16, _XINTCR_REG
	.member	_XINT5CR, 64, 9, 8, 16, _XINTCR_REG
	.member	_XINT6CR, 80, 9, 8, 16, _XINTCR_REG
	.member	_XINT7CR, 96, 9, 8, 16, _XINTCR_REG
	.member	_XNMICR, 112, 9, 8, 16, _XNMICR_REG
	.member	_XINT1CTR, 128, 14, 8, 16
	.member	_XINT2CTR, 144, 14, 8, 16
	.member	_rsvd, 160, 62, 8, 80, , 5
	.member	_XNMICTR, 240, 14, 8, 16
	.eos
	.stag	_XTIMING_BITS, 32
	.member	_XWRTRAIL, 0, 14, 18, 2
	.member	_XWRACTIVE, 2, 14, 18, 3
	.member	_XWRLEAD, 5, 14, 18, 2
	.member	_XRDTRAIL, 7, 14, 18, 2
	.member	_XRDACTIVE, 9, 14, 18, 3
	.member	_XRDLEAD, 12, 14, 18, 2
	.member	_USEREADY, 14, 14, 18, 1
	.member	_READYMODE, 15, 14, 18, 1
	.member	_XSIZE, 16, 14, 18, 2
	.member	_rsvd1, 18, 14, 18, 4
	.member	_X2TIMING, 22, 14, 18, 1
	.member	_rsvd3, 23, 14, 18, 9
	.eos
	.utag	_XTIMING_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _XTIMING_BITS
	.eos
	.stag	_XINTCNF2_BITS, 32
	.member	_WRBUFF, 0, 14, 18, 2
	.member	_CLKMODE, 2, 14, 18, 1
	.member	_CLKOFF, 3, 14, 18, 1
	.member	_rsvd1, 4, 14, 18, 2
	.member	_WLEVEL, 6, 14, 18, 2
	.member	_rsvd2, 8, 14, 18, 1
	.member	_HOLD, 9, 14, 18, 1
	.member	_HOLDS, 10, 14, 18, 1
	.member	_HOLDAS, 11, 14, 18, 1
	.member	_rsvd3, 12, 14, 18, 4
	.member	_XTIMCLK, 16, 14, 18, 3
	.member	_rsvd4, 19, 14, 18, 13
	.eos
	.utag	_XINTCNF2_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _XINTCNF2_BITS
	.eos
	.stag	_XBANK_BITS, 16
	.member	_BANK, 0, 14, 18, 3
	.member	_BCYC, 3, 14, 18, 3
	.member	_rsvd, 6, 14, 18, 10
	.eos
	.utag	_XBANK_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XBANK_BITS
	.eos
	.stag	_XRESET_BITS, 16
	.member	_XHARDRESET, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 15
	.eos
	.utag	_XRESET_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XRESET_BITS
	.eos
	.stag	_XINTF_REGS, 480
	.member	_XTIMING0, 0, 9, 8, 32, _XTIMING_REG
	.member	_rsvd1, 32, 63, 8, 160, , 5
	.member	_XTIMING6, 192, 9, 8, 32, _XTIMING_REG
	.member	_XTIMING7, 224, 9, 8, 32, _XTIMING_REG
	.member	_rsvd2, 256, 63, 8, 64, , 2
	.member	_XINTCNF2, 320, 9, 8, 32, _XINTCNF2_REG
	.member	_rsvd3, 352, 15, 8, 32
	.member	_XBANK, 384, 9, 8, 16, _XBANK_REG
	.member	_rsvd4, 400, 14, 8, 16
	.member	_XREVISION, 416, 14, 8, 16
	.member	_rsvd5, 432, 62, 8, 32, , 2
	.member	_XRESET, 464, 9, 8, 16, _XRESET_REG
	.eos
	.stag	_CPUTIMER_VARS, 128
	.member	_RegsAddr, 0, 24, 8, 22, _CPUTIMER_REGS
	.member	_InterruptCount, 32, 15, 8, 32
	.member	_CPUFreqInMHz, 64, 6, 8, 32
	.member	_PeriodInUSec, 96, 6, 8, 32
	.eos
